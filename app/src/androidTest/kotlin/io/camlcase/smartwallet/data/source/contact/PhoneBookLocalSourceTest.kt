package io.camlcase.smartwallet.data.source.contact

import android.Manifest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.GrantPermissionRule
import io.camlcase.kotlintezos.model.TezosNetwork
import io.camlcase.smartwallet.data.core.AppContext
import io.camlcase.smartwallet.data.model.contact.ContactTezosAddress
import io.camlcase.smartwallet.data.model.contact.MagmaContact
import io.camlcase.smartwallet.data.source.contact.PhoneBookLocalSourceTest.Companion.DEVICE_TEST_CONTACT
import io.camlcase.smartwallet.data.usecase.analytics.SendAnalyticUseCase
import io.camlcase.smartwallet.test.mainTestnetAddress
import io.camlcase.smartwallet.test.secondaryTestnetAddress
import io.mockk.mockk
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import java.lang.ref.WeakReference

/**
 * Test operations on [PhoneBookLocalSource]
 *
 * Preconditions:
 * - Device has no Contact entry with [TezosNetwork.CARTHAGENET] [TEZOS_CONTACT]
 * - Device has one Contact entry like [DEVICE_TEST_CONTACT]. The entry should have a Mainnet tz1 address set
 */
class PhoneBookLocalSourceTest {
    @get:Rule
    var permissionRule: GrantPermissionRule? =
        GrantPermissionRule.grant(Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CONTACTS)

    private val context = InstrumentationRegistry.getInstrumentation().targetContext
    private var appContext: AppContext = WeakReference(context)
    private val analyticUseCase = mockk<SendAnalyticUseCase>(relaxed = true)

    private val testnetSource = init()

    private fun init(network: TezosNetwork = tezosNetwork): PhoneBookLocalSource {
        return PhoneBookLocalSource(appContext, network, analyticUseCase)
    }

    /**
     * Test different operations with contact created within the app
     */
    @Test
    fun test_MagmaContact() {
        val insertedContact = test_Insert_MagmaContact()

        val updatedContact = test_Update_MagmaContact(insertedContact)

        test_Delete_MagmaContact(updatedContact)
    }

    private fun test_Insert_MagmaContact(): MagmaContact {
        // INSERT
        val contact = MagmaContact(
            0,
            "Test Name",
            listOf(TEST_CONTACT_TEZOS_ADDRESS)
        )
        val insertedContact = testnetSource.add("some@gmail.com", contact)
        val id = insertedContact.id
        Assert.assertTrue(id > 0)

        // Check it exists
        val contacts = testnetSource.get()
        Assert.assertTrue(contacts.size == 1)
        Assert.assertTrue(contacts[0] == insertedContact)
        Assert.assertTrue(contacts[0].id == id)

        return insertedContact
    }

    private fun test_Update_MagmaContact(insertedContact: MagmaContact): MagmaContact {
        var result = testnetSource.updateTezosAddress(
            insertedContact,
            ContactTezosAddress(
                ADDRESS2,
                tezosNetwork
            )
        )
        Assert.assertTrue(result)

        val name = "Updated name"
        result = testnetSource.updateName(insertedContact, name)
        Assert.assertTrue(result)

        // Check it's now listed
        val contacts = testnetSource.get()
        Assert.assertTrue(contacts.size == 1)
        Assert.assertTrue(contacts[0].id == insertedContact.id)
        Assert.assertTrue(contacts[0].name == name)
        Assert.assertTrue(contacts[0].tezosAddresses[0].address == ADDRESS2)

        return contacts[0]
    }

    private fun test_Delete_MagmaContact(insertedContact: MagmaContact) {
        // DELETE
        testnetSource.delete(insertedContact)

        // Check it was deleted
        val contacts = testnetSource.get()
        Assert.assertTrue(contacts.isEmpty())
    }

    /**
     * Test different operations with a contact with several types
     */
    @Test
    fun test_ExistentContact() {
        test_Add_To_ExistentContact()

        test_Update_ExistentContact()

        test_Delete_ExistentContact()

        // Check the mainnet address was not deleted
        test_Mainnet_ExistentContact()
    }

    private fun test_Add_To_ExistentContact() {
        val id: Long = DEVICE_TEST_CONTACT.id
        val result = testnetSource.insertTezosAddress(id, TEST_CONTACT_TEZOS_ADDRESS)

        Assert.assertTrue(result)

        // Check it's now listed
        val contacts = testnetSource.get()
        Assert.assertTrue(contacts.size == 1)
        Assert.assertTrue(contacts[0].id == id)
        Assert.assertTrue(contacts[0].tezosAddresses[0].address == ADDRESS1)
    }

    private fun test_Update_ExistentContact() {
        val result = testnetSource.updateTezosAddress(
            DEVICE_TEST_CONTACT,
            ContactTezosAddress(
                ADDRESS2,
                tezosNetwork
            )
        )
        Assert.assertTrue(result)

        // Check it's now listed
        val contacts = testnetSource.get()
        Assert.assertTrue(contacts.size == 1)
        Assert.assertTrue(contacts[0].id == DEVICE_TEST_CONTACT.id)
        Assert.assertTrue(contacts[0].tezosAddresses[0].address == ADDRESS2)
    }

    private fun test_Delete_ExistentContact() {
        val result = testnetSource.deleteData(DEVICE_TEST_CONTACT_UPDATED)
        Assert.assertTrue(result)

        // Check it's NOT listed
        val contacts = testnetSource.get()
        Assert.assertTrue(contacts.isEmpty())
    }

    private fun test_Mainnet_ExistentContact() {
        val source = init(TezosNetwork.MAINNET)
        val contacts = source.get()
        Assert.assertTrue(contacts.size == 1)
        Assert.assertTrue(contacts[0].id == DEVICE_TEST_CONTACT.id)
        Assert.assertTrue(contacts[0].tezosAddresses[0].address == ADDRESS1)
    }

    companion object {
        internal val tezosNetwork = TezosNetwork.EDONET

        private const val ADDRESS1 = mainTestnetAddress
        private const val ADDRESS2 = secondaryTestnetAddress
        private val TEST_CONTACT_TEZOS_ADDRESS = ContactTezosAddress(
            ADDRESS1,
            tezosNetwork
        )
        private val DEVICE_TEST_CONTACT = MagmaContact(
            97,
            "Margaret Atwood",
            listOf(TEST_CONTACT_TEZOS_ADDRESS)
        )

        private val DEVICE_TEST_CONTACT_UPDATED = DEVICE_TEST_CONTACT.copy(
            listOf(
                ContactTezosAddress(
                    ADDRESS2,
                    tezosNetwork
                )
            )
        )
    }
}
