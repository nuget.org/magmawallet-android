package io.camlcase.smartwallet.data.usecase

import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.TezosErrorType
import io.camlcase.smartwallet.data.core.Wallet
import io.camlcase.smartwallet.data.model.AppWallet
import io.camlcase.smartwallet.data.model.error.AppError
import io.camlcase.smartwallet.data.model.error.ErrorType
import io.camlcase.smartwallet.data.model.valid
import io.camlcase.smartwallet.data.source.local.UserPreferences
import io.camlcase.smartwallet.data.source.user.WalletLocalSource
import io.camlcase.smartwallet.data.usecase.user.CreateWalletUseCase
import io.mockk.*
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class CreateWalletUseCaseTest {
    private val localSource = mockk<WalletLocalSource>(relaxed = true) {
        every { storeWallet(any()) } just Runs
    }
    private val logOutUseCase = mockk<LogOutUseCase>(relaxed = true) {
        every { logOut() } just Runs
    }
    private val userPreferences = mockk<UserPreferences>(relaxed = true)
    private val versionCode: Int = 100
    private lateinit var useCase: CreateWalletUseCase

    @Before
    fun before() {
        mockkConstructor(AppWallet::class)
    }

    fun init(): CreateWalletUseCase {
        useCase = spyk(
            CreateWalletUseCase(
                localSource,
                logOutUseCase,
                userPreferences,
                versionCode
            )
        )
        return useCase
    }

    @After
    fun tearDown() {
        confirmVerified(localSource)
        confirmVerified(logOutUseCase)
        confirmVerified(userPreferences)
        confirmVerified(useCase)
    }

    @Test
    fun createNewWallet_NoPassphrase_OK() {
        val result = init().createNewWallet(null)

        Assert.assertNotNull(result)
        Assert.assertTrue(result!!.mnemonic.size == 12)
        Assert.assertTrue(result!!.derivationPath == Wallet.DEFAULT_BIP44_DERIVATION_PATH)
        Assert.assertTrue(result.valid())

        verify {
            useCase.createNewWallet(null)
            logOutUseCase.logOut()
            localSource.storeWallet(result)
            userPreferences.updateVersionCreated(versionCode)
        }
    }

    @Test
    fun createNewWallet_Passphrase_OK() {
        val passphrase = "Blablabla12345!&%"
        val result = init().createNewWallet(passphrase)

        Assert.assertNotNull(result)
        Assert.assertTrue(result!!.mnemonic.size == 12)
        Assert.assertTrue(result!!.derivationPath == Wallet.DEFAULT_BIP44_DERIVATION_PATH)
        Assert.assertTrue(result.valid())

        verify {
            useCase.createNewWallet(passphrase)
            logOutUseCase.logOut()
            localSource.storeWallet(result)
            userPreferences.updateVersionCreated(versionCode)
        }
    }

    @Test
    fun recoverHDWallet_NoPassphrase_DefaultDerivationPath_OK() {
        val mnemonic = "cruel opera bid excite pact timber cream skate machine divide sniff anchor".split(" ")
        val result = init().recoverHDWallet(mnemonic, null, Wallet.DEFAULT_BIP44_DERIVATION_PATH)

        Assert.assertNotNull(result)
        Assert.assertTrue(result!!.mnemonic.size == 12)
        Assert.assertTrue(result!!.derivationPath == Wallet.DEFAULT_BIP44_DERIVATION_PATH)
        Assert.assertTrue(result.valid())

        verify {
            useCase.recoverHDWallet(mnemonic, null, Wallet.DEFAULT_BIP44_DERIVATION_PATH)
            logOutUseCase.logOut()
            localSource.storeWallet(result)
            userPreferences.updateVersionCreated(versionCode)
        }
    }

    @Test
    fun recoverHDWallet_NoPassphrase_OtherIndexDerivationPath_OK() {
        val mnemonic = "cruel opera bid excite pact timber cream skate machine divide sniff anchor".split(" ")
        val derivationPath = "m/44\'/1729\'/0\'/1\'"
        val result = init().recoverHDWallet(mnemonic, null, derivationPath)

        Assert.assertNotNull(result)
        Assert.assertTrue(result!!.mnemonic.size == 12)
        Assert.assertTrue(result!!.derivationPath == derivationPath)
        Assert.assertTrue(result.valid())

        verify {
            useCase.recoverHDWallet(mnemonic, null, derivationPath)
            logOutUseCase.logOut()
            localSource.storeWallet(result)
            userPreferences.updateVersionCreated(versionCode)
        }
    }

    @Test
    fun recoverHDWallet_NotBIPWords_NoPassphrase_DefaultDerivationPath_KO() {
        val mnemonic = "cruel opera bid excite pact timber cream skate machine divide sniff magma".split(" ")
        try {
            val result = init().recoverHDWallet(mnemonic, null, Wallet.DEFAULT_BIP44_DERIVATION_PATH)
        } catch (e: AppError) {
            Assert.assertTrue(e.type == ErrorType.WALLET_CREATE_ERROR)
            Assert.assertTrue(e.exception is TezosError)
            Assert.assertTrue((e.exception as TezosError).type == TezosErrorType.MNEMONIC_GENERATION)

            verify {
                useCase.recoverHDWallet(mnemonic, null, Wallet.DEFAULT_BIP44_DERIVATION_PATH)
                logOutUseCase.logOut()
            }
        }
    }

    @Test
    fun recoverHDWallet_NotMultiple_NoPassphrase_DefaultDerivationPath_KO() {
        val mnemonic = "cruel opera bid excite pact timber cream skate machine divide sniff".split(" ")
        try {
            val result = init().recoverHDWallet(mnemonic, null, Wallet.DEFAULT_BIP44_DERIVATION_PATH)
        } catch (e: AppError) {
            Assert.assertTrue(e.type == ErrorType.WALLET_CREATE_ERROR)
            Assert.assertTrue(e.exception is TezosError)
            Assert.assertTrue((e.exception as TezosError).type == TezosErrorType.MNEMONIC_GENERATION)

            verify {
                useCase.recoverHDWallet(mnemonic, null, Wallet.DEFAULT_BIP44_DERIVATION_PATH)
                logOutUseCase.logOut()
            }
        }
    }

    @Test
    fun recoverHDWallet_NoPassphrase_NoTezosDerivationPath_KO() {
        val mnemonic = "cruel opera bid excite pact timber cream skate machine divide sniff anchor".split(" ")
        val derivationPath = "m/44\'/35\'/0\'/0\'"
        try {
            val result = init().recoverHDWallet(mnemonic, null, derivationPath)
        } catch (e: AppError) {
            println(e)
            Assert.assertTrue(e.type == ErrorType.WALLET_CREATE_ERROR)
            val tezosError = e.exception as TezosError
            Assert.assertTrue(tezosError.type == TezosErrorType.WALLET_CREATION)
            Assert.assertTrue(tezosError.exception is java.lang.IllegalArgumentException)

            verify {
                useCase.recoverHDWallet(mnemonic, null, derivationPath)
                logOutUseCase.logOut()
            }
        }
    }

    @Test
    fun recoverHDWallet_NoPassphrase_GibberishDerivationPath_KO() {
        val mnemonic = "cruel opera bid excite pact timber cream skate machine divide sniff anchor".split(" ")
        val derivationPath = "m//35%$/0\'"
        try {
            val result = init().recoverHDWallet(mnemonic, null, derivationPath)
        } catch (e: AppError) {
            println(e)
            Assert.assertTrue(e.type == ErrorType.WALLET_CREATE_ERROR)
            val tezosError = e.exception as TezosError
            Assert.assertTrue(tezosError.type == TezosErrorType.WALLET_CREATION)
            Assert.assertTrue(tezosError.exception is java.lang.IllegalArgumentException)

            verify {
                useCase.recoverHDWallet(mnemonic, null, derivationPath)
                logOutUseCase.logOut()
            }
        }
    }
}
