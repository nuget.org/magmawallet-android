/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.ellipsize
import io.camlcase.smartwallet.test.launchMainActivity
import io.camlcase.smartwallet.test.resources
import org.hamcrest.CoreMatchers

//@LargeTest
//@RunWith(AndroidJUnit4::class)
class CheckActivityTabTest : ActivityTabTest {

//    @Test
    fun testActivityTab() {
        val activityScenario = launchMainActivity("test-user-tokens-mnemonic")

        onView(
            CoreMatchers.allOf(
                withId(R.id.nav_activity), ViewMatchers.withContentDescription(R.string.wlt_navigation_activity)
            )
        ).check(matches(isDisplayed()))
            .perform(ViewActions.click())

        Thread.sleep(5 * 1000)

        val formattedSendAmount = "0.001 MTZ"
        val feesText = "0.04048 XTZ"
        val destination = "tz1XSXBeWqdupm73qWAJkxJkxRzH16y77i1B"

        // ACTIVITY TAB
        testActivityRow(
            resources.getString(R.string.act_type_send),
            "- $formattedSendAmount",
            feesText
        )
        testSendDestination(destination.ellipsize())

        activityScenario.close()
    }
}
