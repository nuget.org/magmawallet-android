/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.contact

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry.getInstrumentation
import androidx.test.uiautomator.UiDevice
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.test.launchMainActivity
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.not
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Handles device permission dialogs
 *
 * Starts with A to hack it at the top. Even with no @PermissionRule, if other tests have granted it it grants it too...
 *
 * @see ContactPermissionFragment
 */
@LargeTest
@RunWith(AndroidJUnit4::class)
class AContactPermissionTest : ContactsPermissionDialogTest {
    private lateinit var device: UiDevice

    @Before
    fun setUp() {
        device = UiDevice.getInstance(getInstrumentation())
    }

    @Test
    fun test_Contact_Permission_Screen() {
        val activityScenario = launchMainActivity()

        // Navigate to CONTACTS
        onView(
            allOf(
                withId(R.id.nav_contacts),
                withContentDescription(R.string.wlt_navigation_contacts)
            )
        ).check(matches(isDisplayed()))
            .perform(click())

        // Empty state
        onView(
            allOf(
                withId(R.id.empty_status),
                isDisplayed()
            )
        )
            .check(matches(isDisplayed()))

        // PERMISSION SCREEN
        onView(withId(R.id.action_add_contact))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .perform(click())

        onView(withId(R.id.revoke_info))
            .check(matches(isDisplayed()))

        onView(withText(R.string.con_permission_info_detail_text))
            .check(matches(isDisplayed()))

        onView(withId(R.id.action_next))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .perform(click())

        testDialog(device)

        activityScenario.close()

        onView(withText(R.string.con_add_title))
            .check(matches(isDisplayed()))

        onView(withId(R.id.action_next))
            .check(matches(isDisplayed()))
            .check(matches(not(isEnabled())))
    }
}
