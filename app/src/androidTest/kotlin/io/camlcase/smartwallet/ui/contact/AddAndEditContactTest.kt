/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.contact

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.UiDevice
import io.camlcase.kotlintezos.model.Address
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.test.*
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.not
import org.hamcrest.Matchers
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Preconditions:
 * - User only has XTZ
 * - Device has a Google account set
 * - Google account has no testnet// contacts
 */
@LargeTest
@RunWith(AndroidJUnit4::class)
class AddAndEditContactTest : AddContactTest, ContactDetailTest, DeleteContactTest {

    lateinit var userAddress: Address

    private lateinit var device: UiDevice

    @Before
    fun setUp() {
        grantContactsPermission()
        device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
    }

    @Test
    fun test_Add_and_Edit_Contact() {
        val activityScenario = launchMainActivity {
            userAddress = it.address
        }

        // Navigate to CONTACTS
        navigateToContacts()

        // Empty state
        checkEmptyContactLists()

        onView(withId(R.id.action_add_contact))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            // Navigate
            .perform(click())

        // ADD CONTACT screen

        checkAddContactScreen()

        val insertedName = "Jane Doe"
        val insertedAddress = mainTestnetAddress
        addContact(insertedName, insertedAddress, userAddress)

        /**
         * EDIT
         */

        onView(allOf(withId(R.id.contact_list), isDisplayed()))
            .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))
        checkContactDetailScreen(device, insertedName, insertedAddress)

        onView(allOf(withId(R.id.list), isDisplayed()))
            .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(2, click()))

        val newName = "John Doe"
        editContactName(newName)

        /**
         * SEND
         */
        device.pressBack()
        // Navigate To wallet
        navigateToWallet()

        onView(withId(R.id.action_send))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .perform(click())

        onView(withId(R.id.toggle_tab_layout))
            .check(matches(isDisplayed()))
            .perform(selectTabAtPosition(1))

        onView(allOf(withId(R.id.list), isDisplayed()))
            .check(
                matches(
                    RecyclerViewChildActions.childOfViewAtPositionWithMatcher(
                        R.id.contact_name,
                        0,
                        withText(newName)
                    )
                )
            )
        onView(allOf(withId(R.id.list), isDisplayed()))
            .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))

        testContactSendFlow(device, newName, insertedAddress)
        device.pressBack()
        // Go back to contacts
        navigateToContacts()


        onView(allOf(withId(R.id.contact_list), isDisplayed()))
            .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))

        /**
         * DELETE
         */

        deleteContact()

        checkEmptyContactLists()

        activityScenario.close()
    }

    fun editContactName(newName: String) {
        // Picker not displayed
        onView(
            allOf(
                withId(R.id.input_icon),
                withContentDescription(R.string.con_add_device_button)
            )
        )
            .check(matches(not(isDisplayed())))
        // QR is displayed
        onView(
            allOf(
                withId(R.id.input_icon),
                withContentDescription(R.string.snd_scan_qr)
            )
        )
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))

        testNameField(
            newName,
            onView(allOf(withId(R.id.input), isDescendantOfA(withId(R.id.name)))),
            onView(
                Matchers.allOf(
                    withId(R.id.text_input_end_icon),
                    isDescendantOfA(withId(R.id.name)),
                    withContentDescription(R.string.icon_clear_action)
                )
            )
        )

        // Save contact
        buttonInteraction()
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .perform(click())
    }

    companion object {

        fun navigateToContacts() {
            onView(
                allOf(
                    withId(R.id.nav_contacts),
                    withContentDescription(R.string.wlt_navigation_contacts)
                )
            ).check(matches(isDisplayed()))
                .perform(click())
        }

        fun navigateToWallet() {
            onView(
                allOf(
                    withId(R.id.nav_balance),
                    withContentDescription(R.string.wlt_navigation_balance)
                )
            ).check(matches(isDisplayed()))
                .perform(click())
        }

        fun checkEmptyContactLists() {
            onView(
                allOf(
                    withId(R.id.toolbar_title),
                    withText(R.string.wlt_navigation_contacts)
                )
            )
                .check(matches(isDisplayed()))
            // Empty state
            onView(
                allOf(
                    withId(R.id.empty_status),
                    isDisplayed()
                )
            )
                .check(matches(isDisplayed()))
        }
    }
}
