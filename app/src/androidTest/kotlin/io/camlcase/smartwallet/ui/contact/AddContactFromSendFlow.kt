/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.contact

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.UiDevice
import io.camlcase.kotlintezos.model.Address
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.WaitForOperationTest
import io.camlcase.smartwallet.core.extension.ellipsize
import io.camlcase.smartwallet.test.*
import io.camlcase.smartwallet.ui.ActivityTabTest
import io.camlcase.smartwallet.ui.TezosAddressInputTest
import io.camlcase.smartwallet.ui.XTZAmountInputTest
import io.camlcase.smartwallet.ui.contact.AddAndEditContactTest.Companion.checkEmptyContactLists
import io.camlcase.smartwallet.ui.send.SendXTZFromMainTest
import org.hamcrest.CoreMatchers
import org.hamcrest.CoreMatchers.not
import org.hamcrest.Matchers.allOf
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Preconditions:
 * - Device has a Google account set
 * - Google account has no testnet// contacts
 * - Device has "Margaret Atwood" as contact
 * - Account only has XTZ
 * */
@LargeTest
@RunWith(AndroidJUnit4::class)
class AddContactFromSendFlow : TezosAddressInputTest, DeleteContactTest, XTZAmountInputTest, WaitForOperationTest,
    ActivityTabTest {

    lateinit var userAddress: Address
    private lateinit var device: UiDevice

    @Before
    fun setUp() {
        grantContactsPermission()
        device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
    }

    @Test
    fun test_AddContact_From_Send() {
        val activityScenario = launchMainActivity {
            userAddress = it.address
        }

        SendXTZFromMainTest.navigateToMainSend()

        /**
         * SEND TO
         */

        checkSendToScreen()

        val name = "Margaret Atwood"
        val address = mainTestnetAddress
        testInputTezosAddress(
            userAddress,
            destination = address,
            editTextInteraction = onView(
                CoreMatchers.allOf(
                    withId(R.id.input),
                    isDescendantOfA(withId(R.id.input_address))
                )
            ),
            clearTextInteraction = onView(
                allOf(
                    withId(R.id.text_input_end_icon),
                    isDescendantOfA(withId(R.id.input_address)),
                    withContentDescription(R.string.icon_clear_action)
                )
            )
        )

        onView(withId(R.id.action_add))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .perform(click())

        /**
         * ADD
         */

        onView(
            CoreMatchers.allOf(
                withId(R.id.input),
                isDescendantOfA(withId(R.id.address))
            )
        )
            .check(matches(isDisplayed()))
            .check(matches(withText(address)))

        checkAddContactFromSend()

        AddTwoContactsAndDeleteTest.pickAContact(device, name)

        onView(
            allOf(
                withId(R.id.input),
                isDescendantOfA(withId(R.id.address))
            )
        )
            .check(matches(isDisplayed()))
            .check(matches(withText(address)))

        buttonInteraction()
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .perform(click())

        /**
         * SEND AMOUNT
         */

        Thread.sleep(1 * 1000) // Wait for the screen to go back and autonavigate

        val sendAmount = SendXTZFromMainTest.getRandomTezAmount()
        SendXTZFromMainTest.addSendAmount(this, sendAmount)

        buttonInteraction()
            .perform(click())

        Thread.sleep(10 * 1000) // Wait for network call
        // CONFIRM
        val (formattedSendAmount, feesText, total) = SendXTZFromMainTest.confirmSend(sendAmount, address)

        buttonInteraction()
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .perform(click())

        Thread.sleep(2 * 1000) // Wait for network call
        // WAIT FOR INJECTION
        waitForSendOperation(formattedSendAmount)

        // MAIN SCREEN - WALLET
        onView(withId(R.id.bottom_navigation))
            .check(matches(isDisplayed()))

        checkActivityRow(this, name, formattedSendAmount, feesText)

        /**
         * DELETE CONTACT
         */
        // Go back to contacts
        AddAndEditContactTest.navigateToContacts()

        onView(allOf(withId(R.id.contact_list), withEffectiveVisibility(Visibility.VISIBLE)))
            .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))
        deleteContact()

        checkEmptyContactLists()

        // Activity should be updated so now it doesn't show the name
        checkActivityRow(this, address.ellipsize(), formattedSendAmount, feesText)

        activityScenario.close()
    }

    companion object {
        fun checkSendToScreen() {
            onView(withId(R.id.input_address))
                .check(matches(isDisplayed()))

            onView(withId(R.id.action_add))
                .check(matches(not(isDisplayed())))

            onView(withId(R.id.action_add_contact))
                .check(matches(not(isDisplayed())))
            onView(withId(R.id.expanded_add_contact))
                .check(matches(not(isDisplayed())))

            onView(withId(R.id.toggle_tab_layout))
                .check(matches(isDisplayed()))
                .perform(selectTabAtPosition(1))

            onView(withId(R.id.empty_status))
                .check(matches(isDisplayed()))

            onView(withId(R.id.action_add_contact))
                .check(matches(not(isDisplayed())))

            onView(withId(R.id.expanded_add_contact))
                .check(matches(isDisplayed()))
                .check(matches(isEnabled()))

            onView(withId(R.id.toggle_tab_layout))
                .check(matches(isDisplayed()))
                .perform(selectTabAtPosition(0))

            onView(withId(R.id.action_add_contact))
                .check(matches(not(isDisplayed())))
            onView(withId(R.id.expanded_add_contact))
                .check(matches(not(isDisplayed())))
        }

        fun checkAddContactFromSend() {
            onView(withId(R.id.name))
                .check(matches(isDisplayed()))
            onView(withId(R.id.address))
                .check(matches(isDisplayed()))

            onView(
                CoreMatchers.allOf(
                    withId(R.id.input_icon),
                    withContentDescription(R.string.con_add_device_button)
                )
            )
                .check(matches(isDisplayed()))
                .check(matches(isEnabled()))

            buttonAtStartCheck()

            // Test QR
            onView(
                CoreMatchers.allOf(
                    withId(R.id.input_icon),
                    withContentDescription(R.string.snd_scan_qr)
                )
            )
                .check(matches(not(isDisplayed())))
        }

        fun checkActivityRow(
            activityTest: ActivityTabTest,
            destination: String,
            formattedSendAmount: String,
            feesText: String
        ) {
            onView(
                CoreMatchers.allOf(
                    withId(R.id.nav_activity), withContentDescription(R.string.wlt_navigation_activity)
                )
            ).check(matches(isDisplayed()))
                .perform(click())

            Thread.sleep(5 * 1000)

            // ACTIVITY TAB
            activityTest.testActivityRow(
                resources.getString(R.string.act_type_send),
                "- $formattedSendAmount",
                feesText
            )
            activityTest.testSendDestination(destination)
        }
    }
}
