/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.contact

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import io.camlcase.kotlintezos.model.Address
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.test.buttonAtStartCheck
import io.camlcase.smartwallet.test.buttonInteraction
import io.camlcase.smartwallet.ui.NameInputTest
import io.camlcase.smartwallet.ui.TezosAddressInputTest
import io.camlcase.smartwallet.ui.delegate.DelegateTest.Companion.testQR
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.Matchers

interface AddContactTest : TezosAddressInputTest, NameInputTest {

    fun addContact(name: String, address: Address, userAddress: Address) {
        // Test text fields
        onView(allOf(withId(R.id.input), isDescendantOfA(withId(R.id.name))))
            .perform(click())
            .perform(typeText("J"))

        buttonAtStartCheck()

        testInputTezosAddress(
            userAddress,
            destination = address,
            editTextInteraction = onView(allOf(withId(R.id.input), isDescendantOfA(withId(R.id.address)))),
            clearTextInteraction = onView(
                Matchers.allOf(
                    withId(R.id.text_input_end_icon),
                    isDescendantOfA(withId(R.id.address)),
                    withContentDescription(R.string.icon_clear_action)
                )
            )
        )

        onView(allOf(withId(R.id.input), isDescendantOfA(withId(R.id.name))))
            .perform(replaceText(""))

        testNameField(
            name,
            onView(allOf(withId(R.id.input), isDescendantOfA(withId(R.id.name)))),
            onView(
                Matchers.allOf(
                    withId(R.id.text_input_end_icon),
                    isDescendantOfA(withId(R.id.name)),
                    withContentDescription(R.string.icon_clear_action)
                )
            )
        )

        // Save contact
        buttonInteraction()
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .perform(click())
    }

    fun checkAddContactScreen() {
        onView(withId(R.id.name))
            .check(matches(isDisplayed()))
        onView(withId(R.id.address))
            .check(matches(isDisplayed()))

        onView(
            allOf(
                withId(R.id.input_icon),
                withContentDescription(R.string.con_add_device_button)
            )
        )
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))

        buttonAtStartCheck()

        // Test QR
        testQR()
    }

}
