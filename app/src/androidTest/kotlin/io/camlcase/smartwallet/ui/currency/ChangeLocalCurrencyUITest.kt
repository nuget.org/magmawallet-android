/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.currency

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.swipeUp
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.Currency
import io.camlcase.smartwallet.data.core.Wallet
import io.camlcase.smartwallet.data.model.exchange.limitDecimals
import io.camlcase.smartwallet.test.RecyclerViewChildActions
import io.camlcase.smartwallet.test.getTextViewValue
import io.camlcase.smartwallet.test.initUser
import io.camlcase.smartwallet.test.resources
import io.camlcase.smartwallet.ui.contact.AddAndEditContactTest.Companion.navigateToWallet
import io.camlcase.smartwallet.ui.currency.ChangeCurrencyNoConnectionUITest.Companion.LOCAL_CURRENCY_POSITION
import io.camlcase.smartwallet.ui.delegate.DelegateTest
import io.camlcase.smartwallet.ui.migration.MigrationFlowUITest
import org.hamcrest.CoreMatchers.allOf
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class ChangeLocalCurrencyUITest {

    @Test
    fun test_Change_Currency() {
        val activityScenario = MigrationFlowUITest.launchMainActivityWithUser(
            getWallet = {},
            initTestUser = {
                initUser(null, "test-user-tokens-mnemonic", currency = null)
            })
        Thread.sleep(2 * 1000)
        val defaultCurrencyCode = Wallet.DEFAULT_CURRENCY
        checkCurrency("$", defaultCurrencyCode)

        DelegateTest.navigateToSettings()
        DelegateTest.checkSettings()

        checkSettingsRow(defaultCurrencyCode)
            .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(LOCAL_CURRENCY_POSITION, click()))

        // LOCAL CURRENCY
        checkLocalCurrencyScreen()
            .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(2, click()))

        Thread.sleep(5 * 1000)
        // Autonavigate to settings
        DelegateTest.checkSettings()

        val newCurrencyCode = "EUR"
        val newCurrencySymbol = "€"
        checkSettingsRow(newCurrencyCode)

        navigateToWallet()

        checkCurrency(newCurrencySymbol, newCurrencyCode)

        activityScenario.close()
    }

    companion object {
        fun checkCurrency(currencySymbol: String, currencyCode: String) {
            val currencyRateView =
                onView(allOf(withId(R.id.currency_exchange), isDescendantOfA(withId(R.id.balance_xtz))))
                    .check(matches(isDisplayed()))
            val currencyText = getTextViewValue(currencyRateView)
            Assert.assertTrue(currencyText.startsWith(currencySymbol))
            val currencyRate = currencyText.substring(1).toDouble()

            val balanceView = onView(allOf(withId(R.id.token_balance), isDescendantOfA(withId(R.id.balance_xtz))))
                .check(matches(isDisplayed()))
            val balanceText = getTextViewValue(balanceView)
            val balance = balanceText.split(" XTZ")[0].toDouble()

            val balanceCurrencyView =
                onView(allOf(withId(R.id.token_balance_currency), isDescendantOfA(withId(R.id.balance_xtz))))
                    .check(matches(isDisplayed()))
            val balanceCurrencyText = getTextViewValue(balanceCurrencyView)
            val balanceCurrency = balanceCurrencyText.substring(1).toDouble()

            // Second decimal might be rounded, just check in general it makes sense
            Assert.assertTrue(balanceCurrency.limitDecimals(1) == (balance * currencyRate).limitDecimals(1))

            onView(withId(R.id.total_balance_currency_info))
                .check(matches(isDisplayed()))
                .check(matches(withText(resources.getString(R.string.wlt_total_currency_value, currencyCode))))
        }

        fun checkSettingsRow(currencyCode: String): ViewInteraction {
            onView(withId(R.id.settings_list)).perform(swipeUp())
            Thread.sleep(1000)
            return onView(allOf(withId(R.id.settings_list), isDisplayed()))
                .check(
                    matches(
                        RecyclerViewChildActions.childOfViewAtPositionWithMatcher(
                            R.id.setting_title,
                            LOCAL_CURRENCY_POSITION,
                            withText(resources.getString(R.string.set_currency))
                        )
                    )
                )
                .check(
                    matches(
                        RecyclerViewChildActions.childOfViewAtPositionWithMatcher(
                            R.id.setting_detail,
                            LOCAL_CURRENCY_POSITION,
                            withText(currencyCode)
                        )
                    )
                )
        }

        fun checkLocalCurrencyScreen(): ViewInteraction {
            onView(
                allOf(
                    withId(R.id.expanded_title),
                    isDescendantOfA(withId(R.id.app_bar))
                )
            ).check(matches(isDisplayed()))
                .check(matches(withText(R.string.set_currency_title)))

            onView(withId(R.id.list))
                .check(matches(isDisplayed()))

            return onView(allOf(withId(R.id.list), isDisplayed()))
                .check(
                    matches(
                        RecyclerViewChildActions.childOfViewAtPositionWithMatcher(
                            R.id.currency_symbol,
                            1,
                            withText(Currency.POPULAR_CURRENCIES[0].toUpperCase())
                        )
                    )
                )
                .check(
                    matches(
                        RecyclerViewChildActions.childOfViewAtPositionWithMatcher(
                            R.id.currency_symbol,
                            8,
                            withText(Currency.POPULAR_CURRENCIES[Currency.POPULAR_CURRENCIES.size - 1].toUpperCase())
                        )
                    )
                )
                .check(
                    matches(
                        RecyclerViewChildActions.childOfViewAtPositionWithMatcher(
                            R.id.currency_symbol,
                            10,
                            withText("AED")
                        )
                    )
                )
        }
    }
}
