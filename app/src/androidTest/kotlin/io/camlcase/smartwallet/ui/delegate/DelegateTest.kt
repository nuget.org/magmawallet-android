/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.delegate

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.WaitForOperationTest
import io.camlcase.smartwallet.core.extension.ellipsize
import io.camlcase.smartwallet.data.core.Delegate.testnetBakers
import io.camlcase.smartwallet.data.core.XTZ
import io.camlcase.smartwallet.data.core.extension.format
import io.camlcase.smartwallet.test.*
import io.camlcase.smartwallet.ui.ActivityTabTest
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.not
import org.hamcrest.Matchers
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Preconditions:
 * - User has XTZ
 * - Network available
 * - User has "Baking Team" as a delegate
 */
@LargeTest
@RunWith(AndroidJUnit4::class)
class DelegateTest : WaitForOperationTest,
    ActivityTabTest {

    private lateinit var userAddress: Address

    @Test
    fun test_ChangeDelegate() {
        val activityScenario = launchMainActivity("test-user-mnemonic") {
            userAddress = it.address
        }

        navigateToSettings()

        checkSettings()

//        val currentDelegate = "Ceibo XTZ"
        val currentDelegate = "Baking Team"
        val textDelegate = mainTestnetAddress
        val selectedDelegate = testnetBakers[0]
        onView(allOf(withId(R.id.settings_list), isDisplayed()))
            .check(
                matches(
                    RecyclerViewChildActions.childOfViewAtPositionWithMatcher(
                        R.id.setting_title,
                        1,
                        withText(resources.getString(R.string.set_baker))
                    )
                )
            )
            .check(
                matches(
                    RecyclerViewChildActions.childOfViewAtPositionWithMatcher(
                        R.id.setting_detail,
                        1,
                        withText(currentDelegate)
                    )
                )
            )
            .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(1, click()))

        // Delegate screen
        checkDelegateScreen(currentDelegate)

        onView(withId(R.id.action_select_baker))
            .perform(click())

        checkChooseDelegateScreen()

        testNoButtonInputTezosAddress(
            userAddress,
            textDelegate
        )
        Thread.sleep(1000)

        buttonInteraction()
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .perform(click())

        Thread.sleep(7 * 1000)
        // WAIT FOR INJECTION

        waitForDelegateOperation()

        // MAIN SCREEN - WALLET

        onView(withId(R.id.bottom_navigation))
            .check(matches(isDisplayed()))

        onView(allOf(withId(R.id.settings_list), isDisplayed()))
            .check(
                matches(
                    RecyclerViewChildActions.childOfViewAtPositionWithMatcher(
                        R.id.setting_title,
                        1,
                        withText(resources.getString(R.string.set_baker))
                    )
                )
            )
            .check(
                matches(
                    RecyclerViewChildActions.childOfViewAtPositionWithMatcher(
                        R.id.setting_detail,
                        1,
                        withText(textDelegate)
                    )
                )
            )

        onView(
            allOf(
                withId(R.id.nav_activity), withContentDescription(R.string.wlt_navigation_activity)
            )
        ).check(matches(isDisplayed()))
            .perform(click())

        Thread.sleep(8 * 1000)

        // ACTIVITY TAB
        testActivityRow(
            resources.getString(R.string.act_type_delegation),
            "N/A",
            "0.000045 XTZ"
        )
        testSendDestination(textDelegate.ellipsize())

        /**
         * GO BACK TO PREVIOUS DELEGATE
         */
        navigateToBalance()
        val balanceView: ViewInteraction =
            onView(allOf(withId(R.id.token_balance), isDescendantOfA(withId(R.id.balance_xtz))))
        val balanceText = getTextViewValue(balanceView)
        navigateToSettings()

        onView(allOf(withId(R.id.settings_list), isDisplayed()))
            .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(1, click()))

        Thread.sleep(1000)

        checkDelegateScreen(textDelegate)

        onView(withId(R.id.action_select_baker))
            .perform(click())

        // textDelegate will occupy position 0
        val delegatePos = 1
        onView(allOf(withId(R.id.list), isDisplayed()))
            .check(
                matches(
                    RecyclerViewChildActions.childOfViewAtPositionWithMatcher(
                        R.id.baker_name,
                        delegatePos,
                        withText(currentDelegate)
                    )
                )
            )
            .check(
                matches(
                    RecyclerViewChildActions.childOfViewAtPositionWithMatcher(
                        R.id.baker_address,
                        0,
                        withText(textDelegate.ellipsize())
                    )
                )
            )
            .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(delegatePos, click()))

        Thread.sleep(1000)

        onView(withId(R.id.action_select_baker))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .check(matches(withText(currentDelegate)))

        buttonInteraction()
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .perform(click())

        // Delegate warning
        testMinimalWarning(Tez(1000.0), balanceText)
        buttonInteraction(R.id.action_secondary)
            .perform(click())

        Thread.sleep(7 * 1000)
        // WAIT FOR INJECTION

        onView(withId(R.id.progress_title))
            .check(matches(isDisplayed()))
            .check(matches(withText(resources.getString(R.string.del_wait_title))))

        buttonInteraction(R.id.action_progress_main)
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .check(matches(withText(resources.getString(R.string.action_return_to_settings))))
            .perform(click())

        onView(withId(R.id.bottom_navigation))
            .check(matches(isDisplayed()))

        onView(allOf(withId(R.id.settings_list), isDisplayed()))
            .check(
                matches(
                    RecyclerViewChildActions.childOfViewAtPositionWithMatcher(
                        R.id.setting_title,
                        1,
                        withText(resources.getString(R.string.set_baker))
                    )
                )
            )
            .check(
                matches(
                    RecyclerViewChildActions.childOfViewAtPositionWithMatcher(
                        R.id.setting_detail,
                        1,
                        withText(currentDelegate)
                    )
                )
            )


        activityScenario.close()
    }

    companion object {
        fun navigateToSettings() {
            // Navigate to CONTACTS
            onView(
                allOf(
                    withId(R.id.nav_settings),
                    withContentDescription(R.string.wlt_navigation_settings)
                )
            ).check(matches(isDisplayed()))
                .perform(click())
        }

        fun navigateToBalance() {
            onView(
                allOf(
                    withId(R.id.nav_balance),
                    withContentDescription(R.string.wlt_navigation_balance)
                )
            ).check(matches(isDisplayed()))
                .perform(click())
        }

        fun checkSettings() {
            onView(
                allOf(
                    withId(R.id.toolbar_title),
                    withText(R.string.wlt_navigation_settings)
                )
            )
                .check(matches(isDisplayed()))
        }

        fun checkDelegateScreen(currentBaker: String) {
            onView(allOf(withId(R.id.expanded_title), isDescendantOfA(withId(R.id.toolbar_layout))))
                .check(matches(isDisplayed()))
                .check(matches(withText(R.string.del_title)))

            onView(withId(R.id.action_secondary))
                .check(matches(isDisplayed()))
                .check(matches(isEnabled()))
                .check(matches(withText(R.string.action_cancel)))
                .perform(click())

            checkSettings()

            onView(allOf(withId(R.id.settings_list), isDisplayed()))
                .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(1, click()))

            onView(allOf(withId(R.id.expanded_title), isDescendantOfA(withId(R.id.toolbar_layout))))
                .check(matches(isDisplayed()))
                .check(matches(withText(R.string.del_title)))

            Thread.sleep(1000)

            onView(withId(R.id.action_select_baker))
                .check(matches(isDisplayed()))
                .check(matches(isEnabled()))
            // TODO Review why currentBaker doesn't show on button on Test
//                .check(matches(withText(currentBaker)))

            onView(withId(R.id.action_next))
                .check(matches(isDisplayed()))
                .check(matches(not(isEnabled())))
        }

        private fun checkChooseDelegateScreen() {
            onView(withId(R.id.toolbar_title))
                .check(matches(isDisplayed()))
                .check(matches(withText(R.string.del_modal_title)))

            testQR()
        }

        fun testNoButtonInputTezosAddress(
            userAddress: Address,
            destination: Address,
            editTextInteraction: ViewInteraction = onView(
                allOf(
                    withId(R.id.input),
                    isDescendantOfA(withId(R.id.input_baker))
                )
            ),
            clearTextInteraction: ViewInteraction = onView(
                Matchers.allOf(
                    withId(R.id.text_input_end_icon),
                    isDescendantOfA(withId(R.id.input_baker)),
                    withContentDescription(R.string.icon_clear_action)
                )
            )
        ) {

            // Input short address
            editTextInteraction
                .check(matches(isDisplayed()))
                .perform(replaceText("tz1XSXBeWqdupm73qWAJkxJkxRzH16y77i1"))

            onView(withText(R.string.error_invalid_address))
                .check(matches(isDisplayed()))

            // Click on clear
            clearText(editTextInteraction, clearTextInteraction)

            // Input user address
            editTextInteraction
                .perform(replaceText(userAddress))

            onView(withText(R.string.error_own_address))
                .check(matches(isDisplayed()))

            clearText(editTextInteraction, clearTextInteraction)

            editTextInteraction
                .perform(replaceText(destination))

            onView(withText(R.string.error_invalid_address))
                .check(doesNotExist())
        }

        fun clearText(
            editTextInteraction: ViewInteraction,
            clearTextInteraction: ViewInteraction
        ) {
            clearTextInteraction
                .check(matches(isDisplayed()))
                .perform(click())

            editTextInteraction
                .check(matches(withText("")))

            onView(withText(R.string.error_invalid_address))
                .check(doesNotExist())
        }

        fun testQR() {
            // Test QR
            onView(
                allOf(
                    withId(R.id.input_icon),
                    withContentDescription(R.string.snd_scan_qr)
                )
            )
                .check(matches(isDisplayed()))
                .check(matches(isEnabled()))
                .perform(click())

            onView(withId(R.id.info))
                .check(matches(isDisplayed()))
                .check(matches(withText(R.string.snd_scan_qr)))

            onView(withId(R.id.action_back))
                .check(matches(isDisplayed()))
                .check(matches(isEnabled()))
                .perform(click())
        }

        fun testMinimalWarning(minAmount: Tez, balance: String) {
            onView(
                allOf(
                    withId(R.id.expanded_title),
                    withText(R.string.del_min_amount_title)
                )
            )
                .check(matches(isDisplayed()))

            // Min amount
            onView(allOf(withId(R.id.section_title), isDescendantOfA(withId(R.id.price_impact_section))))
                .check(matches(isDisplayed()))
                .check(matches(withText(R.string.del_min_amount_baker_title)))
            onView(allOf(withId(R.id.section_main_value), isDescendantOfA(withId(R.id.price_impact_section))))
                .check(matches(isDisplayed()))
                .check(matches(withText(minAmount.format(XTZ.symbol))))
            // User balance
            onView(allOf(withId(R.id.section_title), isDescendantOfA(withId(R.id.price_impact_section))))
                .check(matches(isDisplayed()))
                .check(matches(withText(R.string.del_min_amount_baker_title)))
            onView(allOf(withId(R.id.section_main_value), isDescendantOfA(withId(R.id.extra_section))))
                .check(matches(isDisplayed()))
                .check(matches(withText("$balance XTZ")))

            buttonInteraction()
                .check(matches(isEnabled()))
                .check(matches(withText(R.string.del_min_amount_change_action)))
                .perform(click())

            onView(allOf(withId(R.id.expanded_title), isDescendantOfA(withId(R.id.toolbar_layout))))
                .check(matches(isDisplayed()))
                .check(matches(withText(R.string.del_title)))

            Thread.sleep(500)

            buttonInteraction()
                .perform(click())

            Thread.sleep(500)

            buttonInteraction(R.id.action_secondary)
                .check(matches(isEnabled()))
                .check(matches(withText(R.string.exc_price_impact_notice_action_ok)))
        }
    }
}
