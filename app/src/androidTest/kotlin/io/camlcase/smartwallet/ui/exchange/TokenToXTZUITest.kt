/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.exchange

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.UiDevice
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.WaitForOperationTest
import io.camlcase.smartwallet.data.core.Tokens.Testnet.USDTZ
import io.camlcase.smartwallet.data.core.XTZ
import io.camlcase.smartwallet.data.core.extension.format
import io.camlcase.smartwallet.data.model.AppWallet
import io.camlcase.smartwallet.data.model.amount.TokenBalance
import io.camlcase.smartwallet.test.buttonInteraction
import io.camlcase.smartwallet.test.launchMainActivity
import io.camlcase.smartwallet.test.resources
import io.camlcase.smartwallet.ui.ActivityTabTest
import io.camlcase.smartwallet.ui.exchange.ExchangeScreenUITest.checkExchangeScreen
import io.camlcase.smartwallet.ui.main.MainActivity
import org.hamcrest.CoreMatchers.allOf
import org.junit.Before
import org.junit.Test
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*

/**
 * Preconditions
 * - Device has network access
 * - User has XTZ for fees
 * - User has >1 USDTez tokens
 */
class TokenToXTZUITest : WaitForOperationTest, ActivityTabTest {
    private lateinit var wallet: AppWallet
    private lateinit var device: UiDevice
    private lateinit var activityScenario: ActivityScenario<MainActivity>

    @Before
    fun setUp() {
        device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
        activityScenario = launchMainActivity("test-user-tokens-mnemonic") {
            wallet = it
        }
    }

    @Test
    fun test_Swap_TokenToXTZ() {
        XTZToTokenUITest.goAndCheckToExchange()

        // Leaves as  XTZ -> USDTez
        checkExchangeScreen(device)
        Thread.sleep(1000)
        // Switch to USDTez to XTZ
        onView(withId(R.id.exchange_rate))
            .check(matches(isDisplayed()))
            .check(matches(isClickable()))
            .perform(click())

        val fromAmount = getRandomTokenAmount(USDTZ.decimals)
        val (toAmount, conversionRate) = XTZToTokenUITest.extractValuesFromExchange(fromAmount)

        buttonInteraction()
            .check(matches(isEnabled()))
            .perform(click())

        // CONFIRMATION
        Thread.sleep(7 * 1000) // Wait for network call

        val fees = ExchangeConfirmationUITest.checkScreenTokenToXTZ(
            fromAmount.toDouble(),
            toAmount,
            USDTZ,
            null,
            conversionRate
        )

        buttonInteraction()
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .perform(click())

        Thread.sleep(7 * 1000) // Wait for network call
        // WAIT FOR INJECTION

        waitForExchangeOperation(USDTZ.symbol, XTZ.symbol)

        // MAIN SCREEN - EXCHANGE

        onView(withId(R.id.bottom_navigation))
            .check(matches(isDisplayed()))
        onView(
            allOf(
                withId(R.id.nav_activity),
                withContentDescription(R.string.wlt_navigation_activity)
            )
        ).check(matches(isDisplayed()))
            .perform(click())

        Thread.sleep(8 * 1000)

        // ACTIVITY TAB
        val formattedFromAmount =
            TokenBalance(fromAmount, USDTZ.decimals).formattedValue + " " + USDTZ.symbol
        val formattedToAmount = Tez(toAmount).format(XTZ.symbol)
        testActivityRow(
            resources.getString(R.string.act_type_exchange),
            "- $formattedFromAmount",
            "+ $formattedToAmount",
            fees.format(XTZ.symbol)
        )

        activityScenario.close()
    }

    companion object {
        fun getRandomTokenAmount(decimals: Int): BigDecimal {
            val number = Random().nextInt(998)
            val inTez = Tez((number + 1).toString())
            val withDecimals = inTez.signedDecimalRepresentation.setScale(decimals, RoundingMode.HALF_UP)
            return withDecimals
        }
    }
}
