/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.exchange

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.UiDevice
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.data.model.AppWallet
import io.camlcase.smartwallet.test.launchMainActivity
import io.camlcase.smartwallet.test.withViewAtPosition
import io.camlcase.smartwallet.ui.exchange.AdvancedOptionsUITest.checkOptionsModal
import io.camlcase.smartwallet.ui.exchange.ExchangeScreenUITest.checkExchangeScreen
import org.hamcrest.CoreMatchers.allOf
import org.junit.Before
import org.junit.Test

/**
 * Only validations, swap will be done in other test
 */
class ValidateExchangeTabUITest {
    private lateinit var wallet: AppWallet
    private lateinit var device: UiDevice

    @Before
    fun setUp() {
        device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
    }

    @Test
    fun test_Exchange_Tab_Validations() {
        val activityScenario = launchMainActivity("test-user-tokens-mnemonic") {
            wallet = it
        }

        // Has tokens
        onView(
            allOf(
                withId(R.id.list),
                isDisplayed()
            )
        )
            .check(matches(withViewAtPosition(1, hasDescendant(allOf(withId(R.id.token_balance), isDisplayed())))))

        navigateToExchange()

        checkExchangeScreen(device)

        onView(withId(R.id.action_extra))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .check(matches(withText(R.string.exc_action_advanced)))
            .perform(click())

        val slippage = 0.02
        checkOptionsModal(
            device,
            slippage,
            5
        )

        activityScenario.close()
    }

    companion object {

        fun navigateToExchange() {
            onView(
                allOf(
                    withId(R.id.nav_exchange),
                    withContentDescription(R.string.wlt_navigation_exchange)
                )
            ).check(matches(isDisplayed()))
                .perform(click())
        }

    }
}
