/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.exchange

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.UiDevice
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.data.model.AppWallet
import io.camlcase.smartwallet.model.operation.PriceImpactInfo
import io.camlcase.smartwallet.test.*
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.not
import org.junit.Before
import org.junit.Test
import java.util.*

class ValidatePriceImpactUITest {
    private lateinit var wallet: AppWallet
    private lateinit var device: UiDevice

    @Before
    fun setUp() {
        device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
    }

    @Test
    fun checkPriceImpactWarning() {
        val activityScenario = launchMainActivity("test-user-tokens-mnemonic") {
            wallet = it
        }
        // Has tokens
        onView(
            allOf(
                withId(R.id.list),
                isDisplayed()
            )
        )
            .check(
                matches(
                    withViewAtPosition(
                        1,
                        hasDescendant(
                            allOf(
                                withId(R.id.token_balance),
                                isDisplayed()
                            )
                        )
                    )
                )
            )

        ValidateExchangeTabUITest.navigateToExchange()
        ExchangeScreenUITest.firstCheck()

        // Change slippage+
        val slippage = 0.02
        changeSlippage(device, slippage)

        onView(
            allOf(
                withId(R.id.action_enter_max),
                isDescendantOfA(withId(R.id.from_amount))
            )
        )
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .perform(clickEvenIfPartiallyVisible())
        Thread.sleep(500)
        val fromAmount = getTextViewValue(ExchangeScreenUITest.fromAmount).toDouble()
        val toAmount = getTextViewValue(ExchangeScreenUITest.toAmount).toDouble()
        val conversionRate = getTextViewValue(onView(withId(R.id.exchange_rate)))

        buttonInteraction()
            .check(matches(isEnabled()))
            .perform(click())

        val priceImpact = checkPriceImpactWarningScreen(PriceImpactInfo.BIG_WARNING)

        Thread.sleep(7 * 1000) // Wait for network call

//        val fees = ExchangeConfirmationUITest.checkScreenXTZToToken(
//            device,
//            Tez(fromAmount),
//            toAmount,
//            TZBTC,
//            priceImpact,
//            conversionRate
//        )
        ExchangeConfirmationUITest.generalCheck(priceImpact, conversionRate)

        activityScenario.close()
    }

    companion object {

        fun changeSlippage(device: UiDevice, slippage: Double) {
            buttonInteraction(R.id.action_extra)
                .check(matches(isEnabled()))
                .check(matches(withText(R.string.exc_action_advanced)))
                .perform(click())

            AdvancedOptionsUITest.slippageInput
                .perform(replaceText(slippage.toString()))
            device.pressBack()
        }

        fun getRandomTezAmount(): Tez {
            val number = Random().nextInt(499_999)
            return Tez((number + 1).toString())
        }

        fun checkPriceImpactWarningScreen(info: PriceImpactInfo): String {
            if (info == PriceImpactInfo.BIG_WARNING) {
                onView(withId(R.id.ic_warning))
                    .check(matches(isDisplayed()))
            } else {
                onView(withId(R.id.ic_warning))
                    .check(matches(not(isDisplayed())))
            }

            onView(
                allOf(
                    withId(R.id.expanded_title),
                    withText(info.title)
                )
            ).check(matches(isDisplayed()))

            onView(withId(R.id.info))
                .check(matches(isDisplayed()))
                .check(matches(withText(info.info)))

            buttonInteraction(R.id.action_secondary)
                .check(matches(isEnabled()))
                .check(matches(withText(R.string.exc_price_impact_notice_action_ko)))
                .perform(click())

            onView(
                allOf(
                    withText(R.string.wlt_navigation_exchange),
                    isDescendantOfA(withId(R.id.toolbar))
                )
            )
                .check(matches(isDisplayed()))

            Thread.sleep(1000)
            buttonInteraction()
                .perform(click())

            val realPriceImpact = getTextViewValue(
                onView(
                    allOf(
                        withId(R.id.section_main_value),
                        isDescendantOfA(withId(R.id.price_impact_section))
                    )
                )
            )

            buttonInteraction()
                .check(matches(withText(R.string.exc_price_impact_notice_action_ok)))
                .perform(click())

            return realPriceImpact
        }
    }
}
