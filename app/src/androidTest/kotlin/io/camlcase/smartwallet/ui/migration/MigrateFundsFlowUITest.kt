/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.migration

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.UiDevice
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.Migration
import io.camlcase.smartwallet.data.model.AppWallet
import io.camlcase.smartwallet.test.*
import io.camlcase.smartwallet.ui.InfoViewUITest
import io.camlcase.smartwallet.ui.base.info.InfoType
import io.camlcase.smartwallet.ui.onboarding.CreateUserTest
import io.camlcase.smartwallet.ui.onboarding.SeedPhraseUITest
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.Matchers
import org.junit.Assert
import org.junit.Before
import org.junit.Test

/**
 * Preconditions:
 * - User needs to have >0.5 XTZ and positive FA1.2 tokens
 */
class MigrateFundsFlowUITest : InfoViewUITest {
    lateinit var wallet: AppWallet
    private lateinit var device: UiDevice

    @Before
    fun setUp() {
        device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
    }

    @Test
    fun test_Migrate_With_Funds() {
        val activityScenario = MigrationFlowUITest.launchMainActivityWithUser(
            getWallet = {
                wallet = it
            },
            initTestUser = {
                initUser(null, "test-user-tokens-mnemonic", (Migration.SECURE_WALLET_CREATION_VERSION - 1))
            })

        // Check dialog appears
        MigrationFlowUITest.checkMigrationDialog(device)

        onView(withId(R.id.action_dismiss))
            .perform(clickEvenIfPartiallyVisible())

        onView(withId(R.id.options_skip_temporary))
            .perform(clickEvenIfPartiallyVisible())
        // Retrieve XTZ balance
        val xtzBalance = getXTZBalance()
        onView(
            allOf(
                withId(R.id.nav_settings), withContentDescription(R.string.wlt_navigation_settings)
            )
        ).check(matches(isDisplayed()))
            .perform(click())

        // SETTINGS
        onView(allOf(withId(R.id.settings_list), isDisplayed()))
            .check(
                matches(
                    RecyclerViewChildActions.childOfViewAtPositionWithMatcher(
                        R.id.setting_title,
                        1,
                        withText(R.string.set_migration_title)
                    )
                )
            )
            .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(1, click()))

//        MigrationFlowUITest.checkBalanceCheck()

        // NEW SEED PHRASE
        Thread.sleep(15 * 1000)
        checkInfoView(InfoType.MIGRATION_SEED_PHRASE_INFO)
        onView(withId(R.id.action_primary))
            .perform(click())
        SeedPhraseUITest.checkSeedPhrase(R.string.mig_seed_phrase_info_action)

        val mnemonic = SeedPhraseUITest.getMnemonic()

        onView(withId(R.id.action_next))
            .perform(click())
        //Confirm seed phrase
        CreateUserTest.checkConfirmSeedPhrase(mnemonic, null)
        Thread.sleep(5 * 1000) // Wait for snackbars to disappear
        buttonInteraction()
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .perform(click())

        // TRANSFER FUNDS
        Thread.sleep(15 * 1000)
        checkTransferFunds(xtzBalance)

        activityScenario.close()
    }

    companion object {
        fun getXTZBalance(): String {
            val interaction = onView(
                allOf(
                    withId(R.id.token_balance),
                    isDescendantOfA(withId(R.id.balance_xtz))
                )
            )
            return getTextViewValue(interaction)
        }

        fun checkTransferFunds(xtzBalance: String) {
            onView(
                Matchers.allOf(
                    withId(R.id.expanded_title),
                    isDisplayed()
                )
            ).check(matches(isDisplayed()))
                .check(matches(withText(R.string.mig_funds_confirm_title)))

            val amountViewInteraction: ViewInteraction =
                onView(allOf(withId(R.id.section_main_value), isDescendantOfA(withId(R.id.funds_section))))
            val amountText = getTextViewValue(amountViewInteraction)

            Assert.assertTrue(amountText.contains(xtzBalance))

            val feesView: ViewInteraction =
                onView(allOf(withId(R.id.section_main_value), isDescendantOfA(withId(R.id.fees_section))))
            val feesText = getTextViewValue(feesView)
            val fees = Tez(feesText.split(" XTZ")[0].toDouble())

            Assert.assertTrue(fees > Tez.zero)

            buttonInteraction()
                .check(matches(isEnabled()))
        }
    }
}
