/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.migration

import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.UiDevice
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.Migration
import io.camlcase.smartwallet.data.model.AppWallet
import io.camlcase.smartwallet.test.*
import io.camlcase.smartwallet.ui.InfoViewUITest
import io.camlcase.smartwallet.ui.base.info.InfoType
import io.camlcase.smartwallet.ui.main.MainActivity
import io.camlcase.smartwallet.ui.onboarding.CreateUserTest
import io.camlcase.smartwallet.ui.onboarding.SeedPhraseUITest
import io.camlcase.smartwallet.ui.onboarding.SeedPhraseUITest.Companion.checkSeedPhrase
import io.camlcase.smartwallet.ui.onboarding.SeedPhraseUITest.Companion.getFirstWord
import io.camlcase.smartwallet.ui.onboarding.SeedPhraseUITest.Companion.getLastWord
import io.camlcase.smartwallet.ui.onboarding.SeedPhraseUITest.Companion.trimMnemonic
import org.hamcrest.CoreMatchers.allOf
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class MigrationFlowUITest : InfoViewUITest {

    lateinit var wallet: AppWallet
    private lateinit var device: UiDevice

    @Before
    fun setUp() {
        device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
    }

    @Test
    fun test_Migrate_Insufficient_Funds() {
        val activityScenario = launchMainActivityWithUser(
            getWallet = {
                wallet = it
            },
            initTestUser = {
                initUser(null, "test-user-migration-buffer", (Migration.SECURE_WALLET_CREATION_VERSION - 1))
            })

        // Check dialog appears
        checkMigrationDialog(device)

        onView(withId(R.id.action_migrate))
            .perform(clickEvenIfPartiallyVisible())

        // BALANCE CHECK
        checkBalanceCheck()

        // INSUFFICIENT FUNDS WARNING
        Thread.sleep(15 * 1000)
        checkInfoView(InfoType.MIGRATION_INSUFFICIENT_FUNDS_WARNING)
        onView(withId(R.id.action_primary))
            .perform(click())

        activityScenario.close()
    }

    @Test
    fun test_Migrate_Empty_Account() {
        val activityScenario = launchMainActivityWithUser(
            getWallet = {
                wallet = it
            },
            initTestUser = {
                initUser(null, "test-user-empty-mnemonic", (Migration.SECURE_WALLET_CREATION_VERSION - 1))
            })

        // Check dialog appears
        checkMigrationDialog(device)

        onView(withId(R.id.action_migrate))
            .perform(clickEvenIfPartiallyVisible())

        checkBalanceCheck()

        // NEW SEED PHRASE
        Thread.sleep(5 * 1000)
        checkInfoView(InfoType.MIGRATION_SEED_PHRASE_INFO)
        onView(withId(R.id.action_primary))
            .perform(click())
        checkSeedPhrase(R.string.mig_seed_phrase_info_action)

        val mnemonic = SeedPhraseUITest.getMnemonic()

        buttonInteraction()
            .perform(click())
        //Confirm seed phrase
        CreateUserTest.checkConfirmSeedPhrase(mnemonic, null)
        Thread.sleep(5 * 1000) // Wait for snackbars to disappear
        buttonInteraction()
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .perform(click())

        // SUCCESS
        val migrationAddress = checkMigrationSuccess()
        onView(withId(R.id.action_primary))
            .perform(click())

        verifyCompleteMigration(device, mnemonic[0], mnemonic[11], migrationAddress)

        activityScenario.close()
    }

    companion object {

        fun checkMigrationDialog(device: UiDevice) {
            onView(withText(R.string.mig_warning_title))
                .check(matches(isDisplayed()))

            onView(withId(R.id.warning_message))
                .check(matches(isDisplayed()))
                .check(matches(withText(R.string.mig_warning_message)))

            onView(withId(R.id.action_migrate))
                .check(matches(isDisplayed()))
                .check(matches(isEnabled()))
                .check(matches(withText(R.string.mig_warning_action)))

            onView(withId(R.id.action_dismiss))
                .check(matches(isDisplayed()))
                .check(matches(isEnabled()))
                .check(matches(withText(R.string.mig_warning_dismiss)))
                .perform(click())

            // Advanced Options
            onView(
                allOf(
                    withId(R.id.title),
                    isDescendantOfA(withId(R.id.options_skip_temporary))
                )
            )
                .check(matches(isDisplayed()))
                .check(matches(withText(R.string.mig_expert_options_skip_title)))

            onView(
                allOf(
                    withId(R.id.message),
                    isDescendantOfA(withId(R.id.options_skip_temporary))
                )
            )
                .check(matches(isDisplayed()))
                .check(matches(withText(R.string.mig_expert_options_skip_message)))

            onView(
                allOf(
                    withId(R.id.title),
                    isDescendantOfA(withId(R.id.options_skip_permanent))
                )
            )
                .check(matches(isDisplayed()))
                .check(matches(withText(R.string.mig_expert_options_never_title)))

            onView(
                allOf(
                    withId(R.id.message),
                    isDescendantOfA(withId(R.id.options_skip_permanent))
                )
            )
                .check(matches(isDisplayed()))
                .check(matches(withText(R.string.mig_expert_options_never_message)))

            device.pressBack()
        }

        fun checkBalanceCheck() {
            onView(withId(R.id.information_text))
                .check(matches(isDisplayed()))
                .check(matches(withText(R.string.mig_balance_check_info)))
        }

        fun launchMainActivityWithUser(
            getWallet: (AppWallet) -> Unit = {},
            initTestUser: () -> AppWallet
        ): ActivityScenario<MainActivity> {
            clearAllPrefs()
            val user = initTestUser()
            getWallet(user)

            val activityScenario: ActivityScenario<MainActivity> = ActivityScenario.launch(MainActivity::class.java)

            Thread.sleep(5 * 1000) // Wait for balances to be retrieved

            return activityScenario
        }

        /**
         * @return tz1 address
         */
        fun checkMigrationSuccess(): String {
            onView(withId(R.id.migration_info))
                .check(matches(isDisplayed()))
                .check(matches(withText(R.string.mig_success_message)))

            onView(withId(R.id.action_copy))
                .check(matches(isDisplayed()))
                .check(matches(isEnabled()))
                .perform(click())

            onView(withText(R.string.action_copy_success))
                .check(matches(isDisplayed()))

            onView(withId(R.id.action_primary))
                .check(matches(isDisplayed()))
                .check(matches(withText(InfoType.MIGRATION_SUCCESS.actionText!!)))

            return getTextViewValue(onView(withId(R.id.migration_address)))
        }



        fun verifyCompleteMigration(device: UiDevice, firstWord: String, lastWord: String, migrationAddress: String) {
            // MAIN SCREEN - WALLET
            onView(withId(R.id.bottom_navigation))
                .check(matches(isDisplayed()))

            // RECEIVE MODAL
            onView(withId(R.id.action_receive))
                .check(matches(isDisplayed()))
                .check(matches(isEnabled()))
                .perform(click())
            Thread.sleep(300)
            onView(withId(R.id.tz_address))
                .check(matches(isDisplayed()))
                .check(matches(withText(migrationAddress)))

            device.pressBack()
            onView(
                allOf(
                    withId(R.id.nav_settings), withContentDescription(R.string.wlt_navigation_settings)
                )
            ).check(matches(isDisplayed()))
                .perform(click())

            // SETTINGS
            onView(allOf(withId(R.id.settings_list), isDisplayed()))
                .check(
                    matches(
                        RecyclerViewChildActions.childOfViewAtPositionWithMatcher(
                            R.id.setting_title,
                            4,
                            withText(R.string.set_recovery)
                        )
                    )
                )
                .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(4, click()))

            checkSeedPhrase(
                timerVisible = true,
                withButton = false,
                derivationPath = true
            )

            val firstWord2 = getFirstWord().trimMnemonic(1)
            val lastWord2 = getLastWord().trimMnemonic(12)

            Assert.assertTrue(firstWord == firstWord2)
            Assert.assertTrue(lastWord == lastWord2)
        }
    }
}
