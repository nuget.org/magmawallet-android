/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.onboarding

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import io.camlcase.smartwallet.R
import org.hamcrest.Matchers.not

interface OnboardingTest {

    fun testPin() {
        onView(withId(R.id.detail))
            .check(matches(withText(R.string.onb_security_prompt_detail)))

        onView(withId(R.id.action_primary))
            .check(matches(isDisplayed()))
            // NEXT
            .perform(click())

        typePin()

        onView(withId(R.id.action_next))
            .check(matches(isEnabled()))
            // NEXT
            .perform(click())

        onView(withId(R.id.expanded_title))
            .check(matches(isDisplayed()))
            .check(matches(withText(R.string.onb_pin_confirm_title)))

        typePin()

        onView(withId(R.id.action_next))
            .check(matches(isEnabled()))

        // TODO Check why it fails on terminal launch with error:
        // Fatal signal 11 (SIGSEGV), code 1 (SEGV_MAPERR), fault addr 0x8 in tid 12577
        Thread.sleep(1 * 1000)

        onView(withId(R.id.action_next))
            .perform(click())

        Thread.sleep(1 * 1000)

        /**
         * FINAL SUCCESS
         */

        onView(withId(R.id.title))
            .check(matches(isDisplayed()))
            .check(matches(withText(R.string.onb_final_success_title)))

        onView(withId(R.id.detail))
            .check(matches(isDisplayed()))
            .check(matches(withText(R.string.onb_final_success_detail)))

        onView(withId(R.id.action_primary))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            // NEXT
            .perform(click())

        onView(withId(R.id.bottom_navigation))
            .check(matches(isDisplayed()))
    }

    private fun typePin() {
        onView(withId(R.id.pin_input))
            .check(matches(isDisplayed()))
            .perform(ViewActions.replaceText("1"))

        onView(withId(R.id.action_next))
            .check(matches(isDisplayed()))
            .check(matches(not(isEnabled())))

        onView(withId(R.id.pin_input))
            .perform(ViewActions.replaceText("12"))

        onView(withId(R.id.action_next))
            .check(matches(not(isEnabled())))

        onView(withId(R.id.pin_input))
            .perform(ViewActions.replaceText("123"))

        onView(withId(R.id.action_next))
            .check(matches(not(isEnabled())))

        onView(withId(R.id.pin_input))
            .perform(ViewActions.replaceText("1234"))
    }
}
