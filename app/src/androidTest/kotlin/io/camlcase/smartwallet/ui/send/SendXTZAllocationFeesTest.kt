/**
 * # Released under MIT License
 *
 * Copyright (c) 2021 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.send

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.UiDevice
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.WaitForOperationTest
import io.camlcase.smartwallet.data.model.AppWallet
import io.camlcase.smartwallet.test.*
import io.camlcase.smartwallet.ui.ActivityTabTest
import io.camlcase.smartwallet.ui.TezosAddressInputTest
import io.camlcase.smartwallet.ui.XTZAmountInputTest
import io.camlcase.smartwallet.ui.contact.AddContactFromSendFlow
import io.camlcase.smartwallet.ui.delegate.DelegateTest
import org.hamcrest.CoreMatchers.*
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class SendXTZAllocationFeesTest : TezosAddressInputTest,
    XTZAmountInputTest,
    WaitForOperationTest,
    ActivityTabTest {

    lateinit var wallet: AppWallet
    private lateinit var device: UiDevice

    @Before
    fun setUp() {
        device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
    }

    @Test
    fun testAllocationFeesInSendConfirm() {
        val activityScenario = launchMainActivity("test-user-tokens-mnemonic") {
            wallet = it
        }

        // TOKEN DETAIL

        onView(withId(R.id.balance_xtz_container))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .perform(click())
        val balanceContainer: ViewInteraction =
            onView(allOf(withId(R.id.token_balance), isDescendantOfA(withId(R.id.balance_xtz_container))))
        val xtzBalance = getTextViewValue(balanceContainer)
        val usdValue = getTextViewValue(
            onView(
                allOf(
                    withId(R.id.currency_exchange),
                    isDescendantOfA(withId(R.id.balance_xtz_container))
                )
            )
        )
        val balanceInUSD = getTextViewValue(
            onView(
                allOf(
                    withId(R.id.token_balance_currency),
                    isDescendantOfA(withId(R.id.balance_xtz_container))
                )
            )
        )
        checkTokenDetail(xtzBalance, usdValue, balanceInUSD)

        onView(
            allOf(
                withId(R.id.action_send),
                isDescendantOfA(withId(R.id.container_token_detail))
            )
        )
            .perform(click())

        // DESTINATION
        buttonAtStartCheck()

        AddContactFromSendFlow.checkSendToScreen()
        DelegateTest.testQR()

        val emptyUser = getTestWallet("test-user-empty-mnemonic")!!
        val destination = emptyUser.address
        testInputTezosAddress(
            wallet.address,
            destination = destination
        )

        buttonInteraction()
            .perform(click())

        // Goes directly to XTZ because it clicked the detail
        // AMOUNT

        val sendAmount = SendXTZFromMainTest.getRandomTezAmount()
        SendXTZFromMainTest.addSendAmount(this, sendAmount)

        buttonInteraction()
            .perform(click())

        Thread.sleep(10 * 1000) // Wait for network call
        // CONFIRM
        val (formattedSendAmount, feesText, total) = SendXTZFromMainTest.confirmSend(sendAmount, destination)
        // FEES DETAILS
        onView(withId(R.id.fees_section))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .perform(click())
        checkFeeDetailWithAllocation(device, feesText)
        device.pressBack()

        activityScenario.close()
    }

    companion object {
        fun checkFeeDetailWithAllocation(device: UiDevice, fees: String) {
            val totalFees = Tez(fees.split(" XTZ")[0].toDouble())
            onView(allOf(withId(R.id.expanded_title), withText(R.string.fees_detail_title)))
                .check(matches(isDisplayed()))

            onView(withId(R.id.baker_section))
                .check(matches(isDisplayed()))
            val bakerFeesText = getTextViewValue(
                onView(
                    allOf(
                        withId(R.id.section_main_value),
                        isDescendantOfA(withId(R.id.baker_section))
                    )
                )
            )
            val bakerFees = Tez(bakerFeesText.split(" XTZ")[0].toDouble())

            onView(withId(R.id.allocation_section))
                .check(matches(isDisplayed()))
            val allocationFeesText = getTextViewValue(
                onView(
                    allOf(
                        withId(R.id.section_main_value),
                        isDescendantOfA(withId(R.id.allocation_section))
                    )
                )
            )
            val allocationFees = Tez(allocationFeesText.split(" XTZ")[0].toDouble())

            onView(withId(R.id.burn_section))
                .check(matches(not(isDisplayed())))
            onView(withId(R.id.reveal_section))
                .check(matches(not(isDisplayed())))

            Assert.assertTrue(totalFees == (bakerFees + allocationFees))
            onView(
                allOf(
                    withId(R.id.section_main_value),
                    isDescendantOfA(withId(R.id.total_fees_section))
                )
            )
                .check(matches(isDisplayed()))
                .check(matches(withText(fees)))

            onView(withId(R.id.fees_info))
                .check(matches(isDisplayed()))
                .check(matches(isEnabled()))
                .check(matches(withText(R.string.fees_detail_about)))
                .perform(click())

            onView(allOf(withId(R.id.toolbar), isDescendantOfA(withId(R.id.app_bar))))
                .check(matches(isDisplayed()))
                .check(matches(hasDescendant(withText(R.string.fees_modal_title))))

            onView(withId(R.id.detail))
                .check(matches(isDisplayed()))
                .check(matches(withText(containsString(resources.getString(R.string.fees_modal_allocation_details)))))

            device.pressBack()
        }

        fun checkTokenDetail(xtzBalance: String, usdValue: String, xtzInUSD: String) {

            onView(
                allOf(
                    withId(R.id.token_balance),
                    isDescendantOfA(withId(R.id.toolbar_layout))
                )
            )
                .check(matches(isDisplayed()))
                .check(matches(withText(xtzBalance)))

            onView(
                allOf(
                    withId(R.id.token_symbol),
                    isDescendantOfA(withId(R.id.toolbar_layout))
                )
            )
                .check(matches(isDisplayed()))
                .check(matches(withText("XTZ")))

            onView(
                allOf(
                    withId(R.id.token_balance_currency),
                    isDescendantOfA(withId(R.id.toolbar_layout))
                )
            )
                .check(matches(isDisplayed()))
                .check(matches(withText(xtzInUSD)))

            onView(
                allOf(
                    withId(R.id.currency_exchange),
                    isDescendantOfA(withId(R.id.toolbar_layout))
                )
            )
                .check(matches(isDisplayed()))
                .check(matches(withText(usdValue)))

            buttonInteraction(R.id.action_exchange)
                .check(matches(isDisplayed()))
                .check(matches(isEnabled()))

            onView(
                allOf(
                    withId(R.id.action_send),
                    isDescendantOfA(withId(R.id.container_token_detail))
                )
            )
                .check(matches(isDisplayed()))
                .check(matches(isEnabled()))

            onView(
                allOf(
                    withId(R.id.action_receive),
                    isDescendantOfA(withId(R.id.container_token_detail))
                )
            )
                .check(matches(isDisplayed()))
                .check(matches(isEnabled()))
        }
    }
}
