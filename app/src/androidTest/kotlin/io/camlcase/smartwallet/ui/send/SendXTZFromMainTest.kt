/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.send

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.UiDevice
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.WaitForOperationTest
import io.camlcase.smartwallet.core.extension.ellipsize
import io.camlcase.smartwallet.data.core.XTZ
import io.camlcase.smartwallet.data.core.extension.format
import io.camlcase.smartwallet.data.model.AppWallet
import io.camlcase.smartwallet.test.*
import io.camlcase.smartwallet.ui.ActivityTabTest
import io.camlcase.smartwallet.ui.TezosAddressInputTest
import io.camlcase.smartwallet.ui.XTZAmountInputTest
import io.camlcase.smartwallet.ui.contact.AddContactFromSendFlow.Companion.checkSendToScreen
import io.camlcase.smartwallet.ui.delegate.DelegateTest.Companion.testQR
import org.hamcrest.CoreMatchers.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.util.*

/**
 * Preconditions:
 * - User has XTZ
 * - User has NOT FA2.1 tokens
 * - Network available
 */
@LargeTest
@RunWith(AndroidJUnit4::class)
class SendXTZFromMainTest : TezosAddressInputTest,
    XTZAmountInputTest,
    WaitForOperationTest,
    ActivityTabTest {
    lateinit var wallet: AppWallet
    private lateinit var device: UiDevice

    @Before
    fun setUp() {
        device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
    }

    @Test
    fun testSendAmount() {
        val activityScenario = launchMainActivity("test-user-mnemonic") {
            wallet = it
        }

        navigateToMainSend()

        // DESTINATION
        buttonAtStartCheck()

        checkSendToScreen()
        testQR()

        val destination = mainTestnetAddress
        testInputTezosAddress(
            wallet.address,
            destination = destination
        )

        onView(withId(R.id.action_next))
            .perform(click())

        // Goes directly to XTZ because it doesn't have tokens
        // AMOUNT

        val sendAmount = getRandomTezAmount()
        addSendAmount(this, sendAmount)

        buttonInteraction()
            .perform(click())

        Thread.sleep(10 * 1000) // Wait for network call
        // CONFIRM
        val (formattedSendAmount, feesText, total) = confirmSend(sendAmount, destination)
        // FEES DETAILS
        onView(withId(R.id.fees_section))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .perform(click())
        checkFeeDetailSimple(device, feesText)
        device.pressBack()

        onView(
            allOf(
                withId(R.id.action_next),
                withEffectiveVisibility(Visibility.VISIBLE),
                withText(R.string.action_confirm)
            )
        )
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .perform(click())

        Thread.sleep(3 * 1000) // Wait for network call
        // WAIT FOR INJECTION

        waitForSendOperation(formattedSendAmount)

        // MAIN SCREEN - WALLET

        onView(withId(R.id.bottom_navigation))
            .check(matches(isDisplayed()))

        onView(
            allOf(
                withId(R.id.nav_activity), withContentDescription(R.string.wlt_navigation_activity)
            )
        ).check(matches(isDisplayed()))
            .perform(click())

        Thread.sleep(10 * 1000)

        // ACTIVITY TAB
        testActivityRow(
            resources.getString(R.string.act_type_send),
            "- $formattedSendAmount",
            feesText
        )
        testSendDestination(destination.ellipsize())

        activityScenario.close()
    }

    companion object {
        fun navigateToMainSend() {
            onView(withId(R.id.action_send))
                .check(matches(isDisplayed()))
                .check(matches(isEnabled()))
                .perform(click())
        }

        fun getRandomTezAmount(): Tez {
            val number = Random().nextInt(499)
            return Tez((number + 1).toString())
        }

        fun addSendAmount(xtzInput: XTZAmountInputTest, tez: Tez) {
            onView(withId(R.id.amount_field))
                .check(matches(isDisplayed()))
                .check(matches(withText("")))

            onView(withId(R.id.action_enter_max))
                .check(matches(isDisplayed()))
                .check(matches(isEnabled()))

            buttonAtStartCheck()

            xtzInput.testInputXTZ(
                tez
            )
        }

        /**
         * @return (formattedSendAmount, feesText, total)
         */
        fun confirmSend(sendAmount: Tez, destination: Address): Triple<String, String, Tez> {
            onView(withId(R.id.send_section))
                .check(matches(isDisplayed()))

            val formattedSendAmount = sendAmount.format(XTZ.symbol)
            onView(allOf(withId(R.id.section_main_value), isDescendantOfA(withId(R.id.send_section))))
                .check(matches(isDisplayed()))
                .check(matches(withText(formattedSendAmount)))

            onView(allOf(withId(R.id.section_main_value), isDescendantOfA(withId(R.id.fees_section))))
                .check(matches(isDisplayed()))
                .check(matches(not(withText(""))))

            val feesView: ViewInteraction =
                onView(allOf(withId(R.id.section_main_value), isDescendantOfA(withId(R.id.fees_section))))
            val feesText = getTextViewValue(feesView)
            val fees = Tez(feesText.split(" XTZ")[0].toDouble())

            val total = sendAmount + fees
            onView(allOf(withId(R.id.section_main_value), isDescendantOfA(withId(R.id.total_section))))
                .check(matches(isDisplayed()))
                .check(matches(withText(total.format(XTZ.symbol))))

            onView(withId(R.id.recipient))
                .check(matches(isDisplayed()))
                .check(matches(withText(destination)))

            return Triple(formattedSendAmount, feesText, total)
        }

        fun checkFeeDetailSimple(device: UiDevice, fees: String) {
            onView(allOf(withId(R.id.expanded_title), withText(R.string.fees_detail_title)))
                .check(matches(isDisplayed()))

            onView(allOf(withId(R.id.section_main_value), isDescendantOfA(withId(R.id.baker_section))))
                .check(matches(isDisplayed()))
                .check(matches(withText(fees)))

            onView(withId(R.id.allocation_section))
                .check(matches(not(isDisplayed())))
            onView(withId(R.id.burn_section))
                .check(matches(not(isDisplayed())))
            onView(withId(R.id.reveal_section))
                .check(matches(not(isDisplayed())))

            onView(allOf(withId(R.id.section_main_value), isDescendantOfA(withId(R.id.total_fees_section))))
                .check(matches(isDisplayed()))
                .check(matches(withText(fees)))

            onView(withId(R.id.fees_info))
                .check(matches(isDisplayed()))
                .check(matches(isEnabled()))
                .check(matches(withText(R.string.fees_detail_about)))
                .perform(click())

            onView(allOf(withId(R.id.toolbar), isDescendantOfA(withId(R.id.app_bar))))
                .check(matches(isDisplayed()))
                .check(matches(hasDescendant(withText(R.string.fees_modal_title))))

            onView(withId(R.id.detail))
                .check(matches(isDisplayed()))
                .check(matches(withText(containsString(resources.getString(R.string.fees_modal_allocation_details)))))

            device.pressBack()
        }
    }
}
