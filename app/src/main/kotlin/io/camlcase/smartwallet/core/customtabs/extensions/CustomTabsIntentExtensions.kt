/**
 * (c) @saurabharora90
 * Added from the CustomTabs-Kotlin library in Github as it was published on JCenter and not
 * available as a library anymore.
 * https://github.com/saurabharora90/CustomTabs-Kotlin
 */
package io.camlcase.smartwallet.core.customtabs.extensions

import android.app.Activity
import android.net.Uri
import androidx.browser.customtabs.CustomTabsIntent
import io.camlcase.smartwallet.core.customtabs.CustomTabFallback
import io.camlcase.smartwallet.core.customtabs.internal.CustomTabsHelper

/**
 * Opens the URL on a Custom Tab if possible. Otherwise fallback
 *
 * @param activity The host activity.
 * @param uri the Uri to be opened.
 * @param fallback a CustomTabFallback to be used if Custom Tabs is not available.
 */
fun CustomTabsIntent.launchWithFallback(activity: Activity,
                                        uri: Uri,
                                        fallback: CustomTabFallback? = null) {
    val packageName = CustomTabsHelper.getPackageNameToUse(activity)

    //If we cant find a package name, it means there is no browser that supports
    //Chrome Custom Tabs installed. So, we do the fallback
    if (packageName == null) {
        fallback?.openUri(activity, uri)
    } else {
        intent.setPackage(packageName)
        launchUrl(activity, uri)
    }
}
