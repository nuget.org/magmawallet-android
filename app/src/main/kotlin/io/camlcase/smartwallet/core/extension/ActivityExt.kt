/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.core.extension

import android.app.Activity
import android.app.ActivityManager
import android.transition.Slide
import android.view.Gravity
import android.view.View
import android.view.ViewTreeObserver
import androidx.annotation.AnimRes
import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import io.camlcase.smartwallet.MagmaApplication
import io.camlcase.smartwallet.data.core.Logger
import io.camlcase.smartwallet.di.ApplicationComponent
import io.camlcase.smartwallet.ui.base.BaseActivity
import io.camlcase.smartwallet.ui.base.navigate.AppNavigator

fun AppCompatActivity.injector(): ApplicationComponent {
    return (this.application as MagmaApplication).component
}

/**
 * Method to replace the fragment. The [fragment] is added to the container view with id
 * [containerViewId] and a [tag]. The operation is performed by the supportFragmentManager.
 * https://proandroiddev.com/kotlin-extensions-to-commit-fragments-safely-de06218a1f4
 */
fun AppCompatActivity.replaceFragmentSafely(
    fragment: Fragment,
    tag: String = fragment.javaClass.name,
    allowStateLoss: Boolean = false,
    addToBackStack: String? = null,
    @IdRes containerViewId: Int,
    @AnimRes enterAnimation: Int = 0,
    @AnimRes exitAnimation: Int = 0
) {
    val ft = supportFragmentManager
        .beginTransaction()
        .setCustomAnimations(enterAnimation, exitAnimation)
        .addToBackStack(addToBackStack)
        .replace(containerViewId, fragment, tag)
    if (!supportFragmentManager.isStateSaved) {
        ft.commit()
    } else if (allowStateLoss) {
        ft.commitAllowingStateLoss()
    }
}

fun AppCompatActivity?.replaceFragmentSafely(
    fragment: Fragment,
    @IdRes containerViewId: Int,
    backStackEnabled: Boolean = false,
    allowStateLoss: Boolean = false
) {
    this?.also {
        val name = fragment.javaClass.name
        val transaction = supportFragmentManager.beginTransaction()
        if (backStackEnabled) {
            transaction.addToBackStack(name)
        }
        transaction.replace(containerViewId, fragment, name)
        if (!supportFragmentManager.isStateSaved) {
            transaction.commit()
        } else if (allowStateLoss) {
            transaction.commitAllowingStateLoss()
        }
    }
}

/**
 * Checks if fragment is already on the stack and replaces to avoid duplicates.
 */
fun AppCompatActivity?.loadFragment(
    fragment: Fragment,
    @IdRes containerViewId: Int,
    backStackEnabled: Boolean = false,
    allowStateLoss: Boolean = false
) {
    this?.also {
        val name = fragment.javaClass.name
        val findFragmentByTag = supportFragmentManager.findFragmentByTag(name)
        val transaction = supportFragmentManager.beginTransaction()
        if (findFragmentByTag == null) { // DKA control
            transaction.add(containerViewId, fragment, name)
            if (backStackEnabled) {
                transaction.addToBackStack(name)
            }
            transaction.commit()
        } else {
            replaceFragmentSafely(fragment, containerViewId, backStackEnabled, allowStateLoss)
        }
    }
}

/**
 * Use it to create flows of fragments.
 */
fun AppCompatActivity?.displayFragment(
    fragment: Fragment,
    @IdRes containerViewId: Int,
    animations: Pair<Int, Int>? = null
) {
    this?.apply {
        val name = fragment.javaClass.name
        val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()

        // Hide fragments
        supportFragmentManager.fragments.forEach {
            if (it.isAdded) {
                transaction.hide(it)
            }
        }

        animations?.apply {
            transaction.setCustomAnimations(animations.first, animations.second)
        }

        transaction.setReorderingAllowed(true)
        if (fragment.isAdded) { // if the fragment is already in container
            transaction.show(fragment)
        } else { // fragment needs to be added to frame container
            transaction.add(containerViewId, fragment, name)
            transaction.addToBackStack(name)
        }

        // Commit changes
        transaction.commit()
    }
}

fun AppCompatActivity?.hideFragments() {
    this?.apply {
        val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()

        // Hide fragments
        supportFragmentManager.fragments.forEach {
            if (it.isAdded) {
                transaction.hide(it)
            }
        }
        // Commit changes
        transaction.commit()
    }
}

fun AppCompatActivity?.popFullBackstack() {
    this?.also {
        val fragmentManager: FragmentManager = supportFragmentManager
        // this will clear the back stack and displays no animation on the screen
//        fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        fragmentManager.popBackStack()
    }
}

fun AppCompatActivity?.getActiveFragmentTag(): String? {
    return this?.let {
        val activeFragments = it.supportFragmentManager.fragments
        if (!activeFragments.isNullOrEmpty()) {
            val currentFragment = activeFragments.last()
            currentFragment.tag
        } else {
            null
        }
    }
}

/**
 * Listens to the global state of the Activity and triggers [keyboardVisibilityChange] whenever height changes (keyboard appears/disappears)
 * @see https://proandroiddev.com/how-to-detect-if-the-android-keyboard-is-open-269b255a90f5
 *
 * @return listener so it can be later dismissed in [clearKeyboardListener] and avoid leaks
 */
fun Activity?.listenToKeyboard(keyboardVisibilityChange: (keyboardVisible: Boolean) -> Unit): ViewTreeObserver.OnGlobalLayoutListener {
    val contentView: View? = this?.findViewById(android.R.id.content)

    val listener = object : ViewTreeObserver.OnGlobalLayoutListener {
        private var previousHeight: Int = 0

        override fun onGlobalLayout() {
            contentView?.also {
                val newHeight = contentView.height
                if (previousHeight != 0) {
                    if (previousHeight > newHeight) {
                        // Height decreased: keyboard was shown
                        keyboardVisibilityChange(true)
                    } else if (previousHeight < newHeight) {
                        // Height increased: keyboard was hidden
                        keyboardVisibilityChange(false)
                    }
                }
                previousHeight = newHeight
            }
        }
    }
    contentView?.viewTreeObserver?.addOnGlobalLayoutListener(listener)
    return listener
}

fun Activity?.clearKeyboardListener(remove: ViewTreeObserver.OnGlobalLayoutListener?) {
    val contentView: View? = this?.findViewById(android.R.id.content)
    contentView?.viewTreeObserver?.removeOnGlobalLayoutListener(remove)
}

inline fun <reified T> BaseActivity?.communicateWithFragment(action: (T) -> Unit) {
    this?.apply {
        for (fragment in supportFragmentManager.fragments) {
            if (fragment is T) {
                action(fragment as T)
            }
        }
    }
}

/**
 * transaction.add() doesn't let you work with shared elements or transitions properly.
 */
fun Fragment?.slideLeftAnimation() {
    this?.enterTransition = Slide(Gravity.END)
    this?.exitTransition = Slide(Gravity.START)
}

fun BaseActivity.activeActivities(): Int {
    val tasks = (getSystemService(AppCompatActivity.ACTIVITY_SERVICE) as? ActivityManager?)?.appTasks
    return if (tasks.isNullOrEmpty()) {
        0
    } else {
        tasks[0]?.taskInfo?.numActivities ?: 0
    }
}

/**
 * Connect to reactive publisher [BaseActivity.appBackgroundDelegate] about coming from background and show the
 * authentication login screen if conditions in [BaseActivity.appBackgroundViewModel] are met.
 *
 * All activities used in a logged in situation should implement this on [BaseActivity.onCreate]
 */
fun BaseActivity?.listenToBackground() {
    this?.apply {
        /**
         * De-subscribe sooner in the lifecycle if there's other activities listening to it (DKA fallback)
         */
        val disposable = if (activeActivities() == 1) {
            disposables
        } else {
            onStopDisposables
        }
        appBackgroundDelegate.stream
            .subscribe(
                {
                    if (appBackgroundViewModel.showAuthentication(it)) {
                        AppNavigator.navigateToAuthLogin(this)
                    }
                },
                { Logger.e("*** ERROR while listening for background: $it") }
            ).addToDisposables(disposable)
    }
}
