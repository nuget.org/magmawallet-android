/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.core.extension

import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.validTezosAddress
import io.camlcase.smartwallet.data.analytic.report
import java.math.BigDecimal
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

// Jan 01 2020
const val OPERATION_DATE_FORMAT = "MMM dd yyyy"

/**
 * Parse milliseconds to a String date.
 */
fun Long.formatDate(format: String): String {
    return try {
        val sdf = SimpleDateFormat(format, Locale.getDefault())
        val netDate = Date(this)
        sdf.format(netDate)
    } catch (ex: Exception) {
        ex.report("formatDate")
        ""
    }
}

/**
 * Display the first 14 characters, followed by an ellipsis, followed by the last 6 characters
 * (e.g. tz178vdv89def0…2010gz)
 */
fun Address.ellipsize(): String {
    if (this.validTezosAddress()) {
        return this.take(14) + "..." + this.takeLast(6)
    }
    return this
}

fun Double.formatPercentage(): String {
    return DecimalFormat("#.##").format(this)
}

fun BigDecimal.formatExchangeRate(): String {
    val decimalFormat = DecimalFormat("###,###.######")
    return decimalFormat.format(this)
}

fun Double.formatExchangeRate(): String {
    val decimalFormat = DecimalFormat("###,###.######")
    return decimalFormat.format(this)
}

/**
 * Dynamic decimals, truncates to first non-zero decimals
 * 0.0XXX -> 0.0X
 * 0.00XX -> 0.00X
 * And so on, until [max]
 */
fun Double.formatAndTruncate(max: Int = 6): String {
    val doubleAsString: String = String.format("%.${max}f", this)
    val indexOfDecimal = doubleAsString.indexOf(".")
    val decimalPart = doubleAsString.substring(indexOfDecimal + 1)

    val format = if (this == 0.0) {
        "0.00"
    } else {
        "###,##0." + parseDecimalFormat(decimalPart)
    }

    val decimalFormat = DecimalFormat(format)
    return decimalFormat.format(this)
}

/**
 * Provides decimal formatting guide for [DecimalFormat]
 * 0.0XXX -> 0.0X
 * 0.00XX -> 0.00X
 * And so on, until [max]
 */
private fun parseDecimalFormat(decimalPart: String, max: Int = 6): String {
    var decimalFormat = ""
    val default = "00"
    if (decimalPart.toInt() == 0) {
        return default
    }

    for (i in decimalPart.indices) {
        if (i == max) {
            return decimalFormat
        }

        if (decimalPart[i] == '0') {
            decimalFormat += "#"
        } else {
            return if (i < 2) {
                "##"
            } else {
                decimalFormat += "#"
                decimalFormat
            }
        }
    }

    return decimalFormat
}

fun BigDecimal.formatExchangeRate(fromTokenSymbol: String, toTokenSymbol: String): String {
    return "1 $fromTokenSymbol = ${this.formatExchangeRate()} $toTokenSymbol"
}
