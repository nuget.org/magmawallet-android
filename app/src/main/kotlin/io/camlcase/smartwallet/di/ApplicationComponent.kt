package io.camlcase.smartwallet.di

import androidx.fragment.app.DialogFragment
import dagger.Component
import io.camlcase.smartwallet.MagmaApplication
import io.camlcase.smartwallet.data.di.AndroidKotlinTezosSourceModule
import io.camlcase.smartwallet.data.di.ContextModule
import io.camlcase.smartwallet.data.di.DataSourceModule
import io.camlcase.smartwallet.data.di.KotlinTezosSourceModule
import io.camlcase.smartwallet.di.viewmodel.ViewModelModule
import io.camlcase.smartwallet.ui.LaunchActivity
import io.camlcase.smartwallet.ui.base.BaseActivity
import io.camlcase.smartwallet.ui.base.BaseBottomSheetFragment
import io.camlcase.smartwallet.ui.base.BaseFragment
import io.camlcase.smartwallet.ui.base.BindBaseFragment
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        ApplicationModule::class,
        ViewModelModule::class,
        ContextModule::class,
        SourceModule::class,
        DataSourceModule::class,
        KotlinTezosSourceModule::class,
        AndroidKotlinTezosSourceModule::class
    ]
)
interface ApplicationComponent {
    fun inject(application: MagmaApplication)
    fun inject(activity: BaseActivity)

    fun inject(activity: LaunchActivity)

    @Deprecated("Use BindBaseFragment and migrate to view binding")
    fun inject(fragment: BaseFragment)
    fun inject(fragment: BindBaseFragment)
    fun inject(fragment: BaseBottomSheetFragment)
    fun inject(fragment: DialogFragment)
}
