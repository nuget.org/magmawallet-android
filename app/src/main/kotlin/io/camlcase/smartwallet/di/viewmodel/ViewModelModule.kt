/**
 * Copyright (C) 2018 Fernando Cejas Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.camlcase.smartwallet.di.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import io.camlcase.smartwallet.ui.LaunchViewModel
import io.camlcase.smartwallet.ui.currency.LocalCurrencyViewModel
import io.camlcase.smartwallet.ui.delegate.DelegateListViewModel
import io.camlcase.smartwallet.ui.delegate.DelegateViewModel
import io.camlcase.smartwallet.ui.delegate.OperationDetailsViewModel
import io.camlcase.smartwallet.ui.exchange.amount.ChooseTokenViewModel
import io.camlcase.smartwallet.ui.exchange.amount.ExchangeAmountViewModel
import io.camlcase.smartwallet.ui.exchange.confirmation.SwapConfirmationViewModel
import io.camlcase.smartwallet.ui.exchange.options.AdvancedExchangeViewModel
import io.camlcase.smartwallet.ui.login.SeedPhraseImportViewModel
import io.camlcase.smartwallet.ui.login.balance.ImportBalanceViewModel
import io.camlcase.smartwallet.ui.main.BackgroundViewModel
import io.camlcase.smartwallet.ui.main.activity.OperationListViewModel
import io.camlcase.smartwallet.ui.main.contact.ContactListViewModel
import io.camlcase.smartwallet.ui.main.contact.detail.ContactDetailViewModel
import io.camlcase.smartwallet.ui.main.contact.edit.AddContactViewModel
import io.camlcase.smartwallet.ui.main.settings.SettingsViewModel
import io.camlcase.smartwallet.ui.main.settings.attributions.AttributionsViewModel
import io.camlcase.smartwallet.ui.main.settings.network.NetworkConfigViewModel
import io.camlcase.smartwallet.ui.main.token.SideDataViewModel
import io.camlcase.smartwallet.ui.main.token.TokenListViewModel
import io.camlcase.smartwallet.ui.migration.DismissMigrationViewModel
import io.camlcase.smartwallet.ui.migration.FinalMigrationViewModel
import io.camlcase.smartwallet.ui.migration.MigrationWarningViewModel
import io.camlcase.smartwallet.ui.migration.ShouldMigrateViewModel
import io.camlcase.smartwallet.ui.migration.funds.BalanceCheckViewModel
import io.camlcase.smartwallet.ui.migration.funds.MigrateFundsViewModel
import io.camlcase.smartwallet.ui.money.BuyTezViewModel
import io.camlcase.smartwallet.ui.onboarding.seed_phrase.SeedPhraseViewModel
import io.camlcase.smartwallet.ui.onboarding.wallet.CreateWalletViewModel
import io.camlcase.smartwallet.ui.receive.ReceiveViewModel
import io.camlcase.smartwallet.ui.security.PinCreateViewModel
import io.camlcase.smartwallet.ui.security.biometric.BiometricSetViewModel
import io.camlcase.smartwallet.ui.security.lock.AuthLoginViewModel
import io.camlcase.smartwallet.ui.send.GetUserAddressViewModel
import io.camlcase.smartwallet.ui.send.SendAmountViewModel
import io.camlcase.smartwallet.ui.send.confirmation.SendConfirmationViewModel

@Module
abstract class ViewModelModule {
    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(LaunchViewModel::class)
    abstract fun bindLaunchViewModel(viewModel: LaunchViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CreateWalletViewModel::class)
    abstract fun bindCreateWalletVM(viewModel: CreateWalletViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SeedPhraseViewModel::class)
    abstract fun bindSeedPhraseViewModel(viewModel: SeedPhraseViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SeedPhraseImportViewModel::class)
    abstract fun bindSeedPhraseImportViewModel(viewModel: SeedPhraseImportViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ImportBalanceViewModel::class)
    abstract fun bindImportBalanceViewModel(viewModel: ImportBalanceViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BackgroundViewModel::class)
    abstract fun bindMainViewModel(viewModel: BackgroundViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SettingsViewModel::class)
    abstract fun bindSettingsViewModel(viewModel: SettingsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DelegateViewModel::class)
    abstract fun bindDelegateViewModel(viewModel: DelegateViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OperationDetailsViewModel::class)
    abstract fun bindOperationDetailsViewModel(viewModel: OperationDetailsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DelegateListViewModel::class)
    abstract fun bindDelegateListViewModel(viewModel: DelegateListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ReceiveViewModel::class)
    abstract fun bindReceiveViewModel(viewModel: ReceiveViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(GetUserAddressViewModel::class)
    abstract fun bindGetUserAddressViewModel(viewModel: GetUserAddressViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SendAmountViewModel::class)
    abstract fun bindSendAmountViewModel(viewModel: SendAmountViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SendConfirmationViewModel::class)
    abstract fun bindSendConfirmationViewModel(viewModel: SendConfirmationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TokenListViewModel::class)
    abstract fun bindTokenListViewModel(viewModel: TokenListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OperationListViewModel::class)
    abstract fun bindOperationListViewModel(viewModel: OperationListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ExchangeAmountViewModel::class)
    abstract fun bindExchangeAmountViewModel(viewModel: ExchangeAmountViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ChooseTokenViewModel::class)
    abstract fun bindChooseTokenViewModel(viewModel: ChooseTokenViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AdvancedExchangeViewModel::class)
    abstract fun bindSlippageViewModel(viewModel: AdvancedExchangeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SwapConfirmationViewModel::class)
    abstract fun bindSwapConfirmationViewModel(viewModel: SwapConfirmationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SideDataViewModel::class)
    abstract fun bindSideDataViewModel(viewModel: SideDataViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PinCreateViewModel::class)
    abstract fun bindPinCreateViewModel(viewModel: PinCreateViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BiometricSetViewModel::class)
    abstract fun bindBiometricSetViewModel(viewModel: BiometricSetViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AuthLoginViewModel::class)
    abstract fun bindAuthLoginViewModel(viewModel: AuthLoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ContactListViewModel::class)
    abstract fun bindContactListViewModel(viewModel: ContactListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AddContactViewModel::class)
    abstract fun bindAddContactViewModel(viewModel: AddContactViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ContactDetailViewModel::class)
    abstract fun bindContactDeleteViewModel(viewModel: ContactDetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MigrationWarningViewModel::class)
    abstract fun bindMigrationWarningViewModel(viewModel: MigrationWarningViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MigrateFundsViewModel::class)
    abstract fun bindTransferFundsViewModel(viewModel: MigrateFundsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BalanceCheckViewModel::class)
    abstract fun bindBalanceCheckViewModel(viewModel: BalanceCheckViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FinalMigrationViewModel::class)
    abstract fun bindFinalMigrationViewModel(viewModel: FinalMigrationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ShouldMigrateViewModel::class)
    abstract fun bindShouldMigrateViewModel(viewModel: ShouldMigrateViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DismissMigrationViewModel::class)
    abstract fun bindDismissMigrationViewModel(viewModel: DismissMigrationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LocalCurrencyViewModel::class)
    abstract fun bindLocalCurrencyViewModel(viewModel: LocalCurrencyViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BuyTezViewModel::class)
    abstract fun bindBuyTezViewModel(viewModel: BuyTezViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AttributionsViewModel::class)
    abstract fun bindAttributionsViewModel(viewModel: AttributionsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NetworkConfigViewModel::class)
    abstract fun bindDynamicUrlsViewModel(viewModel: NetworkConfigViewModel): ViewModel
}
