/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.model

/**
 * Because it's common to load data from the network while showing the disk copy of that data, it's good to create a
 * helper class that you can reuse in multiple places.
 * @see https://developer.android.com/jetpack/docs/guide#addendum
 */
sealed class DataWrapper<T>(
    val data: T? = null,
    val error: Throwable? = null
) {
    class Success<T>(data: T) : DataWrapper<T>(data)
    class Empty<T> : DataWrapper<T>()
    class Loading<T>(data: T? = null) : DataWrapper<T>(data)
    class Error<T>(error: Throwable?, data: T? = null) : DataWrapper<T>(data, error)

    override fun toString(): String {
        return "* ${this::class.java.simpleName} { data: ${data?.toString()},\nerror: $error }"
    }
}

fun <T> DataWrapper<T>.handleResults(
    onValidData: (T) -> Unit,
    onError: (Throwable?) -> Unit,
    onLoading: () -> Unit = {},
    onEmpty: () -> Unit = {}
) {
    when (this) {
        is DataWrapper.Success -> this.data?.let { onValidData(it) }
            ?: onError(IllegalArgumentException("Null data"))
        is DataWrapper.Error -> onError(this.error)
        is DataWrapper.Loading -> onLoading()
        is DataWrapper.Empty -> onEmpty()
    }
}


fun <T> DataWrapper<T>.handleResults(
    onValidData: (T) -> Unit,
    onErrorWithData: (Throwable?, T?) -> Unit,
    onLoading: () -> Unit = {},
    onEmpty: () -> Unit = {}
) {
    when (this) {
        is DataWrapper.Success -> this.data?.let { onValidData(it) }
            ?: onErrorWithData(IllegalArgumentException("Null data"), null)
        is DataWrapper.Error -> onErrorWithData(this.error, this.data)
        is DataWrapper.Loading -> onLoading()
        is DataWrapper.Empty -> onEmpty()
    }
}
