/**
 * # Released under MIT License
 *
 * Copyright (c) 2021 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.model.operation

import android.os.Parcel
import android.os.Parcelable
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.operation.Operation
import io.camlcase.kotlintezos.model.operation.OperationType
import io.camlcase.kotlintezos.model.operation.fees.ExtraFeeType
import io.camlcase.kotlintezos.model.operation.fees.OperationFees
import io.camlcase.smartwallet.data.core.extension.formatAsMoney
import io.camlcase.smartwallet.data.core.extension.tezos.convertToCurrency
import io.camlcase.smartwallet.data.core.extension.tezos.getTotalFees
import io.camlcase.smartwallet.data.core.extension.tezos.initNullableTez
import io.camlcase.smartwallet.data.model.exchange.CurrencyBundle

/**
 * Parcelable version of KotlinTezos' OperationFees
 */
data class OperationFeesBundle(
    val totalFees: Tez,
    val totalFeesInCurrency: String,
    val bakerFee: Tez,
    val bakerFeeInCurrency: String,
    val allocationFee: Tez?,
    val allocationFeeInCurrency: String?,
    val burnFee: Tez?,
    val burnFeeInCurrency: String?,
    val revealFee: Tez?,
    val revealFeeInCurrency: String?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        Tez(parcel.readString() ?: "0"),
        parcel.readString() ?: "",
        Tez(parcel.readString() ?: "0"),
        parcel.readString() ?: "",
        parcel.initNullableTez(),
        parcel.readString(),
        parcel.initNullableTez(),
        parcel.readString(),
        parcel.initNullableTez(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(totalFees.stringRepresentation)
        parcel.writeString(totalFeesInCurrency)
        parcel.writeString(bakerFee.stringRepresentation)
        parcel.writeString(bakerFeeInCurrency)
        parcel.writeString(allocationFee?.stringRepresentation)
        parcel.writeString(allocationFeeInCurrency)
        parcel.writeString(burnFee?.stringRepresentation)
        parcel.writeString(burnFeeInCurrency)
        parcel.writeString(revealFee?.stringRepresentation)
        parcel.writeString(revealFeeInCurrency)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<OperationFeesBundle> {
        override fun createFromParcel(parcel: Parcel): OperationFeesBundle {
            return OperationFeesBundle(parcel)
        }

        override fun newArray(size: Int): Array<OperationFeesBundle?> {
            return arrayOfNulls(size)
        }
    }
}

/**
 * Extracts all different types of fees for the user from the accumulated fees.
 *
 * @param operations List of operations where the accumulated was taken from
 * @param currencyPrice To calculate the fiat ($/€/etc) value of xtz
 * @param currencyBundle The user's selected currency to use with [currencyPrice]
 */
internal fun OperationFees.parseFees(
    operations: List<Operation>,
    currencyPrice: Double,
    currencyBundle: CurrencyBundle
): OperationFeesBundle {
    val allocationFee = this.extraFees?.getFee(ExtraFeeType.ALLOCATION_FEE)
    val burnFee = this.extraFees?.getFee(ExtraFeeType.BURN_FEE)
    val revealFee = operations.firstOrNull { it.type == OperationType.REVEAL }?.fees?.fee
    val bakerFee = if (revealFee != null) {
        this.fee - revealFee
    } else {
        this.fee
    }
    val total = this.getTotalFees()

    val feesAsUSD = total.convertToCurrency(currencyPrice)
    val allocationFeeAsUSD = allocationFee?.convertToCurrency(currencyPrice)
    val burnFeeAsUSD = burnFee?.convertToCurrency(currencyPrice)
    val bakerFeeAsUSD = bakerFee.convertToCurrency(currencyPrice)
    val revealFeeAsUSD = revealFee?.convertToCurrency(currencyPrice)
    return OperationFeesBundle(
        total,
        feesAsUSD.formatAsMoney(currencyBundle),
        bakerFee,
        bakerFeeAsUSD.formatAsMoney(currencyBundle),
        allocationFee,
        allocationFeeAsUSD?.formatAsMoney(currencyBundle),
        burnFee,
        burnFeeAsUSD?.formatAsMoney(currencyBundle),
        revealFee,
        revealFeeAsUSD?.formatAsMoney(currencyBundle)
    )
}
