/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.model.operation

import android.os.Parcel
import android.os.Parcelable
import io.camlcase.kotlintezos.model.OperationResultStatus
import io.camlcase.smartwallet.data.model.exchange.TokenInfoBundle
import io.camlcase.smartwallet.data.model.operation.TezosOperation

/**
 * Element of the operation list
 */
interface OperationForList : Parcelable

/**
 * Header for the operation list with grouped Day/Month/Year
 */
class DateHeader(
    val formattedDate: String
) : OperationForList {
    constructor(parcel: Parcel) : this(parcel.readString() ?: "")

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(formattedDate)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DateHeader> {
        override fun createFromParcel(parcel: Parcel): DateHeader {
            return DateHeader(parcel)
        }

        override fun newArray(size: Int): Array<DateHeader?> {
            return arrayOfNulls(size)
        }
    }
}

/**
 * Operation parsed for user viewing
 */
data class OperationForView(
    val type: TezosOperation.Type,
    val hash: String,
    val timestamp: Long,
    val formattedAmount: String?,
    val formattedAmountExtra: String?,
    val formattedFees: String?,
    val status: OperationResultStatus,
    val source: String, // Address or Name
    val destination: String, // Address or Name
    val detail: String?
) : OperationForList {
    constructor(parcel: Parcel) : this(
        TezosOperation.Type.valueOf(parcel.readString() ?: TezosOperation.Type.UNKNOWN.name),
        parcel.readString() ?: "",
        parcel.readLong(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        OperationResultStatus.valueOf(parcel.readString() ?: OperationResultStatus.FAILED.name),
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(type.name)
        parcel.writeString(hash)
        parcel.writeLong(timestamp)
        parcel.writeString(formattedAmount)
        parcel.writeString(formattedAmountExtra)
        parcel.writeString(formattedFees)
        parcel.writeString(status.name)
        parcel.writeString(source)
        parcel.writeString(destination)
        parcel.writeString(detail)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<OperationForView> {
        override fun createFromParcel(parcel: Parcel): OperationForView {
            return OperationForView(parcel)
        }

        override fun newArray(size: Int): Array<OperationForView?> {
            return arrayOfNulls(size)
        }
    }
}

/**
 * Once a token is selected, return all this so we can navigate to next screen
 *
 */
data class TokenFilteredOperations(
    val info: TokenInfoBundle,
    val operations: List<OperationForList>
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readParcelable(TokenInfoBundle::class.java.classLoader) ?: TokenInfoBundle.empty,
        (parcel.readArrayList(OperationForList::class.java.classLoader) ?: emptyList<OperationForList>()) as List<OperationForList>
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(info, flags)
        parcel.writeTypedList(operations)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TokenFilteredOperations> {

        override fun createFromParcel(parcel: Parcel): TokenFilteredOperations {
            return TokenFilteredOperations(parcel)
        }

        override fun newArray(size: Int): Array<TokenFilteredOperations?> {
            return arrayOfNulls(size)
        }
    }
}

data class OperationForViewBundle(
    val operation: TezosOperation,
    val opForView: OperationForView
)
