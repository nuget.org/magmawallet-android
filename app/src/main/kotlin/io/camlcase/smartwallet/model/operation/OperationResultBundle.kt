/**
 * # Released under MIT License
 *
 * Copyright (c) 2021 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.model.operation

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Parcel
import android.os.Parcelable
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.ui.base.BaseActivity
import io.camlcase.smartwallet.ui.delegate.IncludedOperation
import io.camlcase.smartwallet.ui.operation.WaitForInclusionActivity

/**
 * To pass to [WaitforInclusionActivity]
 */
data class OperationResultBundle(
    val hash: String,
    val type: IncludedOperation,
    val title: String,
    val actionText: String? = null,
    val secondaryActionText: String? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString() ?: "",
        IncludedOperation.valueOf(parcel.readString() ?: IncludedOperation.SEND.name),
        parcel.readString() ?: "",
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(hash)
        parcel.writeString(type.name)
        parcel.writeString(title)
        parcel.writeString(actionText)
        parcel.writeString(secondaryActionText)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<OperationResultBundle> {
        override fun createFromParcel(parcel: Parcel): OperationResultBundle {
            return OperationResultBundle(parcel)
        }

        override fun newArray(size: Int): Array<OperationResultBundle?> {
            return arrayOfNulls(size)
        }
    }
}

fun Context.getOperationResultBundle(
    type: IncludedOperation,
    operationHash: String,
    title: String
): OperationResultBundle {
    return when (type) {
        IncludedOperation.SEND -> OperationResultBundle(
            operationHash,
            IncludedOperation.SEND,
            title,
            getString(R.string.action_return_to_wallet)
        )
        IncludedOperation.EXCHANGE -> OperationResultBundle(
            operationHash,
            IncludedOperation.EXCHANGE,
            title,
            getString(R.string.exc_progress_action),
            getString(R.string.exc_progress_secondary_action)
        )
        IncludedOperation.DELEGATE -> OperationResultBundle(
            operationHash,
            IncludedOperation.DELEGATE,
            getString(R.string.del_wait_title),
            getString(R.string.action_return_to_settings)
        )
        IncludedOperation.UNDELEGATE -> OperationResultBundle(
            operationHash,
            IncludedOperation.UNDELEGATE,
            getString(R.string.del_undelegate_wait_title),
            getString(R.string.action_return_to_settings)
        )
        IncludedOperation.TRANSFER_FUNDS -> OperationResultBundle(
            operationHash,
            IncludedOperation.TRANSFER_FUNDS,
            title
        )
    }
}

fun BaseActivity.finishWithBundleResult(
    type: IncludedOperation,
    operationHash: String,
    title: String
) {
    val bundle = getOperationResultBundle(
        type,
        operationHash,
        title
    )
    val intent = Intent()
    intent.putExtra(WaitForInclusionActivity.ARG_OPERATION_BUNDLE, bundle)
    setResult(Activity.RESULT_OK, intent)
    finish()
}
