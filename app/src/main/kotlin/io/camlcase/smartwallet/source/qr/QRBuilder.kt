/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.source.qr

import com.google.zxing.BarcodeFormat
import com.google.zxing.WriterException
import com.journeyapps.barcodescanner.BarcodeEncoder
import io.camlcase.smartwallet.model.QR

/**
 * Wraps QR generation library
 */
class QRBuilder private constructor() {

    companion object {

        private var height: Int = 0
        private var width: Int = 0
        private var url: String = ""

        fun setQRHeight(height: Int) = apply { Companion.height = height }

        fun setQRWidth(width: Int) = apply { Companion.width = width }

        fun setQRUrl(url: String) = apply { Companion.url = url }

        @Throws(WriterException::class)
        fun build(): QR {
            val barcodeEncoder = BarcodeEncoder()
            val qrBitMap = barcodeEncoder.encodeBitmap(url, BarcodeFormat.QR_CODE, width, height)
            return QR(qrBitMap, url)
        }
    }
}
