package io.camlcase.smartwallet.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.injector
import io.camlcase.smartwallet.data.core.Logger
import io.camlcase.smartwallet.model.navigation.LaunchState
import io.camlcase.smartwallet.model.navigation.MainSource
import io.camlcase.smartwallet.ui.base.navigate.AppNavigator
import javax.inject.Inject

/**
 * Launcher Activity with Splash screen. Initial router
 */
class LaunchActivity : AppCompatActivity() {
    @Inject
    lateinit var viewModel: LaunchViewModel

    private var deeplinkUri: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        // Make sure this is before calling super.onCreate
        setTheme(R.style.AppTheme_Launcher)
        super.onCreate(savedInstanceState)
        injector().inject(this)

        val appLinkData = intent.data
        appLinkData?.toString()?.let {
            Logger.w("[LAUNCHER] DeeplinkUri: $it")
            deeplinkUri = it
        }
    }

    override fun onStart() {
        super.onStart()
        route()
        finish()
    }

    private fun route() {
        when (viewModel.getUserLaunchState(deeplinkUri)) {
            LaunchState.PENDING_ONBOARDING -> AppNavigator.navigateToOnboarding(this)
            LaunchState.PENDING_LOGIN -> AppNavigator.navigateToLogin(this)
            LaunchState.ACTIVE -> AppNavigator.navigateToMain(this, source = MainSource.LAUNCHER)
            LaunchState.UNLOGGED -> AppNavigator.navigateToWelcome(this)
            LaunchState.FIRST_TIME -> AppNavigator.navigateToWelcome(this)
            LaunchState.DEEPLINK -> AppNavigator.navigateToMain(this, deeplinkUri, source = MainSource.LAUNCHER)
        }
    }
}
