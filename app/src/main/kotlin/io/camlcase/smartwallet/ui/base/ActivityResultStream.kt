/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.base

import android.content.Intent
import android.os.Parcel
import android.os.Parcelable
import io.camlcase.smartwallet.core.extension.addToDisposables
import io.camlcase.smartwallet.data.core.Logger
import io.camlcase.smartwallet.ui.base.navigate.RequestCode
import io.camlcase.smartwallet.ui.base.navigate.ResultCode
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.processors.PublishProcessor

/**
 * Bridge between Activity's onActivityResult and any interested listener
 */
class ActivityResultStream {
    private val processor: PublishProcessor<ActivityResult> = PublishProcessor.create()
    var stream: Flowable<ActivityResult> = processor.onBackpressureLatest().share()

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        processor.onNext(ActivityResult(RequestCode.get(requestCode), ResultCode.get(resultCode), data))
    }
}

fun ActivityResultStream?.bind(
    code: RequestCode,
    resultCode: ResultCode = ResultCode.OK,
    disposable: CompositeDisposable,
    onResult: (ActivityResult) -> Unit,
    onError: (Throwable) -> Unit
) {
    this?.apply {
        this.stream
            .filter {
                Logger.i("*** ACTIVITY RESULT $it ?= $code")
                it.requestCode == code && it.resultCode == resultCode
            }
            .subscribe(
                { onResult(it) },
                { onError(it) }
            ).addToDisposables(disposable)
    }
}

fun ActivityResultStream?.bind(
    codes: List<RequestCode>,
    resultCode: ResultCode = ResultCode.OK,
    disposable: CompositeDisposable,
    onResult: (ActivityResult) -> Unit,
    onError: (Throwable) -> Unit
) {
    this?.apply {
        this.stream
            .filter {
                Logger.i("*** ACTIVITY RESULT $it ?= $codes")
                val firstOrNull = codes.firstOrNull { requestCode -> it.requestCode == requestCode }
                firstOrNull != null && it.resultCode == resultCode
            }
            .subscribe(
                { onResult(it) },
                { onError(it) }
            ).addToDisposables(disposable)
    }
}

/**
 * Listens to any resultCode
 */
fun ActivityResultStream?.bindAny(
    code: RequestCode,
    disposable: CompositeDisposable,
    onResult: (ActivityResult) -> Unit,
    onError: (Throwable) -> Unit
) {
    this?.apply {
        this.stream
            .filter {
                Logger.i("*** ACTIVITY RESULT $it ?= $code")
                it.requestCode == code
            }
            .subscribe(
                { onResult(it) },
                { onError(it) }
            ).addToDisposables(disposable)
    }
}

data class ActivityResult(
    val requestCode: RequestCode,
    val resultCode: ResultCode,
    val data: Intent?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        RequestCode.valueOf(parcel.readString() ?: ""),
        ResultCode.valueOf(parcel.readString() ?: ""),
        parcel.readParcelable(Intent::class.java.classLoader)
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(data, flags)
        parcel.writeString(requestCode.name)
        parcel.writeString(resultCode.name)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ActivityResult> {
        override fun createFromParcel(parcel: Parcel): ActivityResult {
            return ActivityResult(parcel)
        }

        override fun newArray(size: Int): Array<ActivityResult?> {
            return arrayOfNulls(size)
        }
    }
}
