package io.camlcase.smartwallet.ui.base

import android.content.ComponentCallbacks2
import android.content.Intent
import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import io.camlcase.smartwallet.MagmaApplication
import io.camlcase.smartwallet.core.extension.viewModel
import io.camlcase.smartwallet.data.usecase.analytics.SendAnalyticUseCase
import io.camlcase.smartwallet.di.ApplicationComponent
import io.camlcase.smartwallet.ui.base.navigate.background.AppBackgroundDelegate
import io.camlcase.smartwallet.ui.main.BackgroundViewModel
import io.camlcase.smartwallet.ui.permissions.ActivityPermissionsDelegate
import io.reactivex.rxjava3.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * Common functionality for all Activities
 *
 * @implements [ComponentCallbacks2]
 */
abstract class BaseActivity(@LayoutRes contentLayoutId: Int) : AppCompatActivity(contentLayoutId),
    ComponentCallbacks2 {
    val appComponent: ApplicationComponent by lazy(mode = LazyThreadSafetyMode.NONE) {
        (this.application as MagmaApplication).component
    }

    /**
     * Disposed on onDestroy
     */
    internal var disposables: CompositeDisposable = CompositeDisposable()

    /**
     * Disposed on onStop
     */
    internal var onStopDisposables: CompositeDisposable = CompositeDisposable()

    /**
     * Stream for onRequestPermissionsResult activity
     */
    internal var permissionResult = ActivityPermissionsDelegate()

    /**
     * Stream for background status listener
     */
    @Inject
    lateinit var appBackgroundDelegate: AppBackgroundDelegate

    /**
     * Logic to show [AuthLoginActivity]
     * @see [ActivityExt.listenToBackground]
     */
    lateinit var appBackgroundViewModel: BackgroundViewModel

    @Inject
    lateinit var activityResult: ActivityResultStream

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var analyticsUseCase: SendAnalyticUseCase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appComponent.inject(this)
        appBackgroundViewModel = viewModel(viewModelFactory) {}
    }

    override fun onResume() {
        super.onResume()
        /**
         * To act upon the app coming from background, remember to use [BaseActivity.listenToBackground] in the onCreate.
         */
        appBackgroundDelegate.checkBackground()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        activityResult.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        permissionResult.onPermissionsResponse(requestCode, permissions, grantResults)
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onStop() {
        onStopDisposables.dispose()
        super.onStop()
    }

    override fun onDestroy() {
        disposables.dispose()
        super.onDestroy()
    }

    fun overrideBackPress() {
        supportFragmentManager.fragments.firstOrNull { it is BackPressOverrider }?.let {
            if (!(it as BackPressOverrider).overrideBackPressed()) {
                super.onBackPressed()
            }
        } ?: super.onBackPressed()
    }
}
