/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.base

import android.content.Context
import android.view.ViewTreeObserver
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import io.camlcase.smartwallet.core.extension.clearKeyboardListener
import io.camlcase.smartwallet.data.usecase.analytics.SendAnalyticUseCase
import io.camlcase.smartwallet.ui.permissions.ActivityPermissionsDelegate
import io.reactivex.rxjava3.disposables.CompositeDisposable
import java.lang.ref.WeakReference
import javax.inject.Inject

/**
 * TODO This should be the BaseFragment once migration to view binding is finished
 */
abstract class BindBaseFragment(@LayoutRes contentLayoutId: Int) : Fragment(contentLayoutId) {
    var disposables: CompositeDisposable = CompositeDisposable()
    var keyboardListener: ViewTreeObserver.OnGlobalLayoutListener? = null

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    internal val activityResult: WeakReference<ActivityResultStream?> by lazy {
        WeakReference(baseActivity?.activityResult)
    }

    internal val permissionResult: WeakReference<ActivityPermissionsDelegate?> by lazy {
        WeakReference(baseActivity?.permissionResult)
    }

    internal val baseActivity: BaseActivity?
        get() = activity as? BaseActivity

    internal val analyticsUseCase: WeakReference<SendAnalyticUseCase?> by lazy {
        WeakReference(baseActivity?.analyticsUseCase)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        baseActivity?.let {
            // Avoid UninitializedPropertyAccessException for viewModelFactory and others when Activity is recreated
            it.appComponent.inject(it)
            it.appComponent.inject(this)
        }
    }

    override fun onDestroyView() {
        activity?.clearKeyboardListener(keyboardListener)
        disposables.dispose()
        super.onDestroyView()
    }
}

/**
 * TODO Delete once migration to view binding has finished.
 */
@Deprecated("Use BindBaseFragment and migrate to view binding")
abstract class BaseFragment() : Fragment() {
    var disposables: CompositeDisposable = CompositeDisposable()
    var keyboardListener: ViewTreeObserver.OnGlobalLayoutListener? = null

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    internal val activityResult: WeakReference<ActivityResultStream?> by lazy {
        WeakReference(baseActivity?.activityResult)
    }

    internal val permissionResult: WeakReference<ActivityPermissionsDelegate?> by lazy {
        WeakReference(baseActivity?.permissionResult)
    }

    internal val baseActivity: BaseActivity?
        get() = activity as? BaseActivity

    override fun onAttach(context: Context) {
        super.onAttach(context)
        baseActivity?.let {
            // Avoid UninitializedPropertyAccessException for viewModelFactory and others when Activity is recreated
            it.appComponent.inject(it)
            it.appComponent.inject(this)
        }
    }

    override fun onDestroyView() {
        activity?.clearKeyboardListener(keyboardListener)
        disposables.dispose()
        super.onDestroyView()
    }
}

internal interface BackPressOverrider {
    fun overrideBackPressed(): Boolean
}
