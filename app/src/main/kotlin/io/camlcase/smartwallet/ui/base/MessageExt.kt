/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.base

import android.app.Activity
import android.content.Context
import android.view.View
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import io.camlcase.kotlintezos.core.ext.isTypeOfError
import io.camlcase.kotlintezos.core.ext.listCauses
import io.camlcase.kotlintezos.model.RPCErrorCause
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.TezosErrorType
import io.camlcase.smartwallet.BuildConfig
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.data.core.api.ConnectionInterceptor
import io.camlcase.smartwallet.data.core.extension.tezos.parseMessage
import java.net.ConnectException

fun View?.showSnackbar(message: String, durationInMillis: Int = 5 * 1000) {
    this?.also {
        Snackbar.make(it, message, Snackbar.LENGTH_INDEFINITE)
            // Don't depend on colorOnSurface
            .setBackgroundTint(it.context.getColor(R.color.black))
            .setDuration(durationInMillis)
            .show()
    }
}

fun View?.showSnackbar(@StringRes stringId: Int, durationInMillis: Int = 5 * 1000) {
    this?.also {
        this.showSnackbar(this.context.getString(stringId), durationInMillis)
    }
}

fun View?.showErrorSnackbar(error: Throwable?) {
    this?.also { view ->
        val message = if (error != null) {
            ErrorMessageParser(view.context).parseMessage(error)
        } else {
            view.context.getString(R.string.error_generic)
        }

        view.showSnackbar(message)
    }
}

fun Throwable?.extractTezosError(): TezosError? {
    if (this is TezosError) {
        return this
    }
    if (this?.cause is TezosError) {
        return this.cause as TezosError
    }
    return null
}

fun Fragment?.showSnackbar(message: String) {
    this?.activity?.findViewById<View>(android.R.id.content).showSnackbar(message)
}

fun Activity?.showSnackbar(message: String) {
    this?.findViewById<View>(android.R.id.content).showSnackbar(message)
}

fun Fragment?.showError(error: Throwable?) {
    if (BuildConfig.DEBUG) {
        error?.printStackTrace()
    }
    this?.activity?.findViewById<View>(android.R.id.content).showErrorSnackbar(error)
}

fun Context?.parseTezosErrorMessage(error: TezosError): String? {
    if (error.exception is ConnectionInterceptor.NoConnectivityException
        || error.exception is ConnectionInterceptor.NoInternetException
        || error.exception is ConnectException
    ) {
        return this?.getString(R.string.error_no_connection)
    }

    if (error.type == TezosErrorType.COUNTER) {
        return this?.getString(R.string.error_transaction_counter)
    }

    if (!error.rpcErrors.isNullOrEmpty()) {
        if (error.rpcErrors?.listCauses().isTypeOfError(RPCErrorCause.INSUFFICIENT_XTZ_BALANCE)) {
            return this?.getString(R.string.error_transaction_insufficient_funds)
        }
        // RPC Errors are the priority as are the most self-explanatory
        val rpcError = error.rpcErrors?.firstOrNull { it.message != null } ?: error.rpcErrors?.get(0)

        return rpcError.parseMessage()
    }

    return error.exception?.message
}

fun Context?.findTezosError(throwable: Throwable?): String? {
    this?.also { context ->
        val tezosError = throwable.extractTezosError()
        if (tezosError != null) {
            return context.parseTezosErrorMessage(tezosError)
        }
    }
    return throwable?.message
}

/**
 * Show a Toast
 */
fun Context?.showToast(message: String, length: Int = Toast.LENGTH_LONG) {
    this?.apply {
        Toast.makeText(this, message, length).show()
    }
}
