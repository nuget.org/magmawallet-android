/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.base.info

import android.content.Context
import android.os.Bundle
import android.view.View
import by.kirich1409.viewbindingdelegate.viewBinding
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.reportView
import io.camlcase.smartwallet.data.core.extension.multiLet
import io.camlcase.smartwallet.databinding.FragmentInfoBinding
import io.camlcase.smartwallet.ui.base.BindBaseFragment

/**
 * Generic screen to show information, usually before the start of a flow.
 * Uses [InfoView]
 */
class InfoFragment : BindBaseFragment(R.layout.fragment_info) {
    private val binding by viewBinding(FragmentInfoBinding::bind)
    private var listener: InfoView.InfoEventsListener? = null
    private var infoType: InfoType = InfoType.DEFAULT
    private var extras: List<String>? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        manageIntent(arguments)
        listener = context as? InfoView.InfoEventsListener
        if (listener == null) {
            throw ClassCastException("$context must implement InfoListener")
        }
    }

    private fun manageIntent(bundle: Bundle?) {
        bundle?.getString(ARG_INFO_TYPE)?.let {
            infoType = InfoType.valueOf(it)
        } ?: throw IllegalStateException("Screen needs an ARG_INFO_TYPE argument")

        bundle.getStringArray(ARG_INFO_EXTRA)?.let {
            extras = it.toList()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listener?.apply {
            binding.containerInfo.init(infoType, this, extras)
            multiLet(infoType.analyticEvent, analyticsUseCase.get()) { event, useCase ->
                reportView(event)
            }
        }
    }

    override fun onDestroyView() {
        binding.containerInfo.unbind()
        super.onDestroyView()
    }

    companion object {
        private const val ARG_INFO_TYPE = "args.info.resources"
        private const val ARG_INFO_EXTRA = "args.info.extra"

        fun init(info: InfoType, vararg extras: String?): InfoFragment {
            val fragment = InfoFragment()
            val args = Bundle()
            args.putString(ARG_INFO_TYPE, info.name)
            args.putStringArray(ARG_INFO_EXTRA, extras)
            fragment.arguments = args
            return fragment
        }
    }
}

