/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.base.info

import android.content.Context
import android.graphics.Paint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.camlcase.smartwallet.core.extension.addToDisposables
import io.camlcase.smartwallet.core.extension.setupToolbar
import io.camlcase.smartwallet.core.extension.show
import io.camlcase.smartwallet.core.extension.throttledClick
import io.camlcase.smartwallet.databinding.FragmentInfoModalBinding
import io.camlcase.smartwallet.ui.base.BaseBottomSheetFragment

/**
 * Generic bottom sheet modal to show information.
 */
class InfoModalFragment : BaseBottomSheetFragment() {
    interface InfoModalListener {
        /**
         * Since an activity can show several info fragments, we pass the [infoType] to the listener
         * so it knows whom to react to
         */
        fun onActionClick(infoType: InfoModalType)
    }

    private val binding: FragmentInfoModalBinding
        get() = _binding as FragmentInfoModalBinding
    private var listener: InfoModalListener? = null
    private var infoType: InfoModalType = InfoModalType.DEFAULT
    private var extras: List<String>? = null

    override fun onCreateViewBind(inflater: LayoutInflater, container: ViewGroup?): FragmentInfoModalBinding {
        return FragmentInfoModalBinding.inflate(inflater, container, false)
    }

    private fun manageIntent(bundle: Bundle?) {
        bundle?.getString(ARG_INFO_TYPE)?.let {
            infoType = InfoModalType.valueOf(it)
        } ?: throw IllegalStateException("Screen needs an ARG_INFO_TYPE argument")

        bundle.getStringArray(ARG_INFO_EXTRA)?.let {
            extras = it.toList()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        manageIntent(arguments)
        listener = context as? InfoModalListener
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupToolbar(binding.toolbar)
        populate()
    }

    private fun populate() {
        infoType.title?.apply {
            binding.toolbar.setTitle(this)
        }

        infoType.detail?.apply {
            if (extras.isNullOrEmpty()) {
                binding.detail.text = getString(this)
            } else {
                binding.detail.text = getString(this, extras?.get(0))
            }
        }

        infoType.smallHightlightText?.apply {
            binding.detailHighlight.show()
            binding.detailHighlight.text = getString(this)
        }

        infoType.linkButtonText?.apply {
            binding.actionContinue.show()
            infoType.linkButtonIcon?.apply {
                binding.actionContinue.setIconResource(this)
            }
            binding.actionContinue.paintFlags = binding.actionContinue.paintFlags or Paint.UNDERLINE_TEXT_FLAG
            binding.actionContinue.text = getString(this)
            binding.actionContinue.throttledClick()
                .subscribe {
                    listener?.onActionClick(infoType)
                    dismiss()
                }
                .addToDisposables(disposables)
        }

        infoType.redButtonText?.apply {
            binding.actionOut.show()
            binding.actionOut.text = getString(this)
            binding.actionOut.throttledClick()
                .subscribe {
                    listener?.onActionClick(infoType)
                    dismiss()
                }
                .addToDisposables(disposables)
        }
    }

    companion object {
        private const val ARG_INFO_TYPE = "args.info.resources"
        private const val ARG_INFO_EXTRA = "args.info.extra"

        fun init(info: InfoModalType, vararg extras: String?): InfoModalFragment {
            val fragment = InfoModalFragment()
            val args = Bundle()
            args.putString(ARG_INFO_TYPE, info.name)
            args.putStringArray(ARG_INFO_EXTRA, extras)
            fragment.arguments = args
            return fragment
        }
    }
}
