/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.base.info

import androidx.annotation.DrawableRes
import androidx.annotation.RawRes
import androidx.annotation.StringRes
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.data.usecase.analytics.ScreenEvent

/**
 * Resources bundle to populate an [InfoFragment]
 */
enum class InfoType(
    @DrawableRes val imageId: Int?,
    @RawRes val animationId: Int? = null,
    @StringRes val title: Int?,
    @StringRes val detail: Int?,
    @StringRes val detailHighlight: Int? = null,
    @StringRes val actionText: Int? = R.string.action_continue,
    @StringRes val secondaryActionText: Int? = null,
    val type: InfoTypeKind = InfoTypeKind.SUCCESS,
    val analyticEvent: ScreenEvent? = null
) {
    DEFAULT(
        R.drawable.ic_success,
        title = R.string.onb_success_title,
        detail = null,
        actionText = null
    ),
    ONBOARDING_PAIRED_CREATE(
        R.drawable.emotional_warning,
        title = R.string.onb_already_paired_title,
        detail = R.string.onb_already_paired_detail,
        actionText = R.string.onb_already_paired_continue,
        secondaryActionText = R.string.onb_already_paired_back,
        type = InfoTypeKind.ERROR,
        analyticEvent = ScreenEvent.ONB_ALREAY_PAIRED
    ),
    ONBOARDING_PAIRED_IMPORT(
        R.drawable.emotional_warning,
        title = R.string.onb_already_paired_title,
        detail = R.string.onb_already_paired_detail,
        actionText = R.string.onb_already_paired_continue,
        secondaryActionText = R.string.onb_already_paired_back,
        type = InfoTypeKind.ERROR,
        analyticEvent = ScreenEvent.ONB_ALREAY_PAIRED
    ),
    ONBOARDING_SEED_PHRASE(
        R.drawable.emotional_seed_phrase,
        title = R.string.onb_recovery_phrase_info_title,
        detail = R.string.onb_recovery_phrase_info_detail,
        detailHighlight = R.string.onb_recovery_phrase_info_detail_highlight,
        actionText = R.string.set_recovery,
        analyticEvent = ScreenEvent.ONB_RECOVERY_PROMPT
    ),
    ONBOARDING_SET_PIN(
        R.drawable.emotional_pin,
        title = null,
        detail = R.string.onb_security_prompt_detail,
        analyticEvent = ScreenEvent.ONB_SECURITY_PROMPT
    ),
    ONBOARDING_SUCCESS(
        imageId = null,
        animationId = R.raw.anim_success,
        title = R.string.onb_final_success_title,
        detail = R.string.onb_final_success_detail,
        actionText = R.string.onb_final_success_action,
        analyticEvent = ScreenEvent.ONB_WALLET_SUCCESS
    ),
    LOGIN_IMPORT_WALLET_SUCCESS(
        imageId = null,
        animationId = R.raw.anim_success,
        title = R.string.onb_success_title,
        detail = R.string.onb_recovery_phrase_success_detail,
        analyticEvent = ScreenEvent.ONB_RECOVERY_SUCCESS
    ),
    SEND_INCLUSION_SUCCESS(
        imageId = null,
        animationId = R.raw.anim_success,
        title = R.string.snd_success_title,
        detail = R.string.snd_success_subtitle,
        actionText = R.string.action_return_to_wallet
    ),
    SEND_INCLUSION_ERROR(
        R.drawable.emotional_failure,
        title = R.string.error_transaction_generic_title,
        detail = R.string.error_transaction_generic_message,
        actionText = R.string.action_try_again,
        type = InfoTypeKind.ERROR
    ),
    SEND_GENERAL_ERROR(
        R.drawable.emotional_failure,
        title = R.string.error_transaction_generic_title,
        detail = R.string.error_transaction_generic_message,
        actionText = R.string.action_try_again,
        type = InfoTypeKind.ERROR
    ),
    SEND_ERROR_INVALID_ADDRESS(
        R.drawable.emotional_failure,
        title = R.string.error_transaction_invalid_address_title,
        detail = R.string.error_transaction_invalid_address,
        actionText = R.string.action_go_back,
        type = InfoTypeKind.ERROR
    ),
    SEND_ERROR_INSUFFICIENT_XTZ(
        R.drawable.emotional_failure,
        title = R.string.error_transaction_insufficient_funds_title,
        detail = R.string.error_transaction_insufficient_funds,
        actionText = R.string.action_return_to_wallet,
        type = InfoTypeKind.ERROR
    ),
    EXCHANGE_INVALID_PRICE_IMPACT(
        R.drawable.emotional_warning,
        title = R.string.exc_price_impact_error_title,
        detail = R.string.exc_price_impact_error_description,
        actionText = R.string.exc_price_impact_error_action,
        type = InfoTypeKind.ERROR
    ),
    EXCHANGE_ERROR_INSUFFICIENT_XTZ(
        R.drawable.emotional_failure,
        title = R.string.error_transaction_insufficient_funds_title,
        detail = R.string.error_transaction_insufficient_funds,
        actionText = R.string.exc_progress_action,
        type = InfoTypeKind.ERROR
    ),
    EXCHANGE_ERROR_ZERO_TOKENS(
        R.drawable.emotional_failure,
        title = R.string.error_transaction_exchange_higher_than_zero_title,
        detail = R.string.error_transaction_exchange_higher_than_zero,
        actionText = R.string.exc_progress_action,
        type = InfoTypeKind.ERROR
    ),
    EXCHANGE_ERROR_INVALID_TOKENS(
        R.drawable.emotional_failure,
        title = R.string.error_transaction_generic,
        detail = R.string.error_transaction_exchange_out_of_sync,
        actionText = R.string.exc_progress_action,
        type = InfoTypeKind.ERROR
    ),
    EXCHANGE_ERROR_INVALID_DEADLINE(
        R.drawable.emotional_failure,
        title = R.string.error_transaction_timeout_title,
        detail = R.string.error_transaction_timeout,
        actionText = R.string.exc_progress_action,
        type = InfoTypeKind.ERROR
    ),
    EXCHANGE_INCLUSION_SUCCESS(
        imageId = null,
        animationId = R.raw.anim_success,
        title = R.string.snd_success_title,
        detail = R.string.snd_success_subtitle,
        actionText = R.string.exc_progress_action,
        secondaryActionText = R.string.exc_progress_secondary_action
    ),
    EXCHANGE_INCLUSION_ERROR(
        R.drawable.emotional_failure,
        title = R.string.error_transaction_generic_title,
        detail = R.string.error_transaction_generic_message,
        actionText = R.string.exc_inclusion_error_action,
        type = InfoTypeKind.ERROR
    ),
    DELEGATE_ERROR_INVALID_ADDRESS(
        R.drawable.emotional_failure,
        title = R.string.error_transaction_invalid_baker_title,
        detail = R.string.error_transaction_invalid_baker,
        actionText = R.string.action_go_back,
        type = InfoTypeKind.ERROR
    ),
    DELEGATE_ERROR_BAKER_CANT_DELEGATE(
        R.drawable.emotional_failure,
        title = R.string.error_transaction_baker_cant_delegate_title,
        detail = R.string.error_transaction_baker_cant_delegate,
        actionText = R.string.action_go_back,
        type = InfoTypeKind.ERROR
    ),
    DELEGATE_ERROR_UNCHANGED_DELEGATED(
        R.drawable.emotional_failure,
        title = R.string.error_transaction_delegation_unchanged_title,
        detail = R.string.error_transaction_delegation_unchanged,
        actionText = R.string.action_go_back,
        type = InfoTypeKind.ERROR
    ),
    DELEGATE_INCLUSION_SUCCESS(
        imageId = null,
        animationId = R.raw.anim_success,
        title = R.string.del_success_title,
        detail = R.string.del_success_subtitle,
        actionText = R.string.del_success_finish
    ),
    UNDELEGATE_INCLUSION_SUCCESS(
        imageId = null,
        animationId = R.raw.anim_success,
        title = R.string.del_undelegate_success_title,
        detail = R.string.del_undelegate_success_subtitle,
        actionText = R.string.del_success_finish
    ),
    DELEGATE_INCLUSION_ERROR(
        R.drawable.emotional_failure,
        title = R.string.error_transaction_generic_title,
        detail = R.string.error_transaction_generic_message,
        actionText = R.string.action_try_again,
        secondaryActionText = R.string.action_return_to_settings,
        type = InfoTypeKind.ERROR
    ),
    OPERATION_COUNTER(
        R.drawable.emotional_failure,
        title = R.string.error_transaction_counter_title,
        detail = R.string.error_transaction_counter,
        actionText = R.string.action_go_back,
        type = InfoTypeKind.ERROR
    ),
    TZKT_UNAVAILABLE(
        R.drawable.emotional_warning,
        title = R.string.tzkt_failure_title,
        detail = R.string.tzkt_failure_message,
        actionText = R.string.action_return_to_wallet,
        type = InfoTypeKind.ERROR
    ),
    MIGRATION_SEED_PHRASE_INFO(
        imageId = R.drawable.emotional_seed_phrase,
        title = R.string.mig_seed_phrase_info_title,
        detail = R.string.mig_seed_phrase_info_message,
        detailHighlight = R.string.onb_recovery_phrase_info_detail_highlight,
        actionText = R.string.mig_seed_phrase_info_action
    ),
    MIGRATION_INSUFFICIENT_FUNDS_WARNING(
        imageId = R.drawable.emotional_warning,
        title = R.string.mig_balance_insufficient_title,
        detail = R.string.mig_balance_insufficient_message,
        actionText = R.string.mig_balance_insufficient_action,
        secondaryActionText = R.string.mig_balance_insufficient_dismiss,
        type = InfoTypeKind.ERROR
    ),
    MIGRATION_SUCCESS(
        imageId = null,
        animationId = R.raw.anim_success,
        title = R.string.mig_success_title,
        detail = null,
        actionText = R.string.mig_success_action
    ),
    MIGRATION_SUCCESS_BAKER(
        imageId = R.drawable.emotional_operation,
        title = R.string.mig_success_baker_title,
        detail = R.string.mig_success_baker_message,
        actionText = R.string.onb_final_success_action
    ),
    MIGRATION_INCLUSION_ERROR(
        R.drawable.emotional_warning,
        title = R.string.mig_funds_error_title,
        detail = R.string.mig_funds_error_message,
        actionText = R.string.mig_funds_error_action,
        secondaryActionText = R.string.mig_funds_error_dismiss,
        type = InfoTypeKind.ERROR
    ),
}

enum class InfoModalType(
    @StringRes val title: Int?,
    @StringRes val detail: Int?,
    /**
     * Underlined transparent button with a link
     */
    @StringRes val linkButtonText: Int? = null,
    /**
     * End drawable icon for the underlined transparent button
     */
    @DrawableRes val linkButtonIcon: Int? = null,
    /**
     * Big red button at the bottom
     */
    @StringRes val redButtonText: Int? = null,
    /**
     * Bolded highlight at the bottom
     */
    @StringRes val smallHightlightText: Int? = null,
) {
    DEFAULT(
        null,
        null
    ),
    EXCHANGE_MAX_BALANCE(
        title = R.string.exc_info_max_fee_title,
        detail = R.string.exc_info_max_fee_description,
        linkButtonText = R.string.exc_info_max_fee_action
    ),
    DEXTER_INFO(
        title = R.string.exc_dexter_info_title,
        detail = R.string.exc_dexter_info_description,
        linkButtonText = R.string.exc_dexter_info_link,
        linkButtonIcon = R.drawable.ic_external_link
    ),
    LIQUIDITY_FEE_INFO(
        title = R.string.liquidity_fees_modal_title,
        detail = R.string.liquidity_fees_modal_details
    ),
    WALLET_SETUP(
        title = R.string.onb_wallet_passphrase_info_title,
        detail = R.string.onb_wallet_passphrase_info_detail,
        redButtonText = R.string.onb_wallet_passphrase_info_action
    ),
    IMPORT_SEED_PHRASE(
        title = R.string.onb_import_seed_phrase_info_modal_title,
        detail = R.string.onb_import_seed_phrase_info_modal_detail_android,
        smallHightlightText = R.string.onb_import_seed_phrase_info_modal_highlight
    ),
    IMPORT_BALANCE_CHECK(
        title = R.string.onb_import_balance_info_modal_title,
        detail = R.string.onb_import_balance_info_modal_detail_android,
    ),
}

enum class InfoTypeKind {
    SUCCESS,
    ERROR
}
