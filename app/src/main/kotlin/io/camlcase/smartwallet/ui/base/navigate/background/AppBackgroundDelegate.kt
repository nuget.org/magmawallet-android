/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.base.navigate.background

import dagger.Reusable
import io.camlcase.smartwallet.data.core.Logger
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.processors.PublishProcessor
import javax.inject.Inject

/**
 * Maintain just an instance of it, so [background] is set to false only once
 */
@Reusable
class AppBackgroundDelegate @Inject constructor() {
    private val processor: PublishProcessor<BackgroundMode> = PublishProcessor.create()
    val stream: Flowable<BackgroundMode> = processor.onBackpressureLatest()

    /**
     * MAIN FLAG. If this is true, means the App has gone to background.
     * True by default as the app will always come first from background.
     */
    private var background: Boolean = true

    fun onBackground() {
        Logger.d("(App goes to Background)")
        background = true
    }

    /**
     * Call when entering the app
     */
    fun checkBackground() {
        if (background) {
            // We're in the foreground
            background = false
            Logger.d("(App comes from Background)")
            processor.onNext(BackgroundMode.COME_FROM_BACKGROUND)
        }
    }
}

enum class BackgroundMode {
    GO_TO_BACKGROUND,
    COME_FROM_BACKGROUND
}
