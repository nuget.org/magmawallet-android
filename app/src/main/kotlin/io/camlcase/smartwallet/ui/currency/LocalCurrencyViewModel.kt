/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.currency

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.camlcase.smartwallet.core.Currency
import io.camlcase.smartwallet.core.architecture.BaseViewModel
import io.camlcase.smartwallet.core.extension.addToDisposables
import io.camlcase.smartwallet.core.extension.subscribeForUI
import io.camlcase.smartwallet.data.core.extension.toLowerCaseUS
import io.camlcase.smartwallet.data.model.exchange.CoinGeckoRate
import io.camlcase.smartwallet.data.model.exchange.CurrencyBundle
import io.camlcase.smartwallet.data.model.exchange.CurrencyType
import io.camlcase.smartwallet.data.usecase.info.GetCurrencyRatesUseCase
import io.camlcase.smartwallet.data.usecase.info.GetXTZCurrencyExchange
import io.camlcase.smartwallet.model.DataWrapper
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class LocalCurrencyViewModel @Inject constructor(
    val useCase: GetCurrencyRatesUseCase,
    val changeUseCase: GetXTZCurrencyExchange
) : BaseViewModel() {
    /**
     * List of currencies
     */
    private val _data = MutableLiveData<DataWrapper<List<CurrencyListItem>>>()
    val data: LiveData<DataWrapper<List<CurrencyListItem>>> = _data

    /**
     * Change currency result
     */
    private val _operation = MutableLiveData<DataWrapper<Double>>()
    val operation: LiveData<DataWrapper<Double>> = _operation

    fun getAvailableCurrencies() {
        _data.value = DataWrapper.Loading()
        useCase.getAvailableCurrencies()
            .map { parseCurrencies(it) }
            .subscribeForUI()
            .subscribe(
                { _data.value = DataWrapper.Success(it) },
                { _data.value = DataWrapper.Error(it) }
            ).addToDisposables(disposables)
    }

    private fun parseCurrencies(availableCurrencies: Map<String, CoinGeckoRate>?): List<CurrencyListItem> {
        val all = ArrayList<CurrencyForView>()
        val popular = ArrayList<CurrencyForView>()
        availableCurrencies?.map {
            val bundle = CurrencyBundle(it.key.toUpperCase(Locale.US), CurrencyType.get(it.value.type), it.value.unit )
            val element = CurrencyForView(it.value.name, bundle)
            all.add(element)
            if (Currency.POPULAR_CURRENCIES.contains(it.key)) {
                popular.add(element)
            }
        }

        val desiredOrder = Currency.POPULAR_CURRENCIES.mapIndexed { i, v -> Pair(v, i) }.toMap()
        val sortedPopular = popular.sortedBy { desiredOrder[it.bundle.code.toLowerCaseUS()] }
        val sortedAll = all.sortedBy { it.bundle.code }
        val list = ArrayList<CurrencyListItem>()
        list.add(Header())
        list.addAll(sortedPopular)
        list.add(Header())
        list.addAll(sortedAll)
        return list
    }

    fun changeCurrency(bundle: CurrencyBundle) {
        _operation.value = DataWrapper.Loading()
        changeUseCase.updateCurrencyFor(bundle)
            .subscribeForUI()
            .subscribe(
                { _operation.value = DataWrapper.Success(it) },
                { _operation.value = DataWrapper.Error(it) }
            ).addToDisposables(disposables)
    }
}

interface CurrencyListItem

class Header : CurrencyListItem

data class CurrencyForView(
    val name: String,
    val bundle: CurrencyBundle
) : CurrencyListItem
