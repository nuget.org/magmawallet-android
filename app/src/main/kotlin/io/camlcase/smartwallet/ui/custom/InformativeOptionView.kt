/**
 * # Released under MIT License
 *
 * Copyright (c) 2021 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.custom

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.hide
import io.camlcase.smartwallet.core.extension.throttledClick
import io.camlcase.smartwallet.databinding.ViewInformativeOptionBinding
import io.reactivex.rxjava3.core.Flowable

/**
 * Shows a clickable option with information.
 */
class InformativeOptionView(context: Context, attrs: AttributeSet?) : ConstraintLayout(context, attrs) {
    private val binding = ViewInformativeOptionBinding.inflate(LayoutInflater.from(context), this, true)

    init {
        attrs?.apply {
            initWithAttrs(context, this)
        }
    }

    private fun initWithAttrs(context: Context, attrs: AttributeSet?) {
        context.theme.obtainStyledAttributes(attrs, R.styleable.InformativeOptionView, 0, 0).apply {
            val titleText: String
            val info: String?
            val warning: String?
            val boldIt: Boolean
            try {
                titleText = getString(R.styleable.InformativeOptionView_title) ?: ""
                info = getString(R.styleable.InformativeOptionView_info)
                warning = getString(R.styleable.InformativeOptionView_warning)
                boldIt = getBoolean(R.styleable.InformativeOptionView_boldIt, true)
            } finally {
                recycle()
            }

            binding.title.text = titleText
            if (info == null) {
                binding.message.hide()
            } else {
                binding.message.text = info
            }
            if (warning == null) {
                binding.extra.hide()
            } else {
                binding.extra.text = info
            }
            if (!boldIt) {
                // TODO
            }
        }
    }

    fun onClick(): Flowable<View> {
        return binding.root.throttledClick()
    }
}
