/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.custom

import android.view.View
import androidx.appcompat.widget.Toolbar
import com.google.android.material.appbar.AppBarLayout
import io.camlcase.smartwallet.core.extension.hide
import io.camlcase.smartwallet.core.extension.show
import kotlin.math.abs

class ScrollOffsetListener(
    private val expandedTitleView: View,
    private val toolbarView: Toolbar,
    private val toolbarTitle: String
) : AppBarLayout.OnOffsetChangedListener {

    var isCollapsed = false
        private set
    private var scrollRange = -1
    override fun onOffsetChanged(appBarLayout: AppBarLayout?, verticalOffset: Int) {
        if (scrollRange == -1) {
            scrollRange = appBarLayout?.totalScrollRange ?: 0
        }
        val scrollingDown = scrollRange + verticalOffset == 0
        if (scrollingDown && !isCollapsed) { // From Expanded to Collapsed
            expandedTitleView.hide()
            toolbarView.title = toolbarTitle
            isCollapsed = true
        } else if (!scrollingDown && isCollapsed) { // From Collapsed to Expanded
            expandedTitleView.show()
            toolbarView.title = null
            isCollapsed = false
        }
    }
}

/**
 * For complex AppBarLayout, use a more customisable class
 */
class AdvancedScrollOffsetListener(
    private val onStateChanged: (AppBarLayout?, State) -> Unit
) : AppBarLayout.OnOffsetChangedListener {

    enum class State {
        EXPANDED, COLLAPSED, IDLE
    }

    var isCollapsed = false
        private set
    private var scrollRange = -1
    private var state = State.IDLE

    override fun onOffsetChanged(appBarLayout: AppBarLayout?, verticalOffset: Int) {
        val appBarHalf = (appBarLayout?.totalScrollRange ?: 0) / 2
        when {
            verticalOffset == 0 -> {
                if (state != State.EXPANDED) {
                    onStateChanged(appBarLayout, State.EXPANDED)
                }
                state = State.EXPANDED
            }
            abs(verticalOffset) >= appBarHalf -> {
                if (state != State.COLLAPSED) {
                    onStateChanged(appBarLayout, State.COLLAPSED)
                }
                state = State.COLLAPSED
            }
            else -> {
                if (state != State.IDLE) {
                    onStateChanged(appBarLayout, State.IDLE)
                }
                state = State.IDLE
            }
        }
    }
}
