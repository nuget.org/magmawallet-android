/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.custom

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.hide
import io.camlcase.smartwallet.core.extension.throttledClick
import io.camlcase.smartwallet.databinding.ViewConfirmationSectionBinding
import io.reactivex.rxjava3.core.Flowable

/**
 * Confirmation screen section
 */
class SectionConfirmationView(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs) {
    internal val binding = ViewConfirmationSectionBinding.inflate(LayoutInflater.from(context), this, true)

    init {
        attrs?.apply {
            initWithAttrs(context, this)
        }
    }

    private fun initWithAttrs(context: Context, attrs: AttributeSet?) {
        context.theme.obtainStyledAttributes(attrs, R.styleable.SectionConfirmationView, 0, 0).apply {
            val sectionTitle: String
            val showSecondaryValue: Boolean
            try {
                sectionTitle = getString(R.styleable.SectionConfirmationView_sectionTitle) ?: ""
                showSecondaryValue = getBoolean(R.styleable.SectionConfirmationView_showSecondaryValue, true)
            } finally {
                recycle()
            }

            binding.sectionTitle.text = sectionTitle
            if (!showSecondaryValue) {
                binding.sectionSecondaryValue.hide()
            }
        }
    }

    /**
     * @return Rx subscription to the on click of the View
     */
    fun makeClickable(): Flowable<View> {
        isClickable = true
        isFocusable = true
        setBackgroundResource(R.drawable.element_selector_dark_ripple)

        ContextCompat.getDrawable(context, R.drawable.ic_arrow)?.apply {
            this.alpha = 150
            this.setBounds(
                0,
                0,
                resources.getDimension(R.dimen.spacing_small).toInt(), // right
                resources.getDimension(R.dimen.fee_detail_icon_bottom_bound).toInt() // bottom
            )
            binding.sectionTitle.setCompoundDrawables(
                this,
                null,
                null,
                null
            )
            binding.sectionTitle.compoundDrawablePadding = resources.getDimension(R.dimen.spacing_smallest_xx).toInt()
        }

        return throttledClick()
    }

    fun setValues(mainValue: String, secondaryValue: String? = null) {
        binding.sectionMainValue.text = mainValue
        secondaryValue?.apply {
            binding.sectionSecondaryValue.text = this
        }
    }
}
