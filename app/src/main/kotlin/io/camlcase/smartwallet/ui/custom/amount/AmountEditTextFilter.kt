/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.custom.amount

import android.text.InputFilter
import android.text.Spanned
import java.math.BigDecimal
import java.util.regex.Matcher
import java.util.regex.Pattern

/**
 * Ensures user doesn't set more than [digit]
 *
 * @param digitsBeforeZero Add +1 so it includes max + decimal symbol
 */
class DecimalInputFilter(
    digitsBeforeZero: Int = 15,
    digitsAfterZero: Int = 6
) : InputFilter {
    private val pattern: Pattern =
        Pattern.compile("[0-9]{0," + (digitsBeforeZero - 1) + "}+((\\.[0-9]{0," + (digitsAfterZero - 1) + "})?)||(\\.)?")

    override fun filter(
        source: CharSequence?,
        start: Int,
        end: Int,
        dest: Spanned?,
        dstart: Int,
        dend: Int
    ): CharSequence {
        val matcher: Matcher = pattern.matcher(dest)
        return if (!matcher.matches()) {
            ""
        } else {
            source ?: ""
        }
    }
}

class MaxValueInputFilter(private val max: BigDecimal) : InputFilter {
    override fun filter(
        source: CharSequence?,
        start: Int,
        end: Int,
        dest: Spanned?,
        dstart: Int,
        dend: Int
    ): CharSequence {
        return try {
            val value = BigDecimal(dest.toString())
            if (value >= max) {
                ""
            } else {
                source ?: ""
            }
        } catch (e: NumberFormatException) {
            source ?: ""
        }
    }
}

interface TokenAmountListener {
    fun onNewValue(amount: Double)
}
