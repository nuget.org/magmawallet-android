/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.delegate

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.URL
import io.camlcase.smartwallet.core.extension.*
import io.camlcase.smartwallet.data.model.delegate.TezosBakerResponse
import io.camlcase.smartwallet.data.model.error.AppError
import io.camlcase.smartwallet.data.model.error.OperationError
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.model.handleResults
import io.camlcase.smartwallet.ui.base.BaseFragment
import io.camlcase.smartwallet.ui.base.navigate.AppNavigator
import io.camlcase.smartwallet.ui.base.navigate.RequestCode
import io.camlcase.smartwallet.ui.base.navigate.navigateToModal
import io.camlcase.smartwallet.ui.base.showError
import io.camlcase.smartwallet.ui.custom.ScrollOffsetListener
import kotlinx.android.synthetic.main.fragment_delegate.*
import kotlinx.android.synthetic.main.view_collapsing_toolbar.*

/**
 * Main screen to access Baker information and delegate
 */
class DelegateFragment : BaseFragment(), MainToDelegate {
    interface DelegateListListener {
        fun scanQR()
        fun onDelegateSuccess(delegateAddress: String, operationHash: String)
        fun onUndelegateSuccess(operationHash: String)

        /**
         * Warn the user baker won't give rewards with the current balance
         */
        fun onMinimalAmountWarning(delegateAddress: String, minAmount: Tez, xtzBalance: Tez)

        /**
         * Full screen errors.
         */
        fun onOperationError(error: AppError)
    }

    private var listener: DelegateListListener? = null
    private lateinit var viewModel: DelegateViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_delegate, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as? DelegateListListener
        if (listener == null) {
            throw ClassCastException("$context must implement DelegateListListener")
        }
        viewModel = viewModel(viewModelFactory) {
            observe(data, { handleData(it) })
            observe(operation, { handleOperation(it) })
        }
        viewModel.getCurrentBaker()
    }

    private fun handleData(wrapper: DataWrapper<String>) {
        wrapper.handleResults(
            onValidData = {
                hideLoading()
                action_select_baker.text = it
            },
            onError = { hideLoading() },
            onLoading = { showLoading() },
            onEmpty = { hideLoading() }
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAppbar()
        scroll_view.requestFocus()
        delegate_fee_info.text = getString(R.string.del_info, viewModel.getDefaultDelegationFee())
    }

    private fun initAppbar() {
        setupToolbar(toolbar)

        expanded_title.text = getString(R.string.del_title)
        app_bar.addOnOffsetChangedListener(
            ScrollOffsetListener(
                expanded_title,
                toolbar,
                getString(R.string.del_title)
            )
        )
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        bindViews()
    }

    private fun handleOperation(wrapper: DataWrapper<Pair<String?, String>>) {
        wrapper.handleResults(
            onValidData = {
                hideLoading()
                val selectedBaker = it.first
                if (selectedBaker != null) {
                    listener?.onDelegateSuccess(selectedBaker, it.second)
                } else {
                    listener?.onUndelegateSuccess(it.second)
                }
            },
            onError = {
                hideLoading()
                showOperationError(it)
            },
            onLoading = {
                showLoading()
            }
        )
    }

    private fun showOperationError(error: Throwable?) {
        if (error is AppError && error.exception is OperationError) {
            listener?.onOperationError(error)
        } else {
            showError(error)
        }
    }

    fun showLoading() {
        delegate_loading.show()
        action_select_baker.isEnabled = false
        action_next.isEnabled = false
    }

    fun hideLoading() {
        delegate_loading.hide()
        action_select_baker.isEnabled = true
        action_next.isEnabled = viewModel.validDelegate()
    }

    private fun bindViews() {

        /**
         * As stated in: https://api.mytezosbaker.com/
         * "If you make use of our APIs, you should acknowledge that fact on your website or application with a
         * mention "Powered by MyTezosBaker" with a direct link to mytezosbaker.com."
         */
        action_sponsor.throttledClick()
            .subscribe { AppNavigator.navigateToUrl(activity, URL.BAKER_PROVIDER) }
            .addToDisposables(disposables)

        action_select_baker.throttledClick()
            .subscribe {
                parentFragmentManager.navigateToModal(
                    DelegateListFragment.init(viewModel.undelegate),
                    this,
                    RequestCode.EXCHANGE_CHOOSE_TOKEN
                )
            }
            .addToDisposables(disposables)

        action_next.throttledClick()
            .subscribe {
                viewModel.delegateWithAmountCheck { bakerAddress, minAmount, xtzBalance ->
                    listener?.onMinimalAmountWarning(bakerAddress, minAmount, xtzBalance)
                }
            }
            .addToDisposables(disposables)

        action_secondary.throttledClick()
            .subscribe {
                activity?.onBackPressed()
            }
            .addToDisposables(disposables)
    }

    override fun onUndelegation() {
        viewModel.selected = null
        viewModel.undelegate = true
        action_select_baker.setText(R.string.del_undelegate_label)
        action_next.setText(R.string.del_undelegate_action)
        delegate_fee_info.text = getString(R.string.del_undelegate_info, viewModel.getDefaultDelegationFee())
        action_next.isEnabled = viewModel.validUndelegate()
    }

    override fun onBakerSelected(baker: TezosBakerResponse) {
        action_select_baker.text = viewModel.parseSelection(baker)
        action_next.setText(R.string.del_confirm)
        delegate_fee_info.text = getString(R.string.del_info, viewModel.getDefaultDelegationFee())
        action_next.isEnabled = viewModel.validDelegate()
    }

    override fun forceDelegate(baker: Address) {
        viewModel.delegate(baker)
    }
}

/**
 * [DelegateLisFragment] -> [DelegateActivity] -> [DelegateFragment]
 */
interface MainToDelegate {
    fun onUndelegation()
    fun onBakerSelected(baker: TezosBakerResponse)
    fun forceDelegate(baker: Address)
}
