/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.delegate

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.smartwallet.core.architecture.BaseViewModel
import io.camlcase.smartwallet.core.extension.addToDisposables
import io.camlcase.smartwallet.core.extension.ellipsize
import io.camlcase.smartwallet.core.extension.subscribeForUI
import io.camlcase.smartwallet.data.core.Logger
import io.camlcase.smartwallet.data.core.extension.format
import io.camlcase.smartwallet.data.core.extension.tezos.parseError
import io.camlcase.smartwallet.data.core.extension.tezos.validTezosAddress
import io.camlcase.smartwallet.data.model.delegate.TezosBaker
import io.camlcase.smartwallet.data.model.delegate.TezosBakerResponse
import io.camlcase.smartwallet.data.source.local.UserPreferences
import io.camlcase.smartwallet.data.usecase.delegate.GetDelegateUseCase
import io.camlcase.smartwallet.data.usecase.operation.DelegateUseCase
import io.camlcase.smartwallet.model.DataWrapper
import javax.inject.Inject

class DelegateViewModel @Inject constructor(
    private val useCase: DelegateUseCase,
    private val infoUseCase: GetDelegateUseCase,
    private val userPreferences: UserPreferences
) : BaseViewModel() {
    /**
     * Information of current baker
     */
    private val _data = MutableLiveData<DataWrapper<String>>()
    val data: LiveData<DataWrapper<String>> = _data

    /**
     * Delegate operation
     * <Baker address (null for undelegation), Operation Hash>
     */
    private val _operation = MutableLiveData<DataWrapper<Pair<String?, String>>>()
    val operation: LiveData<DataWrapper<Pair<String?, String>>> = _operation

    var selected: TezosBakerResponse? = null
    var current: TezosBaker? = null
    var xtzBalance: Tez? = null
    var undelegate: Boolean = false

    fun getDefaultDelegationFee(): String {
        return useCase.getDefaultDelegationFee().fee.format(null)
    }

    fun delegateWithAmountCheck(onWarning: (Address, Tez, Tez) -> Unit) {
        selected.apply {
            when {
                this == null -> delegate(null)
                minimalAmountToDelegate(this) -> delegate(this.address)
                else -> onWarning(this.address, Tez(this.minDelegation), xtzBalance!!)
            }
        }
    }

    fun delegate(selected: Address?) {
        _operation.value = DataWrapper.Loading()

        if (selected == null) {
            useCase.undelegate()
        } else {
            useCase.delegate(selected)
        }
            .subscribeForUI()
            .subscribe(
                {
                    _operation.value = DataWrapper.Success(Pair(selected, it))
                    clearOperationSubscription()
                },
                {
                    _operation.value = DataWrapper.Error(it.parseError())
                    clearOperationSubscription()
                }
            ).addToDisposables(operationDisposables)
    }

    fun parseSelection(baker: TezosBakerResponse): String {
        selected = baker
        undelegate = false

        return if (baker.name.isBlank()) {
            baker.address.ellipsize()
        } else {
            baker.name
        }
    }

    fun getCurrentBaker() {
        _data.value = DataWrapper.Loading()
        xtzBalance = userPreferences.getTezBalance()

        infoUseCase.getDelegate()
            .map { current = it.getOrNull() }
            .subscribeForUI()
            .subscribe(
                {
                    if (current == null) {
                        _data.value = DataWrapper.Empty()
                    } else {
                        current?.let {
                            _data.value = DataWrapper.Success(it.name ?: it.address.ellipsize())
                        }
                    }
                },
                {
                    Logger.e("*** ERROR: $it")
                    _data.value = DataWrapper.Error(it)
                }
            ).addToDisposables(disposables)
    }

    fun validDelegate(): Boolean {
        val selectedAddress = selected?.address
        return selectedAddress != current?.address && selectedAddress?.validTezosAddress() == true && enoughBalanceToDelegate()
    }

    fun validUndelegate(): Boolean {
        return selected?.address != current?.address && enoughBalanceToDelegate()
    }

    private fun enoughBalanceToDelegate(): Boolean {
        val balance = xtzBalance ?: userPreferences.getTezBalance()
        xtzBalance = balance
        return balance > useCase.getDefaultDelegationFee().fee
    }

    private fun minimalAmountToDelegate(selected: TezosBakerResponse): Boolean {
        val balance = xtzBalance ?: userPreferences.getTezBalance()
        xtzBalance = balance
        val minimalAmount = Tez(selected.minDelegation)
        return balance >= minimalAmount
    }
}

