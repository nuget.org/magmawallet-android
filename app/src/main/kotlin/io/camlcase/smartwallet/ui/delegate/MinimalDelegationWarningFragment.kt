/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.delegate

import android.content.Context
import android.os.Bundle
import android.view.View
import by.kirich1409.viewbindingdelegate.viewBinding
import com.google.android.material.button.MaterialButton
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.addToDisposables
import io.camlcase.smartwallet.core.extension.setupToolbar
import io.camlcase.smartwallet.core.extension.show
import io.camlcase.smartwallet.core.extension.throttledClick
import io.camlcase.smartwallet.data.core.XTZ
import io.camlcase.smartwallet.data.core.extension.format
import io.camlcase.smartwallet.databinding.FragmentPriceImpactWarningBinding
import io.camlcase.smartwallet.ui.base.BindBaseFragment
import io.camlcase.smartwallet.ui.custom.ScrollOffsetListener

class MinimalDelegationWarningFragment : BindBaseFragment(R.layout.fragment_price_impact_warning) {
    interface MinimalDelegationListener {
        fun onMinAmounContinue(bakerAddress: Address)
    }

    private val binding by viewBinding(FragmentPriceImpactWarningBinding::bind)
    private lateinit var selectedBaker: Address
    private lateinit var minAmount: Tez
    private lateinit var xtzBalance: Tez

    private var listener: MinimalDelegationListener? = null

    private fun manageIntent(bundle: Bundle?) {
        bundle?.getString(ARG_SELECTED_BAKER)?.let {
            selectedBaker = it
        } ?: throw IllegalStateException("Screen needs an ARG_SELECTED_BAKER argument")

        bundle.getString(ARG_MIN_DELEGATE_AMOUNT)?.let {
            minAmount = Tez(it)
        } ?: throw IllegalStateException("Screen needs an ARG_MIN_DELEGATE_AMOUNT argument")

        bundle.getString(ARG_USER_XTZ_BALANCE)?.let {
            xtzBalance = Tez(it)
        } ?: throw IllegalStateException("Screen needs an ARG_USER_XTZ_BALANCE argument")
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        manageIntent(arguments)
        listener = context as? MinimalDelegationListener
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.priceImpactSection.binding.sectionTitle.setText(R.string.del_min_amount_baker_title)
        binding.extraSection.show()
        binding.extraSection.binding.sectionTitle.setText(R.string.del_min_amount_balance_title)
        binding.info.setText(R.string.del_min_amount_info)
        binding.actionNext.setText(R.string.del_min_amount_change_action)
        binding.actionNext.setIconResource(R.drawable.ic_arrow_back)
        binding.actionNext.iconGravity = MaterialButton.ICON_GRAVITY_TEXT_START
        binding.actionSecondary.setText(R.string.exc_price_impact_notice_action_ok)

        initAppbar()
        populateSections()
        bindViews()
    }

    private fun initAppbar() {
        setupToolbar(binding.appBarContainer.toolbar, true)

        val title = getString(R.string.del_min_amount_title)
        binding.appBarContainer.expandedTitle.text = title
        binding.appBarContainer.appBar.addOnOffsetChangedListener(
            ScrollOffsetListener(
                binding.appBarContainer.expandedTitle,
                binding.appBarContainer.toolbar,
                title
            )
        )
    }

    private fun bindViews() {
        binding.actionNext.throttledClick()
            .subscribe { activity?.onBackPressed() }
            .addToDisposables(disposables)

        binding.actionSecondary.throttledClick()
            .subscribe { listener?.onMinAmounContinue(selectedBaker) }
            .addToDisposables(disposables)
    }

    private fun populateSections() {
        binding.priceImpactSection.setValues(minAmount.format(XTZ.symbol))
        binding.extraSection.setValues(xtzBalance.format(XTZ.symbol))
    }

    companion object {
        private const val ARG_SELECTED_BAKER = "arg.delegate.baker"
        private const val ARG_MIN_DELEGATE_AMOUNT = "arg.delegate.min_amount"
        private const val ARG_USER_XTZ_BALANCE = "arg.delegate.xtz_balance"

        fun init(delegateAddress: String, minAmount: Tez, xtzBalance: Tez): MinimalDelegationWarningFragment {
            val fragment = MinimalDelegationWarningFragment()
            val args = Bundle()
            args.putString(ARG_SELECTED_BAKER, delegateAddress)
            args.putString(ARG_MIN_DELEGATE_AMOUNT, minAmount.stringRepresentation)
            args.putString(ARG_USER_XTZ_BALANCE, xtzBalance.stringRepresentation)
            fragment.arguments = args
            return fragment
        }
    }
}
