/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.delegate

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.camlcase.kotlintezos.model.OperationResultStatus
import io.camlcase.smartwallet.core.architecture.BaseViewModel
import io.camlcase.smartwallet.core.extension.addToDisposables
import io.camlcase.smartwallet.core.extension.retry
import io.camlcase.smartwallet.core.extension.subscribeForUI
import io.camlcase.smartwallet.data.analytic.report
import io.camlcase.smartwallet.data.model.error.OperationNotInjectedYet
import io.camlcase.smartwallet.data.model.operation.MagmaOperationResult
import io.camlcase.smartwallet.data.usecase.analytics.AnalyticEvent
import io.camlcase.smartwallet.data.usecase.analytics.SendAnalyticUseCase
import io.camlcase.smartwallet.data.usecase.conseil.GetOperationDetailsUseCase
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.ui.base.ErrorMessageParser
import io.reactivex.rxjava3.core.Flowable
import javax.inject.Inject

/**
 * Polls the conseil node for operation details.
 */
open class OperationDetailsViewModel @Inject constructor(
    private val useCase: GetOperationDetailsUseCase,
    private val analytics: SendAnalyticUseCase,
    private val errorMessageParser: ErrorMessageParser
) : BaseViewModel() {
    internal val _data = MutableLiveData<DataWrapper<MagmaOperationResult>>()
    val data: LiveData<DataWrapper<MagmaOperationResult>> = _data

    fun waitForOperation(operationHash: String, type: IncludedOperation) {
        _data.value = DataWrapper.Loading()

        retryForOperation(operationHash)
            .map {
                sendAnalytics(type, it)
                it
            }
            .subscribeForUI()
            .subscribe(
                { _data.value = DataWrapper.Success(it) },
                {
                    // If there's an error unrelated to the operation (e.g. Url is down), the stream will skip the map
                    // and come here. We should report it.
                    sendFailedEvent(type, it.message)
                    it.report()
                    _data.value = DataWrapper.Error(it)
                }
            ).addToDisposables(disposables)
    }

    private fun sendAnalytics(type: IncludedOperation, info: MagmaOperationResult) {
        if (info.status == OperationResultStatus.APPLIED) {
            sendSuccessEvent(type)
        } else {
            val cause = info.parsedError?.let { errorMessageParser.parseMagmeErrorMessage(it) }
            sendFailedEvent(type, cause)
        }
    }

    private fun sendSuccessEvent(type: IncludedOperation) {
        val event = when (type) {
            IncludedOperation.SEND -> AnalyticEvent.SEND_SUCCESS
            IncludedOperation.EXCHANGE -> AnalyticEvent.EXCHANGE_SUCCESS
            IncludedOperation.DELEGATE -> AnalyticEvent.BAKER_DELEGATED
            IncludedOperation.UNDELEGATE -> AnalyticEvent.BAKER_UNDELEGATED
            else -> null
        }
        event?.apply {
            analytics.reportEvent(this)
        }
    }

    private fun sendFailedEvent(type: IncludedOperation, cause: String?) {
        val event = when (type) {
            IncludedOperation.SEND -> AnalyticEvent.SEND_ERROR
            IncludedOperation.EXCHANGE -> AnalyticEvent.EXCHANGE_ERROR
            IncludedOperation.DELEGATE -> AnalyticEvent.BAKER_DELEGATED_ERROR
            IncludedOperation.UNDELEGATE -> AnalyticEvent.BAKER_UNDELEGATED_ERROR
            else -> null
        }
        event?.apply {
            if (cause != null) {
                analytics.reportEvent(this, cause)
            } else {
                analytics.reportEvent(this)
            }
        }
    }

    fun retryForOperation(operationHash: String): Flowable<MagmaOperationResult> {
        return useCase.getOperationDetail(operationHash).retry(
            predicate = { it.notYetIncluded() },
            maxRetry = 6, // ~120 seconds
            delayInMillis = 20 * 1000
        )
    }
}

/**
 * TzKt will return an empty list if the operation isn't visible yet
 */
fun Throwable?.notYetIncluded(): Boolean {
    return this?.let { it is OperationNotInjectedYet } ?: false
}

interface OperationProgressListener<T> {
    /**
     * Inclusion on the blockchain, wether APPLIED or not.
     */
    fun onInclusion(info: T)

    /**
     * Explorer gives an error
     */
    fun onInclusionError(error: Throwable?)
}

/**
 * We can react depending on the type after knowing the result of the inclusion
 * (E.g. Send analytics)
 * Use enum to enforce exhaustive clauses
 */
enum class IncludedOperation {
    SEND,
    EXCHANGE,
    DELEGATE,
    UNDELEGATE,
    TRANSFER_FUNDS
}
