/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.exchange.amount

import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import androidx.annotation.StringRes
import by.kirich1409.viewbindingdelegate.viewBinding
import com.google.android.material.bottomnavigation.BottomNavigationView
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.*
import io.camlcase.smartwallet.data.analytic.report
import io.camlcase.smartwallet.data.core.extension.formatAsMoney
import io.camlcase.smartwallet.data.model.Token
import io.camlcase.smartwallet.data.model.exchange.CurrencyBundle
import io.camlcase.smartwallet.data.model.swap.SwapRequest
import io.camlcase.smartwallet.databinding.FragmentExchangeAmountBinding
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.model.handleResults
import io.camlcase.smartwallet.model.navigation.ExchangeChooseTokenNavigation
import io.camlcase.smartwallet.model.token.TokenForView
import io.camlcase.smartwallet.ui.base.*
import io.camlcase.smartwallet.ui.base.info.InfoModalFragment
import io.camlcase.smartwallet.ui.base.info.InfoModalType
import io.camlcase.smartwallet.ui.base.navigate.AppNavigator
import io.camlcase.smartwallet.ui.base.navigate.RequestCode
import io.camlcase.smartwallet.ui.base.navigate.navigateToModal
import io.camlcase.smartwallet.ui.exchange.options.AdvancedExchangeOptionsFragment
import io.camlcase.smartwallet.ui.main.token.TokenListViewModel
import java.math.BigDecimal

/**
 * Main screen in the EXCHANGE flow
 *
 * Part of the bottom navigation pager. Connects to [MainActivity] through [MainToExchange]
 */
class ExchangeAmountFragment : BindBaseFragment(R.layout.fragment_exchange_amount), MainToExchange {

    private val binding by viewBinding(FragmentExchangeAmountBinding::bind)
    private lateinit var viewModel: ExchangeAmountViewModel
    private lateinit var balancesViewModel: TokenListViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = viewModel(viewModelFactory) {
            observe(data, { handleData(it) })
            observe(operation, { handleOperation(it) })
        }
        viewModel.init()
        setupToolbar(binding.toolbar)
        initPullToRefresh()
        bindKeyboardViews()
        initialViewValues(viewModel.getCurrencyBundle())
        toggleViews(false, 0.5f)
        bindViews()
        bindScreenResults()

        // Share view model with other screens
        baseActivity?.apply {
            balancesViewModel = this.viewModel(viewModelFactory) {
                observe(data, { onBalanceUpdated(it) })
            }
        }
    }

    private fun initialViewValues(bundle: CurrencyBundle) {
        binding.amounts.fromBalance.text = getString(R.string.exc_balance_from, "0")
        binding.amounts.toAmount.amountInput.isEnabled = false
        resetCurrencyValues(bundle)
    }

    private fun resetCurrencyValues(bundle: CurrencyBundle) {
        binding.amounts.fromAmount.amountExchange.text = 0.0.formatAsMoney(bundle)
        binding.amounts.toAmount.amountExchange.text = 0.0.formatAsMoney(bundle)
    }

    private fun bindScreenResults() {
        activityResult.get()?.bind(
            RequestCode.EXCHANGE,
            disposable = disposables,
            onResult = { _ ->
                resetScreen()
            },
            onError = { it.report() }
        )

        activityResult.get()?.bind(
            RequestCode.LOCAL_CURRENCIES,
            disposable = disposables,
            onResult = { _ ->
                resetScreen()
                resetCurrencyValues(viewModel.getCurrencyBundle())
            },
            onError = { it.report() }
        )
    }

    private fun onBalanceUpdated(wrapper: DataWrapper<List<TokenForView>>) {
        if (view != null) {
            wrapper.handleResults(
                onValidData = {
                    viewModel.getTokenBalances()
                },
                onEmpty = {
                    hideLoading()
                    toggleViews(false, 0.5f)
                },
                onError = {
                    hideLoading()
                    showError(it)
                },
                onLoading = {
                    showLoading()
                },
            )
        }
    }

    private fun handleData(wrapper: DataWrapper<ExchangeForView>) {
        wrapper.handleResults(
            onValidData = {
                hideLoading()
                if (viewModel.exchangeEnabled()) {
                    toggleViews(true, 1.0f)

                    binding.amounts.fromBalance.text = getString(R.string.exc_balance_from, it.fromBalance)
                    binding.amounts.fromToken.text = it.fromToken.symbol
                    binding.amounts.fromToken.tag = it.fromToken
                    binding.amounts.fromAmount.actionEnterMax.show()
                    binding.amounts.fromAmount.amountInput.changeInputType(it.fromToken.decimals > 0)
                    binding.amounts.fromAmount.amountInput.setFilters(it.maxFrom, it.fromToken.decimals)

                    binding.amounts.toToken.text = it.toToken.symbol
                    binding.amounts.toToken.tag = it.toToken
                    binding.amounts.toAmount.amountInput.changeInputType(it.toToken.decimals > 0)

                    binding.amounts.exchangeRate.text = it.conversionRate
                } else {
                    toggleViews(false, 0.5f)
                }
            },
            onError = {
                hideLoading()
                showError(it)
            },
            onLoading = {
                showLoading()
            },
            onEmpty = {
                hideLoading()
                toggleViews(false, 0.5f)
            }
        )
    }

    private fun bindViews() {
        binding.amounts.exchangeRate.throttledClick()
            .subscribe {
                viewModel.switch()
                clearAmounts()
            }
            .addToDisposables(disposables)

        binding.actionExtra.throttledClick()
            .subscribe {
                navigateToModal(
                    AdvancedExchangeOptionsFragment.init(),
                    RequestCode.EXCHANGE_SLIPPAGE
                )
            }
            .addToDisposables(disposables)


        binding.actionNext.throttledClick()
            .subscribe(
                { viewModel.getRequest(binding.amounts.fromAmount.amountInput.getAmount()) },
                {
                    it.printStackTrace()
                    it.report()
                    showError(it)
                }
            )
            .addToDisposables(disposables)

        binding.amounts.fromAmount.amountInput.listenToChanges {
            // On recreation, the input might trigger this listener with no data having been fetched yet.
            if (viewModel.exchangeEnabled()) {
                onAmountChanged(it)
            }
        }

        binding.amounts.fromAmount.actionEnterMax.throttledClick()
            .subscribe {
                binding.amounts.fromAmount.amountInput.setText(viewModel.getBalanceFrom())
            }
            .addToDisposables(disposables)

        binding.amounts.fromToken.throttledClick()
            .subscribe {
                parentFragmentManager.navigateToModal(
                    ChooseExchangeTokenFragment.init(
                        viewModel.fromToken.token,
                        ExchangeChooseTokenNavigation.SOURCE_FROM
                    ),
                    this,
                    RequestCode.EXCHANGE_CHOOSE_TOKEN
                )
            }
            .addToDisposables(disposables)

        binding.amounts.toToken.throttledClick()
            .subscribe {
                parentFragmentManager.navigateToModal(
                    ChooseExchangeTokenFragment.init(
                        viewModel.toToken.token,
                        ExchangeChooseTokenNavigation.SOURCE_TO
                    ),
                    this,
                    RequestCode.EXCHANGE_CHOOSE_TOKEN
                )
            }
            .addToDisposables(disposables)

        binding.amounts.actionSponsor.throttledClick()
            .subscribe {
                val fragment = InfoModalFragment.init(InfoModalType.DEXTER_INFO)
                navigateToModal(fragment)
            }
            .addToDisposables(disposables)
    }

    private fun clearAmounts() {
        binding.amounts.fromAmount.amountInput.text?.clear()
        binding.amounts.toAmount.amountInput.text?.clear()
    }

    /**
     * FROM amount changed
     */
    private fun onAmountChanged(amount: BigDecimal) {
        viewModel.onAmountFromChanged(amount) { validation ->

            // FROM
            if (validation.fromValidation.valid) {
                // dismiss FROM error
                binding.amounts.fromAmountError.hide()
            } else {
                // show FROM error
                when (validation.fromValidation) {
                    is ExchangeAmountValidation.ZeroValidation -> {
                        // No need to show error
                        binding.amounts.fromAmountError.hide()
                    }
                    is ExchangeAmountValidation.BaseValidation -> {
                        binding.amounts.fromAmountError.setText(R.string.error_insufficient_funds)
                        binding.amounts.fromAmountError.show()
                    }
                    else -> {
                        binding.amounts.fromAmountError.setText(R.string.error_invalid_amount)
                        binding.amounts.fromAmountError.show()
                    }
                }
            }

            binding.amounts.fromAmount.amountExchange.text = validation.amountFromInCurrency
            binding.amounts.exchangeRate.text = validation.conversionRate
            // TO
            binding.amounts.toAmount.amountInput.setText(validation.amountTo)
            binding.amounts.toAmount.amountExchange.text = validation.amountToInCurrency

            fun showAmountToError(@StringRes stringId: Int) {
                binding.amounts.toAmountError.setText(stringId)
                binding.amounts.toAmountError.show()
            }

            val validationTo = validation.toValidation
            if (validationTo.valid) {
                // dismiss TO error
                binding.amounts.toAmountError.hide()
            } else {
                // show TO error
                when (validationTo) {
                    is ExchangeAmountValidation.ZeroValidation -> {
                        // No need to show error
                        binding.amounts.toAmountError.hide()
                    }
                    is ExchangeAmountValidation.TokenPoolBalance -> {
                        showAmountToError(R.string.error_not_enough_fa)
                    }
                    is ExchangeAmountValidation.TokenTezBalance -> {
                        showAmountToError(R.string.error_not_enough_tez)
                    }
                    else -> {
                        showAmountToError(R.string.error_invalid_amount)
                    }
                }
            }

            binding.actionNext.isEnabled = validation.enableExchange
        }
    }

    private fun initPullToRefresh() {
        binding.swipeRefresh?.setOnRefreshListener {
            refresh()
        }
    }

    private fun refresh(clear: Boolean = false) {
        viewModel.refreshTokenBalances(clear)
    }

    /**
     * Hide/Show bottom navigation on soft keyboard
     * @see initKeyboardListener in [ContextExt] For generic use
     */
    private fun bindKeyboardViews() {
        val bottomNavigationView = activity?.findViewById<BottomNavigationView>(R.id.bottom_navigation)
        val buttonParams = binding.actionNext.layoutParams as LinearLayout.LayoutParams
        val buttonMargin = resources.getDimension(R.dimen.spacing_medium_x).toInt()
        keyboardListener = activity?.listenToKeyboard { visible ->
            if (visible) {
                bottomNavigationView.hide()
                buttonParams.bottomMargin = 0
            } else {
                bottomNavigationView.show()
                buttonParams.bottomMargin = buttonMargin
            }
            binding.actionNext.layoutParams = buttonParams
        }
    }

    private fun showLoading() {
        binding.swipeRefresh.isRefreshing = true
        binding.actionExtra.isEnabled = false
    }

    private fun hideLoading() {
        binding.swipeRefresh.isRefreshing = false
        binding.actionExtra.isEnabled = true
    }

    override fun setTokenFrom(token: Token) {
        viewModel.setTokenFrom(token)
        clearAmounts()
    }

    /**
     * Modal -> Activity -> Fragment
     */
    override fun onTokenSelected(token: Token, source: Int) {
        when (source) {
            ExchangeChooseTokenNavigation.SOURCE_FROM -> {
                viewModel.setTokenFrom(token)
            }
            ExchangeChooseTokenNavigation.SOURCE_TO -> {
                viewModel.setTokenTo(token)
            }
        }
        // We can clear, we already controlled the user doesn't click the same token on the modal
        clearAmounts()

        if (!viewModel.oneIsXTZ()) {
            binding.actionNext.isEnabled = false
            view.showSnackbar(R.string.error_token_to_token)
        }
    }

    override fun onOptions(slippage: Double, minutesTimeout: Int) {
        viewModel.selectedSlippage = slippage
        viewModel.timeoutMinutes = minutesTimeout
    }

    /**
     * Updating the tokens balance will be done in BalancesFragment
     */
    private fun resetScreen() {
        clearAmounts()
    }

    private fun handleOperation(wrapper: DataWrapper<SwapRequest>) {
        wrapper.handleResults(
            onValidData = {
                hideLoading()
                AppNavigator.navigateToExchange(activity, it)
            },
            onLoading = { showLoading() },
            onError = {
                hideLoading()
                showError(it)
            }
        )
    }

    private fun toggleViews(enabled: Boolean, alpha: Float) {
        binding.amounts.fromAmount.root.isEnabled = enabled
        binding.amounts.fromAmount.amountInput.isEnabled = enabled
        binding.actionExtra.isEnabled = enabled
        binding.amounts.fromToken.isEnabled = enabled
        binding.amounts.toToken.isEnabled = enabled
        binding.amounts.exchangeRate.isEnabled = enabled

        binding.actionExtra.alpha = alpha
        binding.amounts.fromAmount.root.alpha = alpha
        binding.amounts.fromBalance.alpha = alpha
        binding.amounts.fromTitle.alpha = alpha
        binding.amounts.fromToken.alpha = alpha
        binding.amounts.toToken.alpha = alpha
        binding.amounts.toAmount.root.alpha = alpha
        binding.amounts.toTitle.alpha = alpha
        binding.amounts.exchangeRate.alpha = alpha
        binding.amounts.divider.alpha = alpha
    }
}

/**
 * @see BalancesFragment
 */
interface MainToBalance {
    /**
     * When returning from MoonPay buy, if successful, fetch balances
     */
    fun refresh()
}

/**
 * For parent activity to communicate with this fragment
 */
interface MainToExchange {
    /**
     * Send -> Activity -> Exchange fragment
     */
    fun setTokenFrom(token: Token)

    /**
     * Modal -> Activity -> Exchange fragment
     */
    fun onTokenSelected(token: Token, @ExchangeChooseTokenNavigation.ScreenSource source: Int)

    /**
     * Modal -> Activity -> Exchange fragment
     */
    fun onOptions(slippage: Double, minutesTimeout: Int)
}

/**
 * @see ContactListFragment
 */
interface MainToContacts {
    fun refresh()
}

/**
 * @see OperationListFragment
 */
interface MainToActivity {
    /**
     * When we add/edit/delete a contact, the ones listed in the list need to be updated
     */
    fun refreshContacts()
}

/**
 * @see SettingsFragment
 */
interface MainToSettings {
    /**
     * When user dismisses migration warning permanently, we need to refresh the list
     */
    fun reloadList()
}
