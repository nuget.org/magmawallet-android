/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.exchange.amount

import io.camlcase.kotlintezos.model.Tez
import java.math.BigInteger

enum class AmountValidation {
    OVER_ZERO, // > 0.0
    BALANCE, // Checks amount <= balance
    TOKEN_BALANCE, // Checks tez <= tez dexter balance
    TOKEN_POOL, // Checks amount <= token_pool
    NONE
}

sealed class ExchangeAmountValidation(open val valid: Boolean) {
    data class BaseValidation(override val valid: Boolean) : ExchangeAmountValidation(valid)
    data class ZeroValidation(override val valid: Boolean) : ExchangeAmountValidation(valid)
    data class TokenPoolBalance(override val valid: Boolean, val tokenPool: BigInteger) : ExchangeAmountValidation(valid)
    data class TokenTezBalance(override val valid: Boolean, val tezPool: Tez) : ExchangeAmountValidation(valid)
}
