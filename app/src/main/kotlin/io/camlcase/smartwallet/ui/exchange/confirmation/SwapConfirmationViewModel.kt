/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.exchange.confirmation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.operation.Operation
import io.camlcase.kotlintezos.model.operation.SmartContractCallOperation
import io.camlcase.smartwallet.core.architecture.BaseViewModel
import io.camlcase.smartwallet.core.extension.addToDisposables
import io.camlcase.smartwallet.core.extension.formatExchangeRate
import io.camlcase.smartwallet.core.extension.formatPercentage
import io.camlcase.smartwallet.core.extension.subscribeForUI
import io.camlcase.smartwallet.data.core.Wallet
import io.camlcase.smartwallet.data.core.XTZ
import io.camlcase.smartwallet.data.core.extension.*
import io.camlcase.smartwallet.data.core.extension.tezos.convertToCurrency
import io.camlcase.smartwallet.data.core.extension.tezos.parseError
import io.camlcase.smartwallet.data.model.Token
import io.camlcase.smartwallet.data.model.amount.Balance
import io.camlcase.smartwallet.data.model.amount.CoinBalance
import io.camlcase.smartwallet.data.model.amount.EmptyBalance
import io.camlcase.smartwallet.data.model.error.AppError
import io.camlcase.smartwallet.data.model.error.ErrorType
import io.camlcase.smartwallet.data.model.exchange.TokenInfoBundle
import io.camlcase.smartwallet.data.model.isXTZ
import io.camlcase.smartwallet.data.model.swap.SwapBundleRequest
import io.camlcase.smartwallet.data.model.swap.SwapRequest
import io.camlcase.smartwallet.data.usecase.info.GetBCDBalanceUseCase
import io.camlcase.smartwallet.data.usecase.operation.DexterCalculationsUseCase
import io.camlcase.smartwallet.data.usecase.operation.ExchangeTokensUseCase
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.model.operation.OperationFeesBundle
import io.camlcase.smartwallet.model.operation.parseFees
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class SwapConfirmationViewModel @Inject constructor(
    private val useCase: ExchangeTokensUseCase,
    private val tokensUseCase: GetBCDBalanceUseCase,
    private val calculationsUseCase: DexterCalculationsUseCase
) : BaseViewModel() {
    private val _data = MutableLiveData<DataWrapper<SwapConfirmationForView>>()
    val data: LiveData<DataWrapper<SwapConfirmationForView>> = _data

    private val _operation = MutableLiveData<DataWrapper<String>>()
    val operation: LiveData<DataWrapper<String>> = _operation

    private var bundle: SwapBundleRequest = SwapBundleRequest.empty
    private var swapOperations: List<Operation> = emptyList()
    private var currencyBundle = Wallet.defaultCurrency

    fun getConfirmationDetails(request: SwapRequest) {
        _data.value = DataWrapper.Loading()
        currencyBundle = tokensUseCase.getCurrencyBundle()

        getExchangeData(request.fromToken, request.toToken)
            .map {
                bundle = SwapBundleRequest(
                    it.first,
                    it.second,
                    request.amountFrom,
                    request.selectedSlippage,
                    request.timeout
                )
                bundle
            }
            .flatMap {
                useCase.createSwapOperation(it)
            }
            .flatMap {
                bundle.operationFee = it.accumulatedFee
                swapOperations = it.operations

                parseResults(bundle, request.priceImpactPercentage)
            }
            .subscribeForUI()
            .subscribe(
                {
                    _data.value = DataWrapper.Success(it)
                    clearOperationSubscription()
                },
                {
                    if (bundle.fromAmount !is EmptyBalance) {
                        //val bundleForView = parseResults(bundle, request.slippage)
                        _data.value = DataWrapper.Error(parseError(it), null)
                    } else {
                        _data.value = DataWrapper.Error(parseError(it))
                    }
                    clearOperationSubscription()
                }
            ).addToDisposables(operationDisposables)
    }

    private fun getExchangeData(
        fromToken: Token,
        toToken: Token
    ): Single<Pair<TokenInfoBundle, TokenInfoBundle>> {
        return tokensUseCase.getBalances()
            .map { list ->
                val balanceFromToken = list.firstOrNull { it.getOrNull()?.token == fromToken }?.getOrNull()
                val balanceToToken = list.firstOrNull { it.getOrNull()?.token == toToken }?.getOrNull()

                multiLet(balanceFromToken, balanceToToken) { resultFrom, resultTo ->
                    Pair(resultFrom, resultTo)
                } ?: throw AppError(
                    ErrorType.EMPTY_DATA,
                    exception = list.firstOrNull { it.exceptionOrNull() != null }?.exceptionOrNull()
                )
            }
    }

    private fun parseResults(bundle: SwapBundleRequest, priceImpact: Double): Single<SwapConfirmationForView> {
        val fromTokenSymbol = bundle.fromToken.token.symbol
        val toTokenSymbol = bundle.toToken.token.symbol
        val fromTokenCurrencyExchange = bundle.fromToken.exchange.currencyExchange
        val toTokenCurrencyExchange = bundle.toToken.exchange.currencyExchange

        val fromAmount: Balance<*> = swapOperations.firstOrNull { it is SmartContractCallOperation }?.let {
            val operation = (it as SmartContractCallOperation)
            if (bundle.fromToken.token.isXTZ() && bundle.fromAmount.bigDecimal != operation.amount.signedDecimalRepresentation) {
                CoinBalance(operation.amount)
            } else {
                bundle.fromAmount
            }
        } ?: bundle.fromAmount

        return calculationsUseCase.calculate(bundle.fromToken, bundle.toToken, fromAmount).map { calculations ->
            val formattedRate = calculations.conversionRate.formatExchangeRate(fromTokenSymbol, toTokenSymbol)

            val formattedLiquidityFee: String
            val liquidityFeeInCurrency: String
            val xtzExchange: Double
            if (bundle.fromToken.token.isXTZ()) {
                // XTZ to token: 0.3% fee paid in XTZ
                formattedLiquidityFee =
                    calculations.liquidityFee.formatWithDecimals(bundle.fromToken.token) + " $fromTokenSymbol"
                liquidityFeeInCurrency =
                    Tez(calculations.liquidityFee).convertToCurrency(fromTokenCurrencyExchange)
                        .formatAsMoney(currencyBundle)
                xtzExchange = fromTokenCurrencyExchange
            } else {
                // token to XTZ: 0.3% fee paid in FA1.2 tokens
                formattedLiquidityFee = calculations.liquidityFee.formatAsToken(bundle.fromToken.token)
                liquidityFeeInCurrency =
                        /*calculations.liquidityFee.convertTo(fromTokenCurrencyExchange).formatAsMoney(currencyBundle)*/
                    ""
                xtzExchange = toTokenCurrencyExchange
            }

            // Since Dexter rates are going to jump dramatically, hide the currency value to avoid confusion
            val fromAmountInCurrency = if (bundle.fromToken.token.isXTZ()) {
                fromAmount.inCurrency(fromTokenCurrencyExchange).formatAsMoney(currencyBundle)
            } else {
                ""
            }
            val toAmountInCurrency = if (bundle.toToken.token.isXTZ()) {
                calculations.amountTo.inCurrency(toTokenCurrencyExchange).formatAsMoney(currencyBundle)
            } else {
                ""
            }

            val fees = bundle.operationFee.parseFees(swapOperations, xtzExchange, currencyBundle)

            val formattedFees: String = if (fees.totalFees == Tez.zero) {
                "N/A"
            } else {
                fees.totalFees.format(XTZ.symbol)
            }

            SwapConfirmationForView(
                fromAmount.formattedValue + " $fromTokenSymbol",
                calculations.amountTo.formattedValue + " $toTokenSymbol",
                fromAmountInCurrency,
                toAmountInCurrency,
                formattedRate,
                formattedLiquidityFee,
                liquidityFeeInCurrency,
                formattedFees,
                fees,
                priceImpact.formatPercentage() + "%"

            )
        }
    }

    /**
     * Check if it's an error we know of.
     */
    private fun parseError(error: Throwable?): Throwable? {
        return error.parseError()
    }

    fun swap() {
        _operation.value = DataWrapper.Loading()
        useCase.createSwapOperation(bundle)
            .flatMap { useCase.swap(it.operations) }
            .subscribeForUI()
            .subscribe(
                {
                    _operation.value = DataWrapper.Success(it)
                    clearOperationSubscription()
                },
                {
                    _operation.value = DataWrapper.Error(parseError(it))
                    clearOperationSubscription()
                }
            ).addToDisposables(operationDisposables)
    }

    fun getTokens(): Pair<String, String> {
        return Pair(bundle.fromToken.token.symbol, bundle.toToken.token.symbol)
    }

    fun showHelpOption(): Boolean {
        // If we already have the bundle filled
        return if (bundle.fromToken.balance is CoinBalance) {
            // Check if user wants to do an xtzToToken swap with ~max balance
            bundle.maxXTZ()
        } else {
            false
        }
    }
}

/**
 * Formatted values for the view
 */
data class SwapConfirmationForView(
    val formattedFromValue: String,
    val formattedToValue: String,
    val usdFromValue: String,
    val usdToValue: String,
    val exchangeRate: String, // 1 Tkn = ?? Tkn
    val formattedLiquidityFee: String,
    val currencyLiquidityValue: String,
    val totalFeesFormatted: String,
    var fees: OperationFeesBundle,
    var priceImpact: String // %
)
