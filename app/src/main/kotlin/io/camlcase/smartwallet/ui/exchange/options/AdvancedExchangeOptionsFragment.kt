/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.exchange.options

import android.content.Context
import android.os.Bundle
import android.text.InputType
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.core.content.ContextCompat
import androidx.viewbinding.ViewBinding
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.Exchange.defaultSlippages
import io.camlcase.smartwallet.core.extension.*
import io.camlcase.smartwallet.data.core.Logger
import io.camlcase.smartwallet.databinding.FragmentExchangeOptionsBinding
import io.camlcase.smartwallet.ui.base.BaseBottomSheetFragment
import io.camlcase.smartwallet.ui.custom.amount.DecimalInputFilter

/**
 * Exchange bottom modal
 * - Set Slippage
 * - Set timeout
 */
class AdvancedExchangeOptionsFragment : BaseBottomSheetFragment() {
    interface ExchangeOptionsListener {
        fun onOptions(slippage: Double, minutesTimeout: Int)
    }

    private val binding: FragmentExchangeOptionsBinding
        get() = _binding as FragmentExchangeOptionsBinding

    private lateinit var viewModel: AdvancedExchangeViewModel
    private var listener: ExchangeOptionsListener? = null

    override fun onCreateViewBind(inflater: LayoutInflater, container: ViewGroup?): ViewBinding {
        return FragmentExchangeOptionsBinding.inflate(inflater, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as? ExchangeOptionsListener
        viewModel = viewModel(viewModelFactory) {}
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupToolbar(binding.toolbar)
        binding.toolbar.setTitle(R.string.exc_slippage_title)
        bindViews()
    }

    private fun bindViews() {
        bindSlippageInput()
        bindTimeoutInput()
        bindSlippageButtons()
    }

    private fun bindSlippageButtons() {
        fun clearInputSlippage() {
            if (!binding.mainView.inputSlippage.textField()?.text.isNullOrBlank()) {
                binding.mainView.inputSlippage.textField()?.text = null
            }
        }
        binding.mainView.slippageOption1.setOnClickListener {
            clearInputSlippage()
        }
        binding.mainView.slippageOption2.setOnClickListener {
            clearInputSlippage()
        }
        binding.mainView.slippageOption3.setOnClickListener {
            clearInputSlippage()
        }
    }

    private fun bindTimeoutInput() {
        val timeoutDefaultHint = getString(R.string.exc_timeout_default)
        binding.mainView.inputTimeout.initForModal()
        binding.mainView.inputTimeout.textField()?.apply {
            inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_SIGNED
            hint = timeoutDefaultHint
            imeOptions = EditorInfo.IME_ACTION_DONE
            listenToChanges {
                if (!it.isBlank() && !viewModel.validTimeout(it)) {
                    binding.mainView.inputTimeout.showError(getString(R.string.error_exchange_timeout))
                } else {
                    binding.mainView.inputTimeout.dismissError()
                }
            }

            setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    binding.mainView.inputTimeout.textField()?.hideKeyboard()
                    dismiss()
                    true
                } else {
                    false
                }
            }

            setOnFocusChangeListener { _, hasFocus ->
                // Suffix is only visible when focus/text. Avoid the situation where hint and suffix are shown
                // at the same time. https://github.com/material-components/material-components-android/issues/1459
                hint = if (hasFocus) {
                    null
                } else {
                    timeoutDefaultHint
                }
            }
        }
        binding.mainView.inputTimeout.inputLayout.suffixText = getString(R.string.exc_timeout_suffix)
        binding.mainView.inputTimeout.setMaxLength(4)

        // Set previously typed minutes
        viewModel.initInputTimeout {
            binding.mainView.inputTimeout.textField()?.setText(it.toString())
        }
    }

    private fun bindSlippageInput() {
        binding.root.requestFocus()
        context?.apply {
            binding.mainView.inputSlippageError.setCompoundDrawablesWithIntrinsicBounds(
                null,
                null,
                ContextCompat.getDrawable(this, R.drawable.ic_error_modal),
                null
            )
        }

        val defaultHint = "%"
        binding.mainView.inputSlippage.textField()?.apply {
            hint = defaultHint
            gravity = Gravity.CENTER
            inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL
            filters = arrayOf(DecimalInputFilter(digitsBeforeZero = 3, digitsAfterZero = 2))
            imeOptions = EditorInfo.IME_ACTION_NEXT

            fun showSlippageError(message: String = getString(R.string.error_slippage_exceed)) {
                binding.mainView.inputSlippageError.show()
                binding.mainView.inputSlippageError.text = message
                // We don't pass anything, we only want the red border
                binding.mainView.inputSlippage.showError(null)
            }
            listenToChanges {
                if (!it.isBlank()) {
                    clearSlippageOptions()
                    try {
                        viewModel.validateSlippage(it,
                            valid = {
                                binding.mainView.inputSlippage.dismissError()
                                binding.mainView.inputSlippageError.invisible()
                            },
                            invalid = {
                                showSlippageError()
                            })
                    } catch (e: NumberFormatException) {
                        // Keyboard is set to TYPE_CLASS_NUMBER and all input should be parseable numbers. In the exceptional
                        // case some other type is injected, we should fail gracefully (The app shouldn't crash for this)
                        Logger.e("*** ERROR: $e")
                        showSlippageError(getString(R.string.error_generic))
                    }
                } else if (binding.mainView.slippageOptions.checkedChipId == View.NO_ID) {
                    selectSlippageDefault()
                }
            }

            setOnFocusChangeListener { _, hasFocus ->
                // Suffix is only visible when focus/text. Avoid the situation where hint and suffix are shown
                // at the same time. https://github.com/material-components/material-components-android/issues/1459
                hint = if (hasFocus) {
                    null
                } else {
                    defaultHint
                }
            }
        }

        binding.mainView.inputSlippage.inputLayout.suffixText = defaultHint

        // Set previously selected %
        viewModel.initInputSlippage(
            onCustom = {
                binding.mainView.inputSlippage.textField()?.setText(it)
            },
            onDefault = {
                val selected = when (it) {
                    defaultSlippages[0] -> R.id.slippage_option3
                    defaultSlippages[1] -> R.id.slippage_option2
                    else -> R.id.slippage_option1
                }
                binding.mainView.slippageOptions.check(selected)
            }
        )
    }

    fun clearSlippageOptions() {
        binding.mainView.slippageOptions.clearCheck()
    }

    fun selectSlippageDefault() {
        binding.mainView.slippageOptions.check(R.id.slippage_option2)
    }

    override fun onDestroyView() {
        validateFields()
        super.onDestroyView()
    }

    private fun validateFields() {
        val slippageSelected = when (binding.mainView.slippageOptions.checkedChipId) {
            // Unselected
            View.NO_ID -> 0.0
            R.id.slippage_option1 -> defaultSlippages[2]
            R.id.slippage_option2 -> defaultSlippages[1]
            else -> defaultSlippages[0]
        }
        val finalSlippage = viewModel.parseSlippage(binding.mainView.inputSlippage.text, slippageSelected)
        val finalTimeout = viewModel.parseTimeout(binding.mainView.inputTimeout.text)
        listener?.onOptions(finalSlippage, finalTimeout)
    }

    companion object {
        fun init(): AdvancedExchangeOptionsFragment {
            val fragment = AdvancedExchangeOptionsFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }
}
