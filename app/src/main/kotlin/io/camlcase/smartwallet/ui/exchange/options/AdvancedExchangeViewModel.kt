/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.exchange.options

import io.camlcase.smartwallet.core.Exchange.defaultSlippages
import io.camlcase.smartwallet.core.architecture.BaseViewModel
import io.camlcase.smartwallet.core.extension.formatPercentage
import io.camlcase.smartwallet.data.core.Exchange
import io.camlcase.smartwallet.data.core.Exchange.DEFAULT_SLIPPAGE
import io.camlcase.smartwallet.data.core.Exchange.DEFAULT_TIMEOUT_MINUTES
import io.camlcase.smartwallet.data.core.Exchange.MAX_TIMEOUT_MINUTES
import io.camlcase.smartwallet.data.core.Exchange.MIN_TIMEOUT_MINUTES
import io.camlcase.smartwallet.data.source.local.AppPreferences
import javax.inject.Inject
import kotlin.math.absoluteValue

class AdvancedExchangeViewModel @Inject constructor(
    private val appPreferences: AppPreferences
) : BaseViewModel() {
    /**
     * Pass 12% and return 0.12 to do calculations
     *
     * @param input Formatted percentage
     * @param valid If [input] passes [validInput]. Will pass [input] as a decimal value of the percentage
     */
    fun validateSlippage(input: String, valid: (Double) -> Unit, invalid: () -> Unit = {}) {
        val slippage = convertToSlippage(input)
        if (validInput(slippage)) {
            valid(slippage)
        } else {
            invalid()
        }
    }

    private fun convertToSlippage(input: String): Double {
        val convertedInput = input.toDouble().absoluteValue
        val slippage = convertedInput / 100
        return slippage
    }

    private fun validInput(input: Double): Boolean {
        return input < Exchange.INVALID_SLIPPAGE_THRESHOLD
    }

    fun parseSlippage(input: String, selected: Double): Double {
        fun parseSelected(selected: Double): Double {
            return if (selected > 0.0 && validInput(selected)) {
                selected
            } else {
                defaultSlippages[1]
            }
        }

        val finalSlippage = if (input.isBlank()) {
            parseSelected(selected)
        } else {
            try {
                val slippage = convertToSlippage(input)
                when {
                    validInput(slippage) -> slippage
                    else -> parseSelected(selected)
                }
            } catch (e: NumberFormatException) {
                parseSelected(selected)
            }
        }

        appPreferences.store(AppPreferences.EXCHANGE_SLIPPAGE, finalSlippage)
        return finalSlippage
    }

    /**
     * @param previousSlippage In decimal format (0.005)
     */
    fun initInputSlippage(
        onCustom: (String) -> Unit,
        onDefault: (Double) -> Unit
    ) {
        // Set previously selected %
        var previousSlippage = appPreferences.getDouble(AppPreferences.EXCHANGE_SLIPPAGE)
        if (previousSlippage == -1.0) {
            previousSlippage = DEFAULT_SLIPPAGE
        }
        val default = defaultSlippages.firstOrNull { it == previousSlippage }
        if (default == null) {
            val percentage = (previousSlippage * 100).formatPercentage()
            onCustom(percentage)
        } else {
            onDefault(default)
        }
    }

    fun validTimeout(input: String): Boolean {
        return try {
            val minutes = input.toInt()
            minutes in MIN_TIMEOUT_MINUTES..MAX_TIMEOUT_MINUTES
        } catch (e: NumberFormatException) {
            false
        }
    }

    fun parseTimeout(input: String): Int {
        val timeout = try {
            val minutes = input.toInt()
            if (minutes in MIN_TIMEOUT_MINUTES..MAX_TIMEOUT_MINUTES) {
                minutes
            } else {
                DEFAULT_TIMEOUT_MINUTES
            }
        } catch (e: NumberFormatException) {
            DEFAULT_TIMEOUT_MINUTES
        }
        appPreferences.store(AppPreferences.EXCHANGE_TIMEOUT, timeout)
        return timeout
    }

    fun initInputTimeout(action: (Int) -> Unit) {
        var previousTimeout = appPreferences.getInt(AppPreferences.EXCHANGE_TIMEOUT)
        if (previousTimeout == -1) {
            previousTimeout = DEFAULT_TIMEOUT_MINUTES
        }
        if (previousTimeout != DEFAULT_TIMEOUT_MINUTES) {
            action(previousTimeout)
        }
    }
}
