/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.displayFragment
import io.camlcase.smartwallet.core.extension.popFullBackstack
import io.camlcase.smartwallet.core.extension.replaceFragmentSafely
import io.camlcase.smartwallet.data.model.LoginStep
import io.camlcase.smartwallet.data.model.OnboardingStep
import io.camlcase.smartwallet.data.source.local.AppPreferences
import io.camlcase.smartwallet.ui.base.BaseActivity
import io.camlcase.smartwallet.ui.base.BindBaseFragment
import io.camlcase.smartwallet.ui.base.info.InfoFragment
import io.camlcase.smartwallet.ui.base.info.InfoType
import io.camlcase.smartwallet.ui.base.info.InfoView
import io.camlcase.smartwallet.ui.base.navigate.AppNavigator
import io.camlcase.smartwallet.ui.login.balance.ImportBalanceMenuFragment
import io.camlcase.smartwallet.ui.login.migration.ImportSelectionFragment
import io.camlcase.smartwallet.ui.onboarding.OnboardingActivity
import java.lang.ref.WeakReference

/**
 * - Import seed phrase + Advanced options
 * - Balance menu (HD/SD select)
 * - Migration selection (If SD)
 * - Success!
 * - Navigates the user to SET_PIN in onboarding
 *
 * @see showLoginStep
 * @see OnboardingActivity
 */
class LoginActivity : BaseActivity(R.layout.activity_fragment),
    SeedPhraseImportFragment.SeedPhraseImportListener,
    ImportBalanceMenuFragment.BalanceCheckListener,
    ImportSelectionFragment.LoginSelectionListener,
    InfoView.InfoEventsListener {
    private val appPreferences = AppPreferences(WeakReference(this))

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            showLoginStep()
        }
    }

    /**
     * State control function.
     */
    private fun showLoginStep() {
        val fragment: BindBaseFragment = when (appPreferences.getLoginStep()) {
            LoginStep.UNKNOWN -> SeedPhraseImportFragment.init()
            LoginStep.MIGRATION -> ImportSelectionFragment.init(false)
        }

        show(fragment)
    }

    private fun show(fragment: BindBaseFragment) {
        replaceFragmentSafely(fragment, R.id.container_fragment)
    }

    /**
     * IMPORT SEED PHRASE
     */

    override fun onStandardWalletImported(derivedAddress: String, linearAddress: String) {
        displayFragment(ImportBalanceMenuFragment.init(derivedAddress, linearAddress), R.id.container_fragment)
    }

    override fun onHDWalletImported() {
        importComplete()
    }

    /**
     * BALANCE CHECK
     */

    override fun onHDWalletSelected() {
        importComplete()
    }

    override fun onSDWalletSelected() {
        updateLoginStep(LoginStep.MIGRATION)
        displayFragment(ImportSelectionFragment.init(true), R.id.container_fragment)
    }

    override fun onMigrationStatusSaved() {
        importComplete()
    }

    /**
     * GENERATE WALLET SUCCESS
     */

    override fun onActionClick(infoType: InfoType) {
        when (infoType) {
            InfoType.LOGIN_IMPORT_WALLET_SUCCESS -> {
                // Already updated step on [onValidWallet]
                AppNavigator.navigateToOnboarding(this)
            }
            else -> {
                throw IllegalStateException("Onboarding step not recognized $infoType")
            }
        }
    }

    private fun importComplete() {
        updateLoginStep(LoginStep.UNKNOWN)
        appPreferences.storeOnboardingStep(OnboardingStep.SET_PIN)
        popFullBackstack()
        replaceFragmentSafely(InfoFragment.init(InfoType.LOGIN_IMPORT_WALLET_SUCCESS), R.id.container_fragment)
    }

    private fun updateLoginStep(step: LoginStep) {
        appPreferences.storeLoginStep(step)
    }

    companion object {
        fun init(context: Context): Intent {
            return Intent(context, LoginActivity::class.java)
        }
    }
}
