/**
 * # Released under MIT License
 *
 * Copyright (c) 2021 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.login.balance

import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import io.camlcase.kotlintezos.model.Address
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.*
import io.camlcase.smartwallet.data.model.error.AppError
import io.camlcase.smartwallet.data.model.error.ErrorType
import io.camlcase.smartwallet.data.usecase.analytics.ScreenEvent
import io.camlcase.smartwallet.databinding.FragmentImportBalanceCheckBinding
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.model.handleResults
import io.camlcase.smartwallet.ui.base.BindBaseFragment
import io.camlcase.smartwallet.ui.base.info.InfoModalFragment
import io.camlcase.smartwallet.ui.base.info.InfoModalType
import io.camlcase.smartwallet.ui.base.navigate.AppNavigator
import io.camlcase.smartwallet.ui.base.navigate.navigateToModal
import io.camlcase.smartwallet.ui.base.showError

class ImportBalanceMenuFragment : BindBaseFragment(R.layout.fragment_import_balance_check) {
    interface BalanceCheckListener {
        fun onHDWalletSelected()
        fun onSDWalletSelected()
    }

    private val binding by viewBinding(FragmentImportBalanceCheckBinding::bind)
    private var listener: BalanceCheckListener? = null
    private lateinit var viewModel: ImportBalanceViewModel

    private var hdAddress: Address? = null
    private var sdAddress: Address? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        reportView(ScreenEvent.ONB_RECOVERY_MENU)
        manageIntent(arguments)
        listener = context as? BalanceCheckListener
        if (listener == null) {
            throw ClassCastException("$context must implement BalanceCheckListener")
        }
        viewModel = viewModel(viewModelFactory) {
            observe(data, { handleData(it) })
        }
    }

    private fun manageIntent(bundle: Bundle?) {
        bundle?.getString(ARG_HD_ADDRESS)?.let {
            hdAddress = it
        }

        bundle?.getString(ARG_SD_ADDRESS)?.let {
            sdAddress = it
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_help, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.help -> {
                analyticsUseCase.get()?.reportScreen(ScreenEvent.ONB_RECOVERY_MENU_HELP)
                val fragment = InfoModalFragment.init(InfoModalType.IMPORT_BALANCE_CHECK)
                navigateToModal(fragment)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAppbar()
        binding.list.apply {
            layoutManager = LinearLayoutManager(activity)
            setHasFixedSize(true)
        }
        viewModel.getBalances(hdAddress, sdAddress)
    }

    private fun initAppbar() {
        val title = getString(R.string.onb_import_seed_phrase_title)
        // Add back navigation if we come from the import flow, not from launch
        setupAppBar(binding.appBarContainer, title, hdAddress != null)
        setHasOptionsMenu(true)
    }

    private fun handleData(wrapper: DataWrapper<List<ImportBalanceListItem>>) {
        wrapper.handleResults(
            onValidData = {
                hideLoading()
                populate(it)
            },
            onErrorWithData = { error, data ->
                hideLoading()
                if (error is AppError && error.type == ErrorType.NO_WALLET) {
                    AppNavigator.navigateToWelcome(activity)
                    activity?.finish()
                } else {
                    if (data != null) {
                        // Let the user continue anyways
                        populate(data)
                    }
                    showError(error)
                }
            },
            onLoading = {
                showLoading()
            }
        )
    }

    private fun populate(list: List<ImportBalanceListItem>) {
        binding.list.adapter = ImportBalanceAdapter(list) { item, view ->
            view.throttledClick()
                .subscribe {
                    viewModel.onAddressSelected(item.address) { isSD ->
                        if (isSD) {
                            listener?.onSDWalletSelected()
                        } else {
                            listener?.onHDWalletSelected()
                        }
                    }
                }.addToDisposables(disposables)
        }
    }

    private fun showLoading() {
        binding.loading.containerLoading.show()
    }

    private fun hideLoading() {
        binding.loading.containerLoading.hide()
    }

    companion object {
        private const val ARG_HD_ADDRESS = "args.address.hd"
        private const val ARG_SD_ADDRESS = "args.address.sd"

        fun init(derivedAddress: String?, linearAddress: String?): ImportBalanceMenuFragment {
            val fragment = ImportBalanceMenuFragment()
            val args = Bundle()
            args.putString(ARG_HD_ADDRESS, derivedAddress)
            args.putString(ARG_SD_ADDRESS, linearAddress)
            fragment.arguments = args
            return fragment
        }
    }
}
