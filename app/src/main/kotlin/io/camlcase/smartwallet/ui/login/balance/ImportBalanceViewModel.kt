/**
 * # Released under MIT License
 *
 * Copyright (c) 2021 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.login.balance

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.bcd.BCDAccount
import io.camlcase.smartwallet.core.architecture.BaseViewModel
import io.camlcase.smartwallet.core.extension.addToDisposables
import io.camlcase.smartwallet.core.extension.subscribeForUI
import io.camlcase.smartwallet.data.analytic.report
import io.camlcase.smartwallet.data.core.Logger
import io.camlcase.smartwallet.data.core.XTZ
import io.camlcase.smartwallet.data.core.extension.format
import io.camlcase.smartwallet.data.model.error.AppError
import io.camlcase.smartwallet.data.model.error.ErrorType
import io.camlcase.smartwallet.data.usecase.LogOutUseCase
import io.camlcase.smartwallet.data.usecase.info.GetBCDBalanceUseCase
import io.camlcase.smartwallet.data.usecase.user.ChooseWalletUseCase
import io.camlcase.smartwallet.model.DataWrapper
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.functions.BiFunction
import java.math.BigInteger
import javax.inject.Inject

/**
 * Check balances for a certain mnemonic's derived addresses and let the user choose.
 */
class ImportBalanceViewModel @Inject constructor(
    private val useCase: ChooseWalletUseCase,
    private val balanceUseCase: GetBCDBalanceUseCase,
    private val logOutUseCase: LogOutUseCase
) : BaseViewModel() {
    private val _data: MutableLiveData<DataWrapper<List<ImportBalanceListItem>>> = MutableLiveData()
    val data: LiveData<DataWrapper<List<ImportBalanceListItem>>> = _data

    fun getBalances(derivedAddress: String?, linearAddress: String?) {
        _data.value = DataWrapper.Loading()
        // If no addresses, fetch them from the saved wallet
        if (derivedAddress == null) {
            useCase.getAddressPair()
        } else {
            Single.just(Pair(derivedAddress, linearAddress))
        }.flatMap { getAddressBalance(it.first, it.second) }
            .subscribeForUI()
            .subscribe(
                {
                    _data.value = DataWrapper.Success(it)
                    clearOperationSubscription()
                },
                {
                    if (it is AppError && it.type == ErrorType.NO_WALLET) {
                        logOutUseCase.logOut()
                    }
                    val list: List<ImportBalanceListItem>? = derivedAddress?.let {
                        parseAddressWithNoBalance(derivedAddress, linearAddress)
                    }

                    _data.value = DataWrapper.Error(it, list)
                    clearOperationSubscription()
                }
            ).addToDisposables(operationDisposables)
    }

    private fun getAddressBalance(derivedAddress: String, linearAddress: String?): Single<List<ImportBalanceListItem>> {
        return if (linearAddress != null) {
            Single.zip(
                balanceUseCase.getBalanceInfo(derivedAddress),
                balanceUseCase.getBalanceInfo(linearAddress),
                BiFunction { balanceHD, balanceSD -> parseAddressAccount(balanceHD, balanceSD) }
            )
        } else {
            balanceUseCase.getBalanceInfo(derivedAddress)
                .map { parseAddressAccount(it, null) }
        }
    }

    private fun parseAddressAccount(accountHD: BCDAccount, accountSD: BCDAccount?): List<ImportBalanceListItem> {
        val list = ArrayList<ImportBalanceListItem>()
        list.add(Header())
        list.add(
            ImportBalance(
                accountHD.address,
                accountHD.balance.format(XTZ.symbol),
                !accountHD.tokens.isNullOrEmpty() && accountHD.tokens.firstOrNull { it.balance > BigInteger.ZERO } != null
            )
        )
        if (accountSD != null) {
            list.add(Header())
            list.add(
                ImportBalance(
                    accountSD.address,
                    accountSD.balance.format(XTZ.symbol),
                    !accountSD.tokens.isNullOrEmpty() && accountSD.tokens.firstOrNull { it.balance > BigInteger.ZERO } != null
                )
            )
        }
        return list
    }

    private fun parseAddressWithNoBalance(derivedAddress: String, linearAddress: String?): List<ImportBalanceListItem> {
        val list = ArrayList<ImportBalanceListItem>()
        list.add(Header())
        list.add(ImportBalance(derivedAddress, null, null))
        if (linearAddress != null) {
            list.add(Header())
            list.add(ImportBalance(linearAddress, null, null))
        }
        return list
    }

    fun onAddressSelected(address: Address, onChosenWallet: (Boolean) -> Unit) {
        Single.fromCallable { useCase.chooseWallet(address) }
            .subscribeForUI()
            .subscribe(
                {
                    onChosenWallet(it)
                    clearOperationSubscription()
                },
                {
                    // It should never happen
                    it.report()
                    clearOperationSubscription()
                }
            ).addToDisposables(operationDisposables)
    }
}

data class ImportBalance(
    val address: Address,
    val xtzBalance: String?,
    val faTokenBalance: Boolean?
) : ImportBalanceListItem

internal class Header : ImportBalanceListItem

/**
 * Adapter's main class
 */
interface ImportBalanceListItem
