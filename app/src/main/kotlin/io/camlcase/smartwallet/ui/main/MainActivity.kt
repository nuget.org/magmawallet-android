/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import by.kirich1409.viewbindingdelegate.viewBinding
import io.camlcase.smartwallet.BuildConfig
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.URL
import io.camlcase.smartwallet.core.extension.*
import io.camlcase.smartwallet.data.analytic.report
import io.camlcase.smartwallet.data.model.Token
import io.camlcase.smartwallet.data.model.contact.MagmaContact
import io.camlcase.smartwallet.data.model.exchange.CurrencyBundle
import io.camlcase.smartwallet.data.model.exchange.TokenInfoBundle
import io.camlcase.smartwallet.data.model.money.MoonPayTransactionStatus
import io.camlcase.smartwallet.databinding.ActivityMainBinding
import io.camlcase.smartwallet.model.navigation.MainNavigation
import io.camlcase.smartwallet.model.navigation.MainSource
import io.camlcase.smartwallet.model.navigation.SeedPhraseNavigation
import io.camlcase.smartwallet.ui.base.*
import io.camlcase.smartwallet.ui.base.info.InfoModalFragment
import io.camlcase.smartwallet.ui.base.info.InfoModalType
import io.camlcase.smartwallet.ui.base.navigate.AppNavigator
import io.camlcase.smartwallet.ui.base.navigate.RequestCode
import io.camlcase.smartwallet.ui.base.navigate.ResultCode
import io.camlcase.smartwallet.ui.exchange.amount.*
import io.camlcase.smartwallet.ui.exchange.options.AdvancedExchangeOptionsFragment
import io.camlcase.smartwallet.ui.main.activity.OperationListFragment
import io.camlcase.smartwallet.ui.main.balance.BalancesFragment
import io.camlcase.smartwallet.ui.main.balance.TokenDetailFragment
import io.camlcase.smartwallet.ui.main.contact.ContactListFragment
import io.camlcase.smartwallet.ui.main.contact.detail.ContactDetailFragment
import io.camlcase.smartwallet.ui.main.settings.SettingsFragment
import io.camlcase.smartwallet.ui.main.token.SideDataViewModel
import io.camlcase.smartwallet.ui.migration.DismissMigrationViewModel
import io.camlcase.smartwallet.ui.migration.MigrationWarningFragment
import io.camlcase.smartwallet.ui.money.BuyTezViewModel
import io.camlcase.smartwallet.ui.onboarding.seed_phrase.SeedPhraseFragment

/**
 * Main screen for the app. Handles navigation between central screens.
 *
 * @see BalancesFragment
 * @see ExchangeAmountFragment
 * @see ContactListFragment
 * @see OperationListFragment
 * @see SettingsFragment
 */
class MainActivity : BaseActivity(R.layout.activity_main),
    BalancesFragment.BalanceListener,
    TokenDetailFragment.TokenDetailListener,
    ChooseExchangeTokenFragment.ChooseExchangeTokenListener,
    ContactListFragment.ContactListListener,
    ContactDetailFragment.ContactDetailListener,
    AdvancedExchangeOptionsFragment.ExchangeOptionsListener,
    SettingsFragment.SettingsListener,
    InfoModalFragment.InfoModalListener,
    MigrationWarningFragment.MigrationWarningListener {

    private val binding by viewBinding(ActivityMainBinding::bind)
    private lateinit var dataViewModel: SideDataViewModel
    private lateinit var dismissMigrationViewModel: DismissMigrationViewModel
    private lateinit var buyViewModel: BuyTezViewModel
    private var deeplinkUri: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        initPager()
        initBottomNavigation()
        listenToBackground()
        bindScreenResults()
        loadSideData()
        dismissMigrationViewModel = viewModel(viewModelFactory) {}
        buyViewModel = viewModel(viewModelFactory) {}
    }

    private fun manageIntent(bundle: Bundle?) {
        bundle?.getString(ARG_DEEPLINK_DATA)?.let {
            deeplinkUri = it
        }
    }

    /**
     * LaunchActivity will receive the MoonPay redirect uri and call startActivity with a new intent. If MainActivity is
     * already started, this will be called with the new data.
     */
    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        manageIntent(intent?.extras)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString(ARG_DEEPLINK_DATA, deeplinkUri)
        super.onSaveInstanceState(outState)
    }

    /**
     * DKA fallback
     */
    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        deeplinkUri = savedInstanceState.getString(ARG_DEEPLINK_DATA)
        super.onRestoreInstanceState(savedInstanceState)
    }

    private fun loadSideData() {
        dataViewModel = viewModel(viewModelFactory) {}
        dataViewModel.getData()
    }

    private fun initPager() {
        binding.pager.isUserInputEnabled = false
        val adapter = MainScreensAdapter(supportFragmentManager, lifecycle)
        binding.pager.offscreenPageLimit = adapter.itemCount
        binding.pager.adapter = adapter
    }

    private fun initBottomNavigation() {
        binding.bottomNavigation.menu.clear() //clear old inflated items.
        binding.bottomNavigation.inflateMenu(R.menu.menu_main_navigation)
        binding.bottomNavigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.nav_balance -> {
                    binding.pager.setCurrentItem(MainNavigation.TAB_WALLET, false)
                    binding.mainContainer.setBackgroundResource(R.drawable.background_rich_soft)
                }
                R.id.nav_exchange -> {
                    binding.pager.setCurrentItem(MainNavigation.TAB_EXCHANGE, false)
                    binding.mainContainer.setBackgroundResource(R.drawable.background_gradient)
                }
                R.id.nav_contacts -> {
                    binding.pager.setCurrentItem(MainNavigation.TAB_CONTACTS, false)
                    binding.mainContainer.setBackgroundResource(R.drawable.background_gradient)
                }
                R.id.nav_activity -> {
                    binding.pager.setCurrentItem(MainNavigation.TAB_ACTIVITY, false)
                    binding.mainContainer.setBackgroundResource(R.drawable.background_gradient)
                }
                R.id.nav_settings -> {
                    binding.pager.setCurrentItem(MainNavigation.TAB_SETTINGS, false)
                    binding.mainContainer.setBackgroundResource(R.drawable.background_gradient)
                }
            }
            true
        }
    }

    private fun parseDeeplinkData(deeplinkUri: String) {
        buyViewModel.parseFinishUri(deeplinkUri)?.apply {
            when (this) {
                MoonPayTransactionStatus.COMPLETED -> {
                    showSnackbar(getString(R.string.buy_success_toast))
                    onBuyXTZ()
                }
                MoonPayTransactionStatus.WAITING_AUTHORIZATION,
                MoonPayTransactionStatus.WAITING_PAYMENT,
                MoonPayTransactionStatus.PENDING -> showSnackbar(getString(R.string.buy_pending_toast))
                MoonPayTransactionStatus.FAILED -> showSnackbar(getString(R.string.buy_error_toast))
            }
        }
    }

    private fun navigateTo(@MainNavigation.Tab index: Int) {
        when (index) {
            MainNavigation.TAB_WALLET -> binding.bottomNavigation.selectedItemId = R.id.nav_balance
            MainNavigation.TAB_EXCHANGE -> binding.bottomNavigation.selectedItemId = R.id.nav_exchange
            MainNavigation.TAB_CONTACTS -> binding.bottomNavigation.selectedItemId = R.id.nav_contacts
            MainNavigation.TAB_ACTIVITY -> binding.bottomNavigation.selectedItemId = R.id.nav_activity
            MainNavigation.TAB_SETTINGS -> binding.bottomNavigation.selectedItemId = R.id.nav_settings
        }
    }

    private fun bindScreenResults() {
        fun navigateToTab(activityResult: ActivityResult) {
            activityResult.data?.getIntExtra(ARG_OPERATION_TAB, -1)?.apply {
                if (this != -1) {
                    @MainNavigation.Tab val returnTo = this
                    navigateTo(returnTo)
                }
            }
        }

        activityResult.bind(
            RequestCode.WAIT_FOR_INJECTION,
            disposable = disposables,
            onResult = { activityResult ->
                // Only responsibility of the screen is to navigate to EXC tab
                // See Balances and Exchange fragments for data update
                navigateToTab(activityResult)
            },
            onError = { it.report() }
        )

        activityResult.bind(
            code = RequestCode.DELEGATE_BAKER,
            resultCode = ResultCode.ERROR,
            disposable = disposables,
            onResult = { activityResult ->
                navigateToTab(activityResult)
            },
            onError = { it.report() }
        )

        activityResult.bind(
            code = RequestCode.AUTH_LOGIN,
            disposable = disposables,
            onResult = {
                deeplinkUri?.apply {
                    // After pin, show the deeplink info
                    parseDeeplinkData(this)
                    deeplinkUri = null
                }
            },
            onError = { it.report() }
        )
    }

    override fun openTokenDetail(info: TokenInfoBundle, currencyBundle: CurrencyBundle) {
        val fragment = TokenDetailFragment.init(info, currencyBundle)
        slideDetail(fragment)
    }

    override fun onContactClick(contact: MagmaContact) {
        val fragment = ContactDetailFragment.init(contact)
        slideDetail(fragment)
    }

    /**
     * Open screen on top of bottom navigation
     */
    private fun slideDetail(fragment: BindBaseFragment) {
        fragment.slideLeftAnimation()
        displayFragment(
            fragment,
            R.id.container_fragment
        )
    }

    override fun onRecoveryPhraseClick() {
        val fragment = SeedPhraseFragment.init(SeedPhraseNavigation.SOURCE_SETTINGS)
        fragment.slideLeftAnimation()
        displayFragment(fragment, R.id.container_fragment)
    }

    override fun onEditPinClick() {
        AppNavigator.navigateToChangePin(this)
    }

    /**
     * SWAP METHODS
     */

    override fun onSwapToken(token: Token) {
        binding.bottomNavigation.post {
            navigateTo(MainNavigation.TAB_EXCHANGE)

            binding.pager.postDelayed({
                communicateWithFragment<MainToExchange> {
                    it.setTokenFrom(token)
                }
            }, 100)
        }
    }

    override fun onOptions(slippage: Double, minutesTimeout: Int) {
        communicateWithFragment<MainToExchange> {
            it.onOptions(slippage, minutesTimeout)
        }
    }

    override fun onTokenSelected(tokenInfo: TokenInfoBundle, source: Int) {
        communicateWithFragment<MainToExchange> {
            it.onTokenSelected(tokenInfo.token, source)
        }
    }

    private fun onBuyXTZ() {
        communicateWithFragment<MainToBalance> {
            it.refresh()
        }
    }

    /**
     * CONTACT DETAIL
     */
    override fun editContact(contact: MagmaContact) {
        AppNavigator.navigateToAddContact(this, contact)
    }

    override fun onContactDeleted() {
        refreshContactsScreen()
        communicateWithFragment<MainToActivity> {
            it.refreshContacts()
        }
    }

    private fun refreshContactsScreen() {
        communicateWithFragment<MainToContacts> {
            it.refresh()
        }
    }

    override fun onActionClick(infoType: InfoModalType) {
        if (infoType == InfoModalType.DEXTER_INFO) {
            navigateToUrl(URL.DEXTER_URL)
        }
    }

    override fun goToMigration() {
        AppNavigator.navigateToMigrateWallet(this)
    }

    override fun dismissMigrationWarningPermanently() {
        dismissMigrationViewModel.updateVersionCreated(BuildConfig.VERSION_CODE)
        communicateWithFragment<MainToSettings> {
            it.reloadList()
        }
    }

    companion object {
        private const val ARG_LAUNCH_SOURCE = "arg.launch.source"
        const val ARG_DEEPLINK_DATA = "arg.deeplink.data"
        const val ARG_OPERATION_TAB = "arg.operation.result.tab"

        fun init(context: Context, source: MainSource): Intent {
            val intent = Intent(context, MainActivity::class.java)
            intent.putExtra(ARG_LAUNCH_SOURCE, source.name)
            return intent
        }

        fun init(context: Context, deeplinkUri: String?, source: MainSource): Intent {
            val intent = Intent(context, MainActivity::class.java)
            intent.putExtra(ARG_LAUNCH_SOURCE, source.name)
            intent.putExtra(ARG_DEEPLINK_DATA, deeplinkUri)
            return intent
        }
    }
}
