/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import io.camlcase.smartwallet.model.navigation.MainNavigation
import io.camlcase.smartwallet.ui.exchange.amount.ExchangeAmountFragment
import io.camlcase.smartwallet.ui.main.activity.OperationListFragment
import io.camlcase.smartwallet.ui.main.balance.BalancesFragment
import io.camlcase.smartwallet.ui.main.contact.ContactListFragment
import io.camlcase.smartwallet.ui.main.settings.SettingsFragment

/**
 * Handles main screen pager
 */
class MainScreensAdapter(
    manager: FragmentManager,
    lifecycle: Lifecycle
) : FragmentStateAdapter(manager, lifecycle) {
    override fun getItemCount(): Int = 5

    override fun createFragment(position: Int): Fragment {
        val fragment: Fragment = getFragment(position)
        fragment.arguments = Bundle().apply {
            putInt(ARG_INDEX, position + 1)
        }
        return fragment
    }

    // TODO Make it return BaseFragment once migration to view binding is finished
    private fun getFragment(position: Int): Fragment {
        return when (position) {
            MainNavigation.TAB_WALLET -> BalancesFragment()
            MainNavigation.TAB_EXCHANGE -> ExchangeAmountFragment()
            MainNavigation.TAB_CONTACTS -> ContactListFragment()
            MainNavigation.TAB_ACTIVITY -> OperationListFragment()
            MainNavigation.TAB_SETTINGS -> SettingsFragment()
            else -> throw AssertionError("Pager should not have more than $itemCount pages")
        }
    }

    companion object {
        const val ARG_INDEX = "arg.pager.index"
    }
}
