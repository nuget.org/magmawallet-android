/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.main.activity

import io.camlcase.kotlintezos.model.OperationResultStatus
import io.camlcase.kotlintezos.model.tzkt.*
import io.camlcase.kotlintezos.model.tzkt.dto.TzKtAlias
import io.camlcase.smartwallet.core.extension.ellipsize
import io.camlcase.smartwallet.core.extension.formatExchangeRate
import io.camlcase.smartwallet.data.core.Logger
import io.camlcase.smartwallet.data.core.XTZ
import io.camlcase.smartwallet.data.core.extension.format
import io.camlcase.smartwallet.data.core.extension.tezos.getDate
import io.camlcase.smartwallet.data.model.contact.MagmaContact
import io.camlcase.smartwallet.data.model.delegate.TezosBakerResponse
import io.camlcase.smartwallet.data.model.operation.*
import io.camlcase.smartwallet.model.operation.OperationForView
import java.math.BigDecimal
import java.util.*

class OperationForViewParser(
    private val contacts: List<MagmaContact>,
    private val supportedDelegates: List<TezosBakerResponse>?
) {

    fun parseOperation(
        operation: TezosOperation
    ): OperationForView {
        return when (operation) {
            is TransferOperation -> parseTransaction(operation)
            is SwapOperation -> parseSwapOperation(operation)
            is DelegateOperation -> parseDelegation(operation)
            is OriginateOperation -> parseOrigination(operation)
            is RevealOperation -> parseReveal(operation)
            is UnknownOperation -> parseSmartContractOperation(operation)
            else -> parseDefaultOperation(operation)
        }
    }

    private fun findName(alias: TzKtAlias): String {
        val address = alias.address

        // 1) Find name in contacts list
        val foundContact: MagmaContact? =
            contacts.firstOrNull { contact -> contact.tezosAddresses.any { it.address == address } }

        // 2) Find name in supported delegates name
        val delegateName: String? = supportedDelegates?.firstOrNull { it.address == address }?.name

        // 3) Find name in TzKt alias
        return foundContact?.name ?: delegateName ?: alias.alias ?: address.ellipsize()
    }

    private fun parseTransaction(
        operation: TransferOperation
    ): OperationForView {
        // Tx only have one operation
        val tzKtOperation = operation.tzKtOperations[0] as TzKtTransaction
        val (timestamp, status) = extractDateAndStatus(tzKtOperation)

        val operationSymbol = if (operation is ReceiveOperation) {
            "+"
        } else {
            "-"
        }
        // AMOUNT
        // +/- N {symbol}
        val amount = "$operationSymbol ${operation.amount.formattedValue} ${operation.token.symbol}"

        // FEES
        val formattedFees = operation.fees?.format(XTZ.symbol)

        // TO-FROM
        val destination = findName(operation.destination)
        val source = findName(tzKtOperation.sender)

        return OperationForView(
            operation.type,
            tzKtOperation.hash,
            timestamp,
            amount,
            null,
            formattedFees,
            status,
            source,
            destination,
            null
        )
    }

    private fun parseSwapOperation(operation: SwapOperation): OperationForView {
        val tzKtOperation = operation.tzKtOperations.firstOrNull { it.isSwap() } as TzKtTransaction
        val (timestamp, status) = extractDateAndStatus(tzKtOperation)

        // AMOUNT
        val amountFrom = "- ${operation.amountFrom.formattedValue} ${operation.tokenFrom.symbol}"
        val amountTo = "+ ${operation.amountTo.formattedValue} ${operation.token.symbol}"

        // FEES
        val formattedFees = operation.fees.format(XTZ.symbol)

        // SOURCE - DESTINATION
        // Not shown, no need to find the name
        val destination = tzKtOperation.destination.address.ellipsize()
        val source = tzKtOperation.sender.address.ellipsize()

        // DETAIL
        // 1 token -> ??
        val detail = if (tzKtOperation.status == OperationResultStatus.APPLIED) {
            try {
                val conversion = operation.amountTo.bigDecimal.divide(
                    operation.amountFrom.bigDecimal,
                    6,
                    BigDecimal.ROUND_HALF_UP
                )
                "1 ${operation.tokenFrom.symbol} ↔︎ ${conversion.formatExchangeRate()} ${operation.token.symbol}"
            } catch (e: ArithmeticException) {
                Logger.e("*** ERROR: $e")
                ""
            }
        } else {
            "$amountFrom → $amountTo"
        }

        return OperationForView(
            operation.type,
            tzKtOperation.hash,
            timestamp,
            amountFrom,
            amountTo,
            formattedFees,
            status,
            source,
            destination,
            detail
        )
    }

    private fun parseDelegation(operation: DelegateOperation): OperationForView {
        val tzKtOperation = operation.tzKtOperations[0] as TzKtDelegation
        val (timestamp, status) = extractDateAndStatus(tzKtOperation)

        // FEES
        val formattedFees = operation.fees?.format(XTZ.symbol)

        // SOURCE - DESTINATION
        val source = "" // The user
        val destination = operation.delegate?.let {
            findName(it)
        } ?: "" // Undelegation

        // DETAIL
        val detail = if (operation.delegate == null) {
            // Undelegation, show previous delegate
            tzKtOperation.previousDelegate?.let {
                findName(it)
            }
        } else {
            null
        }

        return OperationForView(
            operation.type,
            tzKtOperation.hash,
            timestamp,
            null,
            null,
            formattedFees,
            status,
            source,
            destination,
            detail
        )
    }

    private fun extractDateAndStatus(operation: TzKtOperation): Pair<Long, OperationResultStatus> {
        val timestamp = (operation.getDate() ?: Date()).time
        val status = operation.status
        return Pair(timestamp, status)
    }

    private fun parseOrigination(operation: OriginateOperation): OperationForView {
        val tzKtOperation = operation.tzKtOperations[0] as TzKtOrigination
        val (timestamp, status) = extractDateAndStatus(tzKtOperation)

        // FEES
        val formattedFees = operation.fees?.format(XTZ.symbol)

        // SOURCE - DESTINATION
        val source = "" // The user
        val destination = tzKtOperation.contract?.address?.ellipsize() ?: ""

        return OperationForView(
            operation.type,
            tzKtOperation.hash,
            timestamp,
            null,
            null,
            formattedFees,
            status,
            source,
            destination,
            null
        )
    }

    private fun parseReveal(operation: RevealOperation): OperationForView {
        val tzKtOperation = operation.tzKtOperations[0] as TzKtReveal
        val (timestamp, status) = extractDateAndStatus(tzKtOperation)
        // FEES
        val formattedFees = operation.fees?.format(XTZ.symbol)

        // SOURCE - DESTINATION
        val source = tzKtOperation.source?.address?.ellipsize() ?: ""
        val destination = "" // The user

        return OperationForView(
            operation.type,
            tzKtOperation.hash,
            timestamp,
            null,
            null,
            formattedFees,
            status,
            source,
            destination,
            null
        )
    }

    private fun parseSmartContractOperation(operation: UnknownOperation): OperationForView {
        val tzKtOperation = operation.tzKtOperations[0] as TzKtTransaction
        val (timestamp, status) = extractDateAndStatus(tzKtOperation)

        // FEES
        val formattedFees = operation.fees?.format(XTZ.symbol)

        // SOURCE - DESTINATION
        val source = findName(tzKtOperation.sender)
        val destination = findName(tzKtOperation.destination)

        // DETAIL
        val detail = tzKtOperation.smartContractCall?.entrypoint ?: destination

        return OperationForView(
            operation.type,
            tzKtOperation.hash,
            timestamp,
            null,
            null,
            formattedFees,
            status,
            source,
            destination,
            detail
        )
    }

    private fun parseDefaultOperation(operation: TezosOperation): OperationForView {
        val tzKtOperation = operation.tzKtOperations[0]
        val (timestamp, status) = extractDateAndStatus(tzKtOperation)

        // FEES
        val formattedFees = operation.fees?.format(XTZ.symbol)

        return OperationForView(
            operation.type,
            tzKtOperation.hash,
            timestamp,
            null,
            null,
            formattedFees,
            status,
            "",
            "",
            null
        )
    }
}
