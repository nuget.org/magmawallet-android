/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.main.contact

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.*
import io.camlcase.smartwallet.data.model.contact.MagmaContact
import io.camlcase.smartwallet.databinding.FragmentContactListBinding
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.model.handleResults
import io.camlcase.smartwallet.ui.base.BindBaseFragment
import io.camlcase.smartwallet.ui.base.navigate.AppNavigator
import io.camlcase.smartwallet.ui.base.showError
import io.camlcase.smartwallet.ui.exchange.amount.MainToContacts
import io.camlcase.smartwallet.ui.permissions.ReadContactsPermission

/**
 * Show filtered contacts with tz1 address
 */
class ContactListFragment : BindBaseFragment(R.layout.fragment_contact_list), MainToContacts {
    interface ContactListListener {
        fun onContactClick(contact: MagmaContact)
    }

    private val binding by viewBinding(FragmentContactListBinding::bind)
    private lateinit var viewModel: ContactListViewModel
    private var listener: ContactListListener? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initToolbar()
        binding.contactList.apply {
            layoutManager = LinearLayoutManager(activity)
            setHasFixedSize(true)
        }

        initPullToRefresh()
        bindViews()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as? ContactListListener
        viewModel = viewModel(viewModelFactory) {
            observe(data, { handleData(it) })
        }
    }

    override fun onStart() {
        super.onStart()
        // If user changes permissions and comes back, screen should react
        refresh()
    }

    private fun handleData(wrapper: DataWrapper<List<MagmaContact>>) {
        wrapper.handleResults(
            onValidData = {
                hideLoading()
                binding.emptyStatus?.hide()
                binding.contactList?.show()
                // Populate list
                binding.contactList?.adapter = ContactListAdapter(it) { contact, view ->
                    view.setOnClickListener {
                        listener?.onContactClick(contact)
                    }
                }
            },
            onError = {
                hideLoading()
                showError(it)
                if (binding.contactList.adapter?.itemCount == 0) {
                    showEmpty()
                }
            },
            onEmpty = {
                hideLoading()
                showEmpty()
            },
            onLoading = {
                showLoading()
            }
        )
    }

    private fun initToolbar() {
        setupToolbar(binding.toolbar)
    }

    private fun showEmpty() {
        binding.emptyStatus?.show()
        binding.contactList?.hide()
    }

    private fun bindViews() {
        binding.actionAddContact.throttledClick()
            .subscribe {
                AppNavigator.navigateToAddContact(context)
            }.addToDisposables(disposables)
    }

    private fun initPullToRefresh() {
        binding.swipeRefresh.setOnRefreshListener {
            refresh()
        }
    }

    override fun refresh() {
        viewModel.getContacts(ReadContactsPermission.hasPermission(activity))
    }

    private fun showLoading() {
        binding.swipeRefresh.isRefreshing = true
    }

    private fun hideLoading() {
        binding.swipeRefresh.isRefreshing = false
    }
}
