/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.main.contact

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.TezosNetwork
import io.camlcase.smartwallet.core.architecture.BaseViewModel
import io.camlcase.smartwallet.core.extension.subscribeForUI
import io.camlcase.smartwallet.data.model.contact.ContactTezosAddress
import io.camlcase.smartwallet.data.model.contact.MagmaContact
import io.camlcase.smartwallet.data.usecase.contact.ListContactsUseCase
import io.camlcase.smartwallet.model.DataWrapper
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class ContactListViewModel @Inject constructor(
    private val useCase: ListContactsUseCase,
    private val tezosNetwork: TezosNetwork
) : BaseViewModel() {

    /**
     * List of all contacts with a "Tezos Address" mimeType for the current Tezos network
     */
    private val _data = MutableLiveData<DataWrapper<List<MagmaContact>>>()
    val data: LiveData<DataWrapper<List<MagmaContact>>> = _data

    private var contacts: List<MagmaContact>? = null

    fun getContacts(hasPermission: Boolean) {
        if (hasPermission) {
            _data.value = DataWrapper.Loading()
            Single.fromCallable { useCase.getMagmaContacts() }
                .subscribeForUI()
                .subscribe(
                    {
                        contacts = it
                        if (it.isEmpty()) {
                            _data.value = DataWrapper.Empty()
                        } else {
                            _data.value = DataWrapper.Success(it)
                        }
                    },
                    {
                        _data.value = DataWrapper.Error(it)
                    }
                )
        } else {
            _data.value = DataWrapper.Empty()
        }
    }

    fun createMagmaContact(address: Address): MagmaContact {
        return MagmaContact(0L, "", listOf(ContactTezosAddress(address, tezosNetwork)))
    }

    /**
     * @return null If it doesn't exist
     */
    fun existsInContacts(address: Address): MagmaContact? {
        return contacts?.firstOrNull { it.address == address }
    }
}
