/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.main.contact.detail

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.*
import io.camlcase.smartwallet.data.analytic.report
import io.camlcase.smartwallet.data.model.contact.MagmaContact
import io.camlcase.smartwallet.databinding.FragmentContactDetailBinding
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.model.handleResults
import io.camlcase.smartwallet.model.navigation.SendNavigation
import io.camlcase.smartwallet.ui.base.*
import io.camlcase.smartwallet.ui.base.navigate.AppNavigator
import io.camlcase.smartwallet.ui.base.navigate.RequestCode
import io.camlcase.smartwallet.ui.custom.ScrollOffsetListener
import io.camlcase.smartwallet.ui.main.contact.edit.AddContactActivity

class ContactDetailFragment : BindBaseFragment(R.layout.fragment_contact_detail) {
    interface ContactDetailListener {
        fun editContact(contact: MagmaContact)
        fun onContactDeleted()
    }

    private val binding by viewBinding(FragmentContactDetailBinding::bind)
    private lateinit var viewModel: ContactDetailViewModel
    private var selectedContact: MagmaContact? = null
    private var listener: ContactDetailListener? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        manageIntent(arguments)
        listener = context as? ContactDetailListener
        if (listener == null) {
            throw ClassCastException("$context must implement ContactDetailListener")
        }
        viewModel = viewModel(viewModelFactory) {
            observe(delete, { handleDelete(it) })
        }
    }

    private fun manageIntent(bundle: Bundle?) {
        bundle?.getParcelable<MagmaContact>(ARG_CONTACT)?.let {
            selectedContact = it
        } ?: throw IllegalStateException("Screen needs an $ARG_CONTACT argument")
    }

    private fun handleDelete(wrapper: DataWrapper<Boolean>) {
        wrapper.handleResults(
            onValidData = {
                hideLoading()
                if (it) {
                    // Leave screen
                    listener?.onContactDeleted()
                    activity?.onBackPressed()
                } else {
                    showSnackbar(getString(R.string.error_generic) + " " + getString(R.string.error_try_again))
                }
            },
            onLoading = {
                showLoading()
            },
            onError = {
                hideLoading()
                showError(it)
            }
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initToolbar(selectedContact)
        bindViews()
        initList()
        bindScreenResults()
    }

    private fun initToolbar(contact: MagmaContact?) {
        setupToolbar(binding.toolbar, true)
        contact?.apply {
            binding.contactName.text = this.name
            binding.contactAddress.text = this.tezosAddresses[0].address

            binding.appBar.addOnOffsetChangedListener(
                ScrollOffsetListener(
                    binding.expandedTitle,
                    binding.toolbar,
                    this.name
                )
            )
        }
    }

    private fun bindViews() {
        binding.contactAddress.throttledClick()
            .subscribe {
                selectedContact?.address?.apply {
                    activity.copyToClipBoard(this, { it.showSnackbar(R.string.action_copy_success) })
                }
            }
            .addToDisposables(disposables)
    }

    private fun initList() {
        binding.list.apply {
            layoutManager = LinearLayoutManager(activity)
            setHasFixedSize(true)
            addLineDivider()
        }
        val canSend = viewModel.sendableTokens().isNotEmpty()
        binding.list.adapter = ContactOptionsAdapter(canSend = canSend) { option, view ->
            view.setOnClickListener {
                when (option) {
                    ContactOption.SEND -> {
                        if (canSend) {
                            selectedContact?.apply {
                                AppNavigator.navigateToSend(
                                    activity,
                                    SendNavigation.SOURCE_CONTACT_DETAIL,
                                    viewModel.getSendToken(),
                                    this
                                )
                            }
                        } else {
                            showSnackbar(getString(R.string.wlt_empty_status))
                        }
                    }
                    ContactOption.ACTIVITY -> {
                        selectedContact?.apply {
                            AppNavigator.navigateToContactActivity(context, this)
                        }
                    }
                    ContactOption.EDIT -> {
                        selectedContact?.apply {
                            listener?.editContact(this)
                        }
                    }
                    ContactOption.DELETE -> onDeleteClick()
                }
            }
        }
    }

    private fun onDeleteClick() {
        context?.apply {
            okOrCancelDialog(
                this,
                title = getString(R.string.con_delete_title),
                message = getString(R.string.con_delete_desc, selectedContact?.name ?: ""),
                cancelable = true,
                okColor = this.getColor(android.R.color.holo_red_dark),
                okString = getString(R.string.action_confirm),
                onOK = {
                    selectedContact?.apply {
                        viewModel.deleteContact(this)
                    }
                }
            )
        }
    }

    private fun bindScreenResults() {
        activityResult.get()?.bind(
            RequestCode.ADD_CONTACT,
            disposable = disposables,
            onResult = { activityResult ->
                activityResult.data?.getParcelableExtra<MagmaContact>(AddContactActivity.ARG_CONTACT)?.apply {
                    selectedContact = this
                    initToolbar(this)
                }
            },
            onError = { it.report() }
        )
    }

    private fun showLoading() {
        binding.loading.root.show()
        activity.disableTouch()
    }

    private fun hideLoading() {
        binding.loading.root.hide()
        activity.enableTouch()
    }

    companion object {
        private const val ARG_CONTACT = "arg.contact"

        fun init(arg: MagmaContact): ContactDetailFragment {
            val fragment = ContactDetailFragment()
            val args = Bundle()
            args.putParcelable(ARG_CONTACT, arg)
            fragment.arguments = args
            return fragment
        }
    }
}
