/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.main.contact.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.camlcase.smartwallet.core.architecture.BaseViewModel
import io.camlcase.smartwallet.core.extension.subscribeForUI
import io.camlcase.smartwallet.data.model.contact.MagmaContact
import io.camlcase.smartwallet.data.model.exchange.TokenInfoBundle
import io.camlcase.smartwallet.data.usecase.contact.WriteContactUseCase
import io.camlcase.smartwallet.data.usecase.info.GetBCDBalanceUseCase
import io.camlcase.smartwallet.model.DataWrapper
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class ContactDetailViewModel @Inject constructor(
    private val useCase: WriteContactUseCase,
    private val balanceUseCase: GetBCDBalanceUseCase
) : BaseViewModel() {

    /**
     * Results of the DELETE operation
     * @see [deleteContact]
     */
    private val _delete = MutableLiveData<DataWrapper<Boolean>>()
    val delete: LiveData<DataWrapper<Boolean>> = _delete

    fun sendableTokens(): List<TokenInfoBundle>{
        return balanceUseCase.getLocalBalances().filter { !it.balance.isZero }
    }

    /**
     * If user has balance of only one token, it will return it
     *
     * If it has more than one or none at all, null
     */
    fun getSendToken(): TokenInfoBundle? {
        val localBalances = sendableTokens()
        if (localBalances.isNotEmpty() && localBalances.size == 1) {
            return localBalances[0]
        }

        return null
    }

    fun deleteContact(contact: MagmaContact) {
        _delete.value = DataWrapper.Loading()
        Single.fromCallable {
            useCase.deleteContact(contact)
        }.subscribeForUI()
            .subscribe(
                { _delete.value = DataWrapper.Success(it) },
                { _delete.value = DataWrapper.Error(it) }
            )
    }
}
