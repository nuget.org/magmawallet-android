/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.main.contact.detail

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import io.camlcase.smartwallet.R
import kotlinx.android.synthetic.main.row_contact_option.view.*

/**
 * List of options for a Contact
 *
 * For now they are static, the same for all we list.
 */
class ContactOptionsAdapter(
    private val values: List<ContactOption> = ContactOption.values().toList(),
    private val canSend: Boolean,
    private val onItemClick: (ContactOption, View) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val name: TextView = view.option_name
        val icon: ImageView = view.option_icon
    }

    override fun getItemCount(): Int {
        return values.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_contact_option, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = values[position]
        val viewHolder = holder as ViewHolder
        val context = viewHolder.itemView.context

        // Option text
        viewHolder.name.setText(
            when (item) {
                ContactOption.SEND -> R.string.con_details_send
                ContactOption.ACTIVITY -> R.string.con_details_history
                ContactOption.EDIT -> R.string.con_details_edit
                ContactOption.DELETE -> R.string.con_details_delete
            }
        )
        // Option name
        val drawable = when (item) {
            ContactOption.SEND -> R.drawable.ic_send
            ContactOption.ACTIVITY -> R.drawable.ic_activity
            ContactOption.EDIT -> R.drawable.ic_edit
            ContactOption.DELETE -> R.drawable.ic_delete
        }
        viewHolder.icon.setImageDrawable(context.getDrawable(drawable))

        if (item == ContactOption.SEND && !canSend) {
            holder.itemView.alpha = .5f
        } else {
            holder.itemView.alpha = 1f
        }
        onItemClick(item, holder.itemView)
    }
}

enum class ContactOption {
    SEND,
    ACTIVITY,
    EDIT,
    DELETE
}
