/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.main.settings

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.biometric.BiometricManager
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.URL
import io.camlcase.smartwallet.core.extension.*
import io.camlcase.smartwallet.data.source.local.AppPreferences
import io.camlcase.smartwallet.databinding.FragmentSettingsBinding
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.model.handleResults
import io.camlcase.smartwallet.ui.base.*
import io.camlcase.smartwallet.ui.base.navigate.AppNavigator
import io.camlcase.smartwallet.ui.base.navigate.RequestCode
import io.camlcase.smartwallet.ui.exchange.amount.MainToSettings
import io.camlcase.smartwallet.ui.main.token.SideDataViewModel
import io.camlcase.smartwallet.ui.security.usesBiometric

/**
 * Show [SettingType] list
 */
class SettingsFragment : BindBaseFragment(R.layout.fragment_settings), MainToSettings {
    interface SettingsListener {
        fun onRecoveryPhraseClick()
        fun onEditPinClick()
    }

    private val binding by viewBinding(FragmentSettingsBinding::bind)
    private lateinit var viewModel: SettingsViewModel
    private lateinit var delegateRefresh: SideDataViewModel
    private var listener: SettingsListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_settings, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as? SettingsListener
        viewModel = viewModel(viewModelFactory) {
            observe(data, { handleData(it) })
        }
        // Share view model with other screens
        baseActivity?.apply {
            // Listen when there's a refresh so screen is updated properly
            delegateRefresh = this.viewModel(viewModelFactory) {
                observe(bakersUpdate, { initSettings() })
            }
        }
        bindScreenResults()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initToolbar()
        binding.settingsList.apply {
            layoutManager = LinearLayoutManager(activity)
            setHasFixedSize(true)
            addItemDecoration(SettingsItemDecoration())
        }
        initSettings()
    }

    private fun initToolbar() {
        setupToolbar(binding.toolbar)
    }

    private fun bindScreenResults() {
        activityResult.get().bindAny(
            RequestCode.DELEGATE_BAKER,
            disposable = disposables,
            onResult = { viewModel.updateDelegate() },
            onError = { showError(it) })

        activityResult.get().bindAny(
            RequestCode.MIGRATE_WALLET,
            disposable = disposables,
            onResult = { viewModel.updateDelegate() },
            onError = { showError(it) })

        activityResult.get().bind(
            RequestCode.LOCAL_CURRENCIES,
            disposable = disposables,
            onResult = { viewModel.updateDelegate() },
            onError = { showError(it) })
    }

    private fun initSettings() {
        val biometricArg = if (context.usesBiometric() == BiometricManager.BIOMETRIC_SUCCESS) {
            AppPreferences.BIOMETRIC_LOGIN_ENABLED
        } else {
            ""
        }
        viewModel.initSettings(biometricArg)
    }

    private fun handleData(wrapper: DataWrapper<List<SettingsItem>>) {
        wrapper.handleResults(
            onValidData = {
                hideLoading()
                binding.settingsList.adapter = SettingsAdapter(it) { item, _ ->
                    onSettingsClick(item)
                }
            },
            onError = {
                hideLoading()
                showError(it)
            },
            onLoading = {
                showLoading()
            }
        )
    }

    private fun showLoading() {
        binding.settingsLoading.root.show()
    }

    private fun hideLoading() {
        binding.settingsLoading.root.hide()
    }

    private fun onSettingsClick(item: SettingsForView) {
        when (item.type) {
            SettingType.BIOMETRIC_LOGIN -> {
                viewModel.updateBiometricLogin(item.checked)
            }
            SettingType.DELEGATE -> {
                AppNavigator.navigateToDelegate(activity)
            }
            SettingType.DELETE_WALLET -> {
                showDeleteWarning()
            }
            SettingType.EDIT_PIN -> {
                listener?.onEditPinClick()
            }
            SettingType.RECOVERY_PHRASE -> {
                listener?.onRecoveryPhraseClick()
            }
            SettingType.FEEDBACK -> {
                navigateToUrl(URL.FEEDBACK_URL)
            }
            SettingType.ABOUT -> {
                navigateToUrl(URL.ABOUT_URL)
            }
            SettingType.PRIVACY_POLICY -> {
                navigateToUrl(URL.PRIVACY_POLICY_URL)
            }
            SettingType.TERMS -> {
                navigateToUrl(URL.TERMS_URL)
            }
            SettingType.NEWSLETTER -> {
                navigateToUrl(URL.NEWSLETTER_URL)
            }
            SettingType.LOCAL_CURRENCY -> {
                AppNavigator.navigateToLocalCurrencies(activity)
            }
            SettingType.MIGRATE_WALLET -> {
                AppNavigator.navigateToMigrateWallet(activity)
            }
            SettingType.ATTRIBUTIONS -> {
                AppNavigator.navigateToAttributions(activity)
            }
            SettingType.NETWORK_CONFIG -> AppNavigator.navigateToNetworkConfig(activity)
            else -> {
                /*Nothing to do*/
            }
        }
    }

    private fun showDeleteWarning() {
        activity?.apply {
            okOrCancelDialog(
                this,
                title = getString(R.string.set_unpair_popup_title),
                message = getString(R.string.set_unpair_popup_body),
                okColor = this.getColor(android.R.color.holo_red_dark),
                okString = R.string.set_unpair_popup_action,
                onOK = {
                    viewModel.deleteWallet {
                        AppNavigator.navigateToLaunchActivity(activity)
                    }
                }
            )
        }
    }

    override fun reloadList() {
        initSettings()
    }
}
