/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.main.settings.attributions

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.setupToolbar
import io.camlcase.smartwallet.core.extension.viewModel
import io.camlcase.smartwallet.databinding.FragmentAttributionsBinding
import io.camlcase.smartwallet.ui.base.BindBaseFragment
import io.camlcase.smartwallet.ui.base.navigate.AppNavigator
import io.camlcase.smartwallet.ui.main.settings.SettingsItemDecoration
import io.camlcase.smartwallet.ui.main.settings.SettingsItemDecoration.Companion.NO_HEADERS

/**
 * Give credit to third-party providers
 */
class AttributionsFragment : BindBaseFragment(R.layout.fragment_attributions) {
    private val binding by viewBinding(FragmentAttributionsBinding::bind)
    private lateinit var viewModel: AttributionsViewModel

    override fun onAttach(context: Context) {
        super.onAttach(context)
        viewModel = viewModel(viewModelFactory) {}
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupToolbar(binding.toolbar, true)
        binding.list?.apply {
            layoutManager = LinearLayoutManager(activity)
            setHasFixedSize(true)
            addItemDecoration(SettingsItemDecoration(NO_HEADERS))
        }

        binding.list.adapter = AttributionsAdapter(viewModel.getAttributionList()) { url, rowView ->
            rowView.setOnClickListener {
                AppNavigator.navigateToUrl(activity, url)
            }
        }
    }

    companion object {
        fun init(): AttributionsFragment {
            return AttributionsFragment()
        }
    }
}
