/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.migration

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import io.camlcase.kotlintezos.model.OperationResultStatus
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.*
import io.camlcase.smartwallet.data.core.Logger
import io.camlcase.smartwallet.data.core.XTZ
import io.camlcase.smartwallet.data.core.extension.format
import io.camlcase.smartwallet.data.model.operation.MagmaOperationResult
import io.camlcase.smartwallet.data.usecase.analytics.AnalyticEvent
import io.camlcase.smartwallet.model.navigation.SeedPhraseNavigation
import io.camlcase.smartwallet.model.navigation.SeedPhraseConfirm
import io.camlcase.smartwallet.ui.base.BaseActivity
import io.camlcase.smartwallet.ui.base.ErrorMessageParser
import io.camlcase.smartwallet.ui.base.findTezosError
import io.camlcase.smartwallet.ui.base.info.InfoFragment
import io.camlcase.smartwallet.ui.base.info.InfoType
import io.camlcase.smartwallet.ui.base.info.InfoView
import io.camlcase.smartwallet.ui.delegate.IncludedOperation
import io.camlcase.smartwallet.ui.migration.funds.MigrateFundsConfirmationFragment
import io.camlcase.smartwallet.ui.migration.funds.MigrationBalanceCheckFragment
import io.camlcase.smartwallet.ui.migration.funds.MigrationTransferFundsType
import io.camlcase.smartwallet.ui.onboarding.seed_phrase.SeedPhraseConfirmFragment
import io.camlcase.smartwallet.ui.onboarding.seed_phrase.SeedPhraseFragment
import io.camlcase.smartwallet.ui.operation.WaitForInclusionFragment

/**
 * Takes the user through the migration flow
 */
class MigrationActivity : BaseActivity(R.layout.activity_migration),
    MigrationBalanceCheckFragment.BalanceCheckListener,
    SeedPhraseFragment.SeedPhraseListener,
    SeedPhraseConfirmFragment.SeedPhraseConfirmListener,
    InfoView.InfoEventsListener,
    WaitForInclusionFragment.WaitForInclusionListener,
    MigrateFundsConfirmationFragment.FundsMigrationListener {

    private lateinit var viewModel: FinalMigrationViewModel
    private var transferType: MigrationTransferFundsType = MigrationTransferFundsType.NO_TRANSFER

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        listenToBackground()

        viewModel = viewModel(viewModelFactory) {}

        if (savedInstanceState == null) {
            navigateToInitialScreen()
        }
    }

    private fun navigateToInitialScreen() {
        replaceFragmentSafely(MigrationBalanceCheckFragment.init(), R.id.container_fragment)
        viewModel.sendEvent(AnalyticEvent.MIGRATION_BALANCE_CHECK)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString(ARG_TRANSFER_TYPE, transferType.name)
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        savedInstanceState.getString(ARG_TRANSFER_TYPE)?.let {
            transferType = MigrationTransferFundsType.valueOf(it)
        }
        super.onRestoreInstanceState(savedInstanceState)
    }

    fun success() {
        setResult(Activity.RESULT_OK)
        finishAfterTransition()
    }

    fun cancel() {
        setResult(Activity.RESULT_CANCELED)
        finishAfterTransition()
    }

    override fun onProgressAction() {
        // Waiting for inclusion SKIP will never happen because we are not showing the button
    }

    fun showMigrationSuccess() {
        val newAddress = viewModel.getNewStandardAddress()
        popFullBackstack()
        replaceFragmentSafely(MigrationSuccessFragment.init(newAddress), R.id.container_fragment)
    }

    private fun finalizeMigration() {
        viewModel.swapMigrationWalletToStandardWallet()
        viewModel.sendEvent(AnalyticEvent.MIGRATION_SUCCESS)
    }

    override fun onInclusion(info: MagmaOperationResult) {
        if (info.status == OperationResultStatus.APPLIED) {
            // We only swap when we know funds were transferred correctly
            finalizeMigration()
        }
        if (info.status == OperationResultStatus.APPLIED) {
            showMigrationSuccess()
        } else {
            viewModel.sendEvent(AnalyticEvent.MIGRATION_ERROR)
            val parser = ErrorMessageParser(applicationContext)
            val errorMessage = info.parsedError?.let { parser.parseMagmeErrorMessage(it) } ?: parser.genericMessage
            loadFragment(InfoFragment.init(InfoType.MIGRATION_INCLUSION_ERROR, errorMessage), R.id.container_fragment)
        }
    }

    /**
     * There's many situations where Conseil fails but the operation has actually gone through.
     * Check the balance of the newly created wallet to see.
     */
    override fun onInclusionError(error: Throwable?) {
        viewModel.checkMigrationWalletBalance(
            onPositiveBalance = {
                showMigrationSuccess()
            },
            onEmptyBalance = {
                navigateToMigrateFundsError(error)
            }
        )
    }

    private fun navigateToMigrateFundsError(error: Throwable?) {
        viewModel.sendEvent(AnalyticEvent.MIGRATION_ERROR)
        loadFragment(
            InfoFragment.init(InfoType.MIGRATION_INCLUSION_ERROR, findTezosError(error)),
            R.id.container_fragment
        )
    }

    override fun onActionClick(infoType: InfoType) {
        when (infoType) {
            InfoType.MIGRATION_INSUFFICIENT_FUNDS_WARNING -> {
                // User pressed "Continue to new wallet"
                navigateToSeedPhrase()
            }
            InfoType.MIGRATION_SEED_PHRASE_INFO -> {
                replaceFragmentSafely(
                    SeedPhraseFragment.init(SeedPhraseNavigation.SOURCE_MIGRATION),
                    R.id.container_fragment
                )
                viewModel.sendEvent(AnalyticEvent.MIGRATION_SEED_PHRASE)
            }
            InfoType.MIGRATION_SUCCESS -> {
                if (transferType == MigrationTransferFundsType.SHOULD_TRANSFER) {
                    // Show extra information about bakers
                    replaceFragmentSafely(InfoFragment.init(InfoType.MIGRATION_SUCCESS_BAKER), R.id.container_fragment)
                } else {
                    success()
                }
            }
            InfoType.MIGRATION_SUCCESS_BAKER -> {
                success()
            }
            InfoType.MIGRATION_INCLUSION_ERROR -> {
                navigateToInitialScreen()
            }
            else -> {
                Logger.i("InfoFragment of type not supported $infoType")
            }
        }
    }

    /**
     * Error click
     */
    override fun onSecondaryActionClick(infoType: InfoType) {
        cancel()
    }

    override fun onSeedPhraseShown() {
        // Show backup confirm to ensure user has written down the migration mnemonic correctly
        displayFragment(SeedPhraseConfirmFragment.init(SeedPhraseConfirm.SOURCE_MIGRATION), R.id.container_fragment)
    }

    override fun onSeedPhraseConfirmed() {
        if (transferType == MigrationTransferFundsType.SHOULD_TRANSFER) {
            // Transfer funds
            replaceFragmentSafely(MigrateFundsConfirmationFragment.init(), R.id.container_fragment)
            viewModel.sendEvent(AnalyticEvent.MIGRATION_TRANSFER_FUNDS)
        } else {
            // User doesn't need to transfer anything, we can finish
            finalizeMigration()
            showMigrationSuccess()
        }
    }

    override fun onNoTransfer() {
        transferType = MigrationTransferFundsType.NO_TRANSFER
        navigateToSeedPhrase()
    }

    override fun onInsufficientBalance(xtzNeeded: Tez) {
        transferType = MigrationTransferFundsType.NO_XTZ_BALANCE
        replaceFragmentSafely(
            InfoFragment.init(InfoType.MIGRATION_INSUFFICIENT_FUNDS_WARNING, xtzNeeded.format(XTZ.symbol)),
            R.id.container_fragment
        )
        viewModel.sendEvent(AnalyticEvent.MIGRATION_INSUFFICIENT_FUNDS)
    }

    override fun onTransferNeeded() {
        transferType = MigrationTransferFundsType.SHOULD_TRANSFER
        navigateToSeedPhrase()
    }

    override fun onBalanceError(error: Throwable?) {
        finishAfterTransition()
    }

    private fun navigateToSeedPhrase() {
        replaceFragmentSafely(InfoFragment.init(InfoType.MIGRATION_SEED_PHRASE_INFO), R.id.container_fragment)
    }

    override fun onMigratedFunds(operationHash: String) {
        val title = getString(R.string.mig_funds_injection_message)
        replaceFragmentSafely(
            WaitForInclusionFragment.init(
                operationHash,
                IncludedOperation.TRANSFER_FUNDS,
                title,
                null
            ),
            R.id.container_fragment
        )
        viewModel.sendEvent(AnalyticEvent.MIGRATION_WAIT_FOR_INJECTION)
    }

    override fun onConfirmationError(error: Throwable?) {
        navigateToMigrateFundsError(error)
    }

    companion object {
        private const val ARG_TRANSFER_TYPE = "args.migration.transfer_type"
        fun init(context: Context): Intent {
            return Intent(context, MigrationActivity::class.java)
        }
    }
}
