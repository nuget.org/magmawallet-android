/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.migration.funds

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.smartwallet.core.architecture.BaseViewModel
import io.camlcase.smartwallet.core.extension.addToDisposables
import io.camlcase.smartwallet.core.extension.subscribeForUI
import io.camlcase.smartwallet.data.core.XTZ
import io.camlcase.smartwallet.data.core.extension.format
import io.camlcase.smartwallet.data.core.extension.formatAsMoney
import io.camlcase.smartwallet.data.core.extension.tezos.convertToCurrency
import io.camlcase.smartwallet.data.model.exchange.TokenInfoBundle
import io.camlcase.smartwallet.data.model.isXTZ
import io.camlcase.smartwallet.data.usecase.info.GetBCDBalanceUseCase
import io.camlcase.smartwallet.data.usecase.migration.MigrateFundsUseCase
import io.camlcase.smartwallet.model.DataWrapper
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.functions.BiFunction
import java.math.BigDecimal
import javax.inject.Inject

class MigrateFundsViewModel @Inject constructor(
    private val useCase: GetBCDBalanceUseCase,
    private val migrateFundsUseCase: MigrateFundsUseCase
) : BaseViewModel() {

    /**
     * Confirmation info
     */
    private val _data = MutableLiveData<DataWrapper<FundsMigrationForView>>()
    val data: LiveData<DataWrapper<FundsMigrationForView>> = _data

    /**
     * Response of the transfer operation
     */
    private val _operation = MutableLiveData<DataWrapper<String>>()
    val operation: LiveData<DataWrapper<String>> = _operation

    private var totalFees: Tez = Tez.zero
    private var totalBalanceInCurrency = BigDecimal.ZERO
    private var xtzBalance: TokenInfoBundle = TokenInfoBundle.empty

    fun getConfirmationDetails() {
        _data.value = DataWrapper.Loading()

        Single.zip(
            useCase.fallbackBalances(),
            migrateFundsUseCase.calculateFees(),
            BiFunction { balances, totalFees -> parseBalances(balances, totalFees) }
        )
            .subscribeForUI()
            .subscribe(
                {
                    _data.value = DataWrapper.Success(it)
                    clearOperationSubscription()
                },
                {
                    _data.value = DataWrapper.Error(it)
                    clearOperationSubscription()
                }
            ).addToDisposables(operationDisposables)
    }

    private fun parseBalances(list: List<Result<TokenInfoBundle>>, totalFees: Tez): FundsMigrationForView {
        this.totalFees = totalFees
        var formattedTotalBalance = ""
        var error: Throwable? = null
        val balancesResult = list.sortedBy { it.getOrNull()?.token }
        for (item in balancesResult) {
            if (item.isFailure) {
                error = item.exceptionOrNull()
            }
            val tokenItem = item.getOrNull()
            tokenItem?.apply {
                if (!this.balance.isZero) {
                    if (this.token.isXTZ()) {
                        xtzBalance = this
                    }
                    val inCurrency = this.balance.inCurrency(this.exchange.currencyExchange)
                    totalBalanceInCurrency += inCurrency
                    if (formattedTotalBalance.isBlank()) {
                        formattedTotalBalance += this.balance.format(this.token)
                    } else {
                        formattedTotalBalance += "\n+ ${this.balance.format(this.token)}"
                    }
                }
            }
        }

        val currencyBundle = useCase.getCurrencyBundle()
        val currencyPrice = xtzBalance.exchange.currencyExchange
        val feesInCurrency = totalFees.convertToCurrency(currencyPrice).formatAsMoney(currencyBundle)
        return FundsMigrationForView(
            formattedTotalBalance,
            totalBalanceInCurrency.formatAsMoney(currencyBundle),
            totalFees.format(XTZ.symbol),
            feesInCurrency,
            error
        )
    }

    fun migrateFunds() {
        _operation.value = DataWrapper.Loading()

        migrateFundsUseCase.transferFunds(totalFees)
            .subscribeForUI()
            .subscribe(
                {
                    _operation.value = DataWrapper.Success(it)
                    clearOperationSubscription()
                },
                {
                    _operation.value = DataWrapper.Error(it)
                    clearOperationSubscription()
                }
            )
            .addToDisposables(operationDisposables)
    }
}

data class FundsMigrationForView(
    val toSend: String,
    val toSendInCurrency: String,
    val fees: String,
    val feesInCurrency: String,
    val error: Throwable?
)

enum class MigrationTransferFundsType {
    /**
     * Zero balance, no need to do a transfer
     */
    NO_TRANSFER,

    /**
     * Needs transfer but xtz funds to do so are too low
     */
    NO_XTZ_BALANCE,

    /**
     * Needs transfer and has xtz funds for it
     */
    SHOULD_TRANSFER
}
