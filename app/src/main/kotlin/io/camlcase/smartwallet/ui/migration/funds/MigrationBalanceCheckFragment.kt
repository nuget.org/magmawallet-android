/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.migration.funds

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.hide
import io.camlcase.smartwallet.core.extension.observe
import io.camlcase.smartwallet.core.extension.show
import io.camlcase.smartwallet.core.extension.viewModel
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.model.handleResults
import io.camlcase.smartwallet.model.token.TokenForView
import io.camlcase.smartwallet.ui.base.BaseFragment
import io.camlcase.smartwallet.ui.base.showErrorSnackbar
import io.camlcase.smartwallet.ui.main.token.TokenListViewModel
import kotlinx.android.synthetic.main.fragment_create_wallet.*
import kotlinx.android.synthetic.main.fragment_withdrawal_limit.*

/**
 * Check funds situation first
 */
class MigrationBalanceCheckFragment : BaseFragment() {
    interface BalanceCheckListener {
        fun onNoTransfer()
        fun onInsufficientBalance(xtzNeeded: Tez)
        fun onTransferNeeded()
        fun onBalanceError(error: Throwable?)
    }

    private var listener: BalanceCheckListener? = null

    private lateinit var viewModel: BalanceCheckViewModel
    private lateinit var balanceViewModel: TokenListViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_create_wallet, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as? BalanceCheckListener
        if (listener == null) {
            throw ClassCastException("$context must implement CreateWalletListener")
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = viewModel(viewModelFactory) {
            observe(data, { handleData(it) })
        }
        balanceViewModel = viewModel(viewModelFactory) {
            observe(data, { handleBalances(it) })
        }
        balanceViewModel.getTokens()
        information_text.setText(R.string.mig_balance_check_info)
    }

    private fun handleBalances(wrapper: DataWrapper<List<TokenForView>>) {
        wrapper.handleResults(
            onValidData = {
                viewModel.getBalanceCheck(balanceViewModel.tokens)
            },
            onError = {
                hideLoading()
                handleError(it)
            },
            onLoading = {
                showLoading()
            }
        )
    }

    private fun handleData(wrapper: DataWrapper<BalanceCheckForView>) {
        wrapper.handleResults(
            onValidData = {
                hideLoading()
                when (it.transferFundsType) {
                    MigrationTransferFundsType.NO_XTZ_BALANCE -> {
                        listener?.onInsufficientBalance(it.neededXTZ)
                    }
                    MigrationTransferFundsType.NO_TRANSFER -> {
                        listener?.onNoTransfer()
                    }
                    else -> {
                        listener?.onTransferNeeded()
                    }
                }
            },
            onError = {
                hideLoading()
                handleError(it)
            },
            onLoading = {
                showLoading()
            }
        )
    }

    private fun handleError(error: Throwable?) {
        view.showErrorSnackbar(error)
        // finish and go back
        view?.postDelayed({
            listener?.onBalanceError(error)
        }, 1000)
    }

    fun showLoading() {
        view_loading.show()
    }

    fun hideLoading() {
        view_loading.hide()
    }

    companion object {
        fun init(): MigrationBalanceCheckFragment {
            return MigrationBalanceCheckFragment()
        }
    }
}
