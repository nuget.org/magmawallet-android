/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.money

import io.camlcase.kotlintezos.model.Address
import io.camlcase.smartwallet.core.architecture.BaseViewModel
import io.camlcase.smartwallet.data.model.money.MoonPayTransactionStatus
import io.camlcase.smartwallet.data.usecase.analytics.AnalyticEvent
import io.camlcase.smartwallet.data.usecase.analytics.SendAnalyticUseCase
import io.camlcase.smartwallet.data.usecase.money.BuyTezDelegate
import io.camlcase.smartwallet.data.usecase.money.GetMoonPayDataUseCase
import javax.inject.Inject

class BuyTezViewModel @Inject constructor(
    private val delegate: BuyTezDelegate,
    private val useCase: GetMoonPayDataUseCase,
    private val analyticUseCase: SendAnalyticUseCase
) : BaseViewModel() {

    /**
     * The option to buy XTZ with money is only visible to users in regions supported by our provider.
     */
    fun showBuyOption(countryCode: String): Boolean {
        return useCase.moonPaySupported(countryCode)
    }

    /**
     * Get the uri to launch the fiat-to-xtz provider
     */
    fun getBuyUrl(languageCode: String): String {
        return delegate.getBuyUrl(languageCode)
    }

    /**
     * We don't pass the user address to the provider so we don't need to sign the url
     */
    fun getUserAddress(): Address {
        return delegate.getUserAddress()
    }

    fun parseFinishUri(uri: String): MoonPayTransactionStatus? {
        val (id, status) = BuyTezDelegate.extractTransactionDetails(uri)
        if (status != null) {
            sendEvent(status)
        }
        return status
    }

    private fun sendEvent(status: MoonPayTransactionStatus) {
        val event = when (status) {
            MoonPayTransactionStatus.COMPLETED -> AnalyticEvent.MOONPAY_BUY_COMPLETE
            MoonPayTransactionStatus.WAITING_AUTHORIZATION,
            MoonPayTransactionStatus.WAITING_PAYMENT,
            MoonPayTransactionStatus.PENDING -> AnalyticEvent.MOONPAY_BUY_PENDING
            MoonPayTransactionStatus.FAILED -> AnalyticEvent.MOONPAY_BUY_ERROR
        }
        analyticUseCase.reportEvent(event)
    }
}
