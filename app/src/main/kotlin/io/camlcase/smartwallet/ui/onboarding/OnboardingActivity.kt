/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.onboarding

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.biometric.BiometricManager
import androidx.fragment.app.Fragment
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.URL.WALLET_SETUP_URL
import io.camlcase.smartwallet.core.extension.clearSecure
import io.camlcase.smartwallet.core.extension.displayFragment
import io.camlcase.smartwallet.core.extension.markSecure
import io.camlcase.smartwallet.core.extension.replaceFragmentSafely
import io.camlcase.smartwallet.data.model.OnboardingStep
import io.camlcase.smartwallet.data.source.local.AppPreferences
import io.camlcase.smartwallet.model.navigation.MainSource
import io.camlcase.smartwallet.model.navigation.PinNavigation
import io.camlcase.smartwallet.model.navigation.SeedPhraseConfirm
import io.camlcase.smartwallet.model.navigation.SeedPhraseNavigation
import io.camlcase.smartwallet.ui.base.BaseActivity
import io.camlcase.smartwallet.ui.base.info.*
import io.camlcase.smartwallet.ui.base.navigate.AppNavigator
import io.camlcase.smartwallet.ui.base.navigate.CLEAR_FLAGS
import io.camlcase.smartwallet.ui.onboarding.seed_phrase.SeedPhraseConfirmFragment
import io.camlcase.smartwallet.ui.onboarding.seed_phrase.SeedPhraseFragment
import io.camlcase.smartwallet.ui.onboarding.wallet.WalletSetupFragment
import io.camlcase.smartwallet.ui.security.PinCreateFragment
import io.camlcase.smartwallet.ui.security.biometric.BiometricSetFragment
import io.camlcase.smartwallet.ui.security.usesBiometric
import java.lang.ref.WeakReference

/**
 * 1_ Create wallet + Success screen
 * 2_ Check seed phrase
 * 3_ Set PIN
 * 4_ Set biometric auth
 */
class OnboardingActivity : BaseActivity(R.layout.activity_fragment),
    WalletSetupFragment.CreateWalletListener,
    InfoModalFragment.InfoModalListener,
    InfoView.InfoEventsListener,
    SeedPhraseFragment.SeedPhraseListener,
    SeedPhraseConfirmFragment.SeedPhraseConfirmListener,
    PinCreateFragment.PinCreateListener,
    BiometricSetFragment.BiometricSetListener {

    private var tezosAddress: String? = null
    private val appPreferences: AppPreferences = AppPreferences(WeakReference(this))

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            showOnboardingStep()
        }
    }

    /**
     * State control function.
     */
    private fun showOnboardingStep() {
        val fragment: Fragment? = when (appPreferences.getOnboardingStep()) {
            OnboardingStep.CREATE_WALLET -> WalletSetupFragment()
            OnboardingStep.SEED_PHRASE -> InfoFragment.init(InfoType.ONBOARDING_SEED_PHRASE)
            OnboardingStep.SET_PIN -> InfoFragment.init(InfoType.ONBOARDING_SET_PIN)
            OnboardingStep.BIOMETRIC -> BiometricSetFragment()
            OnboardingStep.FINAL_SUCCESS -> {
                // User should just jump to complete if they leave the app in this moment
                updateOnboardingStep(OnboardingStep.COMPLETE)
                InfoFragment.init(InfoType.ONBOARDING_SUCCESS)
            }
            OnboardingStep.COMPLETE -> null
        }

        fragment?.let {
            show(it)
        } ?: AppNavigator.navigateToMain(this, CLEAR_FLAGS, MainSource.ONBOARDING)
    }

    private fun updateOnboardingStep(step: OnboardingStep) {
        appPreferences.storeOnboardingStep(step)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString(ARG_ADDRESS_KEY, tezosAddress)
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        tezosAddress = savedInstanceState.getString(ARG_ADDRESS_KEY)
        super.onRestoreInstanceState(savedInstanceState)
    }

    /**
     * CREATE WALLET
     */

    override fun onValidWallet(address: String) {
        tezosAddress = address
        updateOnboardingStep(OnboardingStep.SEED_PHRASE)
        showOnboardingStep()
    }

    override fun onWalletError() {
        finishAfterTransition()
    }

    /**
     * INFO/SUCCESS Screens
     */

    override fun onActionClick(infoType: InfoType) {
        when (infoType) {
            InfoType.ONBOARDING_SET_PIN -> {
                // Start set pin flow
                markSecure()
                show(PinCreateFragment.init(PinNavigation.SOURCE_ONBOARDING))
            }
            InfoType.ONBOARDING_SEED_PHRASE -> {
                // Start seed phrase flow
                show(SeedPhraseFragment.init(SeedPhraseNavigation.SOURCE_ONBOARDING))
            }
            InfoType.ONBOARDING_SUCCESS -> {
                // Already updated step in [showOnboardingStep]
                showOnboardingStep()
            }
            else -> {
                throw IllegalStateException("Onboarding step not recognized $infoType")
            }
        }
    }

    private fun show(fragment: Fragment) {
        replaceFragmentSafely(fragment, R.id.container_fragment)
    }

    /**
     * SEED PHRASE
     */

    override fun onSeedPhraseShown() {
        displayFragment(SeedPhraseConfirmFragment.init(SeedPhraseConfirm.SOURCE_ONBOARDING), R.id.container_fragment)
    }

    override fun onSeedPhraseConfirmed() {
        updateOnboardingStep(OnboardingStep.SET_PIN)
        showOnboardingStep()
    }

    /**
     * SET PIN
     */

    override fun onPinSet() {
        clearSecure()
        if (usesBiometric() == BiometricManager.BIOMETRIC_SUCCESS) {
            updateOnboardingStep(OnboardingStep.BIOMETRIC)
        } else {
            updateOnboardingStep(OnboardingStep.FINAL_SUCCESS)
        }
        showOnboardingStep()
    }

    /**
     * BIOMETRIC
     */

    override fun onBiometricSet() {
        updateOnboardingStep(OnboardingStep.FINAL_SUCCESS)
        showOnboardingStep()
    }

    override fun skipBiometric() {
        onBiometricSet()
    }

    override fun onBackPressed() {
        overrideBackPress()
    }

    override fun onActionClick(infoType: InfoModalType) {
        if (infoType == InfoModalType.WALLET_SETUP) {
            AppNavigator.navigateToUrl(this, WALLET_SETUP_URL)
        } else {
            throw IllegalStateException("onActionClick not implemented for $infoType")
        }
    }

    companion object {
        internal const val ARG_ADDRESS_KEY = "arg.address"
        fun init(context: Context): Intent {
            return Intent(context, OnboardingActivity::class.java)
        }
    }
}
