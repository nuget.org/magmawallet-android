/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.onboarding

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.ViewTreeObserver
import android.view.animation.AccelerateInterpolator
import android.view.animation.DecelerateInterpolator
import by.kirich1409.viewbindingdelegate.viewBinding
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.Arguments
import io.camlcase.smartwallet.core.extension.*
import io.camlcase.smartwallet.data.analytic.report
import io.camlcase.smartwallet.data.usecase.analytics.ScreenEvent
import io.camlcase.smartwallet.databinding.ActivityWelcomeBinding
import io.camlcase.smartwallet.model.navigation.LaunchState
import io.camlcase.smartwallet.model.navigation.MainSource
import io.camlcase.smartwallet.model.navigation.WelcomeNavigation
import io.camlcase.smartwallet.ui.LaunchViewModel
import io.camlcase.smartwallet.ui.base.BaseActivity
import io.camlcase.smartwallet.ui.base.bind
import io.camlcase.smartwallet.ui.base.info.InfoFragment
import io.camlcase.smartwallet.ui.base.info.InfoType
import io.camlcase.smartwallet.ui.base.info.InfoView
import io.camlcase.smartwallet.ui.base.navigate.AppNavigator
import io.camlcase.smartwallet.ui.base.navigate.CLEAR_FLAGS
import io.camlcase.smartwallet.ui.base.navigate.RequestCode
import io.camlcase.smartwallet.ui.base.okOrCancelDialog
import io.camlcase.smartwallet.ui.security.system.SystemSafetyChecker
import io.reactivex.rxjava3.disposables.CompositeDisposable
import java.lang.ref.WeakReference
import javax.inject.Inject

/**
 * First screen a new user will see. Animated in launch state.
 * Can see this when coming back from the [AuthLoginActivity]
 */
class WelcomeActivity : BaseActivity(R.layout.activity_welcome), InfoView.InfoEventsListener {
    private val binding by viewBinding(ActivityWelcomeBinding::bind)

    @Inject
    lateinit var viewModel: LaunchViewModel

    @WelcomeNavigation.ScreenSource
    private var source: Int = WelcomeNavigation.SOURCE_LAUNCH

    private lateinit var safetyChecker: SystemSafetyChecker

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        manageIntent(intent.extras)
        reportView(ScreenEvent.ONB_START_MENU)
        viewModel = viewModel(viewModelFactory) {}

        initViews()
        bindViews()
        bindScreenResults()
        initSafetyChecker()
    }

    private fun manageIntent(bundle: Bundle?) {
        bundle?.getInt(Arguments.ARG_SOURCE)?.let {
            source = it
        }
    }

    private fun initViews() {
        if (source == WelcomeNavigation.SOURCE_LAUNCH) {
            binding.welcomeContainer.viewTreeObserver.addOnGlobalLayoutListener(object :
                ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    binding.welcomeContainer.viewTreeObserver.removeOnGlobalLayoutListener(this)
                    startAnimation()
                }
            })
        } else {
            showItems()
        }
    }

    /**
     * Show everything with no animations
     */
    private fun showItems() {
        binding.launcherTezosWordmark.hide()
        binding.logoImage.show()
        binding.launcherBackground.hide()
        binding.actionCreate.show()
        binding.actionImport.show()
    }

    /**
     * - Fade in and Y translate icon
     * - Fade out background and fade in action buttons
     */
    private fun startAnimation() {
        fadeInOverlayBackground()
        fadeInLogo {
            fadeOutTezosWordmark {
                binding.launcherBackground.animate()
                    .alpha(0.0f)
                    .setDuration(ANIM_ELEMENTS_DURATION)
                    .withEndAction {
                        binding.launcherBackground.hide()
                        // We run the safety check only on Launch.
                        safetyChecker.run()
                    }

                fadeInActionButtons()
            }
        }
    }

    private fun fadeInOverlayBackground() {
        binding.launcherBackground.alpha = 0f
        binding.launcherBackground.show()
        binding.launcherBackground.animate()
            .alpha(1.0f)
            .setDuration(ANIM_ELEMENTS_DURATION)
            .setInterpolator(AccelerateInterpolator())
    }

    private fun fadeOutTezosWordmark(endAction: () -> Unit) {
        binding.launcherTezosWordmark.animate()
            .alpha(0.0f)
            .setDuration(ANIM_TEZOS_DURATION)
            .setInterpolator(AccelerateInterpolator())
            .withEndAction { endAction() }
    }

    private fun fadeInLogo(endAction: () -> Unit) {
        binding.logoImage.alpha = 0f
        binding.logoImage.y = binding.guideline.top.toFloat()
        binding.logoImage.show()
        binding.logoImage.animate()
            .alpha(1.0f)
            .y(binding.guidelineUp.bottom.toFloat())
            .setDuration(ANIM_LOGO_DURATION)
            .setInterpolator(DecelerateInterpolator())
            .withEndAction {
                endAction()
            }
    }

    private fun fadeInActionButtons() {
        binding.actionCreate.alpha = 0f
        binding.actionImport.alpha = 0f
        binding.actionCreate.show()
        binding.actionImport.show()
        binding.actionCreate.animate()
            .alpha(1.0f)
            .setDuration(ANIM_ELEMENTS_DURATION)
        binding.actionImport.animate()
            .alpha(1.0f)
            .setDuration(ANIM_ELEMENTS_DURATION)
    }

    private fun bindScreenResults() {
        activityResult.bind(
            RequestCode.AUTH_LOGIN,
            disposable = disposables,
            onResult = {
                // Navigate to main
                AppNavigator.navigateToMain(this, CLEAR_FLAGS, MainSource.WELCOME_LOGIN)
            },
            onError = { it.report() })
    }

    private fun bindViews() {
        if (viewModel.getUserLaunchState() == LaunchState.UNLOGGED) {
            binding.actionLogin.show()
            binding.actionImport.throttledClick()
                .subscribe { AppNavigator.navigateToAuthLogin(this) }
                .addToDisposables(disposables)
        }

        binding.actionLogin.throttledClick()
            .subscribe { AppNavigator.navigateToAuthLogin(this) }
            .addToDisposables(disposables)

        binding.actionImport.throttledClick()
            .subscribe {
                if (viewModel.showPairingWarning()) {
                    replaceFragmentSafely(InfoFragment.init(InfoType.ONBOARDING_PAIRED_IMPORT), R.id.container_fragment)
                } else {
                    navigateToImportWallet()
                }
            }
            .addToDisposables(disposables)

        binding.actionCreate.throttledClick()
            .subscribe {
                if (viewModel.showPairingWarning()) {
                    replaceFragmentSafely(InfoFragment.init(InfoType.ONBOARDING_PAIRED_CREATE), R.id.container_fragment)
                } else {
                    navigateToOnboarding()
                }
            }
            .addToDisposables(disposables)
    }

    private fun navigateToImportWallet() {
        AppNavigator.navigateToLogin(this)
    }

    private fun navigateToOnboarding() {
        viewModel.checkUserOnboardingStep()
        AppNavigator.navigateToOnboarding(this, false)
    }

    override fun onActionClick(infoType: InfoType) {
        when (infoType) {
            InfoType.ONBOARDING_PAIRED_CREATE -> {
                showPairingConfirmationDialog {
                    hideFragments()
                    navigateToOnboarding()
                }
            }
            InfoType.ONBOARDING_PAIRED_IMPORT -> {
                showPairingConfirmationDialog {
                    hideFragments()
                    navigateToImportWallet()
                }
            }
            else -> {
                throw AssertionError("WelcomeActivity does not support $infoType")
            }
        }
    }

    private fun showPairingConfirmationDialog(onOk: () -> Unit) {
        okOrCancelDialog(
            this,
            title = getString(R.string.onb_already_paired_popup_title),
            message = getString(R.string.onb_already_paired_popup_detail),
            okColor = this.getColor(R.color.black_transparent_90),
            okString = R.string.onb_already_paired_continue,
            onOK = {
                hideFragments()
                onOk()
            }
        )
    }

    override fun onSecondaryActionClick(infoType: InfoType) {
        hideFragments()
    }

    private fun initSafetyChecker() {
        val activityRef = WeakReference(this as BaseActivity)
        safetyChecker = object : SystemSafetyChecker() {
            override var globalActivityRef: WeakReference<BaseActivity> = activityRef
            override var disposablesRef: WeakReference<CompositeDisposable> = WeakReference(disposables)
        }
        safetyChecker.init()
    }

    companion object {
        private const val ANIM_LOGO_DURATION = 900L
        private const val ANIM_ELEMENTS_DURATION = 500L
        private const val ANIM_TEZOS_DURATION = 400L

        fun init(
            context: Context,
            @WelcomeNavigation.ScreenSource source: Int = WelcomeNavigation.SOURCE_LAUNCH
        ): Intent {
            val intent = Intent(context, WelcomeActivity::class.java)
            intent.putExtra(Arguments.ARG_SOURCE, source)
            return intent
        }
    }
}
