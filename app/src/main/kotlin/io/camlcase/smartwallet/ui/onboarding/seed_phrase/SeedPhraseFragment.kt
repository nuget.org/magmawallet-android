/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.onboarding.seed_phrase

import android.content.Context
import android.os.Bundle
import android.os.CountDownTimer
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import by.kirich1409.viewbindingdelegate.viewBinding
import io.camlcase.smartwallet.BuildConfig
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.Arguments
import io.camlcase.smartwallet.core.extension.*
import io.camlcase.smartwallet.data.usecase.analytics.ScreenEvent
import io.camlcase.smartwallet.databinding.FragmentOnboardingSeedPhraseBinding
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.model.handleResults
import io.camlcase.smartwallet.model.navigation.SeedPhraseNavigation
import io.camlcase.smartwallet.ui.base.BindBaseFragment
import io.camlcase.smartwallet.ui.base.showError
import io.camlcase.smartwallet.ui.base.showSnackbar

/**
 * Show mnemonics to the user
 *
 * @see SeedPhraseNavigation.ScreenSource
 */
class SeedPhraseFragment : BindBaseFragment(R.layout.fragment_onboarding_seed_phrase) {
    interface SeedPhraseListener {
        fun onSeedPhraseShown()
    }

    private val binding by viewBinding(FragmentOnboardingSeedPhraseBinding::bind)
    private var listener: SeedPhraseListener? = null
    private lateinit var viewModel: SeedPhraseViewModel

    private var timer: CountDownTimer? = null

    @SeedPhraseNavigation.ScreenSource
    private var source: Int = SeedPhraseNavigation.SOURCE_SETTINGS

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        slideInRight()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        manageIntent(arguments)
        if (source == SeedPhraseNavigation.SOURCE_ONBOARDING) {
            reportView(ScreenEvent.ONB_RECOVERY_DISPLAY)
        }

        activity?.markSecure()
        listener = context as? SeedPhraseListener
        viewModel = viewModel(viewModelFactory) {
            observe(data, { handleData(it) })
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAppbar()
        bindViews()
        when (source) {
            SeedPhraseNavigation.SOURCE_SETTINGS -> {
                setupToolbar(binding.appBarContainer.toolbar, true)
                binding.mainViewContainer.info.setText(R.string.set_recovery_message)
                binding.buttonContainer.root.hide()
            }
            SeedPhraseNavigation.SOURCE_ONBOARDING -> {
                binding.mainViewContainer.info.setText(R.string.onb_recovery_phrase_display_detail)
                binding.buttonContainer.root.show()
                binding.buttonContainer.actionNext.setText(R.string.action_continue)
            }
            SeedPhraseNavigation.SOURCE_MIGRATION -> {
                binding.mainViewContainer.info.setText(R.string.onb_recovery_phrase_display_detail)
                binding.buttonContainer.root.show()
                binding.buttonContainer.actionNext.setText(R.string.mig_next_step)
            }
        }
        viewModel.getSeedPhrase(source)
    }

    override fun onStart() {
        super.onStart()
        if (source == SeedPhraseNavigation.SOURCE_SETTINGS) {
            startTimer()
        }
    }

    /**
     * Run a 30 seconds countdown and leave screen when it finishes
     */
    private fun startTimer() {
        timer = object : CountDownTimer(31 * 1000, 1000) {
            override fun onFinish() {
                binding.timerTag.hide()
                binding.root.postDelayed({ activity?.onBackPressed() }, 500)
            }

            override fun onTick(millisUntilFinished: Long) {
                val secondsRemaining = (millisUntilFinished / 1000).toString()

                binding.timerTag.text = getString(R.string.set_recovery_timeout, secondsRemaining)
                binding.timerTag.show()
            }
        }
        timer?.start()
    }

    private fun manageIntent(bundle: Bundle?) {
        bundle?.getInt(Arguments.ARG_SOURCE)?.let {
            source = it
        } ?: throw IllegalStateException("Screen needs an ARG_SOURCE argument")
    }

    private fun initAppbar() {
        val title = if (source == SeedPhraseNavigation.SOURCE_MIGRATION) {
            getString(R.string.mig_seed_phrase_info_action)
        } else {
            getString(R.string.onb_recovery_phrase_info_title)
        }
        binding.appBarContainer.setupAppBar(title)
    }

    private fun handleData(wrapper: DataWrapper<TwoRowMnemonic>) {
        wrapper.handleResults(
            onValidData = {
                hideLoading()
                populateRows(it)
                if (it.derivationPath != null && source == SeedPhraseNavigation.SOURCE_SETTINGS) {
                    binding.mainViewContainer.derivationPathTitle.show()
                    binding.mainViewContainer.derivationPath.show()
                    binding.mainViewContainer.derivationPath.text = it.derivationPath
                }
                binding.buttonContainer.actionNext.isEnabled = true
            },
            onError = {
                hideLoading()
                showError(it)
            },
            onLoading = {
                showLoading()
            }
        )
    }

    private fun populateRows(data: TwoRowMnemonic) {
        /**
         * Style index+word so index is highlighted
         */
        fun styleMnemonic(number: Int, mnemonic: String): SpannableString {
            val phrase = "$number  $mnemonic"
            val color = context?.getColor(R.color.white_transparent_80) ?: -1
            return context.span(
                SpannableString(phrase),
                ForegroundColorSpan(color),
                number.toString()
            )
        }
        // Set up top margin
        val params: LinearLayout.LayoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        params.setMargins(0, resources.getDimensionPixelSize(R.dimen.mnemonic_item_top_margin), 0, 0)

        /**
         * Create a TextField with index + word
         */
        fun createTextField(index: Int, item: String): TextView {
            val textField = TextView(activity)
            textField.setTextAppearance(R.style.AppTheme_Text_Body_1)
            textField.text = styleMnemonic(index, item)
            if (index > 0) {
                textField.layoutParams = params
            }
            return textField
        }

        for ((index, item) in data.left.withIndex()) {
            val textField = createTextField(index + 1, item)
            binding.mainViewContainer.mnemonicLeftRow.addView(textField)
        }

        for ((index, item) in data.right.withIndex()) {
            val textField = createTextField(index + 1 + data.middle, item)
            binding.mainViewContainer.mnemonicRightRow.addView(textField)
        }

        if (BuildConfig.DEBUG) {
            // ONLY for development purposes: Lets you copy it
            binding.mainViewContainer.mnemonicsContainer.setOnLongClickListener {
                activity.copyToClipBoard(data.toString(), { it.showSnackbar(R.string.action_copy_success) })
                true
            }
        }
    }

    private fun showLoading() {
        binding.mainViewContainer.loading.root.show()
    }

    private fun hideLoading() {
        binding.mainViewContainer.loading.root.hide()
    }

    private fun bindViews() {
        binding.buttonContainer.actionNext.throttledClick()
            .subscribe { listener?.onSeedPhraseShown() }
            .addToDisposables(disposables)
    }

    override fun onStop() {
        timer?.cancel()
        super.onStop()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        activity?.clearSecure()
    }

    companion object {
        fun init(@SeedPhraseNavigation.ScreenSource source: Int): SeedPhraseFragment {
            val fragment = SeedPhraseFragment()
            val args = Bundle()
            args.putInt(Arguments.ARG_SOURCE, source)
            fragment.arguments = args
            return fragment
        }
    }
}
