/**
 * # Released under MIT License
 *
 * Copyright (c) 2021 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.onboarding.seed_phrase

import android.content.Context
import android.text.InputFilter
import android.text.InputType
import android.text.Spanned
import android.view.Gravity
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.getDeviceLocale
import io.camlcase.smartwallet.core.extension.listenToChanges
import io.camlcase.smartwallet.data.core.extension.isOnlyLettersAndSpaces
import io.camlcase.smartwallet.data.core.extension.sanitizeSeedPhrase
import io.camlcase.smartwallet.databinding.ViewWalletImportBinding
import io.camlcase.smartwallet.ui.base.FontCache
import java.lang.ref.WeakReference

interface SeedPhraseImportValidation {
    var seedPhraseBinding: WeakReference<ViewWalletImportBinding>

    /**
     * By default, when you set an EditText element to use the "textPassword" input type, the font family
     * is set to monospace, so you should change its font family to "sans-serif" so that both text fields use
     * a matching font style.
     */
    fun init(context: Context?) {
        seedPhraseBinding.get()?.inputMnemonic?.textField()?.minLines = 3
        seedPhraseBinding.get()?.inputMnemonic?.textField()?.gravity = Gravity.TOP
        seedPhraseBinding.get()?.inputMnemonic?.textField()?.inputType =
            (InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_MULTI_LINE or InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS or InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD)
        seedPhraseBinding.get()?.inputMnemonic?.textField()?.typeface = FontCache["barlow-regular", context]
        // Force all words to be lowercased
        seedPhraseBinding.get()?.inputMnemonic?.textField()?.filters =
            arrayOf<InputFilter>(object : InputFilter.AllCaps() {
                override fun filter(
                    source: CharSequence?, start: Int, end: Int,
                    dest: Spanned?, dstart: Int, dend: Int
                ) =
                    source.toString().toLowerCase(context.getDeviceLocale())
            })

        seedPhraseBinding.get()?.inputPassphrase?.setMaxLength(50)
        seedPhraseBinding.get()?.inputPassphrase?.textField()?.inputType =
            (InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD)
        seedPhraseBinding.get()?.inputPassphrase?.textField()?.typeface = FontCache["barlow-regular", context]
        seedPhraseBinding.get()?.inputPassphrase?.setAsPasswordToggle()
    }

    fun bind() {
        seedPhraseBinding.get()?.inputMnemonic?.textField().listenToChanges {
            validateSeedPhrase(it)
        }
    }

    fun validateSeedPhrase(input: String) {
        // Commas, points and \n
        val sanitized = input.sanitizeSeedPhrase()

        val list = sanitized.split(" ")
        if (list.isNullOrEmpty() || list.size < 12) {
            // Too small: Disable but don't warn
            seedPhraseBinding.get()?.inputMnemonic?.dismissError()
            //action_next.isEnabled = false
            invalidMnemonic()
        } else if (!sanitized.isOnlyLettersAndSpaces()) {
            // Invalid: Disable and warn
            seedPhraseBinding.get()?.inputMnemonic?.showError(seedPhraseBinding.get()?.root?.context?.getString(R.string.error_invalid_seed_phrase))
            //action_next.isEnabled = false
            invalidMnemonic()
        } else {
            // Valid: Enable
            seedPhraseBinding.get()?.inputMnemonic?.dismissError()
            validMnemonic()
        }
    }

    fun invalidMnemonic()

    fun validMnemonic()

    fun getMnemonic(): List<String> {
        val sanitized = seedPhraseBinding.get()?.inputMnemonic?.text.toString().sanitizeSeedPhrase()
        return sanitized.split(" ")
    }

    fun getPassphrase(): String? {
        val text = seedPhraseBinding.get()?.inputPassphrase?.text
        return if (text.isNullOrBlank()) {
            null
        } else {
            text.trim()
        }
    }
}
