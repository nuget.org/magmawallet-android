/**
 * # Released under MIT License
 *
 * Copyright (c) 2021 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.onboarding.wallet

import android.content.Context
import android.os.Bundle
import android.text.InputType
import android.view.View
import androidx.core.content.ContextCompat
import by.kirich1409.viewbindingdelegate.viewBinding
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.*
import io.camlcase.smartwallet.data.usecase.analytics.ScreenEvent
import io.camlcase.smartwallet.databinding.FragmentWalletSetupBinding
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.model.handleResults
import io.camlcase.smartwallet.ui.base.BackPressOverrider
import io.camlcase.smartwallet.ui.base.BindBaseFragment
import io.camlcase.smartwallet.ui.base.info.InfoModalFragment
import io.camlcase.smartwallet.ui.base.info.InfoModalType
import io.camlcase.smartwallet.ui.base.navigate.navigateToModal
import io.camlcase.smartwallet.ui.base.showErrorSnackbar
import io.camlcase.smartwallet.ui.custom.AdvancedScrollOffsetListener

/**
 * Select between create a non-passphrase HDWallet or one with passphrase
 */
class WalletSetupFragment : BindBaseFragment(R.layout.fragment_wallet_setup), BackPressOverrider {
    interface CreateWalletListener {
        fun onValidWallet(address: String)
        fun onWalletError() {}
    }

    private val binding by viewBinding(FragmentWalletSetupBinding::bind)
    private lateinit var viewModel: CreateWalletViewModel
    private var listener: CreateWalletListener? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as? CreateWalletListener
        if (listener == null) {
            throw ClassCastException("$context must implement CreateWalletListener")
        }
        viewModel = viewModel(viewModelFactory) {
            observe(data, { handleData(it) })
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAppbar()
        initSetup()

        binding.passphraseContainer.inputPassphrase.setMaxLength(50)
        binding.passphraseContainer.inputPassphrase.textField()?.inputType =
            (InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD)
        binding.passphraseContainer.inputPassphrase.setAsPasswordToggle()

        binding.buttonContainer.actionNext.setText(R.string.action_continue)
        // User may leave Passphrase field blank and continue without one
        binding.buttonContainer.actionNext.isEnabled = true

        keyboardListener = initKeyboardListener(binding.buttonContainer.actionNext) { visible ->
            if (visible) {
                binding.appBar.setExpanded(false, true)
            } else {
                binding.appBar.setExpanded(true, true)
            }
        }
        bindViews()
    }

    private fun initAppbar() {
        setupToolbar(binding.toolbar, true)

        fun getTitle(): String {
            return if (isSetupVisible()) {
                getString(R.string.onb_wallet_setup_title)
            } else {
                getString(R.string.onb_wallet_passphrase_title)
            }
        }
        binding.title.text = getTitle()
        binding.appBar.addOnOffsetChangedListener(
            AdvancedScrollOffsetListener { _, state ->
                when (state) {
                    AdvancedScrollOffsetListener.State.EXPANDED -> {
                        binding.expandedTitle.show()
                        binding.title.text = getTitle()
                        binding.toolbar.title = null
                    }
                    AdvancedScrollOffsetListener.State.COLLAPSED -> {
                        binding.expandedTitle.hide()
                        binding.toolbar.title = getTitle()
                    }
                    AdvancedScrollOffsetListener.State.IDLE -> {
                    }
                }
            }
        )
    }

    private fun bindViews() {
        binding.actionHelp.throttledClick()
            .subscribe {
                val fragment = InfoModalFragment.init(InfoModalType.WALLET_SETUP)
                analyticsUseCase.get()?.reportScreen(ScreenEvent.ONB_WALLET_SETUP_HELP)
                navigateToModal(fragment)
            }.addToDisposables(disposables)

        binding.setupContainer.common.onClick()
            .subscribe {
                viewModel.createWallet(null)
            }.addToDisposables(disposables)

        binding.setupContainer.advanced.onClick()
            .subscribe {
                // PASSPHRASE
                binding.switcher.showNext()
                initPassphrase()
            }.addToDisposables(disposables)

        binding.buttonContainer.actionNext.throttledClick()
            .subscribe {
                viewModel.createWallet(binding.passphraseContainer.inputPassphrase.text)
            }.addToDisposables(disposables)
    }

    private fun handleData(wrapper: DataWrapper<String>) {
        wrapper.handleResults(
            onValidData = {
                hideLoading()
                onValidWallet(it)
            },
            onError = {
                hideLoading()
                handleError(wrapper.error)
                binding.buttonContainer.actionNext.isEnabled = true
            },
            onLoading = {
                showLoading()
            }
        )
    }

    private fun onValidWallet(address: String) {
        // finish and go forward
        listener?.onValidWallet(address)
    }

    private fun handleError(error: Throwable?) {
        view.showErrorSnackbar(error)
        // finish and go back
        view?.postDelayed({
            listener?.onWalletError()
        }, 2 * 1000)
    }

    private fun showLoading() {
        binding.loading.containerLoading.show()
        binding.buttonContainer.actionNext.isEnabled = false
    }

    private fun hideLoading() {
        binding.loading.containerLoading.hide()
    }

    private fun isSetupVisible(): Boolean {
        return binding.switcher.currentView.id == binding.setupContainer.root.id
    }

    private fun initSetup() {
        analyticsUseCase.get()?.reportScreen(ScreenEvent.ONB_WALLET_SETUP)
        context?.apply {
            binding.appBar.setExpanded(true, true)
            binding.actionHelp.show()
            binding.image.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.emotional_setup))
            binding.title.setText(R.string.onb_wallet_setup_title)
            binding.buttonContainer.root.hide()
            binding.passphraseContainer.inputPassphrase.textField()?.text?.clear()
        }
    }

    private fun initPassphrase() {
        analyticsUseCase.get()?.reportScreen(ScreenEvent.ONB_WALLET_PASSPHRASE)
        context?.apply {
            binding.appBar.setExpanded(true, true)
            binding.actionHelp.hide()
            binding.image.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.emotional_passphrase))
            binding.title.setText(R.string.onb_wallet_passphrase_title)
            binding.buttonContainer.root.show()
        }
    }

    override fun overrideBackPressed(): Boolean {
        return if (!isSetupVisible()) {
            // GO BACK TO SELECTOR
            binding.passphraseContainer.inputPassphrase.textField().hideKeyboard()
            binding.switcher.showPrevious()
            initSetup()
            true
        } else {
            false
        }
    }
}
