/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.operation

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.*
import io.camlcase.smartwallet.data.model.operation.MagmaOperationResult
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.model.handleResults
import io.camlcase.smartwallet.ui.base.BaseFragment
import io.camlcase.smartwallet.ui.delegate.IncludedOperation
import io.camlcase.smartwallet.ui.delegate.OperationDetailsViewModel
import io.camlcase.smartwallet.ui.delegate.OperationProgressListener
import kotlinx.android.synthetic.main.fragment_operation_progress.*

/**
 * Operation being included in the blockchain Progress screen
 */
class WaitForInclusionFragment : BaseFragment() {
    interface WaitForInclusionListener : OperationProgressListener<MagmaOperationResult> {
        /**
         * Main button click
         */
        fun onProgressAction()

        /**
         * Secondary action click
         */
        fun onProgressExtraAction() {}
    }

    private lateinit var viewModel: OperationDetailsViewModel
    private var listener: WaitForInclusionListener? = null

    private var operationHash: String = ""
    private var type: IncludedOperation = IncludedOperation.SEND
    private var title: String? = null
    private var actionText: String? = null
    private var secondaryActionText: String? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        manageIntent(arguments)
        return inflater.inflate(R.layout.fragment_operation_progress, container, false)
    }

    private fun manageIntent(bundle: Bundle?) {
        bundle?.getString(ARG_OPERATION_HASH, null)?.let {
            operationHash = it
        } ?: throw IllegalStateException("Screen needs an ARG_OPERATION_HASH argument")

        bundle.getString(ARG_OPERATION_TYPE)?.let {
            type = IncludedOperation.valueOf(it)
        } ?: throw IllegalStateException("Screen needs an ARG_OPERATION_TYPE argument")

        bundle.getString(ARG_TITLE, null)?.let {
            title = it
        } ?: throw IllegalStateException("Screen needs an ARG_TITLE argument")


        bundle.getString(ARG_MAIN_ACTION, null)?.let {
            actionText = it
        }

        bundle.getString(ARG_SECONDARY_ACTION, null)?.let {
            secondaryActionText = it
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as? WaitForInclusionListener
        if (listener == null) {
            throw ClassCastException("$context must implement SendProcessListener")
        }

        viewModel = viewModel(viewModelFactory) {
            observe(data, { handleData(it) })
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        title?.apply {
            progress_title.text = this
        }

        main_info.setText(R.string.snd_wait_description)
        secondary_info.setText(R.string.snd_wait_info)

        actionText?.apply {
            action_progress_main.text = this
            secondary_info.show()
        } ?: action_progress_main.hide()

        secondaryActionText?.let {
            action_progress_secondary.show()
            action_progress_secondary.text = it
        } ?: action_progress_secondary.invisible()

        bindViews()

        viewModel.waitForOperation(operationHash, type)
    }

    private fun bindViews() {
        action_progress_main.throttledClick()
            .subscribe {
                listener?.onProgressAction()
            }
            .addToDisposables(disposables)

        action_progress_secondary.throttledClick()
            .subscribe {
                listener?.onProgressExtraAction()
            }
            .addToDisposables(disposables)
    }

    private fun handleData(wrapper: DataWrapper<MagmaOperationResult>) {
        wrapper.handleResults(
            onValidData = {
                listener?.onInclusion(it)
            },
            onError = {
                // show error info fragment
                listener?.onInclusionError(it)
            }
        )
    }

    companion object {
        const val ARG_OPERATION_HASH = "arg.operation.hash"
        const val ARG_OPERATION_TYPE = "arg.operation.type"
        const val ARG_TITLE = "arg.progress.title"
        const val ARG_MAIN_ACTION = "arg.progress.main_action.text"
        const val ARG_SECONDARY_ACTION = "arg.progress.secondary_action.text"

        fun init(
            operationHash: String,
            type: IncludedOperation,
            title: String,
            actionText: String?,
            secondaryActionText: String? = null
        ): WaitForInclusionFragment {
            val fragment = WaitForInclusionFragment()
            val args = Bundle()
            args.putString(ARG_OPERATION_HASH, operationHash)
            args.putString(ARG_OPERATION_TYPE, type.name)
            args.putString(ARG_TITLE, title)
            args.putString(ARG_MAIN_ACTION, actionText)
            args.putString(ARG_SECONDARY_ACTION, secondaryActionText)
            fragment.arguments = args
            return fragment
        }
    }
}
