/**
 * # Released under MIT License
 *
 * Copyright (c) 2021 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.operation.fees

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.transition.TransitionInflater
import by.kirich1409.viewbindingdelegate.viewBinding
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.addToDisposables
import io.camlcase.smartwallet.core.extension.setupToolbar
import io.camlcase.smartwallet.core.extension.show
import io.camlcase.smartwallet.core.extension.throttledClick
import io.camlcase.smartwallet.data.core.XTZ
import io.camlcase.smartwallet.data.core.extension.format
import io.camlcase.smartwallet.databinding.FragmentFeesDetailBinding
import io.camlcase.smartwallet.model.operation.OperationFeesBundle
import io.camlcase.smartwallet.ui.base.BindBaseFragment
import io.camlcase.smartwallet.ui.base.navigate.navigateToModal
import io.camlcase.smartwallet.ui.custom.ScrollOffsetListener
import io.camlcase.smartwallet.ui.send.confirmation.SendConfirmationFragment

/**
 * List the different amounts for fees.
 *
 * @see SendConfirmationFragment
 */
class FeesDetailFragment : BindBaseFragment(R.layout.fragment_fees_detail) {
    private val binding by viewBinding(FragmentFeesDetailBinding::bind)
    private var fees: OperationFeesBundle? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        manageIntent(arguments)
    }

    private fun manageIntent(bundle: Bundle?) {
        bundle?.getParcelable<OperationFeesBundle>(ARG_FEES)?.let {
            fees = it
        } ?: throw IllegalStateException("Screen needs an ARG_FEES argument")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val inflater = TransitionInflater.from(requireContext())
        enterTransition = inflater.inflateTransition(R.transition.slide_right)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAppBar()
        bindViews()
        populateSections(fees!!)
    }

    private fun initAppBar() {
        setupToolbar(binding.appBarContainer.toolbar, true)
        val title = getString(R.string.fees_detail_title)
        binding.appBarContainer.expandedTitle.text = title
        binding.appBarContainer.appBar.addOnOffsetChangedListener(
            ScrollOffsetListener(
                binding.appBarContainer.expandedTitle,
                binding.appBarContainer.toolbar,
                title
            )
        )
    }

    private fun bindViews() {
        binding.feesInfo.throttledClick()
            .subscribe {
                navigateToModal(FeesInfoFragment())
            }.addToDisposables(disposables)
    }

    private fun populateSections(fees: OperationFeesBundle) {
        binding.bakerSection.setValues(
            fees.bakerFee.format(XTZ.symbol),
            fees.bakerFeeInCurrency
        )
        if (fees.allocationFee != null) {
            binding.allocationSection.setValues(
                fees.allocationFee.format(XTZ.symbol),
                fees.allocationFeeInCurrency
            )
            binding.allocationSection.show()
        }
        if (fees.burnFee != null) {
            binding.burnSection.setValues(
                fees.burnFee.format(XTZ.symbol),
                fees.burnFeeInCurrency
            )
            binding.burnSection.show()
        }
        if (fees.revealFee != null) {
            binding.revealSection.setValues(
                fees.revealFee.format(XTZ.symbol),
                fees.revealFeeInCurrency
            )
            binding.revealSection.show()
        }
        binding.totalFeesSection.setValues(
            fees.totalFees.format(XTZ.symbol),
            fees.totalFeesInCurrency
        )
    }

    companion object {
        private const val ARG_FEES = "args.fees"
        fun init(fees: OperationFeesBundle): FeesDetailFragment {
            val fragment = FeesDetailFragment()
            val args = Bundle()
            args.putParcelable(ARG_FEES, fees)
            fragment.arguments = args
            return fragment
        }
    }
}
