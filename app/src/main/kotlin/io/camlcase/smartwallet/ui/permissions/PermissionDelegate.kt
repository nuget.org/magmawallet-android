/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.permissions

import android.app.Activity
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import io.camlcase.smartwallet.core.extension.addToDisposables
import io.camlcase.smartwallet.core.extension.subscribeForUI
import io.camlcase.smartwallet.data.core.toAppContext
import io.camlcase.smartwallet.data.source.local.PermissionPreferences
import io.camlcase.smartwallet.data.source.local.PermissionStatus
import io.camlcase.smartwallet.ui.base.ActivityResultStream
import io.camlcase.smartwallet.ui.base.navigate.RequestCode
import io.reactivex.rxjava3.disposables.CompositeDisposable
import java.lang.ref.WeakReference
import java.util.concurrent.TimeUnit

/**
 * Handles logic for asking for permissions and handle the user response
 *
 * 1. [onPermissionButtonClick] to show dialog or navigate to settings
 * 2. [listenToPermissionsDialog] to listen to the dialog response
 * 2b. [listenToPermissionsSettings] to listen to settings (if you navigated)
 * 3. [unbind] ! Remember to unbind so we don't keep listening forever!
 */
abstract class PermissionDelegate(
    private val ui: PermissionsListener,
    private val permissionPreferences: PermissionPreferences
) {
    abstract val permission: AndroidPermission
    abstract val preferenceKey: String
    private val subscriptions: CompositeDisposable = CompositeDisposable()
    private var moreInfoBefore = false

    interface PermissionsListener {
        fun activityReference(): WeakReference<AppCompatActivity?>
        fun navigateToSettings()
        fun onPermission(hasPermission: Boolean)
        fun showPermissionsError(error: Throwable?)
    }

    /**
     * IMPORTANT: Remember to ALWAYS unbind!
     */
    fun unbind() {
        subscriptions.dispose()
    }

    private fun hasPermission(): Boolean = permission.hasPermission(ui.activityReference().get())

    /**
     * Trigger for click
     */
    fun onPermissionButtonClick() {
        if (shouldShowDialog()) {
            showPermissionDialog()
        } else {
            ui.navigateToSettings()
        }
    }

    /**
     * Android standard dialog results listener
     */
    fun listenToPermissionsDialog(delegate: WeakReference<ActivityPermissionsDelegate?>) {
        // If permission comes from dialog
        delegate.get()?.let { permissionsDelegate ->
            permissionsDelegate.stream
                .filter { it.requestCode == permission.permissionCode }
                .subscribeForUI()
                .subscribe(
                    {
                        savePermissionStatus(ui.activityReference().get())
                        onIncomingPermission()
                    },
                    { ui.showPermissionsError(it) }
                ).addToDisposables(subscriptions)
        }
    }

    /**
     * Android settings screen listener.
     *
     * [!]HACK: The activity result is received immediately after opening the settings screen. We postpone checking
     * if the permission is given, assuming the user will take around 10 seconds to read, accept, etc.
     */
    fun listenToPermissionsSettings(delegate: ActivityResultStream) {
        // If permission comes from Settings screen
        delegate.stream
            .filter { it.requestCode == RequestCode.SYSTEM_SETTINGS }
            .delay(10, TimeUnit.SECONDS) // <-- Hack. Check method documentation
            .filter { hasPermission() }
            .subscribeForUI()
            .subscribe(
                { onIncomingPermission() },
                { ui.showPermissionsError(it) }
            )
            .addToDisposables(subscriptions)
    }

    /**
     * We should request the permissions right away when user opens the screen, at least the first time.
     */
    fun showInitialPermissionsDialog() {
        if (shouldShowDialog()) {
            showPermissionDialog()
        }
    }

    private fun showPermissionDialog() {
        val activity = ui.activityReference().get()
        moreInfoBefore = permission.userNeedsMoreInfo(activity)
        permission.showPermissionDialog(activity)
    }

    private fun shouldShowDialog(): Boolean {
        return !hasPermission()
                && PermissionStatus.DONT_REMIND_DIALOG != permissionPreferences.getPermissionStatus(preferenceKey)
    }

    /**
     * If user denied permissions we need to check in which step of his denial we are
     */
    private fun savePermissionStatus(activity: Activity?) {
        val moreInfoAfter = permission.userNeedsMoreInfo(activity)
        when {
            hasPermission() -> permissionPreferences.storePermissionStatus(
                preferenceKey,
                PermissionStatus.PERMISSION_GIVEN
            )
            moreInfoAfter && !moreInfoBefore ->
                // Permission dialog was shown for first time
                permissionPreferences.storePermissionStatus(preferenceKey, PermissionStatus.FIRST_DIALOG)
            moreInfoAfter && moreInfoBefore ->
                // User deny permission without Never ask again checked
                permissionPreferences.storePermissionStatus(preferenceKey, PermissionStatus.SECOND_DIALOG)
            else -> {
                // User has checked Never ask again during this permission request OR
                // No permission dialog shown to user has user has previously checked Never ask again.
                permissionPreferences.storePermissionStatus(preferenceKey, PermissionStatus.DONT_REMIND_DIALOG)
            }
        }
    }

    /**
     * Called when user actively gives the permission through settings or dialog screens
     */
    private fun onIncomingPermission() {
        ui.onPermission(hasPermission())
    }
}

/**
 * @see [CameraPermission]
 */
class CameraPermissionDelegate(
    ui: PermissionsListener,
    permissionPreferences: PermissionPreferences
) : PermissionDelegate(ui, permissionPreferences) {
    override val permission: AndroidPermission = CameraPermission
    override val preferenceKey: String = PermissionPreferences.CAMERA_PERMISSION

    constructor(ui: PermissionsListener, context: Context?) : this(ui, PermissionPreferences(context.toAppContext()))
}

/**
 * @see [WriteContactsPermission]
 */
class WriteContactsPermissionDelegate(
    ui: PermissionsListener,
    permissionPreferences: PermissionPreferences
) : PermissionDelegate(ui, permissionPreferences) {
    override val permission: AndroidPermission = WriteContactsPermission
    override val preferenceKey: String = PermissionPreferences.WRITE_CONTACTS_PERMISSION

    constructor(ui: PermissionsListener, context: Context?) : this(ui, PermissionPreferences(context.toAppContext()))
}

/**
 * @see [ReadContactsPermission]
 */
class ReadContactsPermissionDelegate(
    ui: PermissionsListener,
    permissionPreferences: PermissionPreferences
) : PermissionDelegate(ui, permissionPreferences) {
    override val permission: AndroidPermission = ReadContactsPermission
    override val preferenceKey: String = PermissionPreferences.READ_CONTACTS_PERMISSION

    constructor(ui: PermissionsListener, context: Context?) : this(ui, PermissionPreferences(context.toAppContext()))
}
