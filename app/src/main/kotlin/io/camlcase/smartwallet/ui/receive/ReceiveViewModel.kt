/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.receive

import androidx.lifecycle.MutableLiveData
import io.camlcase.smartwallet.core.architecture.BaseViewModel
import io.camlcase.smartwallet.core.extension.addToDisposables
import io.camlcase.smartwallet.core.extension.subscribeForUI
import io.camlcase.smartwallet.data.source.local.UserPreferences
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.model.QR
import io.camlcase.smartwallet.source.qr.QRBuilder
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class ReceiveViewModel @Inject constructor(
    private val userPreferences: UserPreferences
) : BaseViewModel() {
    val address = MutableLiveData<DataWrapper<String>>()
    val qr = MutableLiveData<DataWrapper<QR>>()

    fun getUserData() {
        qr.value = DataWrapper.Loading()

        Single.fromCallable { userPreferences.getUserAddress() }
            .map { it.mainAddress }
            .subscribeForUI()
            .subscribe(
                {
                    address.value = DataWrapper.Success(it)
                    generateQR(it)
                },
                { address.value = DataWrapper.Error(it) }
            ).addToDisposables(disposables)
    }

    fun generateQR(address: String) {
        Single.fromCallable {
            QRBuilder
                .setQRHeight(QR_SIZE)
                .setQRWidth(QR_SIZE)
                .setQRUrl(address)
                .build()
        }
            .subscribeForUI()
            .subscribe(
                { qr.value = DataWrapper.Success(it) },
                { qr.value = DataWrapper.Error(it) }
            ).addToDisposables(disposables)
    }

    companion object {
        const val QR_SIZE = 600
    }
}
