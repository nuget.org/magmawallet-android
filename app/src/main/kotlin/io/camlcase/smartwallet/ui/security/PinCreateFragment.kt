/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.security

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.core.widget.doOnTextChanged
import by.kirich1409.viewbindingdelegate.viewBinding
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.Arguments
import io.camlcase.smartwallet.core.extension.*
import io.camlcase.smartwallet.data.usecase.analytics.ScreenEvent
import io.camlcase.smartwallet.databinding.FragmentPinCreateBinding
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.model.handleResults
import io.camlcase.smartwallet.model.navigation.PinNavigation
import io.camlcase.smartwallet.ui.base.BackPressOverrider
import io.camlcase.smartwallet.ui.base.BindBaseFragment
import io.camlcase.smartwallet.ui.base.showError

/**
 * Two stages of Set+Confirm to set a PIN for the app.
 */
class PinCreateFragment : BindBaseFragment(R.layout.fragment_pin_create), BackPressOverrider {

    interface PinCreateListener {
        fun onPinSet()
    }

    private val binding by viewBinding(FragmentPinCreateBinding::bind)
    private var listener: PinCreateListener? = null
    private lateinit var viewModel: PinCreateViewModel

    @PinNavigation.ScreenSource
    private var source: Int = PinNavigation.SOURCE_SETTINGS

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupToolbar(binding.appBarContainer.toolbar, false)
        handleStep(DataWrapper.Success(PinStep.TypePin(source)))
        keyboardListener = initKeyboardListener(binding.buttonContainer.actionNext)

        binding.buttonContainer.actionNext.setText(R.string.action_continue)
        bindViews()
        binding.pinContainer.pinInput.postDelayed({ binding.pinContainer.pinInput.showKeyboard() }, 50)

        viewModel.source = source
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        manageIntent(arguments)
        listener = context as? PinCreateListener
        if (listener == null) {
            throw ClassCastException("$context must implement PinCreateListener")
        }
        viewModel = viewModel(viewModelFactory) {
            observe(step, { handleStep(it) })
        }
    }

    private fun manageIntent(bundle: Bundle?) {
        bundle?.getInt(Arguments.ARG_SOURCE)?.let {
            source = it
        } ?: throw IllegalStateException("Screen needs an ARG_SOURCE argument")
    }

    private fun handleStep(wrapper: DataWrapper<PinStep>) {
        wrapper.handleResults(
            onValidData = { step ->
                when (step) {
                    is PinStep.TypePin -> {
                        if (step.source == PinNavigation.SOURCE_SETTINGS) {
                            initToolbar(getString(R.string.set_change_pin_enter_new), true)
                        } else {
                            analyticsUseCase.get()?.reportScreen(ScreenEvent.ONB_PIN_PROMPT)
                            initToolbar(getString(R.string.onb_pin_prompt_title), false)
                        }
                        binding.detail.show()
                        binding.pinContainer.pinInput.text?.clear()
                    }
                    is PinStep.ConfirmPin -> {
                        if (step.source == PinNavigation.SOURCE_SETTINGS) {
                            initToolbar(getString(R.string.set_change_pin_enter_confirm), true)
                        } else {
                            analyticsUseCase.get()?.reportScreen(ScreenEvent.ONB_PIN_CONFIRM)
                            initToolbar(getString(R.string.onb_pin_confirm_title), true)
                        }

                        binding.detail.invisible()
                        binding.pinContainer.pinInput.text?.clear()
                    }
                    PinStep.AllOK -> {
                        binding.pinContainer.pinInput.hideKeyboard()
                        hideLoading()
                        listener?.onPinSet()
                    }
                }
            },
            onError = {
                showError(it)
            },
            onLoading = {
                showLoading()
            }
        )
    }

    private fun initToolbar(title: String, backNavigation: Boolean) {
        setBackNavigation(binding.appBarContainer.toolbar, backNavigation)
        binding.appBarContainer.setupAppBar(title)
    }

    private fun showLoading() {
        binding.loading.root.show()
        binding.buttonContainer.actionNext.isEnabled = false
    }

    private fun hideLoading() {
        binding.loading.root.hide()
        binding.buttonContainer.actionNext.isEnabled = true
    }

    private fun next() {
        viewModel.next(binding.pinContainer.pinInput.text.toString())
    }

    private fun bindViews() {
        binding.buttonContainer.actionNext.throttledClick(500)
            .subscribe { next() }
            .addToDisposables(disposables)

        initPinPinput()
    }

    private fun initPinPinput() {
        binding.pinContainer.pinInput.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                if (validatePin(binding.pinContainer.pinInput.text.toString())) {
                    next()
                }
                true
            } else {
                false
            }
        }

        binding.pinContainer.pinInput.doOnTextChanged { text, _, _, _ ->
            val input = text.toString()
            validatePin(input)
        }
    }

    private fun validatePin(input: String): Boolean {
        return if (viewModel.valid(input)) {
            binding.pinContainer.pinInputLayout.error = null
            binding.pinContainer.pinError.hide()
            binding.buttonContainer.actionNext.isEnabled = true
            true
        } else {
            // TextInputLayout forces error messages to be shown at start.
            binding.pinContainer.pinError.text =
                if (viewModel.bundle.step is PinStep.ConfirmPin && !viewModel.correctType(input)) {
                    binding.pinContainer.pinInputLayout.error = " "
                    binding.pinContainer.pinError.show()
                    getString(R.string.onb_pin_error_confirm)
                } else {
                    binding.pinContainer.pinInputLayout.error = null
                    binding.pinContainer.pinError.hide()
                    null
                }
            binding.buttonContainer.actionNext.isEnabled = false
            false
        }
    }

    override fun overrideBackPressed(): Boolean {
        return viewModel.back()
    }

    companion object {

        fun init(@PinNavigation.ScreenSource source: Int): PinCreateFragment {
            val fragment = PinCreateFragment()
            val args = Bundle()
            args.putInt(Arguments.ARG_SOURCE, source)
            fragment.arguments = args
            return fragment
        }
    }
}

