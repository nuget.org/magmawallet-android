/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.security.pin

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.core.widget.doOnTextChanged
import by.kirich1409.viewbindingdelegate.viewBinding
import com.google.android.material.button.MaterialButton
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.*
import io.camlcase.smartwallet.databinding.FragmentPinCreateBinding
import io.camlcase.smartwallet.ui.base.BindBaseFragment
import io.camlcase.smartwallet.ui.base.showSnackbar
import io.camlcase.smartwallet.ui.security.lock.AuthLoginViewModel
import java.lang.ref.WeakReference

/**
 * Confirm user knows current PIN before changing it
 */
class PinConfirmFragment : BindBaseFragment(R.layout.fragment_pin_create) {
    interface PinConfirmListener {
        fun onPinConfirmed()
    }

    private val binding by viewBinding(FragmentPinCreateBinding::bind)
    private lateinit var pinViewModel: AuthLoginViewModel
    private var pinValidation: PinUIValidation? = null
    private var listener: PinConfirmListener? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as? PinConfirmListener
        pinViewModel = viewModel(viewModelFactory) {}
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initToolbar()
        binding.detail.hide()
        keyboardListener = initKeyboardListener(binding.buttonContainer.actionNext)
        binding.buttonContainer.actionNext.setText(R.string.action_continue)
        binding.pinContainer.pinInput.postDelayed({ binding.pinContainer.pinInput.showKeyboard() }, 50)
        bindViews()

        pinValidation = object : PinUIValidation {
            override val viewModel: AuthLoginViewModel = pinViewModel
            override val buttonRef: WeakReference<MaterialButton> = WeakReference(binding.buttonContainer.actionNext)
            override val errorViewRef: WeakReference<TextView> = WeakReference(binding.pinContainer.pinError)
            override val generalError: String = getString(R.string.error_invalid_pin)
            override fun onCorrectAuthentication(fromInput: Boolean) {
                if (fromInput) {
                    buttonRef.get()?.isEnabled = true
                } else {
                    listener?.onPinConfirmed()
                }
            }

            override fun showAlgorithmError() {
                val message = getString(R.string.error_generic) + " " + getString(R.string.error_try_again)
                binding.root.showSnackbar(message)
                binding.pinContainer.pinInput.setText("")
            }
        }
    }

    private fun bindViews() {
        initPinInput()

        binding.buttonContainer.actionNext.throttledClick()
            .subscribe { pinValidation?.validatePinFromButton(binding.pinContainer.pinInput.text.toString()) }
            .addToDisposables(disposables)
    }

    private fun initToolbar() {
        setupToolbar(binding.appBarContainer.toolbar, true)
        binding.appBarContainer.setupAppBar(getString(R.string.set_change_pin_verify_title))
    }

    private fun initPinInput() {
        binding.pinContainer.pinInput.doOnTextChanged { text, _, _, _ ->
            val input = text.toString()
            pinValidation?.validatePinFromInput(input)
        }
    }
}
