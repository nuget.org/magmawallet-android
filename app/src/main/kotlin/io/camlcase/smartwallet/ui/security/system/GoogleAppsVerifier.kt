/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.security.system

import com.google.android.gms.safetynet.SafetyNet
import io.camlcase.smartwallet.data.core.Logger
import io.camlcase.smartwallet.ui.base.BaseActivity
import java.lang.ref.WeakReference

/**
 * Handles Android Verify Apps functionality
 *
 * Ensure that the user's device has enabled Verify Apps when Magma starts. If Verify Apps has not been enabled,
 * redirects the user to the Security settings of the device to enable Verify Apps.
 */
interface GoogleAppsVerifier {
    var activityRef: WeakReference<BaseActivity>
    var onVerifierEnabled: () -> Unit
    var onVerifierDisabled: (GoogleAppsVerifierError, Exception?) -> Unit

    /**
     * Will show an informative dialog if it's not enabled.
     */
    fun isVerifierEnabled() {
        activityRef.get()?.apply {
            SafetyNet.getClient(this)
                .enableVerifyApps()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        if (task.result?.isVerifyAppsEnabled == true) {
                            Logger.i("✓️ Android Verifier Apps is enabled")
                            onVerifierEnabled()
                        } else {
                            onVerifierDisabled(GoogleAppsVerifierError.VERIFIER_DISABLED, null)
                        }
                    } else {
                        onVerifierDisabled(GoogleAppsVerifierError.GENERAL_ERROR, task.exception)
                    }
                }
        }
    }
}

enum class GoogleAppsVerifierError {
    GENERAL_ERROR,

    /**
     * The user didn't give consent to enable the Verify Apps feature or navigated away
     */
    VERIFIER_DISABLED
}
