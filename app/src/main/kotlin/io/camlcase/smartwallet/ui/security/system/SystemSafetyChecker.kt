/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.security.system

import io.camlcase.smartwallet.data.analytic.report
import io.camlcase.smartwallet.data.core.extension.multiLet
import io.camlcase.smartwallet.ui.base.BaseActivity
import io.camlcase.smartwallet.ui.base.bind
import io.camlcase.smartwallet.ui.base.navigate.RequestCode
import io.reactivex.rxjava3.disposables.CompositeDisposable
import java.lang.ref.WeakReference

/**
 * Run Google SafetyNet APIs.
 */
abstract class SystemSafetyChecker {
    abstract var globalActivityRef: WeakReference<BaseActivity>

    /**
     * In case we don't want to use the Activity's but the Fragment's
     */
    abstract var disposablesRef: WeakReference<CompositeDisposable>

    private var updater: SecurityProviderUpdater? = null
    private var appsVerifier: GoogleAppsVerifier? = null

    /**
     * Set up checkers
     */
    fun init() {
        updater = object : SecurityProviderUpdater() {
            override var activityRef: WeakReference<BaseActivity> = globalActivityRef
            override var onProviderOk: () -> Unit = {
                // Only go to the next check if the one before was a success
                appsVerifier?.isVerifierEnabled()
            }
        }

        appsVerifier = object : GoogleAppsVerifier {
            override var activityRef: WeakReference<BaseActivity> = globalActivityRef
            override var onVerifierEnabled: () -> Unit = { /* Nothng to do */ }
            override var onVerifierDisabled: (GoogleAppsVerifierError, Exception?) -> Unit =
                { _, _ -> /* Nothing to do*/ }
        }
        multiLet(globalActivityRef.get(), disposablesRef.get()) { activity, disposables ->
            activity.activityResult.bind(
                code = RequestCode.PROVIDER_ERROR_DIALOG_CODE,
                disposable = disposables,
                onResult = {
                    updater?.onProviderDialogResponse()
                },
                onError = { it.report() })
        }
    }

    /**
     * Run checkers
     */
    fun run() {
        updater?.init()
    }

    /**
     * Hack needed for [updater]
     */
    fun onResume() {
        updater?.onProviderPostResume()
    }
}
