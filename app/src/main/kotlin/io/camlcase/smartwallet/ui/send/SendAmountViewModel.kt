/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.send

import androidx.annotation.IntDef
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.smartwallet.core.architecture.BaseViewModel
import io.camlcase.smartwallet.core.extension.addToDisposables
import io.camlcase.smartwallet.core.extension.subscribeForUI
import io.camlcase.smartwallet.data.core.XTZ
import io.camlcase.smartwallet.data.core.extension.convertTo
import io.camlcase.smartwallet.data.core.extension.formatAsMoney
import io.camlcase.smartwallet.data.model.amount.Balance
import io.camlcase.smartwallet.data.model.amount.CoinBalance
import io.camlcase.smartwallet.data.model.amount.TokenBalance
import io.camlcase.smartwallet.data.model.exchange.TokenInfoBundle
import io.camlcase.smartwallet.data.model.isXTZ
import io.camlcase.smartwallet.data.usecase.info.GetBCDBalanceUseCase
import io.camlcase.smartwallet.model.DataWrapper
import java.math.BigDecimal
import javax.inject.Inject

/**
 * Checks initial token balance
 */
class SendAmountViewModel @Inject constructor(
    private val useCase: GetBCDBalanceUseCase
) : BaseViewModel() {
    private val _balance = MutableLiveData<DataWrapper<TokenInfoBundle>>()
    val balance: LiveData<DataWrapper<TokenInfoBundle>> = _balance

    var result = TokenInfoBundle.empty
    private var amount: BigDecimal = BigDecimal.ZERO

    fun getBalanceInfo(info: TokenInfoBundle?) {
        _balance.value = DataWrapper.Loading()

        if (info == null) {
            useCase.getTokenInfo(info ?: XTZ)
                .map {
                    result = it
                    it
                }
                .subscribeForUI()
                .subscribe(
                    { _balance.value = DataWrapper.Success(it) },
                    { _balance.value = DataWrapper.Error(it) }
                ).addToDisposables(disposables)
        } else {
            result = info
            _balance.value = DataWrapper.Success(info)
        }
    }

    @SendAmountValidation
    fun validAmount(amount: BigDecimal): Int {
        return when {
            amount.compareTo(BigDecimal.ZERO) == 0 -> IS_ZERO
            amount > result.balance.bigDecimal -> OVER_BALANCE
            else -> CORRECT
        }
    }

    /**
     * @param input Typed value of a token
     * @return Formatted string of the USD/EUR/... value of [input]
     */
    fun convertToCurrency(input: BigDecimal): String {
        saveAmount(input)
        return this.amount.convertTo(result.exchange.currencyExchange).formatAsMoney(useCase.getCurrencyBundle())
    }

    fun saveAmount(input: BigDecimal) {
        this.amount = input
    }

    fun getAmount(): Balance<*> {
        return if (result.token.isXTZ()) {
            CoinBalance(Tez(amount))
        } else {
            TokenBalance(amount, result.token.decimals)
        }
    }

    companion object {
        @IntDef(CORRECT, IS_ZERO, OVER_BALANCE)
        @Retention(AnnotationRetention.SOURCE)
        annotation class SendAmountValidation

        internal const val CORRECT = 0
        internal const val IS_ZERO = 1
        internal const val OVER_BALANCE = 2
    }
}
