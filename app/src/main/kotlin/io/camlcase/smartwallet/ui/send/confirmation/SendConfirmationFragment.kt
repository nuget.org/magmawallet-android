/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.send.confirmation

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import by.kirich1409.viewbindingdelegate.viewBinding
import com.google.android.material.snackbar.Snackbar
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.*
import io.camlcase.smartwallet.data.core.extension.formatAsMoney
import io.camlcase.smartwallet.data.model.amount.Balance
import io.camlcase.smartwallet.data.model.contact.MagmaContact
import io.camlcase.smartwallet.data.model.error.*
import io.camlcase.smartwallet.data.model.send.SendRequest
import io.camlcase.smartwallet.databinding.FragmentSendConfirmationBinding
import io.camlcase.smartwallet.databinding.ViewFullSendConfirmationBinding
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.model.handleResults
import io.camlcase.smartwallet.model.operation.OperationFeesBundle
import io.camlcase.smartwallet.ui.base.BindBaseFragment
import io.camlcase.smartwallet.ui.base.info.InfoType
import io.camlcase.smartwallet.ui.base.info.InfoView
import io.camlcase.smartwallet.ui.base.showError
import io.camlcase.smartwallet.ui.custom.ScrollOffsetListener
import io.camlcase.smartwallet.ui.send.SendActivity

/**
 * Review data enetered before confirming operation
 */
class SendConfirmationFragment : BindBaseFragment(R.layout.fragment_send_confirmation) {
    interface SendConfirmationListener {
        fun navigateToFeesDetail(fees: OperationFeesBundle)
        fun onSendOperationFinished(amount: Balance<*>, operationHash: String)
    }

    private val binding by viewBinding(FragmentSendConfirmationBinding::bind)
    private var confirmViewBinding: ViewFullSendConfirmationBinding? = null
    private var sendArgs: SendRequest? = null
    private var listener: SendConfirmationListener? = null
    private var errorListener: InfoView.InfoEventsListener? = null
    private var snackbar: Snackbar? = null
    private lateinit var viewModel: SendConfirmationViewModel
    private var contact: MagmaContact? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        manageIntent(arguments)
        listener = context as? SendConfirmationListener
        if (listener == null) {
            throw ClassCastException("$context must implement SendConfirmationListener")
        }
        errorListener = context as? InfoView.InfoEventsListener

        viewModel = viewModel(viewModelFactory) {
            observe(data, { handleDetails(it) })
            observe(operation, { handleOperation(it) })
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getConfirmationDetails(sendArgs!!)
    }

    private fun manageIntent(bundle: Bundle?) {
        bundle?.getParcelable<SendRequest>(SendActivity.ARG_SEND_FIELDS)?.let {
            sendArgs = it
        } ?: throw IllegalStateException("Screen needs an ARGS_SEND_TO argument")

        bundle.getParcelable<MagmaContact>(SendActivity.ARG_CONTACT)?.let {
            contact = it
        }
    }

    private fun initConfirmationView() {
        binding.viewError.hide()
        confirmViewBinding =
            ViewFullSendConfirmationBinding.inflate(LayoutInflater.from(context), binding.root, true)
        initAppBar()
        confirmViewBinding?.buttonContainer?.actionNext?.setText(R.string.action_confirm)
        contact?.apply {
            confirmViewBinding?.recipientTitle?.text = getString(R.string.snd_address_contact_section, this.name)
        }
    }

    private fun initAppBar() {
        confirmViewBinding?.apply {
            setupToolbar(this.appBarContainer.toolbar, true)
            val title = getString(R.string.snd_confirmation_title)
            this.appBarContainer.expandedTitle.text = title
            this.appBarContainer.appBar.addOnOffsetChangedListener(
                ScrollOffsetListener(
                    this.appBarContainer.expandedTitle,
                    this.appBarContainer.toolbar,
                    title
                )
            )
        }
    }

    private fun initGeneralErrorView() {
        confirmViewBinding?.container.hide()
        binding.viewError.show()
    }

    private fun handleDetails(wrapper: DataWrapper<ConfirmationForView>) {
        wrapper.handleResults(
            onValidData = {
                hideLoading()
                initConfirmationView()
                bindViews()

                confirmViewBinding?.apply {
                    this.buttonContainer.actionNext.isEnabled = true
                    this.sectionsContainer.sendSection.setValues(it.amountInTez, it.amountInCurrency)
                    this.sectionsContainer.feesSection.setValues(
                        it.totalfeesFormatted,
                        it.fees.totalFeesInCurrency
                    )
                    // Assumes handleDetails will only be called once.
                    confirmViewBinding?.sectionsContainer?.feesSection?.makeClickable()
                        ?.subscribe { _ -> listener?.navigateToFeesDetail(it.fees) }
                        ?.addToDisposables(disposables)
                    this.sectionsContainer.totalSection.setValues(it.total, it.totalInUSD)
                    this.recipient.text = it.addressTo
                }

            },
            onError = {
                hideLoading()
                handleInitialError(it)
            },
            onLoading = { showLoading() }
        )
    }

    private fun handleInitialError(error: Throwable?) {
        showConfirmationError(error)
    }

    private fun handleOperation(wrapper: DataWrapper<Pair<String, Balance<*>>>) {
        wrapper.handleResults(
            onValidData = {
                hideLoading()
                listener?.onSendOperationFinished(it.second, it.first)
            },
            onError = {
                hideLoading()
                confirmViewBinding?.buttonContainer?.actionNext?.isEnabled = false
                if (it is AppError && it.exception is OperationInProgress) {
                    showConfirmationError(it)
                } else {
                    showError(it)
                }
            },
            onLoading = { showLoading() }
        )
    }

    private fun initFields() {
        sendArgs?.let {
            confirmViewBinding?.sectionsContainer?.sendSection?.setValues(
                it.amount.format(it.token),
                0.0.formatAsMoney(viewModel.getCurrencyBundle())
            )
            confirmViewBinding?.recipient?.text = it.to
        }
    }

    private fun showConfirmationError(error: Throwable?) {
        if (error is AppError) {
            when (error.type) {
                ErrorType.EXPIRED_DATA -> {
                    initGeneralErrorView()
                    binding.viewError.init(
                        InfoType.SEND_GENERAL_ERROR,
                        errorListener,
                        listOf(getString(R.string.error_invalid_amount))
                    )
                }
                ErrorType.BLOCKED_OPERATION -> {
                    val info = when (error.exception) {
                        is InvalidAddressError -> InfoType.SEND_ERROR_INVALID_ADDRESS
                        is InsufficientXTZ -> InfoType.SEND_ERROR_INSUFFICIENT_XTZ
                        is OperationInProgress -> InfoType.OPERATION_COUNTER
                        else -> InfoType.SEND_GENERAL_ERROR
                    }
                    initGeneralErrorView()
                    binding.viewError.init(info, errorListener)
                }
                else -> showConfirmationViewForError(error)
            }
        } else {
            showConfirmationViewForError(error)
        }
    }

    private fun showConfirmationViewForError(error: Throwable?) {
        initConfirmationView()
        initFields()
        confirmViewBinding?.buttonContainer?.actionNext?.isEnabled = false
        showError(error)
    }

    private fun isStubInflated(): Boolean {
        return confirmViewBinding != null
    }

    private fun showLoading() {
        if (isStubInflated()) {
            confirmViewBinding?.sendLoading?.containerLoading.show()
            confirmViewBinding?.buttonContainer?.actionNext?.isEnabled = false
        } else {
            binding.parentLoading.containerLoading.show()
        }
    }

    private fun hideLoading() {
        if (isStubInflated()) {
            confirmViewBinding?.sendLoading?.containerLoading.hide()
            confirmViewBinding?.buttonContainer?.actionNext?.isEnabled = true
        } else {
            binding.parentLoading.containerLoading.hide()
        }
    }

    private fun bindViews() {
        confirmViewBinding?.buttonContainer?.actionNext.throttledClick()
            .subscribe {
                viewModel.send()
            }
            .addToDisposables(disposables)
    }

    override fun onDestroyView() {
        binding.viewError.unbind()
        snackbar?.dismiss()
        super.onDestroyView()
    }

    companion object {

        fun init(request: SendRequest, contact: MagmaContact? = null): SendConfirmationFragment {
            val fragment = SendConfirmationFragment()
            val args = Bundle()
            args.putParcelable(SendActivity.ARG_SEND_FIELDS, request)
            args.putParcelable(SendActivity.ARG_CONTACT, contact)
            fragment.arguments = args
            return fragment
        }
    }
}
