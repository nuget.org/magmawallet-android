package io.camlcase.smartwallet.core.extension

import io.camlcase.smartwallet.data.core.Wallet
import io.camlcase.smartwallet.data.core.extension.formatAsMoney
import io.camlcase.smartwallet.data.model.exchange.limitPrecision
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertTrue
import org.junit.Test

/**
 * CurrencyFormatter doesn't show symbols on test. $ will be USD, € will be EUR
 */
class CurrencyExtKtTest {
    private val symbol: String = Wallet.DEFAULT_CURRENCY_SYMBOL

    @Test
    fun `Format 0 BigDecimal USD`() {
        val expected = "${symbol}0.00"
        val input = 0.0.toBigDecimal()

        val result = input.formatAsMoney(Wallet.defaultCurrency)

        assertEquals(expected, result)
    }

    @Test
    fun `Format 1_400 BigDecimal USD`() {
        val expected = "${symbol}1,400.00"
        val input = 1_400.toBigDecimal()

        val result = input.formatAsMoney(Wallet.defaultCurrency)

        assertEquals(expected, result)
    }

    // 098 should be rounded to 1
    @Test
    fun `Format 1_956_423-098 BigDecimal USD`() {
        val expected = "${symbol}1,956,423.10"
        val input = 1_956_423.098.toBigDecimal()

        val result = input.formatAsMoney(Wallet.defaultCurrency)

        assertEquals(expected, result)
    }

    // 0055 should be rounded to 01
    @Test
    fun `Format 23-0055 BigDecimal USD`() {
        val expected = "${symbol}23.01"
        val input = 23.0055.toBigDecimal()

        val result = input.formatAsMoney(Wallet.defaultCurrency)

        assertEquals(expected, result)
    }

    // 003 should be rounded to 00
    @Test
    fun `Format 0-003456 BigDecimal USD`() {
        val expected = "${symbol}0.00"
        val input = 0.003456.toBigDecimal()

        val result = input.formatAsMoney(Wallet.defaultCurrency)

        assertEquals(expected, result)
    }


    @Test
    fun `Format 0 Double USD`() {
        val expected = "${symbol}0.00"
        val input = 0.0

        val result = input.formatAsMoney(Wallet.defaultCurrency)

        assertEquals(expected, result)
    }

    @Test
    fun `Format 1_400 Double USD`() {
        val expected = "${symbol}1,400.00"
        val input = 1_400.0

        val result = input.formatAsMoney(Wallet.defaultCurrency)

        assertEquals(expected, result)
    }

    // 098 should be rounded to 1
    @Test
    fun `Format 1_956_423-098 Double USD`() {
        val expected = "${symbol}1,956,423.10"
        val input = 1_956_423.098

        val result = input.formatAsMoney(Wallet.defaultCurrency)

        assertEquals(expected, result)
    }

    // 0055 should be rounded to 01
    @Test
    fun `Format 23-0055 Double USD`() {
        val expected = "${symbol}23.01"
        val input = 23.0055

        val result = input.formatAsMoney(Wallet.defaultCurrency)

        assertEquals(expected, result)
    }

    // 003 should be rounded to 003
    @Test
    fun `Format 0-003456 Double USD`() {
        val expected = "${symbol}0.003"
        val input = 0.003456

        val result = input.formatAsMoney(Wallet.defaultCurrency, flexibleDecimals = true)

        assertEquals(expected, result)
    }

    // 003 should be rounded to 003
    @Test
    fun `Format 0-003 Double USD`() {
        val expected = "${symbol}0.003"
        val input = 0.003

        val result = input.formatAsMoney(Wallet.defaultCurrency, flexibleDecimals = true)

        assertEquals(expected, result)
    }

    // 00045 should be rounded to 0004
    @Test
    fun `Format 0-000456 Double USD`() {
        val expected = "${symbol}0.0004"
        val input = 0.000456

        val result = input.formatAsMoney(Wallet.defaultCurrency, flexibleDecimals = true)

        assertEquals(expected, result)
    }

    // 00005
    @Test
    fun `Format 0-000058 Double USD`() {
        val expected = "${symbol}0.000058"
        val input = 0.000_058

        val result = input.formatAsMoney(Wallet.defaultCurrency, flexibleDecimals = true)

        assertEquals(expected, result)
    }

    @Test
    fun `Format 0-000 008 Double USD`() {
        val expected = "${symbol}0.000008"
        val input = 0.000_008

        val result = input.formatAsMoney(Wallet.defaultCurrency, flexibleDecimals = true)

        assertEquals(expected, result)
    }

    @Test
    fun `formatAsMoney flexibleDecimals 0-000 005`() {
        val expected = "${symbol}0.00"
        val input = 0.000_000_5

        val result = input.formatAsMoney(Wallet.defaultCurrency, flexibleDecimals = true)

        assertEquals(expected, result)
    }

    // 00005 should be rounded to 0000
    @Test
    fun `Format 0-000056 Double USD`() {
        val expected = "${symbol}0.00"
        val input = 0.000056

        val result = input.formatAsMoney(Wallet.defaultCurrency)

        assertEquals(expected, result)
    }

    @Test
    fun `Truncate 4 decimals`() {
        val result = 0.003456.limitPrecision(3)
        assertTrue(result.equals(0.003))
    }

    @Test
    fun `Truncate 1 decimals`() {
        val result = 0.025.limitPrecision(1)
        assertTrue(result.equals(0.0))
    }

    @Test
    fun `Truncate 1 decimals (2)`() {
        val result = 0.099999.limitPrecision(1)
        assertTrue(result.equals(0.0))
    }
}
