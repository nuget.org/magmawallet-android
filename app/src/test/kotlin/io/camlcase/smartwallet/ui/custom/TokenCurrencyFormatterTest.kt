package io.camlcase.smartwallet.ui.custom

import io.camlcase.smartwallet.data.core.extension.formatToTezDecimals
import junit.framework.TestCase.assertEquals
import org.junit.Test
import java.util.*

/**
 * [TokenCurrencyFormatter]
 */
class TokenCurrencyFormatterTest {

    /**
     * STATIC METHOD
     */

    @Test
    fun `format empty string`() {
        val input = ""
        val expected = "0"
        val result = input.formatToTezDecimals(Locale.US, 6)
        assertEquals(expected, result)
    }

    @Test
    fun `format no numbers string`() {
        val input = "ABCD&"
        val expected = "0"
        val result = input.formatToTezDecimals(Locale.US, 6)
        assertEquals(expected, result)
    }

    @Test
    fun `format 0-00000006 string`() {
        val input = "0.00000006"
        val expected = "0.000006"
        val result = input.formatToTezDecimals(Locale.US, 6)
        assertEquals(expected, result)
    }

    @Test
    fun `format 0-72 string`() {
        val input = "072"
        val expected = "0.000072"
        val result = input.formatToTezDecimals(Locale.US, 6)
        assertEquals(expected, result)
    }

    @Test
    fun `format 0-72 with symbols string`() {
        val input = "%07$2"
        val expected = "0.000072"
        val result = input.formatToTezDecimals(Locale.US, 6)
        assertEquals(expected, result)
    }

    @Test
    fun `format 1_234_567 with symbols string`() {
        val expected = "1.234567"
        val input = "1234567"
        val result = input.formatToTezDecimals(Locale.US, 6)
        assertEquals(expected, result)
    }

    @Test
    fun `format 1-00 string`() {
        val input = "1.000000"
        val expected = "1"
        val result = input.formatToTezDecimals(Locale.US, 6)
        assertEquals(expected, result)
    }

    @Test
    fun `format 1_234_789-0900 string`() {
        val input = "1234789.0900"
        val expected = "12,347.8909"
        val result = input.formatToTezDecimals(Locale.US, 6)
        assertEquals(expected, result)
    }
}
