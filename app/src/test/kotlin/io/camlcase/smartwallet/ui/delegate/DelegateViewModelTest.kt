package io.camlcase.smartwallet.ui.delegate

import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.TezosProtocol
import io.camlcase.kotlintezos.model.operation.OperationType
import io.camlcase.kotlintezos.model.operation.fees.OperationFees
import io.camlcase.kotlintezos.operation.fees.DefaultFeeEstimator
import io.camlcase.smartwallet.core.extension.ellipsize
import io.camlcase.smartwallet.data.core.Delegate
import io.camlcase.smartwallet.data.core.extension.format
import io.camlcase.smartwallet.data.model.delegate.TezosBaker
import io.camlcase.smartwallet.data.model.delegate.TezosBakerResponse
import io.camlcase.smartwallet.data.model.error.AppError
import io.camlcase.smartwallet.data.model.error.ErrorType
import io.camlcase.smartwallet.data.source.local.UserPreferences
import io.camlcase.smartwallet.data.usecase.delegate.GetDelegateUseCase
import io.camlcase.smartwallet.data.usecase.operation.DelegateUseCase
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.test.RxTest
import io.camlcase.smartwallet.test.mock.mainTestnetAddress
import io.mockk.*
import io.reactivex.rxjava3.core.Single
import junit.framework.TestCase
import org.junit.After
import org.junit.Assert
import org.junit.Test
import java.io.IOException
import java.util.concurrent.CountDownLatch

/**
 * @see DelegateViewModel
 */
class DelegateViewModelTest : RxTest() {
    private val useCase = mockk<DelegateUseCase>(relaxed = true)
    private val infoUseCase = mockk<GetDelegateUseCase>(relaxed = true)
    private val userPreferences = mockk<UserPreferences>(relaxed = true)

    private fun initViewModel(): DelegateViewModel {
        return spyk(DelegateViewModel(useCase, infoUseCase, userPreferences))
    }

    @Test
    fun `Test getDefaultDelegationFee OK`() {
        val fee = Tez(123.334)
        every { useCase.getDefaultDelegationFee() } returns OperationFees(fee, 1, 1)

        val viewModel = initViewModel()
        val result = viewModel.getDefaultDelegationFee()

        Assert.assertTrue(!result.isBlank())
        Assert.assertTrue(result == fee.format(null))

        verify {
            useCase.getDefaultDelegationFee()
        }
    }

    @Test
    fun `delegate +Entered delegate OK`() {
        val hash = "ooo000"
        every { useCase.delegate(BAKER) } returns Single.just(hash)
        val viewModel = initViewModel()
        viewModel.selected = baker

        viewModel.delegateWithAmountCheck { _, _, _ ->
            Assert.fail("No warning should be called")
        }

        viewModel.operation.value.apply {
            Assert.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Success)
            Assert.assertTrue((this as DataWrapper.Success).data!!.first == BAKER)
            Assert.assertTrue((this as DataWrapper.Success).data!!.second == hash)
        }

        verify {
            useCase.delegate(BAKER)
            userPreferences.getTezBalance()
            viewModel.clearOperationSubscription()
        }
    }

    @Test
    fun `delegate +Selected delegate +Min balance KO`() {
        val min = 1.000_001
        val balance = 1.0
        every { userPreferences.getTezBalance() } returns Tez(balance)
        val viewModel = initViewModel()
        viewModel.selected = TezosBakerResponse(
            "",
            BAKER,
            Delegate.BAKER_MINIMUM_FEE,
            null,
            Delegate.BAKER_MINIMUM_ROI,
            null,
            null,
            null,
            true,
            min
        )

        val waiter = CountDownLatch(1)
        viewModel.delegateWithAmountCheck { bakerAddress, minAmount, xtzBalance ->
            Assert.assertTrue(bakerAddress == BAKER)
            Assert.assertTrue(minAmount == Tez(min))
            Assert.assertTrue(xtzBalance == Tez(balance))
            waiter.countDown()
        }
        waiter.await()
        verify {
            userPreferences.getTezBalance()
        }
    }

    @Test
    fun `delegate +Selected delegate +Min balance OK`() {
        val min = 1.000_001
        val balance = 1.000_002
        val hash = "ooo000"
        every { useCase.delegate(BAKER) } returns Single.just(hash)
        every { userPreferences.getTezBalance() } returns Tez(balance)
        val viewModel = initViewModel()
        viewModel.selected = TezosBakerResponse(
            "",
            BAKER,
            Delegate.BAKER_MINIMUM_FEE,
            null,
            Delegate.BAKER_MINIMUM_ROI,
            null,
            null,
            null,
            true,
            min
        )

        viewModel.delegateWithAmountCheck { _, _, _ ->
            Assert.fail("No warning should be called")
        }
        verify {
            useCase.delegate(BAKER)
            userPreferences.getTezBalance()
            viewModel.clearOperationSubscription()
        }
    }

    @Test
    fun `delegate +Selected delegate +Min balance OK 2`() {
        val min = 1.000_001
        val balance = min
        val hash = "ooo000"
        every { useCase.delegate(BAKER) } returns Single.just(hash)
        every { userPreferences.getTezBalance() } returns Tez(balance)
        val viewModel = initViewModel()
        viewModel.selected = TezosBakerResponse(
            "",
            BAKER,
            Delegate.BAKER_MINIMUM_FEE,
            null,
            Delegate.BAKER_MINIMUM_ROI,
            null,
            null,
            null,
            true,
            min
        )

        viewModel.delegateWithAmountCheck { _, _, _ ->
            Assert.fail("No warning should be called")
        }
        verify {
            useCase.delegate(BAKER)
            userPreferences.getTezBalance()
            viewModel.clearOperationSubscription()
        }
    }

    @Test
    fun `delegate +Entered delegate KO`() {
        every { useCase.delegate(BAKER) } returns Single.error(expected)
        val viewModel = initViewModel()
        viewModel.selected = baker

        viewModel.delegateWithAmountCheck { _, _, _ ->
            Assert.fail("No warning should be called")
        }

        viewModel.operation.value.apply {
            Assert.assertNotNull(this)
            Assert.assertTrue(this is DataWrapper.Error)
            Assert.assertTrue((this as DataWrapper.Error).error == expected)
        }

        verify {
            useCase.delegate(BAKER)
            userPreferences.getTezBalance()
            viewModel.clearOperationSubscription()
        }
    }

    @Test
    fun `Test parseSelection Baker with name`() {
        val baker = TezosBakerResponse("A", BAKER)

        val viewModel = initViewModel()
        val result = viewModel.parseSelection(baker)

        Assert.assertTrue(result == baker.name)
    }

    @Test
    fun `Test parseSelection Baker only address`() {

        val viewModel = initViewModel()
        val result = viewModel.parseSelection(baker)

        Assert.assertTrue(result == BAKER.ellipsize())
    }

    @Test
    fun `Test getCurrentBaker only address OK`() {
        every { infoUseCase.getDelegate() } returns Single.just(Result.success(TezosBaker(null, BAKER)))
        every { userPreferences.getTezBalance() } returns Tez(1.0)

        val viewModel = initViewModel()
        viewModel.getCurrentBaker()

        viewModel.data.value.apply {
            Assert.assertNotNull(this)
            Assert.assertTrue(this is DataWrapper.Success)
            Assert.assertTrue((this as DataWrapper.Success).data == BAKER.ellipsize())
        }

        verify {
            infoUseCase.getDelegate()
            userPreferences.getTezBalance()
        }
    }

    @Test
    fun `Test getCurrentBaker only address Failure`() {
        every { infoUseCase.getDelegate() } returns Single.just(Result.failure(expected))
        every { userPreferences.getTezBalance() } returns Tez(1.0)

        val viewModel = initViewModel()
        viewModel.getCurrentBaker()

        viewModel.data.value.apply {
            Assert.assertNotNull(this)
            Assert.assertTrue(this is DataWrapper.Empty)
        }

        verify {
            infoUseCase.getDelegate()
            userPreferences.getTezBalance()
        }
    }

    @Test
    fun `Test getCurrentBaker only address KO`() {
        every { infoUseCase.getDelegate() } returns Single.error(expected)
        every { userPreferences.getTezBalance() } returns Tez(1.0)

        val viewModel = initViewModel()
        viewModel.getCurrentBaker()

        viewModel.data.value.apply {
            Assert.assertNotNull(this)
            Assert.assertTrue(this is DataWrapper.Error)
            Assert.assertTrue((this as DataWrapper.Error).error == expected)
        }

        verify {
            infoUseCase.getDelegate()
            userPreferences.getTezBalance()
        }
    }

    @Test
    fun `Test getCurrentBaker no baker OK`() {
        every { infoUseCase.getDelegate() } returns Single.just(Result.failure(AppError(ErrorType.NO_BAKER)))
        every { userPreferences.getTezBalance() } returns Tez(1.0)

        val viewModel = initViewModel()
        viewModel.getCurrentBaker()

        viewModel.data.value.apply {
            Assert.assertNotNull(this)
            Assert.assertTrue(this is DataWrapper.Empty)
        }

        verify {
            infoUseCase.getDelegate()
            userPreferences.getTezBalance()
        }
    }

    @Test
    fun `Test validDelegate valid`() {
        every { userPreferences.getTezBalance() } returns Tez(1.0)
        every { useCase.getDefaultDelegationFee() } returns delegationFees

        val viewModel = initViewModel()
        viewModel.selected = baker

        val result = viewModel.validDelegate()
        Assert.assertTrue(result)

        verify {
            userPreferences.getTezBalance()
            useCase.getDefaultDelegationFee()
        }
    }

    @Test
    fun `Test validDelegate invalid empty`() {
        val viewModel = initViewModel()

        val result = viewModel.validDelegate()
        Assert.assertFalse(result)
    }

    @Test
    fun `Test validDelegate invalid no address`() {
        val viewModel = initViewModel()
        viewModel.selected = TezosBakerResponse("", "tz1ADDRESS")

        val result = viewModel.validDelegate()
        Assert.assertFalse(result)
    }

    @Test
    fun `validDelegate KO 0 xtz`() {
        every { userPreferences.getTezBalance() } returns Tez(0.0)
        every { useCase.getDefaultDelegationFee() } returns delegationFees

        val viewModel = initViewModel()
        viewModel.selected = baker

        val result = viewModel.validDelegate()
        Assert.assertFalse(result)
        verify {
            userPreferences.getTezBalance()
            useCase.getDefaultDelegationFee()
        }
    }

    @Test
    fun `validDelegate KO delegation fee xtz`() {
        every { userPreferences.getTezBalance() } returns Tez(0.001_257)
        every { useCase.getDefaultDelegationFee() } returns delegationFees

        val viewModel = initViewModel()
        viewModel.selected = baker

        val result = viewModel.validDelegate()
        Assert.assertFalse(result)
        verify {
            userPreferences.getTezBalance()
            useCase.getDefaultDelegationFee()
        }
    }

    @Test
    fun `validDelegate OK Over fee`() {
        every { userPreferences.getTezBalance() } returns Tez(0.001_258)
        every { useCase.getDefaultDelegationFee() } returns delegationFees

        val viewModel = initViewModel()
        viewModel.selected = baker

        val result = viewModel.validDelegate()
        Assert.assertTrue(result)

        verify {
            userPreferences.getTezBalance()
            useCase.getDefaultDelegationFee()
        }
    }

    @After
    fun after() {
        confirmVerified(useCase)
        confirmVerified(infoUseCase)
        confirmVerified(userPreferences)
    }

    companion object {
        val expected = IOException("ERROR")

        const val BAKER = mainTestnetAddress
        val baker = TezosBakerResponse("", BAKER)

        val delegationFees = DefaultFeeEstimator.calculateFees(TezosProtocol.CARTHAGE, OperationType.DELEGATION)
    }
}
