package io.camlcase.smartwallet.ui.exchange.amount

import io.camlcase.kotlintezos.model.Tez
import io.camlcase.smartwallet.data.core.XTZ
import io.camlcase.smartwallet.data.model.Token
import io.camlcase.smartwallet.data.model.amount.CoinBalance
import io.camlcase.smartwallet.data.model.amount.TokenBalance
import io.camlcase.smartwallet.data.model.exchange.DexterBalance
import io.camlcase.smartwallet.data.model.exchange.FATokenInfo
import io.camlcase.smartwallet.data.model.exchange.TokenInfoBundle
import io.camlcase.smartwallet.data.model.exchange.XTZTokenInfo
import io.camlcase.smartwallet.data.usecase.info.GetBCDBalanceUseCase
import io.camlcase.smartwallet.model.navigation.ExchangeChooseTokenNavigation
import io.camlcase.smartwallet.test.RxTest
import io.camlcase.smartwallet.test.mock.TEST_TOKEN
import io.mockk.confirmVerified
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.After
import org.junit.Assert
import org.junit.Test
import java.math.BigInteger

/**
 * @see ChooseTokenViewModel
 */
class ChooseTokenViewModelTest : RxTest() {
    private val useCase = mockk<GetBCDBalanceUseCase>(relaxed = true)

    @Test
    fun `Test getBalances Filter zeroBalance OK`() {
        val expected = listOf(
            TokenInfoBundle(
                TEST_TOKEN,
                TokenBalance(BigInteger.ONE, TEST_TOKEN.decimals),
                FATokenInfo(TEST_TOKEN, 1.0, dexterBalance)
            ),
            TokenInfoBundle(
                testToken2,
                TokenBalance(BigInteger.ZERO, testToken2.decimals),
                FATokenInfo(testToken2, 1.0, dexterBalance)
            ),
            TokenInfoBundle(
                XTZ,
                CoinBalance(Tez(1.0)), XTZTokenInfo(1.0)
            )
        )
        every { useCase.getLocalBalances() } returns expected

        val viewModel = ChooseTokenViewModel(useCase)
        val result = viewModel.getBalances(ExchangeChooseTokenNavigation.SOURCE_FROM, null)
        Assert.assertTrue(result.size == (expected.size - 1))
        Assert.assertTrue(result[0] == expected[2]) // XTZ should be sorted at the beginning

        verify {
            useCase.getLocalBalances()
        }
    }

    @Test
    fun `Test getBalances Filter no DexterBalance OK`() {
        val expected = listOf(
            TokenInfoBundle(
                TEST_TOKEN,
                TokenBalance(BigInteger.ONE, TEST_TOKEN.decimals),
                FATokenInfo(TEST_TOKEN, 1.0, dexterBalance)
            ),
            TokenInfoBundle(
                testToken2,
                TokenBalance(BigInteger.ONE, testToken2.decimals),
                FATokenInfo(testToken2, 1.0, DexterBalance.empty)
            ),
            TokenInfoBundle(
                XTZ,
                CoinBalance(Tez(1.0)), XTZTokenInfo(1.0)
            )
        )
        every { useCase.getLocalBalances() } returns expected

        val viewModel = ChooseTokenViewModel(useCase)
        val result = viewModel.getBalances(ExchangeChooseTokenNavigation.SOURCE_TO, null)
        Assert.assertTrue(result.size == expected.size - 1)
        Assert.assertTrue(result[0] == expected[2]) // XTZ should be sorted at the beginning

        verify {
            useCase.getLocalBalances()
        }
    }

    @Test
    fun `Test getBalances Filter null selected token OK`() {
        val expected = listOf(
            TokenInfoBundle(
                TEST_TOKEN,
                TokenBalance(BigInteger.ONE, TEST_TOKEN.decimals),
                FATokenInfo(TEST_TOKEN, 1.0, dexterBalance)
            ),
            TokenInfoBundle(
                testToken2,
                TokenBalance(BigInteger.ZERO, testToken2.decimals),
                FATokenInfo(testToken2, 1.0, dexterBalance)
            ),
            TokenInfoBundle(
                XTZ,
                CoinBalance(Tez(1.0)), XTZTokenInfo(1.0)
            )
        )
        every { useCase.getLocalBalances() } returns expected

        val viewModel = ChooseTokenViewModel(useCase)
        val result = viewModel.getBalances(ExchangeChooseTokenNavigation.SOURCE_TO, null)
        Assert.assertTrue(result.size == expected.size)
        Assert.assertTrue(result[0] == expected[2]) // XTZ should be sorted at the beginning

        verify {
            useCase.getLocalBalances()
        }
    }

    @Test
    fun `Test getBalances Filter selected token OK`() {
        val expected = listOf(
            TokenInfoBundle(
                TEST_TOKEN,
                TokenBalance(BigInteger.ONE, TEST_TOKEN.decimals),
                FATokenInfo(TEST_TOKEN, 1.0, dexterBalance)
            ),
            TokenInfoBundle(
                testToken2,
                TokenBalance(BigInteger.ONE, testToken2.decimals),
                FATokenInfo(testToken2, 1.0, dexterBalance)
            ),
            TokenInfoBundle(
                XTZ,
                CoinBalance(Tez(1.0)), XTZTokenInfo(1.0)
            )
        )
        every { useCase.getLocalBalances() } returns expected

        val viewModel = ChooseTokenViewModel(useCase)
        val result = viewModel.getBalances(ExchangeChooseTokenNavigation.SOURCE_TO, testToken2)
        Assert.assertTrue(result.size == (expected.size - 1))
        Assert.assertTrue(result[0] == expected[2]) // XTZ should be sorted at the beginning

        verify {
            useCase.getLocalBalances()
        }
    }

    @After
    fun after() {
        confirmVerified(useCase)
    }

    companion object {
        val dexterBalance = DexterBalance(Tez(100.0), BigInteger("10000"))

        val testToken2 = Token(
            "TestToken 2",
            "tsTk2",
            0,
            "KT1",
            "KT1"
        )
    }
}
