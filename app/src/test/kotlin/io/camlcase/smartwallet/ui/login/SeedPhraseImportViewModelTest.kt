package io.camlcase.smartwallet.ui.login

import io.camlcase.smartwallet.core.Onboarding
import io.camlcase.smartwallet.data.model.error.AppError
import io.camlcase.smartwallet.data.model.error.ErrorType
import io.camlcase.smartwallet.data.usecase.analytics.AnalyticEvent
import io.camlcase.smartwallet.data.usecase.analytics.SendAnalyticUseCase
import io.camlcase.smartwallet.data.usecase.analytics.StateEvent
import io.camlcase.smartwallet.data.usecase.user.CreateWalletUseCase
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.test.RxTest
import io.camlcase.smartwallet.test.mock.mainTestnetAddress
import io.camlcase.smartwallet.test.mock.mnemonicList
import io.camlcase.smartwallet.test.mock.testWallet
import io.mockk.*
import org.junit.After
import org.junit.Assert
import org.junit.Test

class SeedPhraseImportViewModelTest : RxTest() {
    private val useCase = mockk<CreateWalletUseCase>(relaxed = true) {
        every { recoverHDWallet(any(), any(), any()) } returns testWallet
    }
    private val analytic = mockk<SendAnalyticUseCase>(relaxed = true)
    private lateinit var viewModel: SeedPhraseImportViewModel

    private fun init(): SeedPhraseImportViewModel {
        viewModel = spyk(SeedPhraseImportViewModel(useCase, analytic))
        return viewModel
    }

    @After
    fun tearDown() {
        confirmVerified(useCase)
        confirmVerified(analytic)
        confirmVerified(viewModel)
    }

    @Test
    fun `importWallet NoPassphrase DefaultDerivationPath OK`() {
        init().importWallet(mnemonicList, null, Onboarding.BIP44_DEFAULT)
        viewModel.data.value.apply {
            println(this)
            Assert.assertNotNull(this)
            Assert.assertTrue(this is DataWrapper.Success)

            val result = (this as DataWrapper.Success).data!!
            Assert.assertTrue(result.sdAddress == mainTestnetAddress)
            Assert.assertTrue(result.hdAddress == mainTestnetAddress)
        }

        verify {
            viewModel.importWallet(mnemonicList, null, Onboarding.BIP44_DEFAULT)
            viewModel.data
            viewModel.disposables
            useCase.recoverHDWallet(any(), any(), any())
            analytic.reportEvent(AnalyticEvent.WALLET_RECOVERED)
            analytic.reportState(StateEvent.ONB_RECOVERY_ENTRY_SEED_PHRASE)
            analytic.reportState(StateEvent.ONB_RECOVERY_ENTRY_SUCCESS)
        }
    }

    @Test
    fun `importWallet Passphrase DefaultDerivationPath OK`() {
        init().importWallet(mnemonicList, "null", Onboarding.BIP44_DEFAULT)
        viewModel.data.value.apply {
            println(this)
            Assert.assertNotNull(this)
            Assert.assertTrue(this is DataWrapper.Success)

            val result = (this as DataWrapper.Success).data!!
            Assert.assertTrue(result.sdAddress == mainTestnetAddress)
            Assert.assertTrue(result.hdAddress == mainTestnetAddress)
        }

        verify {
            viewModel.importWallet(mnemonicList, "null", Onboarding.BIP44_DEFAULT)
            viewModel.data
            viewModel.disposables
            useCase.recoverHDWallet(mnemonicList, "null", Onboarding.BIP44_PREFIX + Onboarding.BIP44_DEFAULT)
            analytic.reportEvent(AnalyticEvent.WALLET_RECOVERED)
            analytic.reportState(StateEvent.ONB_RECOVERY_ENTRY_SEED_PHRASE)
            analytic.reportState(StateEvent.ONB_RECOVERY_ENTRY_PASSPHRASE)
            analytic.reportState(StateEvent.ONB_RECOVERY_ENTRY_SUCCESS)
        }
    }

    @Test
    fun `importWallet NoPassphrase CustomDerivationPath OK`() {
        val pathSuffix = "0'/0/0"
        init().importWallet(mnemonicList, null, pathSuffix)
        viewModel.data.value.apply {
            println(this)
            Assert.assertNotNull(this)
            Assert.assertTrue(this is DataWrapper.Success)

            val result = (this as DataWrapper.Success).data!!
            Assert.assertTrue(result.sdAddress == null)
            Assert.assertTrue(result.hdAddress == mainTestnetAddress)
        }

        verify {
            viewModel.importWallet(mnemonicList, null, pathSuffix)
            viewModel.data
            viewModel.disposables
            useCase.recoverHDWallet(mnemonicList, null, Onboarding.BIP44_PREFIX + pathSuffix)
            analytic.reportEvent(AnalyticEvent.WALLET_RECOVERED)
            analytic.reportState(StateEvent.ONB_RECOVERY_ENTRY_SEED_PHRASE)
            analytic.reportState(StateEvent.ONB_RECOVERY_ENTRY_DERIVATION_PATH)
            analytic.reportState(StateEvent.ONB_RECOVERY_ENTRY_SUCCESS)
        }
    }

    @Test
    fun `importWallet KO`() {
        every { useCase.recoverHDWallet(any(), any(), any()) } throws AppError(ErrorType.WALLET_CREATE_ERROR)

        init().importWallet(mnemonicList, null, Onboarding.BIP44_DEFAULT)
        viewModel.data.value.apply {
            println(this)
            Assert.assertNotNull(this)
            Assert.assertTrue(this is DataWrapper.Error)

            Assert.assertTrue(this!!.error is AppError)
        }

        verify {
            viewModel.importWallet(mnemonicList, null, Onboarding.BIP44_DEFAULT)
            viewModel.data
            viewModel.disposables
            useCase.recoverHDWallet(any(), any(), any())
            analytic.reportState(StateEvent.ONB_RECOVERY_ENTRY_SEED_PHRASE)
            analytic.reportState(StateEvent.ONB_RECOVERY_ENTRY_FAILED, ErrorType.WALLET_CREATE_ERROR.name)
        }
    }

    @Test
    fun `validDerivationPath OK`() {
        val validSuffixes = listOf(
            Onboarding.BIP44_DEFAULT,
            "0/0/0",
            "1\'/0\'",
            "1\'/1",
            "2'/0\'/1",
            "3/0\'",
            "489/1\'/489",
            "12345\'/2",
        )

        init()
        validSuffixes.forEach {
            val result = viewModel.validDerivationPath(it)
            Assert.assertTrue(result)
        }

        verify {
            validSuffixes.forEach {
                viewModel.validDerivationPath(it)
            }
        }
    }

    @Test
    fun `validDerivationPath KO`() {
        val invalidSuffixes = listOf(
            "1\'/200\'",
            "magma",
            "2'/0\'\'/1",
            "kotlinTezos/0\'",
            "489/1\'/*",
            "12345//2",
        )

        init()
        invalidSuffixes.forEach {
            val result = viewModel.validDerivationPath(it)
            Assert.assertFalse(result)
        }

        verify {
            invalidSuffixes.forEach {
                viewModel.validDerivationPath(it)
            }
        }
    }
}
