package io.camlcase.smartwallet.ui.main.contact.detail

import io.camlcase.smartwallet.data.model.contact.MagmaContact
import io.camlcase.smartwallet.data.usecase.contact.WriteContactUseCase
import io.camlcase.smartwallet.data.usecase.info.GetBCDBalanceUseCase
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.test.RxTest
import io.mockk.*
import junit.framework.TestCase.assertNotNull
import junit.framework.TestCase.assertTrue
import org.junit.After
import org.junit.Assert
import org.junit.Test

class ContactDetailViewModelTest : RxTest() {
    private val useCase = mockk<WriteContactUseCase>(relaxed = true)
    private val balanceUseCase = mockk<GetBCDBalanceUseCase>(relaxed = true)
    private lateinit var viewModel: ContactDetailViewModel


    private fun init(): ContactDetailViewModel {
        return spyk(ContactDetailViewModel(useCase, balanceUseCase))
    }

    @After
    fun tearDown() {
        confirmVerified(useCase)
        confirmVerified(viewModel)
    }

    @Test
    fun `deleteContact OK`() {
        val contact = MagmaContact(0, "Test Name", emptyList())
        every { useCase.deleteContact(any()) } returns true

        viewModel = init()
        viewModel.deleteContact(contact)

        viewModel.delete.value.apply {
            assertNotNull(this)
            assertTrue(this is DataWrapper.Success)

            val data = this!!.data!!
            Assert.assertTrue(data)
        }

        verify {
            viewModel.deleteContact(contact)
            useCase.deleteContact(contact)
            viewModel.delete
        }
    }

    @Test
    fun `deleteContact Fails`() {
        val contact = MagmaContact(0, "Test Name", emptyList())
        every { useCase.deleteContact(any()) } returns false

        viewModel = init()
        viewModel.deleteContact(contact)

        viewModel.delete.value.apply {
            assertNotNull(this)
            assertTrue(this is DataWrapper.Success)

            val data = this!!.data!!
            Assert.assertFalse(data)
        }

        verify {
            viewModel.deleteContact(contact)
            useCase.deleteContact(contact)
            viewModel.delete
        }
    }

    @Test
    fun `deleteContact KO`() {
        val contact = MagmaContact(0, "Test Name", emptyList())
        val illegalStateException = IllegalStateException()
        every { useCase.deleteContact(any()) } throws illegalStateException

        viewModel = init()
        viewModel.deleteContact(contact)

        viewModel.delete.value.apply {
            assertNotNull(this)
            assertTrue(this is DataWrapper.Error)

            val error = this!!.error!!
            Assert.assertTrue(error == illegalStateException)
        }

        verify {
            viewModel.deleteContact(contact)
            useCase.deleteContact(contact)
            viewModel.delete
        }
    }
}
