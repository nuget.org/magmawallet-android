package io.camlcase.smartwallet.ui.main.contact.edit

import io.camlcase.kotlintezos.model.TezosNetwork
import io.camlcase.smartwallet.data.model.WrappedUri
import io.camlcase.smartwallet.data.model.contact.ContactTezosAddress
import io.camlcase.smartwallet.data.model.contact.MagmaContact
import io.camlcase.smartwallet.data.usecase.analytics.AnalyticEvent
import io.camlcase.smartwallet.data.usecase.analytics.SendAnalyticUseCase
import io.camlcase.smartwallet.data.usecase.contact.WriteContactUseCase
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.test.RxTest
import io.camlcase.smartwallet.test.mock.mainTestnetAddress
import io.mockk.*
import junit.framework.TestCase
import org.junit.After
import org.junit.Assert
import org.junit.Test

class AddContactViewModelTest : RxTest() {
    private val useCase = mockk<WriteContactUseCase>(relaxed = true)
    private val analytics = mockk<SendAnalyticUseCase>(relaxed = true)
    private lateinit var viewModel: AddContactViewModel

    private fun init(): AddContactViewModel {
        return spyk(AddContactViewModel(useCase, analytics))
    }

    @After
    fun tearDown() {
        confirmVerified(useCase)
        confirmVerified(analytics)
        confirmVerified(viewModel)
    }

    @Test
    fun `pickContact OK No TZ1`() {
        val contact = MagmaContact(0, "Test Name", emptyList())
        every { useCase.pickContact(any()) } returns contact

        viewModel = init()
        test_PickContact_OK(contact)

        verify {
            verifyPick()
        }
    }

    @Test
    fun `pickContact OK`() {
        every { useCase.pickContact(any()) } returns PICK_CONTACT

        viewModel = init()
        test_PickContact_OK(PICK_CONTACT)

        verify {
            verifyPick()
        }
    }

    @Test
    fun `pickContact KO`() {
        val exception = NoSuchElementException()
        every { useCase.pickContact(any()) } throws exception

        viewModel = init()
        viewModel.pickContact(WrappedUri(null))

        viewModel.picked.value.apply {
            TestCase.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Error)

            val data = this!!.error!!
            Assert.assertTrue(data == exception)
        }

        verify {
            viewModel.pickContact(WrappedUri(null))
            useCase.pickContact(WrappedUri(null))
            viewModel.picked
        }
    }

    private fun test_PickContact_OK(contact: MagmaContact) {
        viewModel.pickContact(WrappedUri(null))

        viewModel.picked.value.apply {
            TestCase.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Success)

            val data = this!!.data!!
            Assert.assertTrue(data == contact)
        }
    }

    @Test
    fun `pickedContact & saveContact OK`() {
        every { useCase.pickContact(any()) } returns PICK_CONTACT
        every { useCase.editContact(any(), any()) } returns Pair(true, EDIT_CONTACT)

        viewModel = init()
        test_PickContact_OK(PICK_CONTACT)

        val address = "Tz1_CHANGED"
        val account = "a@a.com"
        val name = "Whatever"
        viewModel.saveContact(account, name, address)

        viewModel.save.value.apply {
            TestCase.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Success)

            val data = this!!.data!!
            Assert.assertTrue(data.first)
            Assert.assertTrue(data.second == EDIT_CONTACT)
        }

        verify {
            verifyPick()

            viewModel.saveContact(account, name, address)
            useCase.editContact(PICK_CONTACT, Pair(name, address))
            viewModel.save

            viewModel.mode
            analytics.reportEvent(AnalyticEvent.ADD_CONTACT_SUCCESS)
        }
    }

    @Test
    fun `pickedContact OK & saveContact KO`() {
        every { useCase.pickContact(any()) } returns PICK_CONTACT
        every { useCase.editContact(any(), any()) } returns Pair(false, PICK_CONTACT)

        viewModel = init()
        test_PickContact_OK(PICK_CONTACT)

        val address = "Tz1_CHANGED"
        val account = "a@a.com"
        val name = "Whatever"
        viewModel.saveContact(account, name, address)

        viewModel.save.value.apply {
            TestCase.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Success)

            val data = this!!.data!!
            Assert.assertFalse(data.first)
            Assert.assertTrue(data.second == PICK_CONTACT)
        }

        verify {
            verifyPick()

            viewModel.saveContact(account, name, address)
            useCase.editContact(PICK_CONTACT, Pair(name, address))
            viewModel.save
            viewModel.mode
            analytics.reportEvent(AnalyticEvent.ADD_CONTACT_ERROR)

        }
    }

    @Test
    fun `pickedContact OK & saveContact KO 2`() {
        every { useCase.pickContact(any()) } returns PICK_CONTACT
        val illegalStateException = IllegalStateException()
        every { useCase.editContact(any(), any()) } throws illegalStateException

        viewModel = init()
        test_PickContact_OK(PICK_CONTACT)

        val address = "Tz1_CHANGED"
        val account = "a@a.com"
        val name = "Whatever"
        viewModel.saveContact(account, name, address)

        viewModel.save.value.apply {
            TestCase.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Error)

            val data = this!!.error!!
            Assert.assertTrue(data == illegalStateException)
        }

        verify {
            verifyPick()

            viewModel.saveContact(account, name, address)
            useCase.editContact(PICK_CONTACT, Pair(name, address))
            viewModel.save

            viewModel.mode
            analytics.reportEvent(AnalyticEvent.ADD_CONTACT_ERROR)
        }
    }

    @Test
    fun `saveContact KO`() {
        val illegalStateException = IllegalStateException()
        every { useCase.addContact(any(), any(), any()) } throws illegalStateException

        viewModel = init()
        val address = "Tz1_CHANGED"
        val account = "a@a.com"
        val name = "Whatever"
        viewModel.saveContact(account, name, address)

        viewModel.save.value.apply {
            TestCase.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Error)

            val data = this!!.error!!
            Assert.assertTrue(data == illegalStateException)
        }

        verify {
            viewModel.saveContact(account, name, address)
            useCase.addContact(account, name, address)
            viewModel.save
            analytics.reportEvent(AnalyticEvent.ADD_CONTACT_ERROR)
        }
    }

    @Test
    fun `saveContact OK`() {
        every { useCase.addContact(any(), any(), any()) } returns PICK_CONTACT

        viewModel = init()
        val address = "Tz1_CHANGED"
        val account = "a@a.com"
        val name = "Whatever"
        viewModel.saveContact(account, name, address)

        viewModel.save.value.apply {
            TestCase.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Success)

            val data = this!!.data!!
            Assert.assertTrue(data.first)
            Assert.assertTrue(data.second == PICK_CONTACT)
        }

        verify {
            viewModel.saveContact(account, name, address)
            useCase.addContact(account, name, address)
            viewModel.save
            analytics.reportEvent(AnalyticEvent.ADD_CONTACT_SUCCESS)

        }
    }

    @Test
    fun `pickedContact OK && onNameChanged RESET`() {
        every { useCase.pickContact(any()) } returns PICK_CONTACT

        viewModel = init()
        test_PickContact_OK(PICK_CONTACT)

        val state = viewModel.onNameChanged("")
        Assert.assertTrue(state == AddContactViewModel.RESET)

        verify {
            verifyPick()

            viewModel.onNameChanged("")
        }
    }

    @Test
    fun `pickedContact OK && onNameChanged null RESET`() {
        every { useCase.pickContact(any()) } returns PICK_CONTACT

        viewModel = init()
        test_PickContact_OK(PICK_CONTACT)

        val state = viewModel.onNameChanged(null)
        Assert.assertTrue(state == AddContactViewModel.RESET)

        verify {
            verifyPick()

            viewModel.onNameChanged(null)
        }
    }

    @Test
    fun `pickedContact OK && onNameChanged NORMAL`() {
        every { useCase.pickContact(any()) } returns PICK_CONTACT

        viewModel = init()
        test_PickContact_OK(PICK_CONTACT)

        val state = viewModel.onNameChanged("A")
        Assert.assertTrue(state == AddContactViewModel.NORMAL_ADD)

        verify {
            verifyPick()

            viewModel.onNameChanged("A")
        }
    }

    @Test
    fun `pickedContact KO && onNameChanged empty NORMAL`() {
        val exception = NoSuchElementException()
        every { useCase.pickContact(any()) } throws exception

        viewModel = init()
        viewModel.pickContact(WrappedUri(null))

        viewModel.picked.value.apply {
            TestCase.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Error)

            val data = this!!.error!!
            Assert.assertTrue(data == exception)
        }

        val state = viewModel.onNameChanged("")
        Assert.assertTrue(state == AddContactViewModel.NORMAL_ADD)

        verify {
            viewModel.pickContact(WrappedUri(null))
            useCase.pickContact(WrappedUri(null))
            viewModel.picked

            viewModel.onNameChanged("")
        }
    }

    @Test
    fun `onNameChanged empty NORMAL`() {
        viewModel = init()
        val state = viewModel.onNameChanged("")
        Assert.assertTrue(state == AddContactViewModel.NORMAL_ADD)

        verify {
            viewModel.onNameChanged("")
        }
    }

    @Test
    fun `onNameChanged null NORMAL`() {
        viewModel = init()
        val state = viewModel.onNameChanged(null)
        Assert.assertTrue(state == AddContactViewModel.NORMAL_ADD)

        verify {
            viewModel.onNameChanged(null)
        }
    }

    @Test
    fun `onNameChanged NORMAL`() {
        viewModel = init()
        val state = viewModel.onNameChanged("B")
        Assert.assertTrue(state == AddContactViewModel.NORMAL_ADD)

        verify {
            viewModel.onNameChanged("B")
        }
    }

    @Test
    fun `valid KO-address pickedContact OK`() {
        every { useCase.pickContact(any()) } returns PICK_CONTACT

        viewModel = init()
        test_PickContact_OK(PICK_CONTACT)

        val address = "TZ1_changed"
        viewModel.onAddressChanged(address)

        val result = viewModel.valid()
        Assert.assertFalse(result)

        verify {
            verifyPick()

            viewModel.onAddressChanged(address)
            viewModel.valid()
        }
    }

    private fun verifyPick(mode: Int = AddContactViewModel.PICKED_CONTACT) {
        viewModel.pickContact(WrappedUri(null))
        useCase.pickContact(WrappedUri(null))
        viewModel.picked
        viewModel.mode = mode
    }

    @Test
    fun `valid OK pickedContact OK`() {
        every { useCase.pickContact(any()) } returns PICK_CONTACT

        viewModel = init()
        test_PickContact_OK(PICK_CONTACT)

        // Need this even though in code it's not gonna happen
        viewModel.onNameChanged(PICK_CONTACT.name)

        val address = mainTestnetAddress
        viewModel.onAddressChanged(address)

        val result = viewModel.valid()
        Assert.assertTrue(result)

        verify {
            verifyPick()

            viewModel.onNameChanged(PICK_CONTACT.name)
            viewModel.onAddressChanged(address)
            viewModel.valid()
        }
    }

    @Test
    fun `valid KO-hasNotChanged pickedContact OK`() {
        val contact = MagmaContact(0, "Test Name", emptyList())
        every { useCase.pickContact(any()) } returns contact

        viewModel = init()
        test_PickContact_OK(contact)

        val result = viewModel.valid()
        Assert.assertFalse(result)

        verify {
            verifyPick()

            viewModel.valid()
        }
    }

    @Test
    fun `valid OK`() {
        viewModel = init()
        viewModel.onNameChanged("A")

        val address = mainTestnetAddress
        viewModel.onAddressChanged(address)

        val result = viewModel.valid()
        Assert.assertTrue(result)

        verify {
            viewModel.onNameChanged("A")
            viewModel.onAddressChanged(address)
            viewModel.valid()
        }
    }

    companion object {

        private val PICK_CONTACT = MagmaContact(0, "Test Name", emptyList())
        private val EDIT_CONTACT =
            MagmaContact(0, "Test Name", listOf(ContactTezosAddress("tz1", TezosNetwork.CARTHAGENET)))
    }
}
