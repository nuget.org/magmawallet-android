package io.camlcase.smartwallet.ui.migration

import io.camlcase.kotlintezos.model.Tez
import io.camlcase.smartwallet.data.model.UserTezosAddress
import io.camlcase.smartwallet.data.model.error.AppError
import io.camlcase.smartwallet.data.model.error.ErrorType
import io.camlcase.smartwallet.data.source.local.UserPreferences
import io.camlcase.smartwallet.data.usecase.analytics.AnalyticEvent
import io.camlcase.smartwallet.data.usecase.analytics.SendAnalyticUseCase
import io.camlcase.smartwallet.data.usecase.migration.GetMigrationWalletBalance
import io.camlcase.smartwallet.data.usecase.user.CreateWalletUseCase
import io.camlcase.smartwallet.test.RxTest
import io.mockk.*
import io.reactivex.rxjava3.core.Single
import org.junit.After
import org.junit.Assert
import org.junit.Test

/**
 * @see FinalMigrationViewModel
 */
class FinalMigrationViewModelTest : RxTest() {
    private val createWalletUseCase = mockk<CreateWalletUseCase>(relaxed = true)
    private val balanceUseCase = mockk<GetMigrationWalletBalance>(relaxed = true)
    private val userPreferences = mockk<UserPreferences>(relaxed = true)
    private val sendAnalyticUseCase = mockk<SendAnalyticUseCase>(relaxed = true)

    private fun initViewModel(): FinalMigrationViewModel {
        return spyk(
            FinalMigrationViewModel(
                createWalletUseCase, balanceUseCase, userPreferences, sendAnalyticUseCase
            )
        )
    }

    @After
    fun after() {
        confirmVerified(createWalletUseCase)
        confirmVerified(balanceUseCase)
        confirmVerified(userPreferences)
        confirmVerified(sendAnalyticUseCase)
    }

    @Test
    fun `swapMigrationWalletToStandardWallet OK`() {
        val viewModel = initViewModel()
        viewModel.swapMigrationWalletToStandardWallet()

        verify {
            createWalletUseCase.swapMigrationWalletToStandardWallet()
        }
    }

    @Test
    fun `swapMigrationWalletToStandardWallet Delegate OK`() {
        val viewModel = initViewModel()
        viewModel.swapMigrationWalletToStandardWallet()

        verify {
            createWalletUseCase.swapMigrationWalletToStandardWallet()
        }
    }

    @Test
    fun `swapMigrationWalletToStandardWallet No Delegate OK`() {
        val viewModel = initViewModel()
        viewModel.swapMigrationWalletToStandardWallet()

        verify {
            createWalletUseCase.swapMigrationWalletToStandardWallet()
        }
    }

    @Test
    fun `checkMigrationWalletBalance No balance`() {
        val xtzBalance = Tez.zero
        every { balanceUseCase.getBalance() } returns Single.just(xtzBalance)

        val viewModel = initViewModel()
        viewModel.checkMigrationWalletBalance(
            onPositiveBalance = {
                Assert.fail("Balance is supposed to be empty")
            },
            onEmptyBalance = {
                Assert.assertTrue(true)
            })

        verify {
            balanceUseCase.getBalance()
        }
    }

    @Test
    fun `checkMigrationWalletBalance Filled balance`() {
        val xtzBalance = Tez(1.0)
        every { balanceUseCase.getBalance() } returns Single.just(xtzBalance)

        val viewModel = initViewModel()
        viewModel.checkMigrationWalletBalance(
            onPositiveBalance = {
                Assert.assertTrue(true)
            },
            onEmptyBalance = {
                Assert.fail("Balance is supposed to be positive")
            })

        verify {
            balanceUseCase.getBalance()
        }
    }

    @Test
    fun `checkMigrationWalletBalance KO`() {
        every { balanceUseCase.getBalance() } returns Single.error(AppError(ErrorType.NO_WALLET))

        val viewModel = initViewModel()
        viewModel.checkMigrationWalletBalance(
            onPositiveBalance = {
                Assert.fail("Balance is supposed to be empty")
            },
            onEmptyBalance = {
                Assert.assertTrue(true)
            })

        verify {
            balanceUseCase.getBalance()
        }
    }

    @Test
    fun `sendEvent OK`() {
        every { sendAnalyticUseCase.reportEvent(any<AnalyticEvent>()) } just Runs

        val viewModel = initViewModel()
        viewModel.sendEvent(AnalyticEvent.MIGRATION_SUCCESS)
        viewModel.sendEvent(AnalyticEvent.MIGRATION_WAIT_FOR_INJECTION)

        verify {
            sendAnalyticUseCase.reportEvent(AnalyticEvent.MIGRATION_SUCCESS)
            sendAnalyticUseCase.reportEvent(AnalyticEvent.MIGRATION_WAIT_FOR_INJECTION)
        }
    }

    @Test
    fun `getNewStandardAddress OK`() {
        val userAddress = UserTezosAddress("tz1")
        every { userPreferences.getUserAddress() } returns userAddress
        val viewModel = initViewModel()
        val newStandardAddress = viewModel.getNewStandardAddress()
        Assert.assertTrue(newStandardAddress == userAddress.mainAddress)

        verify {
            userPreferences.getUserAddress()
        }
    }
}
