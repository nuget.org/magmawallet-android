package io.camlcase.smartwallet.ui.migration

import io.camlcase.smartwallet.data.usecase.analytics.SendAnalyticUseCase
import io.mockk.mockk

/**
 * @see MigrationWarningViewModel
 */
class MigrationWarningViewModelTest {
    private val sendAnalyticUseCase = mockk<SendAnalyticUseCase>(relaxed = true)

    // TODO Uncomment when inject fix on MigrationWarningViewModel
//    @After
//    fun after() {
//        confirmVerified(sendAnalyticUseCase)
//    }
//
//    @Test
//    fun sendEvents() {
//        val viewModel = MigrationWarningViewModel(sendAnalyticUseCase)
//
//        viewModel.sendPopupSkipPermanentEvent()
//        viewModel.sendPopupEvent()
//        viewModel.sendPopupSkipEvent()
//
//        verifySequence {
//            sendAnalyticUseCase.reportEvent(AnalyticEvent.MIGRATION_POPUP_SKIP_PERMANENT)
//            sendAnalyticUseCase.reportEvent(AnalyticEvent.MIGRATION_POPUP)
//            sendAnalyticUseCase.reportEvent(AnalyticEvent.MIGRATION_POPUP_SKIP)
//        }
//    }
}
