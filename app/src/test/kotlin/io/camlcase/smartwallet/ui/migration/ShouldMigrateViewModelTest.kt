/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.migration

import io.camlcase.smartwallet.core.Migration
import io.camlcase.smartwallet.data.source.local.UserPreferences
import io.camlcase.smartwallet.test.RxTest
import io.mockk.*
import org.junit.After
import org.junit.Assert
import org.junit.Test

/**
 * @see ShouldMigrateViewModel
 */
class ShouldMigrateViewModelTest : RxTest() {
    private val userPreferences = mockk<UserPreferences>(relaxed = true)

    @After
    fun tearDown() {
        confirmVerified(userPreferences)
    }

    private fun initViewModel(): ShouldMigrateViewModel {
        return spyk(ShouldMigrateViewModel(userPreferences))
    }

    @Test
    fun `shouldMigrate TRUE`() {
        every { userPreferences.versionCreated() } returns (Migration.SECURE_WALLET_CREATION_VERSION - 1)
        val viewModel = initViewModel()
        val shouldMigrate = viewModel.shouldMigrate()
        Assert.assertTrue(shouldMigrate)

        verify {
            userPreferences.versionCreated()
        }
    }

    @Test
    fun `shouldMigrate TRUE 2`() {
        every { userPreferences.versionCreated() } returns -1
        val viewModel = initViewModel()
        val shouldMigrate = viewModel.shouldMigrate()
        Assert.assertTrue(shouldMigrate)

        verify {
            userPreferences.versionCreated()
        }
    }

    @Test
    fun `shouldMigrate FALSE`() {
        every { userPreferences.versionCreated() } returns Migration.SECURE_WALLET_CREATION_VERSION
        val viewModel = initViewModel()
        val shouldMigrate = viewModel.shouldMigrate()
        Assert.assertFalse(shouldMigrate)

        verify {
            userPreferences.versionCreated()
        }
    }

    @Test
    fun `shouldMigrate FALSE 2`() {
        every { userPreferences.versionCreated() } returns (Migration.SECURE_WALLET_CREATION_VERSION + 1)
        val viewModel = initViewModel()
        val shouldMigrate = viewModel.shouldMigrate()
        Assert.assertFalse(shouldMigrate)

        verify {
            userPreferences.versionCreated()
        }
    }
}
