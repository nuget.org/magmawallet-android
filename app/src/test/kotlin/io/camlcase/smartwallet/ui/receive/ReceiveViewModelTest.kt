package io.camlcase.smartwallet.ui.receive

import io.camlcase.smartwallet.data.model.UserTezosAddress
import io.camlcase.smartwallet.data.source.local.UserPreferences
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.test.RxTest
import io.camlcase.smartwallet.test.blockingObserve
import io.mockk.every
import io.mockk.mockk
import io.mockk.spyk
import io.mockk.verify
import junit.framework.TestCase
import org.junit.Assert
import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * [ReceiveViewModel]
 */
class ReceiveViewModelTest : RxTest(){

    private var preferences = mockk<UserPreferences>(relaxed = true) {
        every { getUserAddress() } returns UserTezosAddress(TEST_ADDRESS)
    }

    @Test
    fun testInit() {
        Assert.assertNotNull(preferences)
    }

    @Test
    fun `Normal address`() {
        val viewModel = spyk(ReceiveViewModel(preferences))
        viewModel.getUserData()

        val wrapper = viewModel.address.blockingObserve()

        Assert.assertNotNull(wrapper)
        TestCase.assertTrue(wrapper is DataWrapper.Success)
        assertEquals(TEST_ADDRESS, wrapper!!.data)

        // QR library gives a NullPointerException on test
        val qrWrapper = viewModel.qr.blockingObserve()
        Assert.assertNotNull(qrWrapper)
        TestCase.assertTrue(qrWrapper is DataWrapper.Error)

        verify {
            preferences.getUserAddress()
            viewModel.generateQR(TEST_ADDRESS)
        }
    }

    @Test
    fun `Empty UserTezosAddress address`() {
        val preferences = mockk<UserPreferences>(relaxed = true) {
            every { getUserAddress() } returns UserTezosAddress.empty
        }
        val viewModel = spyk(ReceiveViewModel(preferences))
        viewModel.getUserData()

        val wrapper = viewModel.address.blockingObserve()

        Assert.assertNotNull(wrapper)
        TestCase.assertTrue(wrapper is DataWrapper.Success)
        assertEquals("", wrapper!!.data)

        verify {
            preferences.getUserAddress()
            viewModel.generateQR("")
        }
    }

    companion object {
        const val TEST_ADDRESS = "tz1ABCD"
    }
}

