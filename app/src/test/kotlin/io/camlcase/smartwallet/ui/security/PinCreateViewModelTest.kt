package io.camlcase.smartwallet.ui.security

import io.camlcase.smartwallet.data.model.error.ErrorType
import io.camlcase.smartwallet.data.model.error.AppError
import io.camlcase.smartwallet.data.source.user.PinLocalSource
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.model.navigation.PinNavigation
import io.camlcase.smartwallet.test.RxTest
import io.mockk.*
import org.junit.After
import org.junit.Assert
import org.junit.Test

/**
 * @see PinCreateViewModel
 */
class PinCreateViewModelTest : RxTest() {
    private val pinLocalSource = mockk<PinLocalSource>(relaxed = true)

    private fun initViewModel(): PinCreateViewModel {
        return spyk(PinCreateViewModel(pinLocalSource))
    }

    @Test
    fun `Test going through steps`() {
        val viewModel = initViewModel()
        viewModel.next("")

        viewModel.step.value.apply {
            Assert.assertNotNull(this)
            Assert.assertTrue(this is DataWrapper.Success)
            val data = (this as DataWrapper.Success).data
            Assert.assertTrue(data is PinStep.ConfirmPin)
            Assert.assertTrue((data as PinStep.ConfirmPin).source == PinNavigation.SOURCE_SETTINGS)
        }

        viewModel.next("")

        viewModel.step.value.apply {
            Assert.assertNotNull(this)
            Assert.assertTrue(this is DataWrapper.Success)
            val data = (this as DataWrapper.Success).data
            Assert.assertTrue(data is PinStep.AllOK)
        }

        verify {
            pinLocalSource.storePin("")
        }
    }

    @Test
    fun `Test confirm pin FALSE`() {
        val firstPin = "abcd"
        val viewModel = initViewModel()
        viewModel.next(firstPin)

        val result = viewModel.valid("somethingelse")

        Assert.assertFalse(result)
    }

    @Test
    fun `Test confirm pin TRUE`() {
        val firstPin = "abcd"
        val viewModel = initViewModel()
        viewModel.next(firstPin)

        val result = viewModel.valid(firstPin)

        Assert.assertTrue(result)
    }

    @Test
    fun `Test valid on TypePin step in range`() {
        val firstPin = "abcd"
        val viewModel = initViewModel()

        val result = viewModel.valid(firstPin)

        Assert.assertTrue(result)
    }

    @Test
    fun `Test valid on TypePin step outside range down`() {
        val firstPin = "a"
        val viewModel = initViewModel()

        val result = viewModel.valid(firstPin)

        Assert.assertFalse(result)
    }

    @Test
    fun `Test valid on TypePin step outside range up`() {
        val firstPin = "abcdefg"
        val viewModel = initViewModel()

        val result = viewModel.valid(firstPin)

        Assert.assertFalse(result)
    }

    @Test
    fun `Test savePin OK`() {
        val firstPin = "abcd"
        val viewModel = initViewModel()
        viewModel.savePin(firstPin)

        verify { pinLocalSource.storePin(firstPin) }
    }

    @Test
    fun `Test savePin KO`() {
        every { pinLocalSource.storePin(any()) } throws AppError(ErrorType.PIN_ALGORITHM_ERROR)
        val firstPin = "abcd"
        val viewModel = initViewModel()
        try {
            viewModel.savePin(firstPin)
        } catch (e: AppError) {
            Assert.assertTrue(e.type == ErrorType.PIN_ALGORITHM_ERROR)
        }

        verify { pinLocalSource.storePin(firstPin) }
    }

    @Test
    fun `Test back on confirmPin step`() {
        val firstPin = "abcd"
        val viewModel = initViewModel()
        viewModel.next(firstPin)

        val result = viewModel.back()
        Assert.assertTrue(result)

        viewModel.step.value.apply {
            Assert.assertNotNull(this)
            Assert.assertTrue(this is DataWrapper.Success)
            val data = (this as DataWrapper.Success).data
            Assert.assertTrue(data is PinStep.TypePin)
        }
    }

    @Test
    fun `Test back on TypePin step`() {
        val viewModel = initViewModel()

        val result = viewModel.back()
        Assert.assertFalse(result)
    }

    @Test
    fun `Test correctType empty typedPin`() {
        val viewModel = initViewModel()
        val result = viewModel.correctType("")
        Assert.assertTrue(result)
    }

    @Test
    fun `Test correctType TypePin step`() {
        val input = "abdc"
        val viewModel = initViewModel()
        val result = viewModel.correctType(input)
        Assert.assertFalse(result)
    }

    @Test
    fun `Test correctType ConfirmPin step OK`() {
        val input = "abdc"
        val viewModel = initViewModel()
        viewModel.next(input)
        val result = viewModel.correctType(input)
        Assert.assertTrue(result)
    }

    @Test
    fun `Test correctType ConfirmPin step KO`() {
        val input = "abdc"
        val viewModel = initViewModel()
        viewModel.next(input)
        val result = viewModel.correctType("a")
        Assert.assertTrue(result)
    }

    @Test
    fun `Test back on AllOK step`() {
    }

    @After
    fun after() {
        confirmVerified(pinLocalSource)
    }
}
