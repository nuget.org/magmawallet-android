package io.camlcase.smartwallet.ui.security.pin

import android.widget.TextView
import com.google.android.material.button.MaterialButton
import io.camlcase.smartwallet.core.extension.hide
import io.camlcase.smartwallet.core.extension.show
import io.camlcase.smartwallet.ui.security.lock.AuthLoginViewModel
import io.mockk.*
import org.junit.Assert
import org.junit.Test
import java.lang.ref.WeakReference

/**
 * @see [PinUIValidation]
 */
class PinUIValidationTest {
    val mockedButton = mockk<MaterialButton>(relaxed = true)
    val mockedErrorView = mockk<TextView>(relaxed = true)
    val errorText = "Invalid PIN"

    @Test
    fun `Test empty pin input`() {
        validateInvalidPinInput("")
    }

    @Test
    fun `Test short pin input`() {
        val input = "123"
        validateInvalidPinInput(input)
    }

    @Test
    fun `Test long but wrong pin input`() {
        val input = "1234"
        val viewModel = mockk<AuthLoginViewModel>(relaxed = true) {
            every { validPin(any()) } returns AuthLoginViewModel.INVALID_PIN
        }
        val validation = initValidation(viewModel, onCorrectAuth = { Assert.fail("Shouldn't be called") })
        validation.validatePinFromInput(input)

        verifySequence {
            validation.validatePinFromInput(input)
            validation.viewModel
            viewModel.validPin(input)
            validation.buttonRef
            mockedButton.isEnabled = true
            validation.errorViewRef
            mockedErrorView.hide()
        }

        verify(inverse = true) {
            validation.onCorrectAuthentication(any())
        }
    }

    @Test
    fun `Test correct pin input`() {
        val input = "1234"
        validateValidPinInput(input)
    }

    @Test
    fun `Test suuuper long and correct pin input`() {
        val input =
            "Tu38kD\$Z44R!mtk*9TQOLDd!O@ecnt0^c6tlVO%eUmLp2TqJI&SySmuxDrArWXo8WJnMD9KY^iaqe47^9mFpsdH5*AkmMxqiU#y"
        validateValidPinInput(input)
    }

    @Test
    fun `Test empty pin button`() {
        val input = ""
        validateInvalidPinButton(input)
    }

    @Test
    fun `Test short pin button`() {
        val input = "12"
        validateInvalidPinButton(input)
    }

    @Test
    fun `Test long but wrong pin button`() {
        val input = "1234"
        validateInvalidPinButton(input)
    }

    @Test
    fun `Test correct pin button`() {
        val input = "1234"
        validateValidPinButton(input)
    }

    @Test
    fun `Test suuuper long and correct pin button`() {
        val input =
            "Tu38kD\$Z44R!mtk*9TQOLDd!O@ecnt0^c6tlVO%eUmLp2TqJI&SySmuxDrArWXo8WJnMD9KY^iaqe47^9mFpsdH5*AkmMxqiU#y"
        validateValidPinButton(input)
    }

    private fun initValidation(
        viewModel: AuthLoginViewModel,
        onCorrectAuth: (Boolean) -> Unit,
        onAlgoritmError: () -> Unit = { Assert.fail() }
    ): PinUIValidation {
        return spyk(object : PinUIValidation {
            override val viewModel: AuthLoginViewModel = viewModel
            override val buttonRef: WeakReference<MaterialButton> = WeakReference(mockedButton)
            override val errorViewRef: WeakReference<TextView> = WeakReference(mockedErrorView)
            override val generalError: String = errorText

            override fun onCorrectAuthentication(fromInput: Boolean) {
                onCorrectAuth(fromInput)
            }

            override fun showAlgorithmError() {
                onAlgoritmError()
            }
        })
    }

    private fun validateValidPinButton(input: String) {
        val viewModel = mockk<AuthLoginViewModel>(relaxed = true) {
            every { validPin(any()) } returns AuthLoginViewModel.VALID_PIN
        }
        val validation = initValidation(viewModel, onCorrectAuth = { Assert.assertFalse(it) })
        validation.validatePinFromButton(input)

        verifySequence {
            validation.validatePinFromButton(input)
            validation.viewModel
            viewModel.validPin(input)
            validation.errorViewRef
            mockedErrorView.hide()
            validation.onCorrectAuthentication(any())
        }

        verify(inverse = true) {
            validation.buttonRef
            mockedButton.isEnabled = any()
            validation.validatePinFromInput(input)
        }
    }

    private fun validateInvalidPinButton(input: String) {
        val viewModel = mockk<AuthLoginViewModel>(relaxed = true) {
            every { validPin(any()) } returns AuthLoginViewModel.INVALID_PIN
        }
        val validation = initValidation(viewModel, onCorrectAuth = { Assert.fail("Shouldn't be called") })
        validation.validatePinFromButton(input)

        verifySequence {
            validation.validatePinFromButton(input)
            validation.viewModel
            viewModel.validPin(input)
            validation.errorViewRef
            mockedErrorView.show()
            validation.errorViewRef
            validation.generalError
            mockedErrorView.text = errorText
        }

        verify(inverse = true) {
            validation.buttonRef
            mockedButton.isEnabled = any()
            validation.validatePinFromInput(input)
            validation.onCorrectAuthentication(any())
        }
    }

    private fun validateValidPinInput(input: String) {
        val viewModel = mockk<AuthLoginViewModel>(relaxed = true) {
            every { validPin(any()) } returns AuthLoginViewModel.VALID_PIN
        }
        val validation = initValidation(viewModel, onCorrectAuth = { Assert.assertTrue(it) })
        validation.validatePinFromInput(input)

        verifySequence {
            validation.validatePinFromInput(input)
            validation.viewModel
            viewModel.validPin(input)
            validation.onCorrectAuthentication(true)
            validation.errorViewRef
            mockedErrorView.hide()
        }

        verify(inverse = true) {
            validation.buttonRef
            mockedButton.isEnabled = true
            validation.validatePinFromButton(input)
        }
    }

    private fun validateInvalidPinInput(input: String) {
        val viewModel = mockk<AuthLoginViewModel>(relaxed = true) {
            every { validPin(any()) } returns AuthLoginViewModel.INVALID_PIN
        }
        val validation = initValidation(viewModel, onCorrectAuth = { Assert.fail("Shouldn't be called") })
        validation.validatePinFromInput(input)

        verifySequence {
            validation.validatePinFromInput(input)
            validation.buttonRef
            mockedButton.isEnabled = false
            validation.errorViewRef
            mockedErrorView.hide()
        }

        verify(inverse = true) {
            validation.onCorrectAuthentication(any())
            viewModel.validPin(any())
            validation.validatePinFromButton(input)
        }
    }

    @Test
    fun `Test Algorithm error`() {
        val input = "1234"
        val viewModel = mockk<AuthLoginViewModel>(relaxed = true) {
            every { validPin(any()) } returns AuthLoginViewModel.PIN_ERROR
        }
        val validation = initValidation(viewModel, onCorrectAuth = { Assert.fail() }, onAlgoritmError = {
            Assert.assertTrue(true)
        })
        validation.validatePinFromInput(input)

        verifySequence {
            validation.validatePinFromInput(input)
            validation.viewModel
            validation.showAlgorithmError()
            validation.buttonRef
            mockedButton.isEnabled = false
            validation.errorViewRef
            mockedErrorView.hide()
        }

        verify(inverse = true) {
            validation.buttonRef
            mockedButton.isEnabled = true
            validation.validatePinFromButton(input)
        }
    }
}
