package io.camlcase.smartwallet.ui.send

import io.camlcase.smartwallet.data.model.UserTezosAddress
import io.camlcase.smartwallet.data.source.local.UserPreferences
import io.camlcase.smartwallet.test.RxTest
import io.mockk.*
import org.junit.After
import org.junit.Assert
import org.junit.Test

/**
 * @see GetUserAddressViewModel
 */
class GetUserAddressViewModelTest : RxTest() {
    private val preferences = mockk<UserPreferences>()

    @After
    fun after() {
        confirmVerified(preferences)
    }

    private fun initViewModel(): GetUserAddressViewModel {
        return spyk(GetUserAddressViewModel(preferences))
    }

    @Test
    fun `Test getUserAddress OK`() {
        val userTezosAddress = UserTezosAddress("tz1")
        every { preferences.getUserAddress() } returns userTezosAddress
        val viewModel = initViewModel()

        val pair = viewModel.getUserAddress()
        Assert.assertTrue(pair == "tz1")
        Assert.assertTrue(viewModel.address == pair)

        verify {
            preferences.getUserAddress()
        }
    }

    @Test
    fun `Test getUserAddress Blank KO`() {
        val userTezosAddress = UserTezosAddress.empty
        every { preferences.getUserAddress() } returns userTezosAddress
        val viewModel = initViewModel()

        try {
            viewModel.getUserAddress()
            Assert.fail()
        } catch (e: IllegalStateException) {
            Assert.assertTrue(true)
        }

        verify {
            preferences.getUserAddress()
        }
    }

    @Test
    fun `Test isUserAddress with no data FALSE`() {
        val inputs = listOf(
            "", "tz1", "magma", "camlcase"
        )

        val viewModel = initViewModel()
        for (input in inputs) {
            val result = viewModel.isUserAddress(input)
            Assert.assertFalse(result)
        }
    }

    @Test
    fun `Test isUserAddress with address FALSE`() {
        val inputs = listOf(
            "", "tz122", "magma", "camlcase"
        )

        val userTezosAddress = UserTezosAddress("tz1")
        every { preferences.getUserAddress() } returns userTezosAddress
        val viewModel = initViewModel()
        viewModel.getUserAddress()

        for (input in inputs) {
            val result = viewModel.isUserAddress(input)
            Assert.assertFalse(result)
        }

        verify {
            preferences.getUserAddress()
        }
    }

    @Test
    fun `Test isUserAddress with address TRUE`() {
        val userTezosAddress = UserTezosAddress("tz1")
        every { preferences.getUserAddress() } returns userTezosAddress
        val viewModel = initViewModel()
        viewModel.getUserAddress()

        val result = viewModel.isUserAddress("tz1")
        Assert.assertTrue(result)

        verify {
            preferences.getUserAddress()
        }
    }

}
