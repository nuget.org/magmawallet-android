package io.camlcase.smartwallet.ui.send.confirmation

import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.TezosErrorType
import io.camlcase.kotlintezos.model.operation.RevealOperation
import io.camlcase.kotlintezos.model.operation.TransactionOperation
import io.camlcase.kotlintezos.model.operation.fees.AllocationFee
import io.camlcase.kotlintezos.model.operation.fees.BurnFee
import io.camlcase.kotlintezos.model.operation.fees.ExtraFees
import io.camlcase.kotlintezos.model.operation.fees.OperationFees
import io.camlcase.kotlintezos.wallet.PublicKey
import io.camlcase.smartwallet.data.core.Exchange.minimumXTZBalance
import io.camlcase.smartwallet.data.core.Wallet
import io.camlcase.smartwallet.data.core.XTZ
import io.camlcase.smartwallet.data.model.UserTezosAddress
import io.camlcase.smartwallet.data.model.amount.CoinBalance
import io.camlcase.smartwallet.data.model.amount.EmptyBalance
import io.camlcase.smartwallet.data.model.amount.TokenBalance
import io.camlcase.smartwallet.data.model.error.AppError
import io.camlcase.smartwallet.data.model.error.ErrorType
import io.camlcase.smartwallet.data.model.exchange.XTZTokenInfo
import io.camlcase.smartwallet.data.model.send.SendRequest
import io.camlcase.smartwallet.data.source.local.UserPreferences
import io.camlcase.smartwallet.data.usecase.OperationBundle
import io.camlcase.smartwallet.data.usecase.info.GetBCDBalanceUseCase
import io.camlcase.smartwallet.data.usecase.operation.SendTokenUseCase
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.test.RxTest
import io.camlcase.smartwallet.ui.exchange.amount.ExchangeAmountViewModelTest.Companion.currentToken
import io.camlcase.smartwallet.ui.exchange.amount.ExchangeAmountViewModelTest.Companion.currentTokenBalance
import io.camlcase.smartwallet.ui.exchange.amount.ExchangeAmountViewModelTest.Companion.xtzBalance
import io.camlcase.smartwallet.ui.exchange.confirmation.SwapConfirmationViewModelTest.Companion.bundle
import io.mockk.*
import io.reactivex.rxjava3.core.Single
import org.junit.After
import org.junit.Assert
import org.junit.Test
import java.io.IOException
import java.math.BigDecimal

/**
 * @see SendConfirmationViewModel
 */
class SendConfirmationViewModelTest : RxTest() {
    private val useCase = mockk<GetBCDBalanceUseCase>(relaxed = true)
    private val userPreferences = mockk<UserPreferences>()
    private val sendUseCase = mockk<SendTokenUseCase>()

    private fun initViewModel(): SendConfirmationViewModel {
        return spyk(SendConfirmationViewModel(useCase, userPreferences, sendUseCase))
    }

    @Test
    fun `Test getConfirmationDetails undelegated XTZ OK`() {
        every { useCase.getTokenInfo(XTZ) } returns Single.just(xtzBalance)
        every { useCase.getCurrencyBundle() } returns Wallet.defaultCurrency
        every { sendUseCase.createSendOperation(any()) } returns Single.just(bundle)
        every { sendUseCase.operationOverXTZFunds(any(), any()) } returns false
        every { userPreferences.getUserAddress() } returns UserTezosAddress(FROM)
        every { userPreferences.preferences?.getString(UserPreferences.DELEGATE_ADDRESS, null) } returns null

        val viewModel = initViewModel()
        viewModel.getConfirmationDetails(sendXTZ)

        viewModel.data.value.apply {
            println(this)
            Assert.assertNotNull(this)
            Assert.assertTrue(this is DataWrapper.Success)

            val result = this!!.data!!
            Assert.assertTrue(result.addressTo == TO)
            Assert.assertTrue(result.total == "10 XTZ")
            Assert.assertTrue(result.amountInTez == "10 XTZ")
            Assert.assertTrue(result.fees.totalFees == Tez.zero)
        }

        verify {
            useCase.getTokenInfo(XTZ)
            useCase.getCurrencyBundle()
            sendUseCase.createSendOperation(any())
            sendUseCase.operationOverXTZFunds(any(), any())
            userPreferences.preferences?.getString(UserPreferences.DELEGATE_ADDRESS, null)
            viewModel.clearOperationSubscription()
        }
    }

    @Test
    fun `Test getConfirmationDetails XTZ KO`() {
        every { useCase.getTokenInfo(XTZ) } returns Single.just(xtzBalance)
        every { useCase.getCurrencyBundle() } returns Wallet.defaultCurrency
        every { sendUseCase.createSendOperation(any()) } returns Single.error(TezosError(TezosErrorType.FAILED_OPERATION))
        every { userPreferences.getUserAddress() } returns UserTezosAddress(FROM)
        every { userPreferences.preferences?.getString(UserPreferences.DELEGATE_ADDRESS, null) } returns null

        val viewModel = initViewModel()
        viewModel.getConfirmationDetails(sendXTZ)

        viewModel.data.value.apply {
            println(this)
            Assert.assertNotNull(this)
            Assert.assertTrue(this is DataWrapper.Error)
        }

        verify {
            useCase.getTokenInfo(XTZ)
            useCase.getCurrencyBundle()
            sendUseCase.createSendOperation(any())
            userPreferences.preferences?.getString(UserPreferences.DELEGATE_ADDRESS, null)
            viewModel.clearOperationSubscription()
        }
    }

    @Test
    fun `Test getConfirmationDetails delegated non-max XTZ OK`() {
        every { useCase.getTokenInfo(XTZ) } returns Single.just(xtzBalance)
        every { useCase.getCurrencyBundle() } returns Wallet.defaultCurrency
        every { sendUseCase.createSendOperation(any()) } returns Single.just(bundle)
        every { sendUseCase.operationOverXTZFunds(any(), any()) } returns false
        every { userPreferences.getUserAddress() } returns UserTezosAddress(FROM)
        every { userPreferences.preferences?.getString(UserPreferences.DELEGATE_ADDRESS, null) } returns "tz1DELEGATE"

        val viewModel = initViewModel()
        viewModel.getConfirmationDetails(sendXTZ)

        viewModel.data.value.apply {
            println(this)
            Assert.assertNotNull(this)
            Assert.assertTrue(this is DataWrapper.Success)

            val result = this!!.data!!
            Assert.assertTrue(result.addressTo == TO)
            Assert.assertTrue(result.total == "10 XTZ")
            Assert.assertTrue(result.amountInTez == "10 XTZ")
            Assert.assertTrue(result.fees.totalFees == Tez.zero)
        }

        verify {
            useCase.getTokenInfo(XTZ)
            useCase.getCurrencyBundle()
            sendUseCase.createSendOperation(any())
            sendUseCase.operationOverXTZFunds(any(), any())
            userPreferences.preferences?.getString(UserPreferences.DELEGATE_ADDRESS, null)
            viewModel.clearOperationSubscription()
        }
    }

    // Leave 0.00001 XTZ for delegated accounts
    @Test
    fun `Test getConfirmationDetails delegated max XTZ OK`() {
        every { useCase.getTokenInfo(XTZ) } returns Single.just(xtzBalance)
        every { useCase.getCurrencyBundle() } returns Wallet.defaultCurrency
        every { sendUseCase.createSendOperation(any()) } returns Single.just(bundle)
        every { sendUseCase.operationOverXTZFunds(any(), any()) } returns true
        every { userPreferences.getUserAddress() } returns UserTezosAddress(FROM)
        every { userPreferences.preferences?.getString(UserPreferences.DELEGATE_ADDRESS, null) } returns "tz1DELEGATE"

        val request = SendRequest(
            xtzBalance.balance,
            TO,
            XTZ
        )
        val viewModel = initViewModel()
        viewModel.getConfirmationDetails(request)

        viewModel.data.value.apply {
            println(this)
            Assert.assertNotNull(this)
            Assert.assertTrue(this is DataWrapper.Success)

            val result = this!!.data!!
            Assert.assertTrue(result.addressTo == TO)
            Assert.assertTrue(result.total == "19.999999 XTZ")
            Assert.assertTrue(result.amountInTez == "19.999999 XTZ")
            Assert.assertTrue(result.fees.totalFees == Tez.zero)
        }

        verify {
            useCase.getTokenInfo(XTZ)
            useCase.getCurrencyBundle()
            sendUseCase.createSendOperation(any())
            sendUseCase.operationOverXTZFunds(any(), any())
            userPreferences.preferences?.getString(UserPreferences.DELEGATE_ADDRESS, null)
            viewModel.clearOperationSubscription()
        }
    }

    @Test
    fun `Test getConfirmationDetails delegated max-0-5 XTZ OK`() {
        every { useCase.getTokenInfo(XTZ) } returns Single.just(xtzBalance)
        every { useCase.getCurrencyBundle() } returns Wallet.defaultCurrency
        every { sendUseCase.createSendOperation(any()) } returns Single.just(bundle)
        every { sendUseCase.operationOverXTZFunds(any(), any()) } returns true
        every { userPreferences.getUserAddress() } returns UserTezosAddress(FROM)
        every { userPreferences.preferences?.getString(UserPreferences.DELEGATE_ADDRESS, null) } returns "tz1DELEGATE"

        val request = SendRequest(
            CoinBalance((xtzBalance.balance.value as Tez) - minimumXTZBalance),
            TO,
            XTZ
        )
        val viewModel = initViewModel()
        viewModel.getConfirmationDetails(request)

        viewModel.data.value.apply {
            println(this)
            Assert.assertNotNull(this)
            Assert.assertTrue(this is DataWrapper.Success)

            val result = this!!.data!!
            Assert.assertTrue(result.addressTo == TO)
            Assert.assertTrue(result.total == "19.5 XTZ")
            Assert.assertTrue(result.amountInTez == "19.5 XTZ")
            Assert.assertTrue(result.fees.totalFees == Tez.zero)
        }

        verify {
            useCase.getTokenInfo(XTZ)
            useCase.getCurrencyBundle()
            sendUseCase.createSendOperation(any())
            sendUseCase.operationOverXTZFunds(any(), any())
            userPreferences.preferences?.getString(UserPreferences.DELEGATE_ADDRESS, null)
            viewModel.clearOperationSubscription()
        }
    }

    @Test
    fun `getConfirmationDetails FA12 token + Max value + delegated  OK`() {
        every { useCase.getTokenInfo(currentToken) } returns Single.just(currentTokenBalance)
        every { useCase.getCurrencyBundle() } returns Wallet.defaultCurrency
        every { useCase.getXTZExchange() } returns Single.just(XTZTokenInfo(1.0))
        every { sendUseCase.createSendOperation(any()) } returns Single.just(bundle)
        every { userPreferences.getUserAddress() } returns UserTezosAddress(FROM)
        every { userPreferences.preferences?.getString(UserPreferences.DELEGATE_ADDRESS, null) } returns "tz1DELEGATE"

        val request = SendRequest(
            currentTokenBalance.balance,
            TO,
            currentToken
        )
        val viewModel = initViewModel()
        viewModel.getConfirmationDetails(request)

        viewModel.data.value.apply {
            println(this)
            Assert.assertNotNull(this)
            Assert.assertTrue(this is DataWrapper.Success)

            val result = this!!.data!!
            Assert.assertTrue(result.addressTo == TO)
            Assert.assertTrue(result.total == "10.00 ${currentToken.symbol}\n+ 0 XTZ")
            Assert.assertTrue(result.amountInTez == "10.00 ${currentToken.symbol}")
            Assert.assertTrue(result.fees.totalFees == Tez.zero)
        }

        verify {
            useCase.getTokenInfo(currentToken)
            useCase.getCurrencyBundle()
            useCase.getXTZExchange() // We need the exchange value for fees
            sendUseCase.createSendOperation(any())
            userPreferences.preferences?.getString(UserPreferences.DELEGATE_ADDRESS, null)
            viewModel.clearOperationSubscription()
        }
    }

    @Test
    fun `Test send XTZ OK`() {
        val hash = "ooo000"
        every { sendUseCase.send(any()) } returns Single.just(hash)

        val viewModel = initViewModel()
        viewModel.send()

        viewModel.operation.value.apply {
            println(this)
            Assert.assertNotNull(this)
            Assert.assertTrue(this is DataWrapper.Success)

            val result = this!!.data!!
            Assert.assertTrue(result.first == hash)
            Assert.assertTrue(result.second is EmptyBalance) // Default request is EMPTY
        }

        verify {
            sendUseCase.send(any())
        }
    }

    @Test
    fun `Test send KO`() {
        val exception = IOException("ERROR")
        every { sendUseCase.send(any()) } returns Single.error(exception)

        val viewModel = initViewModel()
        viewModel.send()

        viewModel.operation.value.apply {
            println(this)
            Assert.assertNotNull(this)
            Assert.assertTrue(this is DataWrapper.Error)
        }

        verify {
            sendUseCase.send(any())
        }
    }

    @Test
    fun `getConfirmationDetails parsed Fees`() {
        val extraFees = ExtraFees()
        extraFees.add(AllocationFee(Tez("100")))
        extraFees.add(BurnFee(Tez("100")))
        val expected = OperationBundle(
            listOf(
                TransactionOperation(
                    Tez(1.0), "tz1", "tz1",
                    OperationFees(Tez("100"), 0, 0, extraFees)
                ),
                RevealOperation(
                    "tz1",
                    PublicKey(ByteArray(0)),
                    OperationFees(Tez("100"), 0, 0)
                )
            ),
            OperationFees(Tez("200"), 0, 0, extraFees)
        )


        every { useCase.getTokenInfo(XTZ) } returns Single.just(xtzBalance)
        every { useCase.getCurrencyBundle() } returns Wallet.defaultCurrency
        every { sendUseCase.createSendOperation(any()) } returns Single.just(expected)
        every { sendUseCase.operationOverXTZFunds(any(), any()) } returns false
        every { userPreferences.getUserAddress() } returns UserTezosAddress(FROM)
        every { userPreferences.preferences?.getString(UserPreferences.DELEGATE_ADDRESS, null) } returns "tz1DELEGATE"

        val request = SendRequest(
            CoinBalance(Tez(1.0)),
            TO,
            XTZ
        )
        val viewModel = initViewModel()
        viewModel.getConfirmationDetails(request)

        viewModel.data.value.apply {
            println(this)
            Assert.assertNotNull(this)
            Assert.assertTrue(this is DataWrapper.Success)

            val result = this!!.data!!
            Assert.assertTrue(result.addressTo == TO)
            Assert.assertTrue(result.total == "1.0004 XTZ")
            Assert.assertTrue(result.amountInTez == "1 XTZ")
            Assert.assertTrue(result.fees.totalFees == Tez("400"))
            Assert.assertTrue(result.fees.bakerFee == Tez("100"))
            Assert.assertTrue(result.fees.allocationFee == Tez("100"))
            Assert.assertTrue(result.fees.burnFee == Tez("100"))
            Assert.assertTrue(result.fees.revealFee == Tez("100"))
        }

        verify {
            useCase.getTokenInfo(XTZ)
            useCase.getCurrencyBundle()
            sendUseCase.createSendOperation(any())
            sendUseCase.operationOverXTZFunds(any(), any())
            userPreferences.preferences?.getString(UserPreferences.DELEGATE_ADDRESS, null)
            viewModel.clearOperationSubscription()
        }
    }

    @Test
    fun `getConfirmationDetails ExpiredData XTZ`() {
        every { useCase.getTokenInfo(XTZ) } returns Single.just(xtzBalance)
        every { useCase.getCurrencyBundle() } returns Wallet.defaultCurrency

        val request = SendRequest(
            CoinBalance(Tez(xtzBalance.balance.bigDecimal + BigDecimal.ONE)),
            TO,
            XTZ
        )

        val viewModel = initViewModel()
        viewModel.getConfirmationDetails(request)

        viewModel.data.value.apply {
            println(this)
            Assert.assertNotNull(this)
            Assert.assertTrue(this is DataWrapper.Error)

            val error = this!!.error!!
            Assert.assertTrue(this!!.data == null)
            Assert.assertTrue(error is AppError)
            Assert.assertTrue((error as AppError).type == ErrorType.EXPIRED_DATA)
        }

        verify {
            useCase.getTokenInfo(XTZ)
            useCase.getCurrencyBundle()
        }
    }

    @Test
    fun `getConfirmationDetails ExpiredData FA12 Token`() {
        every { useCase.getTokenInfo(any()) } returns Single.just(currentTokenBalance)
        every { useCase.getCurrencyBundle() } returns Wallet.defaultCurrency
        every { useCase.getXTZExchange() } returns Single.just(XTZTokenInfo(1.0))

        val request = SendRequest(
            TokenBalance(currentTokenBalance.balance.bigDecimal + BigDecimal.ONE, currentToken.decimals),
            TO,
            currentToken
        )

        val viewModel = initViewModel()
        viewModel.getConfirmationDetails(request)

        viewModel.data.value.apply {
            println(this)
            Assert.assertNotNull(this)
            Assert.assertTrue(this is DataWrapper.Error)

            val error = this!!.error!!
            Assert.assertTrue(this!!.data == null)
            Assert.assertTrue(error is AppError)
            Assert.assertTrue((error as AppError).type == ErrorType.EXPIRED_DATA)
        }

        verify {
            useCase.getTokenInfo(currentToken)
            useCase.getCurrencyBundle()
            useCase.getXTZExchange()
        }
    }

    @After
    fun after() {
        confirmVerified(useCase)
        confirmVerified(userPreferences)
        confirmVerified(sendUseCase)
    }

    companion object {
        private const val TO = "tz1_To"
        private const val FROM = "tz1_From"

        val sendXTZ = SendRequest(
            CoinBalance(Tez(10.0)),
            TO,
            XTZ
        )
    }
}
