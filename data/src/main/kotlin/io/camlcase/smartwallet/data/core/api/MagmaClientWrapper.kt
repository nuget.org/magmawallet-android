/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.core.api

import io.camlcase.kotlintezos.ClientWrapper
import io.camlcase.kotlintezos.network.defaultClientBuilder
import io.camlcase.smartwallet.data.core.Api
import okhttp3.OkHttpClient

/**
 * Client wrapper for camlCase Mainnet node.
 */
class MagmaClientWrapper(
    private val debug: Boolean = false,
    private val connectionInterceptor: ConnectionInterceptor,
    private val operationErrorInterceptor: OperationErrorInterceptor
) : ClientWrapper<OkHttpClient> {
    override val client: OkHttpClient by lazy {
        val builder = defaultClientBuilder(Api.TIMEOUT_SECONDS, debug)
        builder.addInterceptor(connectionInterceptor)
        builder.addInterceptor(operationErrorInterceptor)
        builder.build()
    }
}



