/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.core.api

import io.camlcase.kotlintezos.core.ext.parseErrors
import io.camlcase.kotlintezos.core.ext.parseGeneralErrors
import io.camlcase.kotlintezos.data.Parser
import io.camlcase.kotlintezos.model.OperationResultStatus
import io.camlcase.kotlintezos.model.RPCErrorResponse
import io.camlcase.kotlintezos.model.RPCErrorType
import kotlinx.serialization.json.*

/**
 * When checking operation responses, parse only the errors to know if they have been successful.
 * Should work for both simulations and preapplies, and return null/empty list for all the others.
 */
class OperationErrorParser : Parser<List<RPCErrorResponse>> {
    val format = Json {
        isLenient = true
        ignoreUnknownKeys = true
    }

    override fun parse(jsonData: String): List<RPCErrorResponse>? {
        /**
         * Operation responses should treat simple string responses as errors, but this is an all-for-any-response-parser.
         * There's a lot of endpoints who just return a string value and we shouldn't parse that value as an error.
         */
        if (!jsonData.startsWith("{") && !jsonData.startsWith("[")) {
            val error = parseRequestBodyError(jsonData)
            return if (!error.isNullOrBlank()) {
                listOf(
                    RPCErrorResponse(
                        RPCErrorType.PERMANENT,
                        error,
                    )
                )
            } else {
                null
            }
        }

        val errors = parseGeneralErrors(jsonData)
        if (!errors.isNullOrEmpty()) {
            return errors
        }

        val operationErrors: ArrayList<RPCErrorResponse> = ArrayList()
        val map: JsonElement = format.parseToJsonElement(jsonData)
        if (map is JsonObject) {
            val contents: JsonElement? = map["contents"]
            if (contents is JsonArray) {
                for (content in contents) {
                    if (content is JsonObject) {
                        val metadata = content["metadata"] as JsonObject
                        val results = metadata["operation_result"] as? JsonObject

                        // Kotlin serialization parses String to JsonLiteral without escaping quotes.
                        // We need to get the content of the primitive to get the real value.
                        results?.get("status")?.jsonPrimitive?.content?.apply {
                            when (OperationResultStatus.get(this)) {
                                OperationResultStatus.SUCCESS,
                                OperationResultStatus.APPLIED -> {
                                    // Do nothing
                                }
                                else -> operationErrors.addAll(parseErrors(results))
                            }
                        }

                        val internalResults = metadata["internal_operation_results"] as? JsonArray
                        internalResults?.apply {
                            for (internalResult in this) {
                                if (internalResult is JsonObject) {
                                    val moreResult = internalResult["result"] as JsonObject
                                    val status = moreResult["status"]?.jsonPrimitive?.content
                                    when (OperationResultStatus.get(status)) {
                                        OperationResultStatus.SUCCESS,
                                        OperationResultStatus.APPLIED -> {
                                            // Do nothing
                                        }
                                        else -> operationErrors.addAll(parseErrors(moreResult))
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return operationErrors
    }

    // TODO Use KotlinTezos'
    fun parseRequestBodyError(jsonData: String): String? {
        val unhandledMessage = listOf("Unhandled error (Failure \"", "\")")
        if (jsonData.contains(unhandledMessage[0])) {
            val split = jsonData.split(unhandledMessage[0], unhandledMessage[1])
            if (split.size == 3 && split[1].isNotBlank()) {
                return split[1]
            }
        }
        return null
    }
}
