/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.core.db

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import io.camlcase.smartwallet.data.core.AppContext
import io.camlcase.smartwallet.data.core.db.dao.MetadataDao
import io.camlcase.smartwallet.data.core.db.entity.MetadataEntity

@Database(entities = [MetadataEntity::class], version = 2)
abstract class MagmaDatabase : RoomDatabase() {

    abstract fun metadataDao(): MetadataDao

    companion object {

        private const val DATABASE_NAME = "database-magma.db"

        // For Singleton instantiation
        @Volatile // All threads have immediate access to this property
        private var instance: MagmaDatabase? = null
        // Makes sure no threads making the same thing at the same time
        private val LOCK = Any()

        operator fun invoke(context: AppContext) = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also { instance = it }
        }

        // Create and pre-populate the database. See this article for more details:
        // https://medium.com/google-developers/7-pro-tips-for-room-fbadea4bfbd1#4785
        private fun buildDatabase(context: AppContext): MagmaDatabase {
            return Room.databaseBuilder(
                context.get()!!,
                MagmaDatabase::class.java,
                DATABASE_NAME
            )
                .fallbackToDestructiveMigration()
                .build()
        }
    }
}

object SQL {
    const val SELECT_ALL = "SELECT * from"
}

object Table {
    const val METADATA = "metadata"
}
