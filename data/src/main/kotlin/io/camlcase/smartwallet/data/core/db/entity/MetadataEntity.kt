/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.core.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import io.camlcase.kotlintezos.model.blockchain.BlockchainMetadata
import io.camlcase.kotlintezos.model.blockchain.NetworkConstants
import io.camlcase.smartwallet.data.core.db.Table
import kotlinx.serialization.json.Json

@Entity(tableName = Table.METADATA)
data class MetadataEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val id: Int = 0,
    /**
     * The hash of the head of the chain being operated on.
     */
    @ColumnInfo(name = "block_hash")
    val blockHash: String,
    /**
     * The hash of the protocol being operated on.
     */
    @ColumnInfo(name = "protocol")
    val protocol: String,
    /**
     * The chain unique identifier.
     */
    @ColumnInfo(name = "chain_id")
    val chainId: String,
    /**
     * The base58encoded public key for the address, or nil if the key is not yet revealed.
     */
    @ColumnInfo(name = "manager_key")
    val key: String?,

    @ColumnInfo(name = "network_constants")
    val networkConstants: String
)

fun BlockchainMetadata.toEntity(): MetadataEntity {
    return MetadataEntity(
        blockHash = this.blockHash, protocol = this.protocol, chainId = this.chainId, key = this.key,
        networkConstants = Json.encodeToString(NetworkConstants.serializer(), this.constants)
    )
}

fun MetadataEntity.toTezosModel(counter: Int): BlockchainMetadata {
    return BlockchainMetadata(
        this.blockHash,
        this.protocol,
        this.chainId,
        counter,
        this.key,
        Json.decodeFromString(NetworkConstants.serializer(), this.networkConstants)
    )
}
