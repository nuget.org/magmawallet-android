/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.core.extension

import io.camlcase.kotlintezos.model.Tez
import io.camlcase.smartwallet.data.model.Token
import io.camlcase.smartwallet.data.model.exchange.CurrencyBundle
import io.camlcase.smartwallet.data.model.exchange.CurrencyType
import io.camlcase.smartwallet.data.model.exchange.limitDecimals
import io.camlcase.smartwallet.data.model.isXTZ
import java.math.BigDecimal
import java.math.BigInteger
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.NumberFormat
import java.util.*

fun BigDecimal.formatAsMoney(currencyBundle: CurrencyBundle): String {
    return this.formatAsMoney(currencyBundle.symbol, currencyBundle.type)
}

/**
 * Display a price in phone's current locale (correct decimal mark and thousand separator) but with a currency of your choice.
 * @throws IllegalArgumentException if BigDecimal cannot be parsed
 */
fun BigDecimal.formatAsMoney(currencySymbol: String, type: CurrencyType): String {
    val format: NumberFormat = NumberFormat.getCurrencyInstance(Locale.getDefault())
    if (format is DecimalFormat) {
        if (type == CurrencyType.FIAT) {
            format.changeCurrencySymbol(currencySymbol)
        } else {
            format.changeCurrencySymbol("")
        }
    }

    val formatted = if (type == CurrencyType.FIAT) {
        format.format(this)
    } else {
        format.format(this) + " $currencySymbol"
    }
    return formatted
}

fun DecimalFormat.changeCurrencySymbol(currencySymbol: String) {
    val decimalFormatSymbols = this.decimalFormatSymbols
    decimalFormatSymbols.currencySymbol = currencySymbol
    this.decimalFormatSymbols = decimalFormatSymbols
}

fun Double.formatAsMoney(currencyBundle: CurrencyBundle, flexibleDecimals: Boolean = false): String {
    return this.formatAsMoney(currencyBundle.symbol, currencyBundle.type, flexibleDecimals)
}

fun Double.formatAsMoney(currencySymbol: String, type: CurrencyType, flexibleDecimals: Boolean = false): String {
    val format: NumberFormat = NumberFormat.getCurrencyInstance(Locale.getDefault())
    val finalValue: Double = if (flexibleDecimals) {
        this.flexibleDecimals(format)
    } else {
        this
    }
    if (format is DecimalFormat) {
        if (type == CurrencyType.FIAT) {
            format.changeCurrencySymbol(currencySymbol)
        } else {
            format.changeCurrencySymbol("")
        }
    }

    val formatted = if (type == CurrencyType.FIAT) {
        format.format(finalValue)
    } else {
        format.format(finalValue) + " $currencySymbol"
    }
    return formatted
}

/**
 * No currency symbol
 */
fun BigInteger.formatAsToken(): String {
    val format: NumberFormat = NumberFormat.getCurrencyInstance(Locale.getDefault())
    format.maximumFractionDigits = 0
    val decimalFormatSymbols = (format as DecimalFormat).decimalFormatSymbols
    decimalFormatSymbols.currencySymbol = ""
    format.decimalFormatSymbols = decimalFormatSymbols
    return format.format(this)
}

fun BigDecimal.formatAsToken(token: Token): String {
    return this.formatAsToken(token.decimals) + " ${token.symbol}"
}

fun BigDecimal.formatAsToken(decimals: Int): String {
    val format: NumberFormat = NumberFormat.getCurrencyInstance(Locale.getDefault())
    format.maximumFractionDigits = decimals
    val decimalFormatSymbols = (format as DecimalFormat).decimalFormatSymbols
    decimalFormatSymbols.currencySymbol = ""
    format.decimalFormatSymbols = decimalFormatSymbols
    return format.format(this).trim()
}

fun BigDecimal.convertTo(conversionRate: Double): BigDecimal {
    return this.multiply(BigDecimal(conversionRate))
}

/**
 * Converts Tez into a user-friendly format. E.g.:
 * 20100000 -> 20.100000[symbol]
 * 1234567002345 -> 1,234,567.002345[symbol]
 */
fun Tez?.format(symbol: String?, locale: Locale = Locale.getDefault()): String {
    val symbolText = if (symbol.isNullOrBlank()) {
        ""
    } else {
        " $symbol"
    }
    return if (this == null || this == Tez.zero) {
        "0$symbolText"
    } else {
        this.stringRepresentation.formatToTezDecimals(locale, 6) + symbolText
    }
}

fun Tez?.formatAsToken(token: Token, locale: Locale = Locale.getDefault()): String {
    return this?.let {
        when {
            token.isXTZ() -> {
                it.format(token.symbol, locale)
            }
            token.decimals > 0 -> {
                it.signedDecimalRepresentation.formatAsToken(token.decimals) + " ${token.symbol}"
            }
            else -> {
                it.integerAmount.formatAsToken() + " ${token.symbol}"
            }
        }
    } ?: ""
}

/**
 * Formats thousands and decimals of a nanoTez format String
 * E.g.: "1234567" to "1.234567"
 */
fun String.formatToTezDecimals(locale: Locale, decimals: Int = 6): String {
    val currencyFormatter = DecimalFormat.getInstance(locale) as DecimalFormat

    // Strip all non-digits
    var onlyNumbers = this.stripNonDigits()
    // If not enough decimals, pad the value
    if (onlyNumbers.length <= decimals) {
        val formatString = "%" + decimals.toString() + "s"
        onlyNumbers = String.format(formatString, onlyNumbers).replace(' ', '0')
    }

    // This is NOT the decimal separator for the currency value. That will be done on the currencyFormatter
    val preparedValue: String =
        StringBuilder(onlyNumbers).insert(onlyNumbers.length - decimals, '.').toString()

    val newTextValue: Double = preparedValue.toDouble()

    // The actual formatting
    val trailingZerosRegex = Regex("\\" + locale.getDecimalSeparator() + "?0*\$")
    currencyFormatter.minimumFractionDigits = decimals
    onlyNumbers = currencyFormatter.format(newTextValue)
        .replace(trailingZerosRegex, "")
    return onlyNumbers
}

/**
 * Cuts short to 2 or 6 decimals depending on the type of token. Used for exchange rates.
 */
fun Double.formatWithDecimals(token: Token): String {
    val decimalPattern = if (token.isXTZ()) {
        "#.######"
    } else {
        "#.##"
    }
    return DecimalFormat(decimalPattern).format(this)
}

fun BigDecimal.formatWithDecimals(token: Token): String {
    val decimalPattern = if (token.isXTZ()) {
        "#.######"
    } else {
        "#.##"
    }
    return DecimalFormat(decimalPattern).format(this)
}

/**
 * Rounds up:
 *
 * ZERO is 0
 * ZERO.(...)1 is 1
 * ANY.<5 is ANY
 * ANY.>=5 is ANY+1
 */
fun BigDecimal.roundUp(): BigInteger {
    val rounded = if (this.toBigInteger().compareTo(BigInteger.ZERO) == 0) {
        this.setScale(0, RoundingMode.UP)
    } else {
        this.setScale(0, RoundingMode.HALF_UP)
    }
    return rounded.toBigInteger()
}

fun Locale.getGroupingSeparator(): Char {
    return DecimalFormatSymbols(this).groupingSeparator
}

fun Locale.getDecimalSeparator(): Char {
    return DecimalFormatSymbols(this).decimalSeparator
}

fun Double.flexibleDecimals(format: NumberFormat): Double {
    if (this > 0.0 && this < 0.01) {
        format.maximumFractionDigits = 6
        return this.limitDecimals()
    }
    return this
}

fun Double.limitDecimals(): Double {
    if (this > 0.0 && this < 0.01) {
        return when {
            this < 0.000_1 -> {
                limitDecimals(6)
            }
            this < 0.001 -> {
                // 0.000XYZ to 0.000X
                limitDecimals(4)
            }
            else -> {
                // 0.00XYZ to 0.00X
                limitDecimals(3)
            }

        }
    }
    return this
}
