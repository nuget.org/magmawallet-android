/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.core.extension

import io.camlcase.kotlintezos.TezosNodeClient
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.operation.BundledFees
import io.camlcase.kotlintezos.model.operation.Operation
import io.camlcase.kotlintezos.operation.OperationFeesPolicy
import io.camlcase.kotlintezos.wallet.Wallet
import io.camlcase.smartwallet.data.core.EmitterSource
import io.camlcase.smartwallet.data.core.RxEmitter
import io.reactivex.rxjava3.core.Single

internal fun TezosNodeClient.bundleFees(
    operations: List<Operation>,
    source: Address,
    wallet: Wallet,
    feesPolicy: OperationFeesPolicy,
    emitter: RxEmitter
): Single<BundledFees> {
    return Single.create<BundledFees> {
        this.bundleFees(operations, source, wallet, feesPolicy, emitter.emitCallback(it, EmitterSource.BUNDLE_FEES))
    }
}

/**
 * Runs a given list of operations with **their fees already calculated**.
 * To use with [bundleFees]
 */
internal fun TezosNodeClient.run(
    operations: List<Operation>,
    source: Address,
    wallet: Wallet,
    emitter: RxEmitter
): Single<String> {
    return Single.create<String> {
        this.runOperations(operations, source, wallet, emitter.emitCallback(it, EmitterSource.RUN_OPERATION))
    }
}
