/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.core.extension.tezos

import android.os.Parcel
import io.camlcase.kotlintezos.exchange.DexterTokenClient
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.OperationResultStatus
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.conseil.OperationDetail
import io.camlcase.kotlintezos.model.conseil.OperationGroup
import io.camlcase.kotlintezos.model.operation.Operation
import io.camlcase.kotlintezos.model.operation.OperationType
import io.camlcase.kotlintezos.model.operation.fees.ExtraFees
import io.camlcase.kotlintezos.model.operation.fees.OperationFees
import io.camlcase.kotlintezos.model.tzkt.TzKtOperation
import io.camlcase.kotlintezos.model.validTezosAddress
import io.camlcase.smartwallet.data.analytic.report
import io.camlcase.smartwallet.data.core.MAINNET_FLAVOR
import java.math.BigDecimal
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

fun String?.validTezosAddress(): Boolean {
    return this.validTezosAddress()
}

fun Tez?.convertToCurrency(currencyPrice: Double): BigDecimal {
    return this.convertTo(currencyPrice.toBigDecimal())
}

fun Tez?.convertTo(conversionRate: BigDecimal): BigDecimal {
    this?.apply {
        return this.toBigDecimal().multiply(conversionRate)
    }
    return BigDecimal.ZERO
}

fun Tez?.toBigDecimal(): BigDecimal {
    this?.apply {
        return this.signedDecimalRepresentation
    }
    return BigDecimal.ZERO
}

/**
 * @throws IllegalStateException If no client with the given [dexterAddress] exists
 */
fun <T> LinkedHashMap<Address, DexterTokenClient>.getDexterClientOrDie(
    dexterAddress: Address?,
    block: (DexterTokenClient) -> T
): T {
    val dexterClient = this[dexterAddress]
    dexterClient?.let { client ->
        return block(client)

    } ?: throw IllegalStateException("Dexter Client for token $dexterAddress is null")
}

/**
 * Aggregate all the separate values of every operation's [OperationFees] into one.
 */
fun List<Operation>.getAccumulatedFee(): OperationFees {
    var accumulatedFees = Tez.zero
    var accumulatedExtraFees = ExtraFees()
    var accumulatedGas = 0
    var accumulatedStorage = 0

    for (operation in this) {
        operation.fees.extraFees?.apply {
            accumulatedExtraFees += this
        }
        accumulatedFees += operation.fees.fee
        accumulatedGas += operation.fees.gasLimit
        accumulatedStorage += operation.fees.storageLimit
    }
    return OperationFees(accumulatedFees, accumulatedGas, accumulatedStorage, accumulatedExtraFees)
}

fun String.isMainnet(): Boolean {
    return this == MAINNET_FLAVOR
}

fun OperationGroup.getDetails(): OperationDetail {
    return if (this.details.isNotEmpty()) {
        this.details[0]
    } else {
        OperationDetail(OperationType.TRANSACTION, OperationResultStatus.FAILED, null, null)
    }
}

/**
 * Aggregate allocation and burn fees with baker fees.
 */
fun OperationFees.getTotalFees(): Tez {
    var extraFees = Tez.zero
    this.extraFees?.asList?.forEach {
        extraFees += it.fee
    }
    return this.fee + extraFees
}

fun Parcel.initNullableTez(): Tez? {
    val value = this.readString()
    return if (value == null) {
        null
    } else {
        Tez(value)
    }
}

// ISO 8601 format
// Quoted "Z" to indicate UTC, no timezone offset
const val TZKT_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'"

fun Long.getTzktDate(): String? {
    return try {
        val df: DateFormat = SimpleDateFormat(TZKT_DATE_FORMAT, Locale.getDefault())
        df.timeZone = TimeZone.getTimeZone("UTC")
        df.format(Date(this))
    } catch (e: ParseException) {
        e.report("formatDate")
        return null
    }
}

/**
 * Map [TZKT_DATE_FORMAT] to Date
 */
fun TzKtOperation.getDate(): Date? {
    return try {
        val df: DateFormat = SimpleDateFormat(TZKT_DATE_FORMAT, Locale.getDefault())
        df.timeZone = TimeZone.getTimeZone("UTC")
        df.parse(this.timestamp)
    } catch (e: ParseException) {
        e.report("formatDate")
        return null
    }
}

