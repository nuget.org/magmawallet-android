/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.core.security

import io.camlcase.kotlintezos.wallet.crypto.toHexString
import io.camlcase.smartwallet.data.analytic.report
import io.camlcase.smartwallet.data.model.error.AppError
import io.camlcase.smartwallet.data.model.error.ErrorType
import java.security.SecureRandom
import java.security.spec.KeySpec
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.PBEKeySpec


object CryptoUtils {

    private const val STRENGTH = 65536

    fun generateSalt(): String {
        val salt = ByteArray(256)
        val random = SecureRandom()
        random.nextBytes(salt)
        return salt.contentToString()
    }

    /**
     * @throws AppError If salt is empty or SecretKeyFactory cannot be loaded with algorithm
     */
    fun encryptWithSalt(string: String, salt: String): String {
        try {
            val spec: KeySpec = PBEKeySpec(string.toCharArray(), salt.toByteArray(), STRENGTH, 128)
            val factory: SecretKeyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1")
            val hash = factory.generateSecret(spec).encoded
            return hash.toHexString()
        } catch (e: Exception) {
            e.report("encryptWithSalt")
            throw AppError(ErrorType.PIN_ALGORITHM_ERROR)
        }
    }
}

internal fun String.addSalt(salt: String): String {
    return salt + this + salt
}

