/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.di

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.Reusable
import io.camlcase.smartwallet.data.core.AppContext
import io.camlcase.smartwallet.data.source.local.AppPreferences
import io.camlcase.smartwallet.data.source.local.PermissionPreferences
import io.camlcase.smartwallet.data.source.local.UserPreferences
import io.camlcase.smartwallet.data.source.user.PinLocalSource
import io.camlcase.smartwallet.data.source.user.WalletLocalSource
import java.lang.ref.WeakReference

@Module
open class ContextModule {

    @Provides
    @Reusable
    open fun providesAppContext(context: Context): AppContext {
        return WeakReference(context)
    }

    @Provides
    @Reusable
    open fun providesUserPreferences(context: AppContext): UserPreferences {
        return UserPreferences(context)
    }

    @Provides
    @Reusable
    open fun providesAppPreferences(context: AppContext): AppPreferences {
        return AppPreferences(context)
    }

    @Provides
    @Reusable
    open fun providesPermissionPreferences(context: AppContext): PermissionPreferences {
        return PermissionPreferences(context)
    }

    @Provides
    @Reusable
    open fun provideWalletLocalSource(context: AppContext): WalletLocalSource {
        return WalletLocalSource(appContext = context)
    }

    @Provides
    @Reusable
    open fun providePinLocalSource(context: AppContext): PinLocalSource {
        return PinLocalSource(appContext = context)
    }
}
