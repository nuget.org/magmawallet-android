/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.model

import io.camlcase.kotlintezos.wallet.HDWallet
import io.camlcase.kotlintezos.wallet.SDWallet
import io.camlcase.kotlintezos.wallet.Wallet
import io.camlcase.smartwallet.data.analytic.report
import io.camlcase.smartwallet.data.core.Logger
import io.camlcase.smartwallet.data.core.extension.tezos.validTezosAddress
import io.camlcase.smartwallet.data.model.error.AppError
import io.camlcase.smartwallet.data.model.error.ErrorType
import org.json.JSONObject

/**
 * A Tezos Wallet with a passphrase.
 *
 * @param passphrase Needed for serialize/deserialize and recreation of the [wallet] object
 * @param derivationPath BIPP 44 derivation path or null if linear wallet
 */
data class AppWallet(
    val wallet: Wallet,
    val passphrase: String? = null,
    val derivationPath: String? = null
) : Serializable<String> {
    val address: String = wallet.mainAddress
    val mnemonic: List<String> = wallet.mnemonic

    constructor(mnemonic: List<String>?, passphrase: String?, derivationPath: String?) : this(
        generateInternalWallet(mnemonic, passphrase, derivationPath),
        passphrase,
        derivationPath
    )

    override fun serialize(): String {
        val jsonObject = JSONObject()
        jsonObject.put(ARG_PASSPHRASE, passphrase)
        jsonObject.put(ARG_MNEMONIC, mnemonic?.joinToString(", "))
        jsonObject.put(ARG_DERIVATION_PATH, derivationPath)
        return jsonObject.toString()
    }

    override fun toString(): String {
        return "* ${this::class.java.simpleName} { address=$address }"
    }

    companion object : Deserializer<AppWallet, String> {
        const val ARG_PASSPHRASE = "passphrase"
        const val ARG_MNEMONIC = "mnemonic"
        const val ARG_DERIVATION_PATH = "derivationPath"

        override fun deserialize(serialized: String): AppWallet? {
            return try {
                val jsonObject = JSONObject(serialized)
                val mnemonic = (jsonObject[ARG_MNEMONIC] as String).split(", ")
                val passphrase = if (serialized.contains(ARG_PASSPHRASE)) {
                    jsonObject[ARG_PASSPHRASE] as? String
                } else {
                    null
                }
                val derivationPath = if (serialized.contains(ARG_DERIVATION_PATH)) {
                    jsonObject[ARG_DERIVATION_PATH] as? String
                } else {
                    null
                }

                AppWallet(mnemonic, passphrase, derivationPath)
            } catch (e: Throwable) {
                e.report()
                Logger.e("ERROR in deserialize AppWallet ($serialized): $e")
                null
            }
        }

        /**
         * Loads native library Trustwallet-core to run the cryptographic algorithms that generate the seed for the wallet.
         *
         * @param derivationPath If null, will generate a [SDWallet], if filled with a BIP44 derivation path, will generate an [HDWallet]
         * @throws TezosError if [derivationPath] is not a valid BIP44 path and [AppError] if mnemonic is not valid
         */
        internal fun generateInternalWallet(
            mnemonic: List<String>?,
            passphrase: String?,
            derivationPath: String?
        ): Wallet {
            return if (derivationPath == null) {
                SDWallet(
                    mnemonic = mnemonic,
                    passphrase = passphrase ?: ""
                ) ?: throw AppError(ErrorType.WALLET_CREATE_ERROR)
            } else {
                HDWallet(
                    mnemonic = mnemonic,
                    passphrase = passphrase,
                    derivationPath = derivationPath
                )
            }
        }
    }
}

/**
 * - It's not null
 * - Has a mnemonic with 12 words or more
 * - Address is a valid tezos address
 *
 * @return true if it's VALID
 */
fun AppWallet?.valid(): Boolean {
    return this != null && !this.mnemonic.isNullOrEmpty() && this.mnemonic.size > 11 && this.address.validTezosAddress()
}
