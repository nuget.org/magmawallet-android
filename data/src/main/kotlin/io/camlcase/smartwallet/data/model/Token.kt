/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.model

import android.os.Parcel
import android.os.Parcelable
import io.camlcase.kotlintezos.model.Address
import io.camlcase.smartwallet.data.core.XTZ
import io.camlcase.smartwallet.data.core.extension.toLowerCaseUS

/**
 * Representation of a token in the blockchain
 *
 * @param convertibleToCurrency Exists on third-party currency exchanges like Messari/CoinGecko
 */
data class Token(
    val name: String,
    val symbol: String,
    val decimals: Int = 0,
    val contractAddress: Address?,
    val dexterAddress: Address?
) : Parcelable, Comparable<Token> {
    var existsOnDexter = !dexterAddress.isNullOrBlank()

    constructor(parcel: Parcel) : this(
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(symbol)
        parcel.writeInt(decimals)
        parcel.writeString(contractAddress)
        parcel.writeString(dexterAddress)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Token> {
        override fun createFromParcel(parcel: Parcel): Token {
            return Token(parcel)
        }

        override fun newArray(size: Int): Array<Token?> {
            return arrayOfNulls(size)
        }

        val comparator = compareByDescending<Token> { it.isXTZ() }.thenBy { it.symbol.toLowerCaseUS() }

        val empty = Token("", "", 0, null, null)
    }

    override fun compareTo(other: Token): Int {
        return comparator.compare(this, other)
    }
}

fun Token?.isXTZ(): Boolean {
    return this?.symbol == XTZ.symbol
}

fun Token?.showExchange(): Boolean {
    return this?.isXTZ() == true || this?.existsOnDexter == true
}
