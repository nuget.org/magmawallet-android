/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.model.amount

import android.os.Parcel
import android.os.Parcelable
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.smartwallet.data.core.extension.format
import io.camlcase.smartwallet.data.core.extension.tezos.convertToCurrency
import io.camlcase.smartwallet.data.core.extension.tezos.toBigDecimal
import io.camlcase.smartwallet.data.model.Token
import java.math.BigDecimal

/**
 * XTZ is considered Tezos coin, a 6 point decimal positive
 */
data class CoinBalance(
    override val value: Tez
) : Balance<Tez> {
    override val formattedValue: String = value.format(symbol = null)

    override val isZero: Boolean = value == Tez.zero

    override fun inCurrency(currencyPrice: Double): BigDecimal {
        return value.convertToCurrency(currencyPrice)
    }

    override fun format(token: Token): String {
        return value.format(symbol = token.symbol)
    }

    override fun compareTo(other: Tez): Int {
        return value.compareTo(other)
    }

    override fun compareTo(double: Double): Int {
        return compareTo(Tez(double))
    }

    override fun plus(other: Tez): Tez {
        return value + other
    }

    override fun minus(other: Tez): Tez {
        return value - other
    }

    override val bigDecimal: BigDecimal
        get() = value.toBigDecimal()

    constructor(parcel: Parcel) : this(Tez(parcel.readString() ?: "0"))

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(value.stringRepresentation)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CoinBalance> {
        override fun createFromParcel(parcel: Parcel): CoinBalance {
            return CoinBalance(parcel)
        }

        override fun newArray(size: Int): Array<CoinBalance?> {
            return arrayOfNulls(size)
        }
    }
}
