/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.model.exchange

import io.camlcase.smartwallet.data.core.extension.getDecimalSeparator
import java.math.BigDecimal
import java.math.BigInteger
import java.text.DecimalFormat
import java.util.*
import kotlin.math.absoluteValue
import kotlin.math.pow

fun Double.limitDecimals(maxDecimals: Int): Double {
    val stringRepresentation = DecimalFormat("0.######").format(this)
    val decimalSeparator = Locale.getDefault().getDecimalSeparator()
    if (stringRepresentation.contains(decimalSeparator)) {
        val decimals = stringRepresentation.split(decimalSeparator)[1].length
        val limit = if (decimals > maxDecimals) {
            maxDecimals
        } else {
            decimals
        }
        return this.limitPrecision(limit)
    }
    return this
}

fun Double.limitPrecision(maxDecimals: Int): Double {
    val multiplier = 10.0.pow(maxDecimals.toDouble()).toInt()
    val truncated = (this * multiplier).toLong().toDouble() / multiplier
    return truncated
}

/**
 * (1000, 0) -> 1000
 * (1000, 2) -> 10.00
 * (1000, 5) -> 0.1
 */
fun BigInteger.moveComma(decimals: Int): BigDecimal {
    return this.toBigDecimal().moveComma(decimals)
}

fun BigDecimal.moveComma(decimals: Int): BigDecimal {
    return if (decimals > 0) {
        this.divide(BigDecimal(10.0.pow(decimals.absoluteValue)))
    } else {
        this.multiply(BigDecimal(10.0.pow(decimals.absoluteValue)))
    }
}
