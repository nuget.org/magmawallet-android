/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.model.exchange

import android.os.Parcel
import android.os.Parcelable
import io.camlcase.smartwallet.data.core.XTZ
import io.camlcase.smartwallet.data.model.Token
import io.camlcase.smartwallet.data.model.amount.Balance
import io.camlcase.smartwallet.data.model.amount.EmptyBalance

/**
 * @param currencyExchange 1 [token] = [currencyExchange] USD/EUR/GBP/...
 */
data class TokenExchange(
    val token: Token,
    val balance: Balance<*>,
    var dexterBalance: DexterBalance = DexterBalance.empty,
    val currencyExchange: Double
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readParcelable(Token::class.java.classLoader) ?: XTZ,
        parcel.readParcelable(Balance::class.java.classLoader) ?: EmptyBalance(),
        parcel.readParcelable(DexterBalance::class.java.classLoader) ?: DexterBalance.empty,
        parcel.readDouble()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(token, flags)
        parcel.writeParcelable(balance, flags)
        parcel.writeParcelable(dexterBalance, flags)
        parcel.writeDouble(currencyExchange)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TokenExchange> {
        override fun createFromParcel(parcel: Parcel): TokenExchange {
            return TokenExchange(parcel)
        }

        override fun newArray(size: Int): Array<TokenExchange?> {
            return arrayOfNulls(size)
        }

        val empty = TokenExchange(XTZ, EmptyBalance(), currencyExchange = 1.0)
    }
}
