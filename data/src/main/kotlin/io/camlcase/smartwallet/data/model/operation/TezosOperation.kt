/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.model.operation

import io.camlcase.kotlintezos.exchange.DexterEntrypoint
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.operation.SmartContractCallOperation
import io.camlcase.kotlintezos.model.tzkt.TzKtOperation
import io.camlcase.kotlintezos.model.tzkt.TzKtReveal
import io.camlcase.kotlintezos.model.tzkt.TzKtTransaction
import io.camlcase.kotlintezos.model.tzkt.dto.TzKtAlias
import io.camlcase.smartwallet.data.model.Token
import io.camlcase.smartwallet.data.model.amount.Balance

/**
 * An operation from/to the user that it's already on the blockchain
 */
interface TezosOperation {
    /**
     * Type distinction used inside the app. Different from the blockchain operation type.
     */
    val type: Type

    /**
     * Destination token
     */
    val token: Token

    /**
     * Fees burned for this operation
     */
    val fees: Tez?

    /**
     * Related Conseil operations. Usually just one, but smart contract calls might involve several
     * (E.g. approvals for tokenToXTZ swaps).
     */
    val tzKtOperations: List<TzKtOperation>

    enum class Type {
        SEND,
        RECEIVE,
        SWAP,
        DELEGATE,
        ORIGINATION,
        REVEAL,
        UNKNOWN
    }
}

interface TransferOperation : TezosOperation {
    val amount: Balance<*>
    val destination: TzKtAlias
}

fun TzKtOperation?.isReveal(): Boolean {
    return this is TzKtReveal
}

fun TzKtOperation?.isSwap(): Boolean {
    val entrypoint = (this as? TzKtTransaction)?.smartContractCall?.entrypoint
    return entrypoint == DexterEntrypoint.TOKEN_TO_TEZ.entrypoint
            || entrypoint == DexterEntrypoint.TEZ_TO_TOKEN.entrypoint
}

fun TzKtOperation?.isApprove(): Boolean {
    val entrypoint = (this as? TzKtTransaction)?.smartContractCall?.entrypoint
    return entrypoint == SmartContractCallOperation.SMART_CONTRACT_ENTRYPOINT_APPROVE
}

fun List<TzKtOperation>.groupFees(): Tez {
    var fees = Tez.zero
    this.map { operation ->
        fees += operation.fees?.fee ?: Tez.zero
        operation.fees?.extraFees?.asList?.map {
            fees += it.fee
        }
    }
    return fees
}

fun TzKtTransaction.getSource(): String {
    return this.sender.alias ?: this.sender.address
}

fun TzKtTransaction.getDestination(): String {
    return this.destination.alias ?: this.destination.address
}

