/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.model.smartcontract

import android.os.Parcel
import android.os.Parcelable
import io.camlcase.smartwallet.data.model.Deserializer
import io.camlcase.smartwallet.data.model.Serializable
import org.json.JSONObject

/**
 * Representation of an originated contract in the blockchain
 *
 * @param contractAddress The KT1 address
 * @param operationHash The operation hash where this contract was originated. To query if included.
 * @param included Is included in the blockchain
 */
data class OriginatedContract(
    val contractAddress: String,
    val operationHash: String,
    val included: Boolean = false
) : Serializable<String>, Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readByte() != 0.toByte()
    )

    override fun serialize(): String {
        val jsonObject = JSONObject()
        jsonObject.put(ARG_CONTRACT_ADDRESS, contractAddress)
        jsonObject.put(ARG_CONTRACT_ORIGINATION_OPERATION_HASH, operationHash)
        jsonObject.put(ARG_INCLUDED, included)
        return jsonObject.toString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(contractAddress)
        parcel.writeString(operationHash)
        parcel.writeByte(if (included) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Deserializer<OriginatedContract, String>, Parcelable.Creator<OriginatedContract> {
        const val ARG_CONTRACT_ADDRESS = "contract_address"
        const val ARG_CONTRACT_ORIGINATION_OPERATION_HASH = "contract_operation_hash"
        const val ARG_INCLUDED = "contract_is_included"

        override fun deserialize(serialized: String): OriginatedContract? {
            return try {
                val jsonObject = JSONObject(serialized)
                val address = jsonObject[ARG_CONTRACT_ADDRESS] as String
                val hash = jsonObject[ARG_CONTRACT_ORIGINATION_OPERATION_HASH] as String
                val included = jsonObject[ARG_INCLUDED] as Boolean
                return OriginatedContract(address, hash, included)
            } catch (e: Exception) {
//                Logger.e("ERROR in deserialize OriginatedContract ($serialized): $e")
                null
            }
        }

        override fun createFromParcel(parcel: Parcel): OriginatedContract {
            return OriginatedContract(parcel)
        }

        override fun newArray(size: Int): Array<OriginatedContract?> {
            return arrayOfNulls(size)
        }
    }
}
