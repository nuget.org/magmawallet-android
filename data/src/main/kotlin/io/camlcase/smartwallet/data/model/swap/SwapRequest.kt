/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.model.swap

import android.os.Parcel
import android.os.Parcelable
import io.camlcase.smartwallet.data.core.Exchange
import io.camlcase.smartwallet.data.core.XTZ
import io.camlcase.smartwallet.data.model.Token
import io.camlcase.smartwallet.data.model.amount.Balance
import io.camlcase.smartwallet.data.model.amount.EmptyBalance
import io.camlcase.smartwallet.data.model.isXTZ
import java.math.BigDecimal
import java.math.RoundingMode

data class SwapRequest(
    val fromToken: Token,
    val toToken: Token,
    val amountFrom: Balance<*>,
    val amountTo: Balance<*>,
    val selectedSlippage: Double = Exchange.DEFAULT_SLIPPAGE,
    val priceImpact: BigDecimal,
    val timeout: Int = Exchange.DEFAULT_TIMEOUT_MINUTES
) : Parcelable {
    val tokenToTez: Boolean
        get() {
            return toToken.isXTZ()
        }
    val tezToToken: Boolean
        get() {
            return fromToken.isXTZ()
        }

    val priceImpactPercentage: Double
        get() = priceImpact.toDouble() * 100

    val priceImpactRounded: Double
        get() = priceImpact.setScale(4, RoundingMode.HALF_UP).toDouble()

    constructor(parcel: Parcel) : this(
        parcel.readParcelable(Token::class.java.classLoader) ?: XTZ,
        parcel.readParcelable(Token::class.java.classLoader) ?: XTZ,
        parcel.readParcelable(Balance::class.java.classLoader) ?: EmptyBalance(),
        parcel.readParcelable(Balance::class.java.classLoader) ?: EmptyBalance(),
        parcel.readDouble(),
        BigDecimal(parcel.readString() ?: "0.0"),
        parcel.readInt()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(fromToken, flags)
        parcel.writeParcelable(toToken, flags)
        parcel.writeParcelable(amountFrom, flags)
        parcel.writeParcelable(amountTo, flags)
        parcel.writeDouble(selectedSlippage)
        parcel.writeString(priceImpact.toPlainString())
        parcel.writeInt(timeout)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SwapRequest> {
        override fun createFromParcel(parcel: Parcel): SwapRequest {
            return SwapRequest(parcel)
        }

        override fun newArray(size: Int): Array<SwapRequest?> {
            return arrayOfNulls(size)
        }
    }
}

/**
 * In decimals (E.g. 0.12 for 12%)
 */
@Deprecated("Use price impact")
data class Slippage(
    val selectedSlippage: Double = Exchange.DEFAULT_SLIPPAGE,
    val calculatedSlippage: Double
) : Parcelable {

    val calculatedSlippagePercentage: Double
        get() = calculatedSlippage * 100

    val selectedSlippagePercentage: Double
        get() = selectedSlippage * 100

    constructor(parcel: Parcel) : this(
        parcel.readDouble(),
        parcel.readDouble()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeDouble(selectedSlippage)
        parcel.writeDouble(calculatedSlippage)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Slippage> {
        val empty = Slippage(1.0, 1.0)

        override fun createFromParcel(parcel: Parcel): Slippage {
            return Slippage(parcel)
        }

        override fun newArray(size: Int): Array<Slippage?> {
            return arrayOfNulls(size)
        }
    }
}
