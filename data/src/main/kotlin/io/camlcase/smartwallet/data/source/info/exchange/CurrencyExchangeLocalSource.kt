/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.source.info.exchange

import io.camlcase.smartwallet.data.model.exchange.CurrencyEntity
import io.camlcase.smartwallet.data.source.local.UserPreferences
import io.camlcase.smartwallet.data.source.local.UserPreferences.Companion.CURRENCY_EXCHANGE
import kotlinx.serialization.json.Json
import javax.inject.Inject

class CurrencyExchangeLocalSource @Inject constructor(
    private val preferences: UserPreferences
) {
    fun storeCurrencyValue(value: CurrencyEntity) {
        val serialised = Json.encodeToString(CurrencyEntity.serializer(), value)
        preferences.store(CURRENCY_EXCHANGE, serialised)
    }

    fun getCurrencyValue(): CurrencyEntity? {
        return preferences.getString(CURRENCY_EXCHANGE)?.let {
            try {
                Json.decodeFromString(CurrencyEntity.serializer(), it)
            } catch (e: Exception) {
                // It will fail on first time since it won't know how to deserialise
                null
            }
        }
    }
}

