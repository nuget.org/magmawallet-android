/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.source.local

import android.content.SharedPreferences
import io.camlcase.smartwallet.data.core.AppContext

/**
 * Protocol for saving key-value pairs in an internal xml file
 */
interface Preferences {
    val context: AppContext
    val fileName: String
    val preferences: SharedPreferences?

    fun store(key: String, data: Any) {
        preferences?.edit().apply {
            storeType(key, data)
        }?.apply()
    }

    fun getString(key: String): String? {
        return preferences?.get(key)
    }

    fun getBoolean(key: String): Boolean {
        return preferences?.get(key) ?: false
    }

    fun getLong(key: String): Long {
        return preferences?.get(key) ?: -1
    }

    fun getInt(key: String): Int {
        return preferences?.get(key) ?: -1
    }

    fun getFloat(key: String): Float {
        return preferences?.get(key) ?: -1f
    }

    fun getByteArray(key: String): ByteArray? {
        val stringArray: String? = preferences?.get(key)
        if (stringArray != null) {
            val split: List<String> = stringArray.substring(1, stringArray.length - 1).split(", ")
            val array = ByteArray(split.size)
            for (i in split.indices) {
                array[i] = split[i].toByte()
            }
            return array
        }
        return null
    }

    fun getDouble(key: String): Double {
        return preferences?.get(key) ?: -1.0
    }

    /**
     * Delete all entries
     */
    fun clear() {
        preferences?.apply {
            edit().clear().apply()
        }
    }

    fun clearPreference(key: String) {
        preferences?.apply {
            edit().remove(key).apply()
        }
    }
}

@Suppress("IMPLICIT_CAST_TO_ANY")
inline operator fun <reified T> SharedPreferences.get(key: String): T? =
    when (T::class) {
        String::class -> getString(key, null)
        Int::class -> getInt(key, -1)
        Boolean::class -> getBoolean(key, false)
        Float::class -> getFloat(key, -1f)
        Long::class -> getLong(key, -1)
        Double::class -> getString(key, null)?.toDouble() ?: -1.0
        else -> null
    } as T?

private fun SharedPreferences.Editor?.storeType(key: String, data: Any) {
    when (data) {
        is String -> this?.putString(key, data)
        is Int -> this?.putInt(key, data)
        is Long -> this?.putLong(key, data)
        is Boolean -> this?.putBoolean(key, data)
        is Float -> this?.putFloat(key, data)
        is ByteArray -> this?.putString(key, data.contentToString())
        is Double -> this?.putString(key, data.toString())
    }
}
