/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.source.money

import io.camlcase.kotlintezos.data.RPC
import io.camlcase.smartwallet.data.model.money.MoonPayCurrency
import io.camlcase.smartwallet.data.model.money.MoonPaySupportedCountry
import io.camlcase.smartwallet.data.source.RemoteSource
import io.camlcase.smartwallet.data.source.money.MoonPayRemoteSource.Companion.API_VERSION
import io.camlcase.smartwallet.data.source.money.MoonPayRemoteSource.Companion.COUNTRIES_ENDPOINT
import io.camlcase.smartwallet.data.source.money.MoonPayRemoteSource.Companion.CURRENCIES_ENDPOINT
import io.reactivex.rxjava3.core.Single

/**
 *
 * @see DataSourceModule
 */
class MoonPayRemoteSource(
    debug: Boolean = false,
    moonPayApiUrl: String,
    private val moonPayApiKey: String
) : RemoteSource(moonPayApiUrl, moonPayApiKey, debug) {

    internal fun getSupportedCountries(): Single<List<MoonPaySupportedCountry>> {
        return Single.fromFuture(networkClient.send(GetSupportedCountriesRPC(moonPayApiKey)))
    }

    internal fun getSupportedCurrencies(): Single<List<MoonPayCurrency>> {
        return Single.fromFuture(networkClient.send(GetCurrenciesRPC()))
    }

    companion object {
        internal const val API_VERSION = "/v3"
        internal const val COUNTRIES_ENDPOINT = "/countries"
        internal const val CURRENCIES_ENDPOINT = "/currencies"
    }
}

internal class GetSupportedCountriesRPC(
    moonPayApiKey: String
) : RPC<List<MoonPaySupportedCountry>>(
    endpoint = API_VERSION + COUNTRIES_ENDPOINT,
    queryParameters = mapOf("apiKey" to moonPayApiKey),
    parser = MoonPaySupportedCountryParser()
)

internal class GetCurrenciesRPC : RPC<List<MoonPayCurrency>>(
    endpoint = API_VERSION + CURRENCIES_ENDPOINT,
    parser = MoonPayCurrencyParser()
)

