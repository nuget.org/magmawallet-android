/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.usecase.activity

import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.SmartContractCall
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.operation.SmartContractCallOperation.Companion.SMART_CONTRACT_ENTRYPOINT_TRANSFER
import io.camlcase.kotlintezos.model.tzkt.TzKtOperation
import io.camlcase.kotlintezos.model.tzkt.TzKtTransaction
import io.camlcase.kotlintezos.smartcontract.michelson.BigIntegerMichelsonParameter
import io.camlcase.kotlintezos.smartcontract.michelson.PairMichelsonParameter
import io.camlcase.smartwallet.data.core.Tokens.unknownToken
import io.camlcase.smartwallet.data.core.XTZ
import io.camlcase.smartwallet.data.model.Token
import io.camlcase.smartwallet.data.model.amount.CoinBalance
import io.camlcase.smartwallet.data.model.amount.TokenBalance
import io.camlcase.smartwallet.data.model.operation.SwapOperation
import io.camlcase.smartwallet.data.model.operation.isReveal
import java.math.BigInteger

internal class MapSwapUseCase(
    private val supportedFATokens: List<Token>
) {
    fun mapTokenToTez(
        transaction: TzKtTransaction,
        smartContract: SmartContractCall,
        operations: List<TzKtOperation>
    ): SwapOperation {
        val tokenFrom = parseToken(transaction.destination.address)

        val (tokenAmount, tezAmount) = parseTokenToTezAmounts(smartContract, operations)
        val amountFrom = TokenBalance(tokenAmount, tokenFrom.decimals)
        val amountTo = CoinBalance(tezAmount)

        val tzKtOperations = operations.filter { !it.isReveal() }
        return SwapOperation(tokenFrom, XTZ, amountFrom, amountTo, tzKtOperations)
    }

    private fun parseTokenToTezAmounts(
        smartContract: SmartContractCall,
        operations: List<TzKtOperation>
    ): Pair<BigInteger, Tez> {
        // On tokenToXtz, the real amount of XTZ comes as amount of one of the operations
        val tzKtTransaction = operations.firstOrNull {
            val operation = it as? TzKtTransaction
            operation?.smartContractCall == null && (operation?.amount ?: Tez.zero) > Tez.zero
        } as? TzKtTransaction

        // Pair (Pair "SOURCE" "DEXTER ADDRESS") (Pair TOKENS (Pair MINIMUM_XTZ "DEADLINE"))
        return smartContract.parameters?.let {
            val pairMichelsonParameter = (it as? PairMichelsonParameter)?.right as? PairMichelsonParameter
            val tez =
                ((pairMichelsonParameter?.right as? PairMichelsonParameter)?.left as? BigIntegerMichelsonParameter)?.value
            val tokenAmount = (pairMichelsonParameter?.left as? BigIntegerMichelsonParameter)?.value

            Pair(tokenAmount ?: BigInteger.ZERO, tzKtTransaction?.amount ?: Tez(tez?.toString() ?: "0"))
        } ?: Pair(BigInteger.ZERO, Tez.zero)
    }

    fun mapTezToToken(
        transaction: TzKtTransaction,
        smartContract: SmartContractCall,
        operations: List<TzKtOperation>
    ): SwapOperation {
        val tokenTo = parseToken(transaction.destination.address)

        val amountFrom = CoinBalance(transaction.amount)
        // On xtzToToken, the real token amount received is in the internal [SMART_CONTRACT_ENTRYPOINT_TRANSFER] operation.
        val internalCall =
            operations.firstOrNull { (it as? TzKtTransaction)?.smartContractCall?.entrypoint == SMART_CONTRACT_ENTRYPOINT_TRANSFER } as? TzKtTransaction
        val tokenAmount = if (internalCall != null) {
            internalCall?.smartContractCall?.parameters?.let {
                (((it as? PairMichelsonParameter)?.right as? PairMichelsonParameter)?.right as? BigIntegerMichelsonParameter)?.value
            }
        } else {
            smartContract.parameters?.let {
                (((it as? PairMichelsonParameter)?.right as? PairMichelsonParameter)?.left as? BigIntegerMichelsonParameter)?.value
            }
        }
        val amountTo = TokenBalance(tokenAmount ?: BigInteger.ZERO, tokenTo.decimals)

        val tzKtOperations = operations.filter { !it.isReveal() }
        return SwapOperation(XTZ, tokenTo, amountFrom, amountTo, tzKtOperations)
    }

    private fun parseToken(address: Address): Token {
        // Currently supported Dexter tokens
        return supportedFATokens.firstOrNull { it.dexterAddress == address }
        // v1.0 Dexter tokens
            ?: oldMainnetFATokens.firstOrNull { it.dexterAddress == address }
            // Fallback token
            ?: unknownToken
    }

    companion object {
        /**
         * v1.0 dexter addresses to match with old transactions
         */
        private val TZBTC_10 by lazy {
            Token(
                "Wrapped BTC",
                "tzBTC",
                8,
                contractAddress = "KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn",
                dexterAddress = "KT1DrJV8vhkdLEj76h1H9Q4irZDqAkMPo1Qf"
            )
        }
        private val USDTZ_10 by lazy {
            Token(
                "USDTez",
                "USDtz",
                6,
                contractAddress = "KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9",
                dexterAddress = "KT1Puc9St8wdNoGtLiD2WXaHbWU7styaxYhD"
            )
        }
        private val WXTZ_10 by lazy {
            Token(
                "Wrapped Tezos",
                "wXTZ",
                6,
                contractAddress = "KT1VYsVfmobT7rsMVivvZ4J8i3bPiqz12NaH",
                dexterAddress = "KT1XTXBsEauzcv3uPvVXW92mVqrx99UGsb9T"
            )
        }

        private val ETHTZ_10 by lazy {
            Token(
                "ETH Tez",
                "ETHtz",
                18,
                contractAddress = "KT19at7rQUvyjxnZ2fBv7D9zc8rkyG7gAoU8",
                dexterAddress = "KT19c8n5mWrqpxMcR3J687yssHxotj88nGhZ"
            )
        }

        private val oldMainnetFATokens: List<Token> = listOf(
            ETHTZ_10,
            TZBTC_10,
            USDTZ_10,
            WXTZ_10,
        )
    }
}
