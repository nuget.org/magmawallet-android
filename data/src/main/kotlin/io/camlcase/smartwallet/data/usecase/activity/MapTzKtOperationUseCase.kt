/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.usecase.activity

import io.camlcase.kotlintezos.model.operation.OperationType
import io.camlcase.kotlintezos.model.tzkt.*
import io.camlcase.smartwallet.data.analytic.report
import io.camlcase.smartwallet.data.core.Logger
import io.camlcase.smartwallet.data.model.operation.*
import javax.inject.Inject

/**
 * Maps a list of [TzKtOperation] to a list of [TezosOperation]
 */
class MapTzKtOperationUseCase @Inject constructor(
    private val mapTransactionUseCase: MapTzKtTransactionUseCase
) {
    private var operations: List<TzKtOperation>? = null

    fun map(operations: List<TzKtOperation>): List<TezosOperation> {
        this.operations = operations

        val list = ArrayList<TezosOperation>()
        val groupedOperations = operations.groupBy { it.hash }

        for (hash in groupedOperations) {
            try {
                val mainOperation = findMainOperation(hash.value)
                mainOperation?.apply {
                    list.add(map(this, hash.value))
                }
                // Reveals are part of another operation
                hash.value.firstOrNull { it.isReveal() }?.apply {
                    list.add(mapReveal(this as TzKtReveal))
                }
            } catch (e: Exception) {
                e.report()
                // Don't add to the list and move on
                Logger.e("*** ERROR parsing $hash :\n$e")
            }
        }

        return list
    }

    /**
     * From a group of operations for a given execution/hash:
     * - Order them by descending IDs. If the group is a migration this ensures we get the XTZ transaction first (the last).
     * - If it's swap, search for the one that has "xtzToToken"/"tokenToXTZ" entrypoints
     * - If it's a transaction, get the one with no internal operations
     * - If not, just get the one that is not an approve or a reveal
     */
    private fun findMainOperation(operations: List<TzKtOperation>): TzKtOperation? {
        val ordered = operations.sortedByDescending { it.id }
        return ordered.firstOrNull { it.isSwap() }
            ?: ordered.firstOrNull { it.isExternalTransaction() }
            ?: ordered.firstOrNull { !it.isApprove() && !it.isReveal() }
    }

    private fun TzKtOperation.isExternalTransaction(): Boolean {
        return this is TzKtTransaction && this.hasInternals
    }

    fun map(tzKtOperation: TzKtOperation, operations: List<TzKtOperation>): TezosOperation {
        return when (tzKtOperation.kind) {
            OperationType.TRANSACTION -> {
                mapTransactionOperation(tzKtOperation as TzKtTransaction, operations)
            }
            OperationType.DELEGATION -> mapDelegation(tzKtOperation as TzKtDelegation)
            OperationType.ORIGINATION -> mapOrigination(tzKtOperation as TzKtOrigination)
            OperationType.REVEAL -> mapReveal(tzKtOperation as TzKtReveal)
            else -> UnknownOperation(operations)
        }
    }

    private fun mapReveal(conseilReveal: TzKtReveal): RevealOperation {
        return RevealOperation(conseilReveal)
    }

    private fun mapOrigination(origination: TzKtOrigination): OriginateOperation {
        return OriginateOperation(origination)
    }

    private fun mapDelegation(delegation: TzKtDelegation): DelegateOperation {
        return DelegateOperation(delegation.newDelegate, tzKtOperation = delegation)
    }

    private fun mapTransactionOperation(
        transaction: TzKtTransaction,
        operations: List<TzKtOperation>
    ): TezosOperation {
        return mapTransactionUseCase.map(transaction, operations)
    }
}
