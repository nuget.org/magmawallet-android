/**
 * # Released under MIT License
 *
 * Copyright (c) 2021 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.usecase.analytics

const val EVENT_DEFINITION = "Event Definition"


enum class AnalyticEvent(val message: String) {
    WALLET_CREATED("Wallet Created"),
    WALLET_RECOVERED("Wallet Recovered"),
    WALLET_UNPAIRED("Wallet Unpaired"),
    BAKER_DELEGATED("Baker Selection Succeeded"),
    BAKER_UNDELEGATED("Baker Undelegation Succeeded"),
    BAKER_DELEGATED_ERROR("Baker Selection Failed"),
    BAKER_UNDELEGATED_ERROR("Baker Undelegation Failed"),
    SEND_SUCCESS("Send Succeeded"),
    SEND_ERROR("Send Failed"),
    EXCHANGE_SUCCESS("Exchange Succeeded"),
    EXCHANGE_ERROR("Exchange Failed"),
    ADD_CONTACT_SUCCESS("Add Contact Succeeded"),
    ADD_CONTACT_ERROR("Add Contact Failed"),

    /**
     * MoonPay events. Only received when user clicks "Return to Magma".
     * We are blind to them if the user decides to close the browser.
     */
    MOONPAY_BUY_COMPLETE("MoonPay Buy Completed"),
    MOONPAY_BUY_PENDING("MoonPay Buy Pending"),
    MOONPAY_BUY_ERROR("MoonPay Buy Pending"),

    /**
     * MIGRATION
     */
    MIGRATION_POPUP("Migration Popup"),
    MIGRATION_POPUP_SKIP("Migration Skip"),
    MIGRATION_POPUP_SKIP_PERMANENT("Migration Skip Permanent"),
    MIGRATION_BALANCE_CHECK("Migration Balance Check"),
    MIGRATION_INSUFFICIENT_FUNDS("Migration Insufficient Funds"),
    MIGRATION_SEED_PHRASE("Migration Seed Phrase"),
    MIGRATION_TRANSFER_FUNDS("Migration Transfer Funds"),
    MIGRATION_WAIT_FOR_INJECTION("Migration Wait for Injection"),
    MIGRATION_SUCCESS("Migration Success"),
    MIGRATION_ERROR("Migration Failed"),
}

/**
 * When arriving to screen
 */
enum class ScreenEvent(val eventName: String) {
    ONB_START_MENU("ONB_1.0-Start-Menu"),
    ONB_ALREAY_PAIRED("ONB_1.10-Already-Paired"),
    ONB_WALLET_SETUP("ONB_2.0-New-Wallet-Setup"),
    ONB_WALLET_SETUP_HELP("ONB_2.1-New-Wallet-Setup-help"),
    ONB_WALLET_PASSPHRASE("ONB_3.0-New-Wallet-Passphrase"),
    ONB_RECOVERY_PROMPT("ONB_4.0-Recovery-Prompt"),
    ONB_RECOVERY_DISPLAY("ONB_4.1-Recovery-Display"),
    ONB_CONFIRM_BACKUP("ONB_4.2-Confirm-Backup"),
    ONB_SECURITY_PROMPT("ONB_5.0-Security-Prompt"),
    ONB_PIN_PROMPT("ONB_6.0-PIN-Prompt"),
    ONB_PIN_CONFIRM("ONB_6.1-PIN-Confirm"),
    ONB_BIOMETRIC_PROMPT("ONB_6.2-Biometric-Prompt"),
    ONB_BIOMETRIC_CAPTURE("ONB_6.3-Biometric-Capture"),
    ONB_WALLET_SUCCESS("ONB_7.0-Wallet-Success"),
    ONB_RECOVERY_ENTRY("ONB_100.0-Recovery-entry"),
    ONB_RECOVERY_ENTRY_HELP("ONB_100.3-Recovery-entry-help"),
    ONB_RECOVERY_MENU("ONB_101.0-Recovery-menu"),
    ONB_RECOVERY_MENU_HELP("ONB_101.1-Recovery-menu-help"),
    ONB_RECOVERY_MENU_NONHD("ONB_102.0-Recovery-nonHD-Android"),
    ONB_RECOVERY_SUCCESS("ONB_103.0-Recovery-Success"),
    LOGIN_AUTH_BIOMETRIC("LOG_1.0-Login-Return-Prompt"),
    LOGIN_AUTH_PIN("LOG_2.0-Login-Return-Prompt-PIN"),
}

/**
 * When the screen state changes to something
 */
enum class StateEvent(val eventName: String, val argument: String) {
    ONB_WALLET_PASSPHRASE("ONB_3.0-New-Wallet-Passphrase", "Input validated passphrase"),
    ONB_CONFIRM_CONFIRM_BUTTON("ONB_4.2-Confirm-Backup", "Tap Confirm button"),
    ONB_CONFIRM_SKIP_BUTTON("ONB_4.2-Confirm-Backup", "Tap Skip button"),
    ONB_RECOVERY_ENTRY_SEED_PHRASE("ONB_100.0-Recovery-entry", "Input validated recovery phrase"),
    ONB_RECOVERY_ENTRY_PASSPHRASE("ONB_100.0-Recovery-entry", "Input validated optional passphrase"),
    ONB_RECOVERY_ENTRY_DERIVATION_PATH("ONB_100.1-Recovery-entry-advanced", "Input valid HD derivation path"),
    ONB_RECOVERY_ENTRY_SUCCESS("ONB_100.2-Recovery-entry-entered", "Recovery succeeded"),
    ONB_RECOVERY_ENTRY_FAILED("ONB_100.2-Recovery-entry-entered", "Recovery failed (%s)"),
    ONB_RECOVERY_NONHD_REGULAR("ONB_102.0-Recovery-nonHD-Android", "Tap Regular Recovery"),
    ONB_RECOVERY_NONHD_MIGRATION("ONB_102.0-Recovery-nonHD-Android", "Tap Android Security Migration")
}
