/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.usecase.contact

import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.TezosNetwork
import io.camlcase.smartwallet.data.model.WrappedUri
import io.camlcase.smartwallet.data.model.contact.ContactTezosAddress
import io.camlcase.smartwallet.data.model.contact.MagmaContact
import io.camlcase.smartwallet.data.source.contact.PhoneBookLocalSource
import java.io.IOException
import java.util.*
import javax.inject.Inject

class WriteContactUseCase @Inject constructor(
    private val tezosNetwork: TezosNetwork,
    private val phoneBookLocalSource: PhoneBookLocalSource
) {

    @Throws(java.lang.IllegalStateException::class, IOException::class)
    fun addContact(account: String, name: String, address: Address): MagmaContact {
        val contact = MagmaContact(0, name, listOf(ContactTezosAddress(address, tezosNetwork)))
        val insertedContact = phoneBookLocalSource.add(account, contact)
        return insertedContact
    }

    /**
     * Given an uri from the Contacts picker, fetch its related values and build a [MagmaContact] object.
     */
    fun pickContact(wrappedUri: WrappedUri): MagmaContact {
        return phoneBookLocalSource.getContactFromUri(wrappedUri)
            ?: throw NoSuchElementException()
    }

    fun editContact(contact: MagmaContact, updates: Pair<String, Address>): Pair<Boolean, MagmaContact> {
        var edited = false
        var name: String = contact.name
        var addresses = contact.tezosAddresses
        if (contact.name != updates.first) {
            edited = editContactName(contact, updates.first)
            name = updates.first
        }

        if (contact.tezosAddresses.firstOrNull { it.address == updates.second } == null) {
            edited = editContactAddress(contact, updates.second)
            addresses = listOf(ContactTezosAddress(updates.second, tezosNetwork))
        }

        return Pair(
            edited,
            MagmaContact(contact.id, name, addresses)
        )
    }

    fun editContactName(contact: MagmaContact, name: String): Boolean {
        return phoneBookLocalSource.updateName(contact, name)
    }

    /**
     * If the [contact] doesn't have a Tezos Address, it will create it instead of updating it
     */
    fun editContactAddress(contact: MagmaContact, address: Address): Boolean {
        val tezosAddress = ContactTezosAddress(address, tezosNetwork)
        return if (contact.tezosAddresses.isEmpty()) {
            phoneBookLocalSource.insertTezosAddress(contact.id, tezosAddress)
        } else {
            phoneBookLocalSource.updateTezosAddress(contact, tezosAddress)
        }
    }

    fun isMagmaContact(contact: MagmaContact): Boolean {
        return !phoneBookLocalSource.hasExtraData(contact)
    }

    /**
     * Only deletes the full contact on device if it only has the tz1 address row. If not, deletes the tz1 address and maintains the contact.
     */
    fun deleteContact(contact: MagmaContact): Boolean {
        return if (isMagmaContact(contact)) {
            phoneBookLocalSource.delete(contact)
        } else {
            deleteContactData(contact)
        }
    }

    /**
     * Delete the tezos address data type row on a contact
     */
    private fun deleteContactData(contact: MagmaContact): Boolean {
        return phoneBookLocalSource.deleteData(contact)
    }
}
