/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.usecase.delegate

import io.camlcase.smartwallet.data.model.error.ErrorType
import io.camlcase.smartwallet.data.model.error.AppError
import io.camlcase.smartwallet.data.model.delegate.TezosBaker
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.processors.PublishProcessor

/**
 * Remote fetching of the Delegate is done asynchronously from the screen loading.
 * Screens that need to be pinged on delegate changes should listen to this.
 *
 * @see GetDelegateUseCase
 */
class DelegateStream {
    private val processor: PublishProcessor<Result<TezosBaker>> = PublishProcessor.create()
    var stream: Flowable<Result<TezosBaker>> = processor.onBackpressureLatest().share()

    fun onDelegate(baker: TezosBaker?) {
        processor.onNext(
            if (baker == null) {
                Result.failure(AppError(ErrorType.NO_BAKER))
            } else {
                Result.success(baker)
            }
        )
    }

    fun onDelegate(result: Result<TezosBaker>) {
        processor.onNext(result)
    }
}
