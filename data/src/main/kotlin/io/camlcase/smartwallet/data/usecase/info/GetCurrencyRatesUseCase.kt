/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.usecase.info

import io.camlcase.smartwallet.data.model.exchange.CoinGeckoRate
import io.camlcase.smartwallet.data.source.info.exchange.CoinGeckoRemoteSource
import io.camlcase.smartwallet.data.source.local.UserPreferences
import io.camlcase.smartwallet.data.usecase.user.ShouldUpdateUseCase
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class GetCurrencyRatesUseCase @Inject constructor(
    private val remoteSource: CoinGeckoRemoteSource,
    private val updater: ShouldUpdateUseCase,
    private val userPreferences: UserPreferences
) {

    fun updateAvailableCurrencies(): Single<Boolean> {
        val shouldUpdate = updater.shouldUpdateCoinGeckoRates()
        val localList = localAvailableCurrencies()
        if (shouldUpdate || localList.isNullOrEmpty()) {
            return fetchAvailableCurrencies()
                .map { true }
        }
        return Single.just(false)
    }

    private fun fetchAvailableCurrencies(): Single<Map<String, CoinGeckoRate>> {
        return remoteSource.getBTCToCurrencyRates()
            .map {
                userPreferences.updateAvailableCurrencies(it)
                updater.updateCoinGeckoRates()
                it
            }
    }

    fun localAvailableCurrencies(): Map<String, CoinGeckoRate>? {
        return userPreferences.getAvailableCurrencies()
    }

    fun getAvailableCurrencies(): Single<Map<String, CoinGeckoRate>> {
        val localList = localAvailableCurrencies()
        return if (localList.isNullOrEmpty()) {
            fetchAvailableCurrencies()
        } else {
            Single.just(localList)
        }
    }
}
