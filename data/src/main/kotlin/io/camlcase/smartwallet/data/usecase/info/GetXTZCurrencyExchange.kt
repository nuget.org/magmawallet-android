/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.usecase.info

import io.camlcase.smartwallet.data.core.Wallet
import io.camlcase.smartwallet.data.core.extension.overTime
import io.camlcase.smartwallet.data.model.exchange.CurrencyBundle
import io.camlcase.smartwallet.data.model.exchange.CurrencyEntity
import io.camlcase.smartwallet.data.model.exchange.XTZTokenInfo
import io.camlcase.smartwallet.data.source.info.exchange.CoinGeckoRemoteSource
import io.camlcase.smartwallet.data.source.info.exchange.CurrencyExchangeLocalSource
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class GetXTZCurrencyExchange @Inject constructor(
    private val currencyRemoteSource: CoinGeckoRemoteSource,
    private val currencyLocalSource: CurrencyExchangeLocalSource
) {

    fun updateCurrencyFor(currency: CurrencyBundle): Single<Double> {
        return currencyRemoteSource.getXTZPriceFor(currency.code)
            .map {
                val currencyEntity = CurrencyEntity(currency, it)
                currencyLocalSource.storeCurrencyValue(currencyEntity)
                it
            }
    }

    /**
     * XTZ -> USD (->? EUR/GBP/...)
     */
    fun getXTZExchange(): Single<XTZTokenInfo> {
        val currencyValue = currencyLocalSource.getCurrencyValue()
        val bundle = currencyValue?.toBundle() ?: Wallet.defaultCurrency

        // Update only once every 30s to avoid overloading
        val shouldUpdate = currencyValue?.timestamp?.overTime(30 * 1000) ?: true
        return if (shouldUpdate) {
            updateCurrencyFor(bundle)
                .map { XTZTokenInfo(it) }
        } else {
            Single.just(XTZTokenInfo(currencyValue?.price ?: 1.0))
        }
    }

    fun getCurrencyBundle(): CurrencyBundle {
        val currencyValue = currencyLocalSource.getCurrencyValue()
        return currencyValue?.toBundle() ?: Wallet.defaultCurrency
    }

    fun getCurrencyCode(): String {
        return getCurrencyBundle().code
    }
}
