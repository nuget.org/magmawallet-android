/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.usecase.migration

import io.camlcase.kotlintezos.TezosNodeClient
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.smartwallet.data.core.EmitterSource
import io.camlcase.smartwallet.data.core.RxEmitter
import io.camlcase.smartwallet.data.model.error.AppError
import io.camlcase.smartwallet.data.model.error.ErrorType
import io.camlcase.smartwallet.data.source.user.WalletLocalSource
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class GetMigrationWalletBalance @Inject constructor(
    private val emitter: RxEmitter,
    private val tezosNodeClient: TezosNodeClient,
    private val walletLocalSource: WalletLocalSource
) {
    fun getBalance(): Single<Tez> {
        return walletLocalSource.getMigrationWallet()?.let { appWallet ->
            return Single.create<Tez> {
                tezosNodeClient.getBalance(appWallet.address, emitter.emitCallback(it, EmitterSource.MIGRATION_BALANCE))
            }

        } ?: Single.error(AppError(ErrorType.NO_WALLET))
    }
}
