/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.usecase.operation

import io.camlcase.kotlintezos.TezosNodeClient
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.TezosProtocol
import io.camlcase.kotlintezos.model.operation.OperationType
import io.camlcase.kotlintezos.model.operation.fees.OperationFees
import io.camlcase.kotlintezos.operation.OperationFeesPolicy
import io.camlcase.kotlintezos.operation.fees.DefaultFeeEstimator
import io.camlcase.kotlintezos.wallet.Wallet
import io.camlcase.smartwallet.data.core.EmitterSource
import io.camlcase.smartwallet.data.core.RxEmitter
import io.camlcase.smartwallet.data.source.local.UserPreferences
import io.camlcase.smartwallet.data.source.user.WalletLocalSource
import io.camlcase.smartwallet.data.usecase.OperationUseCase
import io.camlcase.smartwallet.data.usecase.analytics.AnalyticEvent
import io.camlcase.smartwallet.data.usecase.analytics.SendAnalyticUseCase
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject
import javax.inject.Named

class DelegateUseCase @Inject constructor(
    override val emitter: RxEmitter,
    override val walletLocalSource: WalletLocalSource,
    override val tezosNodeClient: TezosNodeClient,
    private val userPreferences: UserPreferences,
    private val tezosProtocol: TezosProtocol,
    private val analytics: SendAnalyticUseCase
) : OperationUseCase {

    fun delegate(bakerAddress: String): Single<String> {
        return walletLocalSource.getWalletOrDie()
            .flatMap {
                delegate(it.wallet, bakerAddress)
            }.map {
                saveIncludedData(bakerAddress)
                it
            }
    }

    fun undelegate(): Single<String> {
        return walletLocalSource.getWalletOrDie()
            .flatMap { undelegate(it.wallet) }
            .map {
                userPreferences.clearPreference(UserPreferences.DELEGATE_ADDRESS)
                it
            }
    }

    private fun delegate(wallet: Wallet, bakerAddress: String): Single<String> {
        return Single.create<String> {
            tezosNodeClient.delegate(
                wallet.mainAddress,
                bakerAddress,
                wallet,
                OperationFeesPolicy.Estimate(),
                emitter.emitCallback(it, EmitterSource.DELEGATE)
            )
        }
            .doOnError { analytics.reportEvent(AnalyticEvent.BAKER_DELEGATED_ERROR) }
    }

    private fun undelegate(wallet: Wallet): Single<String> {
        return Single.create<String> {
            tezosNodeClient.undelegate(
                wallet.mainAddress,
                wallet,
                OperationFeesPolicy.Estimate(),
                emitter.emitCallback(it, EmitterSource.UNDELEGATE)
            )
        }
            .doOnError { analytics.reportEvent(AnalyticEvent.BAKER_UNDELEGATED_ERROR) }
    }

    /**
     * Store delegate locally
     */
    fun saveIncludedData(address: Address) {
        userPreferences.store(UserPreferences.DELEGATE_ADDRESS, address)
    }

    fun getDefaultDelegationFee(): OperationFees {
        return DefaultFeeEstimator.calculateFees(tezosProtocol, OperationType.DELEGATION)
    }
}
