/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.usecase.operation

import io.camlcase.kotlintezos.TezosNodeClient
import io.camlcase.kotlintezos.core.ext.isTypeOfError
import io.camlcase.kotlintezos.core.ext.listCauses
import io.camlcase.kotlintezos.model.*
import io.camlcase.kotlintezos.model.operation.*
import io.camlcase.kotlintezos.model.operation.fees.OperationFees
import io.camlcase.kotlintezos.operation.OperationFactory
import io.camlcase.kotlintezos.operation.OperationFeesPolicy
import io.camlcase.kotlintezos.operation.fees.DefaultFeeEstimator
import io.camlcase.smartwallet.data.core.RxEmitter
import io.camlcase.smartwallet.data.core.extension.tezos.getTotalFees
import io.camlcase.smartwallet.data.core.extension.tezos.toBigDecimal
import io.camlcase.smartwallet.data.model.Token
import io.camlcase.smartwallet.data.model.UserTezosAddress
import io.camlcase.smartwallet.data.model.amount.CoinBalance
import io.camlcase.smartwallet.data.model.error.AppError
import io.camlcase.smartwallet.data.model.error.ErrorType
import io.camlcase.smartwallet.data.model.error.InsufficientXTZ
import io.camlcase.smartwallet.data.model.isXTZ
import io.camlcase.smartwallet.data.model.send.SendRequest
import io.camlcase.smartwallet.data.source.local.UserPreferences
import io.camlcase.smartwallet.data.source.user.WalletLocalSource
import io.camlcase.smartwallet.data.usecase.OperationBundle
import io.camlcase.smartwallet.data.usecase.OperationUseCase
import io.camlcase.smartwallet.data.usecase.analytics.AnalyticEvent
import io.camlcase.smartwallet.data.usecase.analytics.SendAnalyticUseCase
import io.camlcase.smartwallet.data.usecase.operation.SendType.Companion.createSendTokenOperation
import io.reactivex.rxjava3.core.Single
import java.math.BigDecimal
import java.math.BigInteger
import javax.inject.Inject
import javax.inject.Named

/**
 * SEND XTZ/FA1.2 to another address
 */
class SendTokenUseCase @Inject constructor(
    override val emitter: RxEmitter,
    override val walletLocalSource: WalletLocalSource,
    override val tezosNodeClient: TezosNodeClient,
    private val userPreferences: UserPreferences,
    @Named("SupportedFATokens")
    private val supportedFATokens: List<Token>,
    private val analytics: SendAnalyticUseCase
) : OperationUseCase {

    fun send(operations: List<Operation>): Single<String> {
        return run(operations)
            .doOnError { analytics.reportEvent(AnalyticEvent.SEND_ERROR) }
    }

    /**
     * - Create operations needed to send xtz/fa1.2 token
     * - Simulate fees
     * _ Bundle operations with fees
     */
    fun createSendOperation(request: SendRequest): Single<OperationBundle> {
        val user: UserTezosAddress = userPreferences.getUserAddress()
        val type = SendType.get(request.token, supportedFATokens)

        val operations = createOperations(type, request)
        return bundleFees(operations, OperationFeesPolicy.Estimate())
            .flatMap { bundle ->
                val insufficientXTZ =
                    bundle.internalErrors.listCauses().isTypeOfError(RPCErrorCause.INSUFFICIENT_XTZ_BALANCE)
                // Insufficient funds error can be ignored *except* if there's a reveal operation.
                // This will mess up gas calculations.
                // Need to recalculate with less XTZ
                val recalculateXTZ = request.maxXTZ && bundle.operations.firstOrNull { it is RevealOperation } != null
                when {
                    recalculateXTZ -> {
                        recalculateSendXTZAndReveal(request, bundle, type)
                    }
                    request.maxXTZ -> {
                        val updatedBundle = updateSendXTZ(request, bundle, user)
                        Single.just(updatedBundle)
                    }
                    insufficientXTZ -> {
                        throw AppError(ErrorType.BLOCKED_OPERATION, exception = InsufficientXTZ())
                    }
                    !bundle.internalErrors.isNullOrEmpty() -> {
                        throw TezosError(TezosErrorType.FAILED_OPERATION, bundle.internalErrors)
                    }
                    else -> {
                        Single.just(bundle)
                    }
                }
            }
    }

    /**
     * When estimating max xtz + reveal, the consumed gas reveal is calculated only for that operation (the tx fails), giving a fee that is <1268.
     */
    private fun recalculateSendXTZAndReveal(
        request: SendRequest,
        bundle: OperationBundle,
        type: SendType
    ): Single<OperationBundle> {
        val revealFeesDefault = DefaultFeeEstimator.calculateFees(OperationType.REVEAL).fee
        val revealFeesCalculated = bundle.operations.firstOrNull { it is RevealOperation }?.fees?.fee ?: Tez.zero
        val realRevealFees = revealFeesDefault - revealFeesCalculated

        val totalFees = bundle.accumulatedFee.getTotalFees() + realRevealFees
        val updatedRequest = updateSendXTZRequest(request, bundle, totalFees)
        return recalculateSendMaxXTZAmount(request, updatedRequest, type)
            .map {
                if (!it.internalErrors.isNullOrEmpty()) {
                    throw TezosError(TezosErrorType.FAILED_OPERATION, it.internalErrors)
                }
                it
            }
    }

    /**
     * Updates the [bundle] with a XTZ Send Operations whose amount is xtzBalance - calculatedFees
     * @throws AppError If fees > xtz balance
     */
    private fun updateSendXTZ(request: SendRequest, bundle: OperationBundle, user: UserTezosAddress): OperationBundle {
        val updatedRequest = updateSendXTZRequest(request, bundle)
        val updatedOperations = ArrayList<Operation>()
        bundle.operations.forEach {
            val operation = if (it is TransactionOperation) {
                // Create new tx operation with updated amount
                createOperation(updatedRequest, user.address, it.fees)
            } else {
                it
            }
            updatedOperations.add(operation)
        }
        return OperationBundle(updatedOperations, bundle.accumulatedFee, bundle.internalErrors)
    }

    private fun updateSendXTZRequest(
        request: SendRequest,
        bundle: OperationBundle,
        totalFees: Tez = bundle.accumulatedFee.getTotalFees()
    ): SendRequest {
        // Recalculate with fees substracted from amount
        val realAmount = request.xtzValue - totalFees
        if (realAmount.toBigDecimal() < BigDecimal.ZERO) {
            throw AppError(ErrorType.BLOCKED_OPERATION, exception = InsufficientXTZ())
        }
        return request.copy(CoinBalance(realAmount))
    }

    /**
     * For [SendType.SEND_XTZ] checks if
     * - amount = balance
     * - amount + fees > balance
     */
    fun operationOverXTZFunds(request: SendRequest, fees: Tez): Boolean {
        if (request.token.isXTZ()) {
            val operationTotal = request.xtzValue + fees

            return request.maxXTZ
                    || request.balance.compareTo(operationTotal) < 0
        }
        return false
    }

    /**
     * When max xtz, we substract the fees returned from the max xtz and estimate the fees.
     * Our final operation should have max xtz - estimated fees.
     *
     * We need to go through [bundleFees] again in case we get a Reveal operation added.
     *
     * @param initialRequest Max XTZ
     * @param updatedRequest Max XTZ - default fees
     *
     * @return Max XTZ - estimated fees
     */
    private fun recalculateSendMaxXTZAmount(
        initialRequest: SendRequest,
        updatedRequest: SendRequest,
        type: SendType
    ): Single<OperationBundle> {
        val operations = createOperations(type, updatedRequest)
        return bundleFees(operations)
            .map {
                val realAmount = initialRequest.xtzValue - it.accumulatedFee.getTotalFees()
                val updatedOperations = it.operations.map { operation ->
                    if (operation is TransactionOperation) {
                        TransactionOperation(
                            realAmount,
                            operation.source,
                            operation.destination,
                            operation.fees
                        )
                    } else {
                        operation
                    }
                }
                OperationBundle(updatedOperations, it.accumulatedFee)
            }
    }

    private fun createOperations(
        sendType: SendType,
        request: SendRequest,
        fees: OperationFees = OperationFees.simulationFees
    ): List<Operation> {
        val user = userPreferences.getUserAddress()
        return when (sendType) {
            SendType.SEND_XTZ -> listOf(createOperation(request, user.address, fees))
            SendType.SEND_TOKEN -> {
                createSendTokenOperation(
                    request.token,
                    request.tokenValue,
                    user.address,
                    request.to,
                    fees
                )!!
            }
        }
    }

    private fun createOperation(
        request: SendRequest,
        from: Address,
        fees: OperationFees = OperationFees.simulationFees
    ): TransactionOperation {
        return TransactionOperation(
            request.xtzValue,
            from,
            request.to,
            fees
        )
    }
}

enum class SendType {
    SEND_XTZ,
    SEND_TOKEN;

    companion object {
        /**
         * @param token Token to be sent
         * @param tokenContractClients List of supported tokens
         */
        fun get(
            token: Token,
            supportedFATokens: List<Token>,
        ): SendType {
            val isFAToken = supportedFATokens.firstOrNull { it.contractAddress == token.contractAddress } != null

            return if (isFAToken) {
                SEND_TOKEN
            } else {
                SEND_XTZ
            }
        }

        /**
         * Create necessary operations for transferring an [amount] of a certain FA1.2 [token]
         * @param source Address which is sending the [token]
         */
        fun createSendTokenOperation(
            token: Token,
            amount: BigInteger,
            source: Address,
            destination: Address,
            fees: OperationFees = OperationFees.simulationFees
        ): List<SmartContractCallOperation>? {
            val contractAddress = token.contractAddress!!
            val params = TokenOperationParams.Transfer(
                amount,
                contractAddress,
                destination,
                source
            )
            return OperationFactory.createTokenOperation(
                TokenOperationType.TRANSFER,
                params,
                fees
            )
        }
    }
}
