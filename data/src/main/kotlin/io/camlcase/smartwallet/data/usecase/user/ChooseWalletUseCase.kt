/**
 * # Released under MIT License
 *
 * Copyright (c) 2021 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.usecase.user

import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.wallet.HDWallet
import io.camlcase.kotlintezos.wallet.SDWallet
import io.camlcase.smartwallet.data.source.user.WalletLocalSource
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

/**
 * Handle user choosing between HD or SD wallet after importing
 */
class ChooseWalletUseCase @Inject constructor(
    private val walletLocalSource: WalletLocalSource,
    private val createWalletUseCase: CreateWalletUseCase
) {

    fun getAddressPair(): Single<Pair<Address, Address?>> {
        return walletLocalSource.getWalletOrDie()
            .map {
                Pair(it.address, (it.wallet as? HDWallet)?.linearAddress)
            }
    }

    /**
     * Make sure it's called in a new thread!
     * @return true If it had to overwrite the wallet
     */
    fun chooseWallet(address: Address): Boolean {
        val appWallet = walletLocalSource.getWallet()
        return if (address == (appWallet?.wallet as? HDWallet)?.linearAddress) {
            // We need to save the wallet as an SD
            // isMigrated FALSE. We'll let the user choose TRUE on another screen
            createWalletUseCase.recoverSDWallet(appWallet.mnemonic, appWallet.passphrase, false)
            true
        } else {
            // If it's coming back after already choosing SD, wallet will be already saved as SD
            address == (appWallet?.wallet as? SDWallet)?.mainAddress
        }
    }
}
