/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.usecase.user

import io.camlcase.smartwallet.data.core.extension.MILLIS_PER_DAY
import io.camlcase.smartwallet.data.core.extension.ONE_HOUR_MILLIS
import io.camlcase.smartwallet.data.core.extension.is24hOld
import io.camlcase.smartwallet.data.core.extension.overTime
import io.camlcase.smartwallet.data.source.local.AppPreferences
import java.util.*
import javax.inject.Inject

/**
 * We should only ask for data every certain amount of time.
 */
class ShouldUpdateUseCase @Inject constructor(
    private val appPreferences: AppPreferences
) {
    fun shouldUpdateBakers(): Boolean {
        val timestamp = appPreferences.getLong(AppPreferences.BAKERS_DATA_TIMESTAMP)
        return if (timestamp > 0L) {
            timestamp.is24hOld()
        } else {
            true
        }
    }

    fun updateBakersTimestamp() {
        update(AppPreferences.BAKERS_DATA_TIMESTAMP)
    }

    fun shouldUpdateMetadata(): Boolean {
        return shouldUpdate(AppPreferences.META_DATA_TIMESTAMP, ONE_HOUR_MILLIS)
    }

    fun shouldUpdateMoonPaySupportedCountries(): Boolean {
        return shouldUpdate(AppPreferences.MOONPAY_COUNTRIES_TIMESTAMP, MILLIS_PER_DAY)
    }

    fun updateMoonPaySupportedCountriesTimestamp() {
        update(AppPreferences.MOONPAY_COUNTRIES_TIMESTAMP)
    }

    fun updateMetadataTimestamp() {
        update(AppPreferences.META_DATA_TIMESTAMP)
    }

    fun shouldUpdateCoinGeckoRates(): Boolean {
        return shouldUpdate(AppPreferences.COINGECKO_RATES_TIMESTAMP, MILLIS_PER_DAY)
    }

    fun updateCoinGeckoRates() {
        update(AppPreferences.COINGECKO_RATES_TIMESTAMP)
    }

    fun shouldUpdate(key: String, millis: Long): Boolean {
        val timestamp = appPreferences.getLong(key)
        return if (timestamp > 0L) {
            timestamp.overTime(millis)
        } else {
            true
        }
    }

    fun update(key: String) {
        val now = Date()
        appPreferences.store(key, now.time)
    }
}
