package io.camlcase.smartwallet.data.core.api

import io.mockk.mockk
import org.junit.Assert
import org.junit.Test

class MagmaNetworkClientTest {
    private val connectionInterceptor = mockk<ConnectionInterceptor>(relaxed = true)
    private val errorInterceptor = mockk<OperationErrorInterceptor>(relaxed = true)

    @Test
    fun `Instance of client is correct with debug default`() {
        val wrapper = MagmaClientWrapper(connectionInterceptor = connectionInterceptor,operationErrorInterceptor =  errorInterceptor)
        Assert.assertNotNull(wrapper)
        Assert.assertNotNull(wrapper.client)
        Assert.assertTrue(wrapper.client.interceptors.size == 2)
    }

    @Test
    fun `Instance of client is correct with debug true`() {
        val wrapper = MagmaClientWrapper(true, connectionInterceptor, errorInterceptor)
        Assert.assertNotNull(wrapper)
        Assert.assertNotNull(wrapper.client)
        Assert.assertTrue(wrapper.client.interceptors.size == 3)
    }
}
