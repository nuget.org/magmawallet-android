package io.camlcase.smartwallet.data.core.extension.tezos

import org.junit.Assert
import org.junit.Test

class TezosExtKtTest {
    @Test
    fun `Test isMainnet OK`() {
        Assert.assertTrue("mainnet".isMainnet())
    }

    @Test
    fun `Test isMainnet empty KO`() {
        Assert.assertFalse("".isMainnet())
    }

    @Test
    fun `Test isMainnet KO`() {
        Assert.assertFalse("testnet".isMainnet())
    }
}
