package io.camlcase.smartwallet.data.model.exchange

//class AppDexterExchangeTest {
//
//    @Test
//    fun `Test calculations tokenToXtz`() {
//        val json = JSONTestLoader.getStringFromFile("token_to_xtz_advanced.json")
//        val list = JSONArray(json)
//        for (item in list) {
//            // PARSE
//            val jsonObj = (item as JSONObject).toMap()
//            val amount = BigDecimal(jsonObj["token_in"] as String)
//            val expected = Tez(jsonObj["xtz_out"] as String)
//            val decimals = jsonObj["decimals"] as Int
//            val xtzPool = Tez(jsonObj["xtz_pool"] as String)
//            val tokenPool = BigInteger(jsonObj["token_pool"] as String)
//            val conversion = BigDecimal(jsonObj["conversion"] as String)
//            val expectedSlippage = jsonObj["slippage"] as Double
//
//            val tokenAmount = amount.moveComma(decimals)
//            getExchangeRate_tokenToXtz(
//                decimals,
//                tokenAmount,
//                xtzPool,
//                tokenPool,
//                conversion,
//                expected.signedDecimalRepresentation,
//                expectedSlippage
//            )
//        }
//    }
//
//    private fun getExchangeRate_tokenToXtz(
//        decimals: Int,
//        tokenAmount: BigDecimal,
//        xtzPool: Tez,
//        tokenPool: BigInteger,
//        conversionRate: BigDecimal,
//        expectedTez: BigDecimal,
//        expectedSlippage: Double? = null
//    ) {
//        val token = Token(
//            TEST_TOKEN.name,
//            TEST_TOKEN.symbol,
//            decimals,
//            TEST_TOKEN.contractAddress,
//            TEST_TOKEN.dexterAddress,
//            false
//        )
//        val fromToken = TokenInfoBundle(
//            token,
//            TokenBalance(BigInteger("13312"), token.decimals),
//            FATokenInfo(
//                token,
//                1.0,
//                DexterBalance(
//                    xtzPool,
//                    tokenPool
//                )
//            )
//        )
//        val toToken = TokenInfoBundle(
//            XTZ,
//            CoinBalance(Tez(4097.312037)),
//            XTZTokenInfo(1.0)
//        )
//        val amountFrom = TokenBalance(tokenAmount, decimals)
//        val result = AppDexterExchange.getExchangeRate(
//            amountFrom.bigDecimal,
//            fromToken,
//            toToken
//        )
//        println(result)
//
//        val calculations = AppDexterExchange(
//            fromToken,
//            toToken,
//            amountFrom
//        )
//        println(calculations.valueToBuy)
//
//        Assert.assertEquals(expectedTez, calculations.valueToBuy)
//        Assert.assertEquals(conversionRate, result)
//        Assert.assertEquals(conversionRate, calculations.conversionRate)
//
//        expectedSlippage?.apply {
//            val slippage = AppDexterExchange.calculateSlippage(
//                tokenAmount,
//                fromToken, toToken
//            )
//            println(slippage)
//            Assert.assertEquals(this.toBigDecimal(), slippage.setScale(4, RoundingMode.HALF_UP))
//        }
//    }
//
//    // Tokens need to be initialized
//    @Test(expected = ClassCastException::class)
//    fun `constructor with empty tokens`() {
//        var fromToken: TokenInfoBundle = TokenInfoBundle.empty
//        var toToken: TokenInfoBundle = TokenInfoBundle.empty
//
//        val result = AppDexterExchange(fromToken, toToken, 1.0)
//    }
//}
