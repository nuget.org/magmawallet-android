/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.model.exchange

/**
 * Test from a centralised source of truth that [AppDexterExchange] are calculated correctly.
 * Already tested DexterExchange in KotlinTezos, we test that wrapping layer is done correctly.
 *
 * @see https://gitlab.com/camlcase-dev/dexter-integration
 */
//class DexterIntegrationTest {
//
//    @Test
//    fun `XTZ to Token Calculations`() {
//        val json = JSONT.getStringFromFile("xtz_to_token.json")
//        val list = JSONArray(json)
//        for (item in list) {
//            // PARSE
//            val jsonObj = (item as JSONObject).toMap()
//            val xtzFrom = Tez(jsonObj["xtz_in"] as String)
//            val expectedTo = BigDecimal(jsonObj["token_out"] as String)
//            val dexterPool = DexterBalance(
//                Tez(jsonObj["xtz_pool"] as String),
//                BigInteger(jsonObj["token_pool"] as String)
//            )
//
//            // CALCULATE
//            val tokenFrom = TokenInfoBundle(XTZ, CoinBalance(Tez.zero), XTZTokenInfo(1.0))
//            val tokenTo = TokenInfoBundle(
//                TEST_TOKEN,
//                TokenBalance(BigInteger.ZERO, TEST_TOKEN.decimals),
//                FATokenInfo(TEST_TOKEN, 1.0, dexterPool)
//            )
//
//            val calculations = AppDexterExchange(tokenFrom, tokenTo, CoinBalance(xtzFrom))
//
//            // ASSERT
//            Assert.assertEquals(expectedTo, calculations.amountTo.value)
//        }
//    }
//
//    @Test
//    fun `Token to XTZ Calculations`() {
//        val json = JSONTestLoader.getStringFromFile("token_to_xtz.json")
//        val list = JSONArray(json)
//        for (item in list) {
//            // PARSE
//            val jsonObj = (item as JSONObject).toMap()
//            val amount = BigInteger(jsonObj["token_in"] as String)
//            val expected = Tez(jsonObj["xtz_out"] as String)
//            val dexterPool = DexterBalance(
//                Tez(jsonObj["xtz_pool"] as String),
//                BigInteger(jsonObj["token_pool"] as String)
//            )
//
//            // CALCULATE
//            val tokenFrom = TokenInfoBundle(
//                TEST_TOKEN,
//                TokenBalance(BigInteger.ZERO, TEST_TOKEN.decimals),
//                FATokenInfo(TEST_TOKEN, 1.0, dexterPool)
//            )
//            val tokenTo = TokenInfoBundle(XTZ, CoinBalance(Tez.zero), XTZTokenInfo(1.0))
//
//            val calculations = AppDexterExchange(tokenFrom, tokenTo, TokenBalance(amount, 0))
//
//            // ASSERT
//            Assert.assertEquals(expected, calculations.amountTo.value)
//        }
//    }
//}
