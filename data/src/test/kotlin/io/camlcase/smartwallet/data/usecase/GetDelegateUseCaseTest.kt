package io.camlcase.smartwallet.data.usecase

import io.camlcase.kotlintezos.TezosNodeClient
import io.camlcase.smartwallet.data.core.Delegate
import io.camlcase.smartwallet.data.core.RxEmitter
import io.camlcase.smartwallet.data.core.extension.MILLIS_PER_DAY
import io.camlcase.smartwallet.data.model.UserTezosAddress
import io.camlcase.smartwallet.data.model.delegate.TezosBaker
import io.camlcase.smartwallet.data.model.delegate.TezosBakerResponse
import io.camlcase.smartwallet.data.source.delegate.BakerListParser
import io.camlcase.smartwallet.data.source.delegate.BakerLocalSource
import io.camlcase.smartwallet.data.source.delegate.BakerRemoteSource
import io.camlcase.smartwallet.data.source.local.AppPreferences
import io.camlcase.smartwallet.data.source.local.UserPreferences
import io.camlcase.smartwallet.data.test.RxTest
import io.camlcase.smartwallet.data.usecase.delegate.DelegateStream
import io.camlcase.smartwallet.data.usecase.delegate.GetDelegateUseCase
import io.camlcase.smartwallet.data.usecase.user.ShouldUpdateUseCase
import io.mockk.*
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.core.SingleEmitter
import org.junit.After
import org.junit.Assert
import org.junit.Test
import java.util.*
import kotlin.collections.ArrayList

/**
 * @see [GetDelegateUseCase]
 */
class GetDelegateUseCaseTest : RxTest() {
    private val emitter = mockk<RxEmitter>(relaxed = true)
    private val userPreferences = mockk<UserPreferences>(relaxed = true) {
        every { getUserAddress() } returns UserTezosAddress("tz1Address")
    }
    private val tezosNodeClient = mockk<TezosNodeClient>(relaxed = true)
    private val bakerRemoteSource = mockk<BakerRemoteSource>(relaxed = true)
    private val bakerLocalSource = mockk<BakerLocalSource>(relaxed = true)
    private val testnetBakers = Delegate.testnetBakers
    private val isMainnet: Boolean = false
    private val delegateStream = mockk<DelegateStream>(relaxed = true)
    private val appPreferences = mockk<AppPreferences>(relaxed = true)
    private val updateUseCase = spyk(ShouldUpdateUseCase(appPreferences))

    private fun initUseCase(): GetDelegateUseCase {
        return GetDelegateUseCase(
            emitter,
            userPreferences,
            updateUseCase,
            tezosNodeClient,
            bakerRemoteSource,
            bakerLocalSource,
            testnetBakers,
            isMainnet,
            delegateStream
        )
    }

    @Test
    fun `Test fetchDelegate OK`() {
        val expected = "tz1BAKER"
        every { emitter.tezosCallback(any<SingleEmitter<*>>(), any()) } answers {
            firstArg<SingleEmitter<String>>().onSuccess(expected)
            mockk()
        }
        val list = BakerListParser().parse(TEST_BAKERS_JSON)
        every { bakerLocalSource.get() } returns Single.just(ArrayList(list!!))

        val useCase = initUseCase()
        useCase.fetchDelegate()
            .test()
            .assertNoErrors()
            .assertValue {
                Assert.assertTrue(!it.isNullOrBlank())
                Assert.assertTrue(it == expected)
                true
            }

        verify {
            userPreferences.getUserAddress()
            tezosNodeClient.getDelegate("tz1Address", any())
            userPreferences.store(UserPreferences.DELEGATE_ADDRESS, expected)
            bakerLocalSource.get()
            delegateStream.onDelegate(TezosBaker(null, expected))
        }
    }

    @Test
    fun `Test getDelegate with saved list OK`() {
        val storedDelegate = testnetBakers[0]
        val expected = TezosBaker("A", storedDelegate)
        every { userPreferences.getString(UserPreferences.DELEGATE_ADDRESS) } returns storedDelegate

        every { bakerLocalSource.get() } returns Single.just(
            arrayListOf(
                TezosBakerResponse("A", testnetBakers[0]),
                TezosBakerResponse("B", testnetBakers[1]),
                TezosBakerResponse("C", testnetBakers[2])
            )
        )

        val useCase = initUseCase()
        useCase.getDelegate()
            .test()
            .assertNoErrors()
            .assertValue {
                Assert.assertTrue(it.isSuccess)
                Assert.assertTrue(it.getOrNull() == expected)
                true
            }

        verify {
            userPreferences.getString(UserPreferences.DELEGATE_ADDRESS)
            bakerLocalSource.get()
        }
    }

    @Test
    fun `Test supportedDelegates Remote Testnet OK`() {
        every { bakerLocalSource.get() } returns Single.just(ArrayList<TezosBakerResponse>())
        every { bakerLocalSource.store(any()) } answers {
            Single.just(Result.success(firstArg()))
        }
        val list = BakerListParser().parse(TEST_BAKERS_JSON)
        every { bakerRemoteSource.getListOfSupportedDelegates() } returns Single.just(list)

        val useCase = initUseCase()
        useCase.supportedDelegates()
            .test()
            .assertNoErrors()
            .assertValue {
                Assert.assertTrue(!it.isNullOrEmpty())
                Assert.assertTrue(it[0].address == testnetBakers[0])
                Assert.assertTrue(it.size == 8) // filtered the others
                true
            }

        verify {
            bakerLocalSource.get()
            bakerLocalSource.store(any())
            bakerRemoteSource.getListOfSupportedDelegates()
        }
    }

    @Test
    fun `updateDelegates first time`() {
        every { appPreferences.getLong(AppPreferences.BAKERS_DATA_TIMESTAMP) } returns -1
        val list = BakerListParser().parse(TEST_BAKERS_JSON)
        every { bakerRemoteSource.getListOfSupportedDelegates() } returns Single.just(list)
        every { bakerLocalSource.store(any()) } answers {
            Single.just(Result.success(firstArg()))
        }

        initUseCase().updateDelegates()
            .test()
            .assertNoErrors()
            .assertValue {
                Assert.assertTrue(it)
                it
            }
        verify {
            bakerRemoteSource.getListOfSupportedDelegates()
            updateUseCase.shouldUpdateBakers()
            updateUseCase.update(any())
            updateUseCase.updateBakersTimestamp()
            bakerLocalSource.store(any())
        }
    }

    @Test
    fun `updateDelegates Withing 24hrs`() {
        every { appPreferences.getLong(AppPreferences.BAKERS_DATA_TIMESTAMP) } returns Date().time
        initUseCase().updateDelegates()
            .test()
            .assertNoErrors()
            .assertValue {
                Assert.assertFalse(it)
                true
            }
        verify {
            updateUseCase.shouldUpdateBakers()
        }
    }

    @Test
    fun `updateDelegates After 24hrs`() {
        val time = Date().time - MILLIS_PER_DAY
        every { appPreferences.getLong(AppPreferences.BAKERS_DATA_TIMESTAMP) } returns time
        val list = BakerListParser().parse(TEST_BAKERS_JSON)
        every { bakerRemoteSource.getListOfSupportedDelegates() } returns Single.just(list)
        every { bakerLocalSource.store(any()) } answers {
            Single.just(Result.success(firstArg()))
        }

        initUseCase().updateDelegates()
            .test()
            .assertNoErrors()
            .assertValue {
                Assert.assertTrue(it)
                true
            }
        verify {
            bakerRemoteSource.getListOfSupportedDelegates()
            updateUseCase.shouldUpdateBakers()
            updateUseCase.updateBakersTimestamp()
            updateUseCase.update(any())
            bakerLocalSource.store(any())
        }
    }

    @Test
    fun `listenToDelegate OK`() {
        // GIVEN
        val expected = "tz1BAKER"
        every { emitter.tezosCallback(any<SingleEmitter<*>>(), any()) } answers {
            firstArg<SingleEmitter<String>>().onSuccess(expected)
            mockk()
        }
        val list = BakerListParser().parse(TEST_BAKERS_JSON)
        every { bakerLocalSource.get() } returns Single.just(ArrayList(list!!))

        every { userPreferences.store(UserPreferences.DELEGATE_ADDRESS, any()) } just Runs
        val storedDelegate = testnetBakers[0]
        every { userPreferences.getString(UserPreferences.DELEGATE_ADDRESS) } returns storedDelegate

        // DO
        val useCase = initUseCase()

        useCase.fetchDelegate()
            .test()
            .assertNoErrors()
            .assertValue {
                Assert.assertTrue(it == expected)
                true
            }

        useCase.pingLocalDelegate()
            .test()
            .assertNoErrors()
            .assertValue {
                Assert.assertTrue(it.getOrNull()!!.address == storedDelegate)
                true
            }

        verify {
            userPreferences.getUserAddress()
            tezosNodeClient.getDelegate("tz1Address", any())
            userPreferences.store(UserPreferences.DELEGATE_ADDRESS, expected)
            bakerLocalSource.get()
            delegateStream.onDelegate(TezosBaker(null, expected))

            userPreferences.getString(UserPreferences.DELEGATE_ADDRESS)
            delegateStream.onDelegate(Result.success(TezosBaker(null, storedDelegate)))
        }
    }

    @After
    fun after() {
        confirmVerified(tezosNodeClient)
        confirmVerified(updateUseCase)
        confirmVerified(userPreferences)
        confirmVerified(bakerLocalSource)
        confirmVerified(bakerRemoteSource)
        confirmVerified(delegateStream)
    }

    companion object {
        private const val TEST_BAKERS_JSON =
            "[{\"address\":\"tz1fJHFn6sWEd3NnBPngACuw2dggTv6nQZ7g\",\"name\":\"Baking Team\",\"logo\":\"https://services.tzkt.io/v1/logos/baking_team.png\",\"balance\":30781.065482,\"stakingBalance\":331292.230287,\"stakingCapacity\":332401.627493,\"maxStakingBalance\":332401.627493,\"freeSpace\":1109.3972059999942,\"fee\":0,\"minDelegation\":1000,\"payoutDelay\":6,\"payoutPeriod\":1,\"openForDelegation\":true,\"estimatedRoi\":0.061274,\"serviceType\":\"tezos_only\",\"serviceHealth\":\"active\",\"payoutTiming\":\"stable\",\"payoutAccuracy\":\"precise\",\"audit\":\"5ebe91b548ca160f990110d2\",\"insuranceCoverage\":0},{\"address\":\"tz1fUjvVhJrHLZCbhPNvDRckxApqbkievJHN\",\"name\":\"Tezzieland\",\"logo\":\"https://services.tzkt.io/v1/logos/tezzieland.png\",\"balance\":15387.644834,\"stakingBalance\":154085.639662,\"stakingCapacity\":166169.627529,\"maxStakingBalance\":166169.627529,\"freeSpace\":12083.987866999989,\"fee\":0.01,\"minDelegation\":10,\"payoutDelay\":6,\"payoutPeriod\":1,\"openForDelegation\":true,\"estimatedRoi\":0.060441,\"serviceType\":\"tezos_only\",\"serviceHealth\":\"active\",\"payoutTiming\":\"stable\",\"payoutAccuracy\":\"precise\",\"audit\":\"5ef0c08748ca160f9901110e\",\"insuranceCoverage\":1},{\"address\":\"tz1UWcKQDC67Vvm257gWiUsmbV2eNjbnUwbN\",\"name\":\"Tezos Maximalist\",\"logo\":\"https://services.tzkt.io/v1/logos/tezos_maximalist.png\",\"balance\":44721.872318,\"stakingBalance\":466067.736295,\"stakingCapacity\":482946.997131,\"maxStakingBalance\":482946.997131,\"freeSpace\":16879.260835999972,\"fee\":0.025,\"minDelegation\":100,\"payoutDelay\":6,\"payoutPeriod\":1,\"openForDelegation\":true,\"estimatedRoi\":0.060074,\"serviceType\":\"tezos_only\",\"serviceHealth\":\"active\",\"payoutTiming\":\"stable\",\"payoutAccuracy\":\"precise\",\"audit\":\"5f2c15f95e0a85c68fb18cb8\",\"insuranceCoverage\":0},{\"address\":\"tz1Vd1rXpV8hTHbFXCXN3c3qzCsgcU5BZw1e\",\"name\":\"TzNode\",\"logo\":\"https://services.tzkt.io/v1/logos/tznode.png\",\"balance\":211610.298219,\"stakingBalance\":2257349.101197,\"stakingCapacity\":2285158.308226,\"maxStakingBalance\":2285158.308226,\"freeSpace\":27809.207028999925,\"fee\":0.04,\"minDelegation\":0,\"payoutDelay\":6,\"payoutPeriod\":1,\"openForDelegation\":true,\"estimatedRoi\":0.059378,\"serviceType\":\"tezos_only\",\"serviceHealth\":\"active\",\"payoutTiming\":\"stable\",\"payoutAccuracy\":\"precise\",\"audit\":\"5e79fc345e0a85c68fb18c2b\",\"insuranceCoverage\":0},{\"address\":\"tz1NEKxGEHsFufk87CVZcrqWu8o22qh46GK6\",\"name\":\"Money Every 3 Days\",\"logo\":\"https://services.tzkt.io/v1/logos/money_every_3_days.png\",\"balance\":228962.52133,\"stakingBalance\":2431595.443438,\"stakingCapacity\":2472543.2188,\"maxStakingBalance\":2472543.2188,\"freeSpace\":40947.77536199987,\"fee\":0.17,\"minDelegation\":100,\"payoutDelay\":1,\"payoutPeriod\":1,\"openForDelegation\":true,\"estimatedRoi\":0.059228,\"serviceType\":\"tezos_only\",\"serviceHealth\":\"active\",\"payoutTiming\":\"stable\",\"payoutAccuracy\":\"precise\",\"audit\":\"5e8077e648ca160f99011098\",\"insuranceCoverage\":0},{\"address\":\"tz1V3yg82mcrPJbegqVCPn6bC8w1CSTRp3f8\",\"name\":\"Staking Shop\",\"logo\":\"https://services.tzkt.io/v1/logos/staking_shop.png\",\"balance\":21596.918112,\"stakingBalance\":259661.944208,\"stakingCapacity\":233222.944588,\"maxStakingBalance\":233222.944588,\"freeSpace\":-26438.999619999988,\"fee\":0.03,\"minDelegation\":100,\"payoutDelay\":6,\"payoutPeriod\":1,\"openForDelegation\":true,\"estimatedRoi\":0.059186,\"serviceType\":\"multiasset\",\"serviceHealth\":\"active\",\"payoutTiming\":\"stable\",\"payoutAccuracy\":\"precise\",\"audit\":\"5eef3e5c48ca160f99011109\",\"insuranceCoverage\":0},{\"address\":\"tz1aRoaRhSpRYvFdyvgWLL6TGyRoGF51wDjM\",\"name\":\"Everstake\",\"logo\":\"https://services.tzkt.io/v1/logos/atticpool.png\",\"balance\":1016910.48034,\"stakingBalance\":9360903.585686,\"stakingCapacity\":10981513.907541,\"maxStakingBalance\":10981513.907541,\"freeSpace\":1620610.3218549993,\"fee\":0.05,\"minDelegation\":0,\"payoutDelay\":6,\"payoutPeriod\":1,\"openForDelegation\":true,\"estimatedRoi\":0.058789,\"serviceType\":\"multiasset\",\"serviceHealth\":\"active\",\"payoutTiming\":\"stable\",\"payoutAccuracy\":\"precise\",\"audit\":\"5f2bebaa5e0a85c68fb18cae\",\"insuranceCoverage\":0},{\"address\":\"tz1dRKU4FQ9QRRQPdaH4zCR6gmCmXfcvcgtB\",\"name\":\"Spice\",\"logo\":\"https://services.tzkt.io/v1/logos/spice.png\",\"balance\":41651.850985,\"stakingBalance\":200169.97912,\"stakingCapacity\":449794.145807,\"maxStakingBalance\":449794.145807,\"freeSpace\":249624.166687,\"fee\":0.0499,\"minDelegation\":10,\"payoutDelay\":6,\"payoutPeriod\":1,\"openForDelegation\":true,\"estimatedRoi\":0.076544,\"serviceType\":\"tezos_only\",\"serviceHealth\":\"active\",\"payoutTiming\":\"stable\",\"payoutAccuracy\":\"precise\",\"audit\":\"5f2bf68d5e0a85c68fb18cb2\",\"insuranceCoverage\":0},{\"address\":\"tz1ei4WtWEMEJekSv8qDnu9PExG6Q8HgRGr3\",\"name\":\"BakeTz\",\"logo\":\"https://services.tzkt.io/v1/logos/baketz.png\",\"balance\":373615.807724,\"stakingBalance\":3603455.828023,\"stakingCapacity\":4034639.49671,\"maxStakingBalance\":4034639.49671,\"freeSpace\":431183.66868699994,\"fee\":0.05,\"minDelegation\":0,\"payoutDelay\":6,\"payoutPeriod\":1,\"openForDelegation\":true,\"estimatedRoi\":0.058738,\"serviceType\":\"tezos_dune\",\"serviceHealth\":\"active\",\"payoutTiming\":\"stable\",\"payoutAccuracy\":\"precise\",\"audit\":\"5e1ef991b006b90008a9dfe8\",\"insuranceCoverage\":0}]"
    }
}
