package io.camlcase.smartwallet.data.usecase

import io.camlcase.kotlintezos.TezosNodeClient
import io.camlcase.kotlintezos.data.dexter.DexterPool
import io.camlcase.kotlintezos.exchange.DexterTokenClient
import io.camlcase.kotlintezos.exchange.IndexterService
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.TezosNetwork
import io.camlcase.smartwallet.data.core.RxEmitter
import io.camlcase.smartwallet.data.core.Tokens
import io.camlcase.smartwallet.data.model.exchange.DexterBalance
import io.camlcase.smartwallet.data.test.RxTest
import io.camlcase.smartwallet.data.usecase.operation.DexterCalculationsUseCase
import io.mockk.confirmVerified
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.core.SingleEmitter
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.math.BigDecimal
import java.math.BigInteger

/**
 * @see GetExchangeUseCase
 */
class GetExchangeUseCaseTest : RxTest() {
    private val emitter = mockk<RxEmitter>(relaxed = true)
    private val dexterContractClients = LinkedHashMap<Address, DexterTokenClient>()
    private val supportedFATokens = Tokens.testnetFATokens
    private val indexterService = mockk<IndexterService>(relaxed = true)
    private val calculationsUseCase = mockk<DexterCalculationsUseCase>(relaxed = true)

    private fun initUseCase(): GetExchangeUseCase {
        return GetExchangeUseCase(
            emitter,
            dexterContractClients,
            calculationsUseCase
        )
    }

    @Before
    fun before() {
        mockDexterClients()
    }

    private fun mockDexterClients() {
        val tezosNodeClient = mockk<TezosNodeClient>(relaxed = true)
        for (supported in supportedFATokens) {
            val dexterAddress = supported.dexterAddress
            if (!dexterAddress.isNullOrBlank()) {
                dexterContractClients[dexterAddress] =
                    DexterTokenClient(
                        tezosNodeClient,
                        TezosNetwork.DELPHINET,
                        dexterAddress,
                        supported.contractAddress!!,
                        indexterService
                    )
            }
        }
    }

    @Test
    fun `getTokenExchange OK`() {
        val expectedPool = DexterBalance(Tez(1.0), BigInteger.TEN)
        every { emitter.tezosCallback(any<SingleEmitter<DexterPool>>(), any()) } answers {
            firstArg<SingleEmitter<DexterPool>>().onSuccess(DexterPool(expectedPool.balance, expectedPool.tokens))
            mockk()
        }
        every { calculationsUseCase.getTokenToXtzMarketRate(any(), any()) } returns Single.just(BigDecimal.ONE)
        val useCase = initUseCase()
        useCase.getTokenExchange(supportedFATokens[0], 1.5)
            .test()
            .assertNoErrors()
            .assertValue {
                Assert.assertTrue(it.dexterBalance == expectedPool)
                Assert.assertTrue(it.token == supportedFATokens[0])
                true
            }

        verify {
            calculationsUseCase.getTokenToXtzMarketRate(any(), any())
        }
    }

    @After
    fun after() {
        confirmVerified(indexterService)
        confirmVerified(calculationsUseCase)
    }
}
