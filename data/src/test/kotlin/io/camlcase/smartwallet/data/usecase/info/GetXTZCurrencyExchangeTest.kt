package io.camlcase.smartwallet.data.usecase.info

import io.camlcase.smartwallet.data.core.Wallet
import io.camlcase.smartwallet.data.model.exchange.CurrencyEntity
import io.camlcase.smartwallet.data.model.exchange.CurrencyType
import io.camlcase.smartwallet.data.model.exchange.XTZTokenInfo
import io.camlcase.smartwallet.data.source.info.exchange.CoinGeckoRemoteSource
import io.camlcase.smartwallet.data.source.info.exchange.CurrencyExchangeLocalSource
import io.camlcase.smartwallet.data.test.RxTest
import io.mockk.*
import io.reactivex.rxjava3.core.Single
import org.junit.After
import org.junit.Assert
import org.junit.Test
import java.io.IOException
import java.util.*

class GetXTZCurrencyExchangeTest : RxTest() {
    private val currencyRemoteSource = mockk<CoinGeckoRemoteSource>(relaxed = true)
    private val currencyLocalSource = mockk<CurrencyExchangeLocalSource>(relaxed = true)

    @After
    fun tearDown() {
        confirmVerified(currencyRemoteSource)
        confirmVerified(currencyLocalSource)
    }

    fun initUseCase(): GetXTZCurrencyExchange {
        return spyk(GetXTZCurrencyExchange(currencyRemoteSource, currencyLocalSource))
    }

    @Test
    fun `getXTZExchange No local`() {
        val expected = XTZTokenInfo(2.0)

        every { currencyLocalSource.getCurrencyValue() } returns null
        every { currencyRemoteSource.getXTZPriceFor("USD") } returns Single.just(expected.currencyExchange)

        val useCase = initUseCase()
        useCase.getXTZExchange()
            .test()
            .assertNoErrors()
            .assertValue {
                Assert.assertNotNull(it)
                Assert.assertTrue(it == expected)
                true
            }

        verify {
            currencyLocalSource.getCurrencyValue()
            currencyLocalSource.storeCurrencyValue(any())
            currencyRemoteSource.getXTZPriceFor("USD")
        }
    }

    @Test
    fun `getXTZExchange Recent Local BTC`() {
        val local = CurrencyEntity("BTC", CurrencyType.CRYPTO, "BTC", 1.0, Date().time)
        every { currencyLocalSource.getCurrencyValue() } returns local

        val useCase = initUseCase()
        useCase.getXTZExchange()
            .test()
            .assertNoErrors()
            .assertValue {
                Assert.assertNotNull(it)
                Assert.assertTrue(it.currencyExchange == local.price)
                true
            }

        verify {
            currencyLocalSource.getCurrencyValue()
        }
    }

    @Test
    fun `getXTZExchange Local BTC`() {
        val local = CurrencyEntity("BTC", CurrencyType.CRYPTO, "BTC", 1.0, 0L)
        val expected = XTZTokenInfo(2.0)
        every { currencyLocalSource.getCurrencyValue() } returns local
        every { currencyRemoteSource.getXTZPriceFor(local.code) } returns Single.just(expected.currencyExchange)

        val useCase = initUseCase()
        useCase.getXTZExchange()
            .test()
            .assertNoErrors()
            .assertValue {
                Assert.assertNotNull(it)
                Assert.assertTrue(it == expected)
                true
            }

        verify {
            currencyLocalSource.getCurrencyValue()
            currencyLocalSource.storeCurrencyValue(any())
            currencyRemoteSource.getXTZPriceFor(local.code)
        }
    }

    @Test
    fun `getXTZExchange No local KO`() {
        val expected = IOException("Error")

        every { currencyLocalSource.getCurrencyValue() } returns null
        every { currencyRemoteSource.getXTZPriceFor("USD") } returns Single.error(expected)

        val useCase = initUseCase()
        useCase.getXTZExchange()
            .test()
            .assertError {
                Assert.assertNotNull(it)
                Assert.assertTrue(it == expected)
                true
            }

        verify {
            currencyLocalSource.getCurrencyValue()
            currencyRemoteSource.getXTZPriceFor("USD")
        }
    }

    @Test
    fun `getCurrencyBundle Default`() {
        every { currencyLocalSource.getCurrencyValue() } returns null

        val useCase = initUseCase()
        val result = useCase.getCurrencyBundle()

        Assert.assertTrue(result == Wallet.defaultCurrency)

        verify {
            currencyLocalSource.getCurrencyValue()
        }
    }

    @Test
    fun `getCurrencyBundle Local`() {
        val local = CurrencyEntity("BTC", CurrencyType.CRYPTO, "BTC", 1.0, 0L)
        every { currencyLocalSource.getCurrencyValue() } returns local

        val useCase = initUseCase()
        val result = useCase.getCurrencyBundle()

        Assert.assertTrue(result == local.toBundle())

        verify {
            currencyLocalSource.getCurrencyValue()
        }
    }

    @Test
    fun `getCurrencyCode Default`() {
        every { currencyLocalSource.getCurrencyValue() } returns null

        val useCase = initUseCase()
        val result = useCase.getCurrencyCode()

        Assert.assertTrue(result == Wallet.defaultCurrency.code)

        verify {
            currencyLocalSource.getCurrencyValue()
        }
    }

    @Test
    fun `getCurrencyCode Local`() {
        val local = CurrencyEntity("BTC", CurrencyType.CRYPTO, "BTC", 1.0, 0L)
        every { currencyLocalSource.getCurrencyValue() } returns local

        val useCase = initUseCase()
        val result = useCase.getCurrencyCode()

        Assert.assertTrue(result == "BTC")

        verify {
            currencyLocalSource.getCurrencyValue()
        }
    }
}
