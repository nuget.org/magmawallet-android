package io.camlcase.smartwallet.data.usecase.operation

import io.camlcase.kotlintezos.TezosNodeClient
import io.camlcase.kotlintezos.model.*
import io.camlcase.kotlintezos.model.operation.BundledFees
import io.camlcase.kotlintezos.model.operation.TransactionOperation
import io.camlcase.kotlintezos.model.operation.fees.OperationFees
import io.camlcase.kotlintezos.token.TokenContractClient
import io.camlcase.smartwallet.data.core.RxEmitter
import io.camlcase.smartwallet.data.core.Tokens
import io.camlcase.smartwallet.data.core.Tokens.testnetFATokens
import io.camlcase.smartwallet.data.core.XTZ
import io.camlcase.smartwallet.data.model.Token
import io.camlcase.smartwallet.data.model.amount.CoinBalance
import io.camlcase.smartwallet.data.model.amount.TokenBalance
import io.camlcase.smartwallet.data.model.error.AppError
import io.camlcase.smartwallet.data.model.send.SendRequest
import io.camlcase.smartwallet.data.test.RxTest
import io.camlcase.smartwallet.data.test.mock.ADDRESS
import io.camlcase.smartwallet.data.test.mock.mockUserPreferences
import io.camlcase.smartwallet.data.test.mock.mockWalletLocalSource
import io.camlcase.smartwallet.data.usecase.analytics.AnalyticEvent
import io.camlcase.smartwallet.data.usecase.OperationBundle
import io.camlcase.smartwallet.data.usecase.analytics.SendAnalyticUseCase
import io.mockk.*
import io.reactivex.rxjava3.core.SingleEmitter
import org.junit.After
import org.junit.Assert
import org.junit.Test
import java.math.BigInteger

/**
 * @see SendTokenUseCase
 */
class SendTokenUseCaseTest : RxTest() {
    private val emitter = mockk<RxEmitter>(relaxed = true)
    private val walletLocalSource = mockWalletLocalSource()
    private val tezosNodeClient = mockk<TezosNodeClient>(relaxed = true)
    private val userPreferences = mockUserPreferences()
    private val tokenContractClients = testnetFATokens
    private val analytics = mockk<SendAnalyticUseCase>(relaxed = true)

    private fun initUseCase(): SendTokenUseCase {
        return SendTokenUseCase(
            emitter,
            walletLocalSource,
            tezosNodeClient,
            userPreferences,
            tokenContractClients,
            analytics
        )
    }

    @Test
    fun `Test createSendOperation XTZ OK`() {
        every { emitter.tezosCallback(any<SingleEmitter<*>>(), any()) } answers {
            firstArg<SingleEmitter<BundledFees>>().onSuccess(bundled)
            mockk()
        }

        val useCase = initUseCase()
        val request = SendRequest(CoinBalance(amount), FROM, XTZ)
        request.balance = CoinBalance(amount + Tez(10.0))

        useCase.createSendOperation(request)
            .test()
            .assertNoErrors()
            .assertValue {
                Assert.assertNotNull(it)
                Assert.assertTrue(it.operations == operations)
                Assert.assertTrue(it.accumulatedFee.fee == fees.fee)
                Assert.assertTrue(it.accumulatedFee.gasLimit == fees.gasLimit)
                Assert.assertTrue(it.accumulatedFee.storageLimit == fees.storageLimit)
                Assert.assertTrue(it.accumulatedFee.extraFees?.isEmpty() == true)
                true
            }

        verify {
            walletLocalSource.getWalletOrDie()
            tezosNodeClient.bundleFees(any(), any(), any(), any(), any())
            userPreferences.getUserAddress()
            userPreferences.getUserAddress()
        }
    }

    @Test
    fun `Test createSendOperation XTZ Insufficient XTZ`() {
        val operations = listOf(TransactionOperation(amount, TO, FROM, OperationFees((amount + Tez(1.0)), 0, 0)))
        every { emitter.tezosCallback(any<SingleEmitter<*>>(), any()) } answers {
            firstArg<SingleEmitter<BundledFees>>().onSuccess(BundledFees(operations, null))
            mockk()
        }

        val useCase = initUseCase()
        val request = SendRequest(CoinBalance(amount), FROM, XTZ)
        request.balance = CoinBalance(amount)

        useCase.createSendOperation(request)
            .test()
            .assertError(AppError::class.java)

        verify {
            walletLocalSource.getWalletOrDie()
            tezosNodeClient.bundleFees(any(), any(), any(), any(), any())
            userPreferences.getUserAddress()
            userPreferences.getUserAddress()
        }
    }

    @Test
    fun `Test createSendOperation TZG OK`() {
        every { emitter.tezosCallback(any<SingleEmitter<*>>(), any()) } answers {
            firstArg<SingleEmitter<BundledFees>>().onSuccess(bundled)
            mockk()
        }

        val useCase = initUseCase()
        val request = SendRequest(
            TokenBalance(amount.signedDecimalRepresentation, Tokens.Testnet.USDTZ.decimals), FROM,
            Tokens.Testnet.USDTZ
        )
        request.balance = TokenBalance(amount.integerAmount + BigInteger.ONE, 0)

        useCase.createSendOperation(request)
            .test()
            .assertNoErrors()
            .assertValue {
                Assert.assertNotNull(it)
                Assert.assertTrue(it.operations == operations)
                Assert.assertTrue(it.accumulatedFee.fee == fees.fee)
                Assert.assertTrue(it.accumulatedFee.gasLimit == fees.gasLimit)
                Assert.assertTrue(it.accumulatedFee.storageLimit == fees.storageLimit)
                Assert.assertTrue(it.accumulatedFee.extraFees?.isEmpty() == true)
                true
            }

        verify {
            walletLocalSource.getWalletOrDie()
            tezosNodeClient.bundleFees(any(), any(), any(), any(), any())
            userPreferences.getUserAddress()
        }
    }

    // TODO Uncomment when having data
//    @Test
//    fun `Test createSendOperation TZG Insufficient XTZ KO`() {
//        every { tokenContractClients.containsKey(Tokens.Testnet.USDTZ.contractAddress) } returns true
//        every { emitter.tezosCallback(any<SingleEmitter<*>>(), any()) } answers {
//            firstArg<SingleEmitter<BundledFees>>().onSuccess(bundled)
//            mockk()
//        }
//
//        val useCase = initUseCase()
//        val request = SendRequest(
//            TokenBalance(amount.signedDecimalRepresentation, Tokens.Testnet.USDTZ.decimals), FROM,
//            Tokens.Testnet.USDTZ
//        )
//        request.balance = TokenBalance(amount.integerAmount + BigInteger.ONE, 0)
//
//        useCase.createSendOperation(request)
//            .test()
//            .assertError(AppError::class.java)
//
//        verify {
//            walletLocalSource.getWalletOrDie()
//            tezosNodeClient.bundleFees(any(), any(), any(), any(), any())
//            userPreferences.getUserAddress()
//        }
//    }

    @Test
    fun `Test send OK`() {
        val hash = "oo000"
        every { emitter.tezosCallback(any<SingleEmitter<*>>(), any()) } answers {
            firstArg<SingleEmitter<String>>().onSuccess(hash)
            mockk()
        }

        val useCase = initUseCase()
        useCase.send(operations)
            .test()
            .assertNoErrors()
            .assertValue { it == hash }

        verify {
            walletLocalSource.getWalletOrDie()
            tezosNodeClient.runOperations(operations, TO, any(), any())
        }
    }

    @Test
    fun `Test send KO`() {
        every { emitter.tezosCallback(any<SingleEmitter<*>>(), any()) } answers {
            firstArg<SingleEmitter<BundledFees>>().onError(
                TezosError(
                    TezosErrorType.RPC_ERROR, rpcErrors = listOf(
                        RPCErrorResponse(
                            RPCErrorType.PERMANENT, "PSCARTHA.contract.balance_too_low.12345"
                        )
                    )
                )
            )
            mockk()
        }
        every { analytics.reportEvent(any<AnalyticEvent>(), any()) } just Runs

        val useCase = initUseCase()
        useCase.send(operations)
            .test()
            .assertError(TezosError::class.java)

        verify {
            walletLocalSource.getWalletOrDie()
            tezosNodeClient.runOperations(operations, TO, any(), any())
            analytics.reportEvent(AnalyticEvent.SEND_ERROR)
        }
    }

    @After
    fun after() {
        confirmVerified(walletLocalSource)
        confirmVerified(tezosNodeClient)
        confirmVerified(userPreferences)
        confirmVerified(analytics)
    }

    companion object {
        const val TO = ADDRESS
        const val FROM = "tz1Address2"

        val amount = Tez(123.456)
        val fees = OperationFees(Tez(0.002), 100, 100)
        val operations = listOf(TransactionOperation(amount, TO, FROM, fees))

        val bundled = BundledFees(operations, null)
        val bundle = OperationBundle(
            operations,
            OperationFees.empty
        )
    }
}
